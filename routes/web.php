<?php

use App\Models\Product;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
use App\Models\Media_File;
use App\Models\Price_List;
use App\Models\Price_List_Detail;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\MemoController;
use App\Http\Controllers\BuyerController;
use App\Http\Controllers\TermsController;
use App\Http\Controllers\AttackController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RequestController;
use App\Http\Controllers\SupportController;
use App\Http\Controllers\ImporterController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\MediaFileController;
use App\Http\Controllers\TranslateController;
use App\Http\Controllers\Price_List_Controller;
use App\Http\Controllers\SendProductController;
use App\Http\Controllers\MailTemplateController;
use App\Http\Controllers\free\FreeHomeController;
use App\Http\Controllers\free\FreeMailController;
use App\Http\Controllers\free\FreeBuyerController;
use App\Http\Controllers\free\FreeAttackController;
use App\Http\Controllers\free\FreeRequestController;
use App\Http\Controllers\free\FreeMediaFileController;
use App\Http\Controllers\free\FreeTranslateController;
use App\Http\Controllers\free\FreeMailTemplateController;



Auth::routes();

//Route::get('/', 'HomeController@index');

Route::get('/', [HomeController::class, 'index']);

Route::get('/home', [HomeController::class, 'home']);

Route::post('/home_4.html', [HomeController::class, 'home_4']);

Route::get('/news', [HomeController::class, 'news']);
Route::get('/news/details/{id}', [HomeController::class, 'details']);

/*Supplier*/
Route::get('/profile',[SupplierController::class,'edit']);

Route::post('/profile',[SupplierController::class, 'edit_handler']);

/*ReportController*/
Route::get('/report_list',[ReportController::class, 'report_list']);


Route::get('/report_detail/{id}',[ReportController::class, 'report_detail']);

Route::get('/report_create',[ReportController::class, 'create_report']);

Route::post('/report_create',[ReportController::class, 'create_report_handler']);

/*ProductController*/
Route::get('/product_list', [ProductController::class, 'product_list']);

Route::post('/product_list', [ProductController::class, 'product_list']);

Route::post('/list_product_sort', [ProductController::class, 'sort']);

Route::get('/create_product', [ProductController::class, 'create_product']);

Route::post('/create_product', [ProductController::class, 'create_product_handler']);

Route::get('/product_detail/{id}', [ProductController::class, 'product_detail']);

Route::get('/product_edit/{id}', [ProductController::class, 'product_edit']);

Route::post('/update_product', [ProductController::class, 'product_update']);

/*Request*/

Route::get('/request_list',[RequestController::class,'request_list']);

Route::post('/request_list',[RequestController::class,'request_list']);

Route::post('/list_request_sort', [RequestController::class,'sort']);
/*Attack*/
Route::get('/attack_list', [AttackController::class, 'attack_list']);

Route::post('/attack_list', [AttackController::class, 'attack_list']);

Route::post('/new_attack',[AttackController::class, 'new_attack']);

Route::post('/change_attack_status',[AttackController::class, 'change_attack_status']);

Route::post('/list_attack_sort',[AttackController::class, 'sort']);

Route::post('/change_status_agency',[AttackController::class, 'change_status_agency']);
/*Buyer*/
Route::get('/buyer_detail/{attack_id}', [BuyerController::class, 'buyer_detail']);

/*Template mail*/
Route::get('/template_mail_list',[MailTemplateController::class, 'index']);

Route::get('/template_mail_create',[MailTemplateController::class, 'create_new_template']);

Route::post('/template_mail_create',[MailTemplateController::class, 'create_new_template_handler']);

Route::get('/template_mail_detail/{id}',[MailTemplateController::class, 'edit_template_mail']);

Route::post('/template_mail_detail/{id}',[MailTemplateController::class, 'update_template_mail']);

Route::get('/template_mail_delete/{id}', [MailTemplateController::class, 'delete_template_mail']);

Route::post('/load_mail_template',[MailTemplateController::class, 'load_mail_template']);
/*End Template mail*/

/*ContactController*/
Route::get('/contact',[SupportController::class, 'create_support']);
Route::post('/contact',[SupportController::class, 'create_support_handler']);
/*MediaFileController*/
Route::get('/report_download/{file_type}/{media_file_id}', [MediaFileController::class,'download_report']);
Route::get('/image/{media_file_id}', [MediaFileController::class,'display_image']);

/*Price list*/
Route::get('/price_list',[Price_List_Controller::class,'index']);
Route::get('/create_new_price_list_detail',[Price_List_Controller::class,'create_new_price_list_detail']);
Route::get('/delete_price_list/{id}',[Price_List_Controller::class,'delete_price_list']);
Route::get('/edit_price_list_detail/{id}',[Price_List_Controller::class,'edit_price_list_detail']);
Route::get('/download_file_pdf/{id}',[Price_List_Controller::class,'download_file_pdf']);
Route::get('/download_file_excel/{id}',[Price_List_Controller::class,'download_file_excel']);

Route::get('/test',[Price_List_Controller::class,'test']);

Route::post('/create_new_price_list_detail',[Price_List_Controller::class,'create_new_price_list_detail_handler']);
Route::post('/delete_price_list_detail',[Price_List_Controller::class,'delete_price_list_detail']);
Route::post('/save_diff_name',[Price_List_Controller::class,'save_diff_name']);
Route::post('/get_product_list',[Price_List_Controller::class,'get_product_list']);

/*MemoController*/
Route::post('/add_memo',[MemoController::class,'add_memo']);
/*MailController*/
Route::post('/send_email_to_buyer',[MailController::class,'send_email_to_buyer']);
/* Translate */
Route::get('/translate-list',[TranslateController::class,'translate_list']);
Route::post('/translate-list',[TranslateController::class,'translate_list']);
Route::post('/translate-sort',[TranslateController::class,'sort']);
Route::get('/create-translate-request',[TranslateController::class,'create']);
Route::post('/create-translate-request',[TranslateController::class,'create_handler']);
Route::get('/translate-detail/{translate_id}',[TranslateController::class,'edit_translate']);
Route::post('/translate-detail/{translate_id}',[TranslateController::class,'edit_translate_handler']);
Route::delete('delete-translate-request',[TranslateController::class,'delete_translate']);
/* End Translate */

/* Send Product */
Route::get('/send-product',[SendProductController::class, 'index']);
Route::post('/send-product',[SendProductController::class, 'index']);
Route::post('/send-product-sort',[SendProductController::class, 'sort']);
Route::get('/create-send-product',[SendProductController::class, 'create']);
Route::post('/create-send-product',[SendProductController::class, 'create_handler']);
Route::get('/detail-send-product/{id}',[SendProductController::class, 'detail']);
Route::post('/send-product-update',[SendProductController::class, 'update']);
/* Importer*/
Route::get('/importer_supplier',[ImporterController::class, 'importer_supplier']);
Route::get('/update_kana',[ImporterController::class, 'update_kana_contact']);
Route::get('/import_buyer',[ImporterController::class, 'importer_buyer']);
/* End Importer*/

/* Po Graph */
Route::get('/graph.html',function (){
    return view('home.graph1');
})->middleware('auth');
/* End Po Graph */
/*Graph*/
Route::post('load_graph_3',[HomeController::class,'load_graph_3']);
Route::post('/graph/get_data_graph_4',[HomeController::class,'get_data_graph_handler']);

Route::get('/po_mail',function (){
    return view('mails.list');
});

Route::get('/mail-list',[MailController::class, 'index']);
Route::post('/mail-list',[MailController::class, 'index']);
Route::post('/mail-sort',[MailController::class, 'sort']);
Route::get('/mail-detail/{mail_id}',[MailController::class, 'edit']);
Route::get('/mail-download/{mail_attachment_id}', [MediaFileController::class, 'download_mail_attach_file']);
Route::get('/mail-template-download/{media_file_id}', [MediaFileController::class, 'download_mail_template_attach_file']);

Route::get('/form-create-buyer',function(){
    return view('api.createbuyer');
});

Route::get('/form-create-request',function(){
    return view('api.createrequest');
});

Route::get('/interim',function(){
    return view('api.interim');
});

Route::get('/login-company/{id}/{temp_pass}',function(\Illuminate\Http\Request $request){

    $redirectTo = '/news';
    if(isset($request->id)&&isset($request->temp_pass)){
        $temp_login = \App\Models\TempLogin::where('user_id',$request->id)
            ->where('password_temp',$request->temp_pass)->get()->first();
        $user_deleted = \App\Models\User::find($temp_login->user_id);
        if($user_deleted->deleted_at != null){
            return redirect('/login');
        }
        if(isset($temp_login)){
            $delta_time = time() - strtotime($temp_login->created_at);
            $time = date('s', $delta_time);
            if($time <= 30)
            {
                Auth::loginUsingId($temp_login->user_id, true);
            }
            $temp = \App\Models\TempLogin::find($temp_login->id);
            $temp->delete();
        }
    }
    return redirect($redirectTo);
});

Route::get('/thanks',function(){
    return view('api.thanks');
});
//Route::post('/create-buyer','ApiController@createCompanyForeign');
//Route::post('/create-request','ApiController@createRequest');

Route::post('/create-buyer-by-form',[ApiController::class,'createCompanyForeign']);
Route::post('/create-request-by-form',[ApiController::class,'createRequest']);



/* For Free User */
Route::get('/register-free', function () {
    if($user = Auth::user())
    {
        return redirect( '/news' );
    }
    return view('free.register.register');
});
/* Home */
Route::get('/free/home', [FreeHomeController::class,'home']);
/* News */
Route::get('/free/news', [FreeHomeController::class,'news']);
Route::get('/free/news/details/{id}', [FreeHomeController::class,'details']);
/* Attack List */
Route::get('/free/attack_list', [FreeAttackController::class, 'attack_list']);
Route::post('/free/attack_list', [FreeAttackController::class, 'attack_list']);
Route::post('/free/list_attack_sort',[FreeAttackController::class, 'sort']);
Route::get('/free/buyer_detail/{attack_id}', [FreeBuyerController::class, 'buyer_detail']);
/* Terms of service */
Route::get('/terms-of-service', [TermsController::class, 'terms_of_service']);
/* Request list */
Route::get('/free/request_list', [FreeRequestController::class, 'request_list']);
Route::post('/free/request_list', [FreeRequestController::class, 'request_list']);
/* Mail list */
Route::get('/free/mail-list', [FreeMailController::class, 'index']);
Route::post('/free/mail-list', [FreeMailController::class, 'index']);
Route::post('/free/mail-sort', [FreeMailController::class, 'sort']);
Route::get('/free/mail-detail/{mail_id}',[FreeMailController::class, 'edit']);
Route::get('/free/mail-download/{mail_attachment_id}', [FreeMediaFileController::class, 'download_mail_attach_file']);
/* Template mail list */
Route::get('/free/template_mail_list',[FreeMailTemplateController::class, 'index']);
Route::get('/free/template_mail_detail/{id}',[FreeMailTemplateController::class, 'edit_template_mail']);
Route::post('/free/template_mail_detail/{id}',[FreeMailTemplateController::class, 'edit_template_mail']);
Route::get('/free/template_mail_create',[FreeMailTemplateController::class, 'create_new_template']);
Route::post('/free/template_mail_create',[FreeMailTemplateController::class, 'create_new_template_handler']);
/* Translate list */
Route::get('/free/translate-list',[FreeTranslateController::class, 'translate_list']);
Route::post('/free/translate-list',[FreeTranslateController::class, 'translate_list']);
Route::post('/free/translate-sort',[FreeTranslateController::class, 'sort']);
Route::get('/free/create-translate-request',[FreeTranslateController::class, 'create']);
Route::post('/free/create-translate-request',[FreeTranslateController::class, 'create_handler']);
Route::get('/free/translate-detail/{translate_id}',[FreeTranslateController::class, 'edit_translate']);
Route::post('/free/translate-detail/{translate_id}',[FreeTranslateController::class, 'edit_translate_handler']);
Route::delete('/free/delete-translate-request',[FreeTranslateController::class, 'delete_translate']);
/*sc-177*/
Route::get('/delete_attack/{attack_id}', [BuyerController::class, 'delete_attack']);
Route::get('/hidden_attack/{attack_id}', [BuyerController::class, 'hidden_attack']);