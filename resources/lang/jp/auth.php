<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '入力内容が誤っています',
    'password' => 'The provided password is incorrect.',
    'not_yet_active' => 'あなたのアカウントはまだ有効になっていません。',
    'throttle' => 'ログイン試行回数が多すぎます。 :seconds 秒後で再度お試しください。',

];
