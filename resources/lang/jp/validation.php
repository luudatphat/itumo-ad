<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'kana'                 => 'これはかな文字でなければなりません。',
    'one_byte'             => 'これは半角英字でなければなりません',
    'accepted'             => 'これは受け入れなければなりません。',
    'active_url'           => 'これは有効なURLではありません。',
    'after'                => 'これは:date日付後の日付でなければなりません。',
    'alpha'                => 'これには文字のみが含まれています。',
    // 'alpha_dash'           => 'これは、文字、数字、およびダッシュだけを含むことが出来ます。',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, dashes and underscores.',
    'alpha_num'            => 'これには文字と数字のみが含まれています。',
    'array'                => 'これは配列でなければなりません。',
    'before'               => 'これは:date日付前の日付でなければなりません。',
    'between'              => [
        'numeric' => 'これは、(最小) a と(最大) b の間でなければなりません。',
        'file'    => 'これは、(最小) a と(最大) b キロバイトの間でなければなりません。',
        'string'  => 'これは、(最少) a 文字と(最多) b 文字の間でなければなりません。',
        'array'   => 'これは、(最小) a と(最大) b の間にある必要があります。',
    ],
    'boolean'              => 'This field must be true or false.',
    'confirmed'            => 'この確認は一致しません。',
    'date'                 => 'これは有効な日付ではありません。',
    'date_format'          => 'これはフォーマット :format と一致しません',
    'different'            => 'これと :other フィールドは異なる必要があります。',
    'digits'               => 'これは :digits 文字でなければなりません。',
    'digits_between'       => '文字数は、(最少) :min 文字と(最多) :max 文字の間になければなりません。',
    'dimensions'           => 'これには無効な画像サイズがあります。',
    'distinct'             => 'このフィールドには値が重複しています。',
    'email'                => 'これは有効なメールアドレスでなければなりません。',
    'ends_with' => 'The :attribute must end with one of the following: :values.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'これはファイルでなければなりません。',
    'filled'               => 'このフィールドは必須です。',
    'gt'                   => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file'    => 'The :attribute must be greater than :value kilobytes.',
        'string'  => 'The :attribute must be greater than :value characters.',
        'array'   => 'The :attribute must have more than :value items.',
    ],
    'gte'                  => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file'    => 'The :attribute must be greater than or equal :value kilobytes.',
        'string'  => 'The :attribute must be greater than or equal :value characters.',
        'array'   => 'The :attribute must have :value items or more.',
    ],
    'image'                => 'これは、画像でなければなりません。',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'This field does not exist in :other.',
    'integer'              => 'これは整数でなければなりません。',
    'ip'                   => 'これは有効なIPアドレスでなければなりません。',
    'ipv4'                 => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'これは有効なJSON文字列でなければなりません。',
    'lt'                   => [
        'numeric' => 'The :attribute must be less than :value.',
        'file'    => 'The :attribute must be less than :value kilobytes.',
        'string'  => 'The :attribute must be less than :value characters.',
        'array'   => 'The :attribute must have less than :value items.',
    ],
    'lte'                  => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file'    => 'The :attribute must be less than or equal :value kilobytes.',
        'string'  => 'The :attribute must be less than or equal :value characters.',
        'array'   => 'The :attribute must not have more than :value items.',
    ],
    'max'                  => [
        'numeric' => 'これは(最大) b より大きくてはなりません。',
        'file'    => 'これは、(最大) b キロバイトを超えてはならない。',
        'string'  => 'これは、(最多) b 文字より大きくてはならない。',
        'array'   => 'これは(最多) b アイテムを超えてはいけません。',
    ],
    'mimes'                => 'これは :values タイプのファイルでなければなりません。',
    'mimetypes'            => 'これは :values タイプのファイルでなければなりません。',
    'min'                  => [
        'numeric' => 'この数字は(最少) a より小さいであってはなりません。',
        'file'    => 'これは (最小) abc キロバイトでなければなりません。',
        'string'  => ':min文字以上にしてください。',
        'array'   => '配列の要素数はabc以上であります。',
    ],
    'multiple_of' => 'The :attribute must be a multiple of :value.',
    'not_in'               => 'The selected :attribute is invalid.',
    'not_regex'            => 'The :attribute format is invalid.',
    'numeric'              => 'これは数字でなければなりません。',
    'password' => 'The password is incorrect.',
    'present'              => 'このフィールドは存在しなければなりません。',
    'regex'                => 'この形式は無効です。',
    'required'             => 'テキストを入力してください。',
    'required_if'          => 'This field is required when :other is :value.',
    'required_unless'      => 'This field is required unless :other is in :values.',
    'required_with'        => 'This field is required when :values is present.',
    'required_with_all'    => 'This field is required when :values is present.',
    'required_without'     => 'This field is required when :values is not present.',
    'required_without_all' => 'This field is required when none of :values are present.',
    'prohibited' => 'The :attribute field is prohibited.',
    'prohibited_if' => 'The :attribute field is prohibited when :other is :value.',
    'prohibited_unless' => 'The :attribute field is prohibited unless :other is in :values.',
    'same'                 => 'これと :other フィールドは一致する必要があります。',
    'size'                 => [
        'numeric' => 'この数はABCと等しくなければなりません。',
        'file'    => 'ファイルサイズはabcキロバイトに等しくなければなりません。',
        'string'  => 'この文字列の長さはabc字でなければなりません。',
        'array'   => '配列の要素数は、ABCに等しくなければなりません。',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values.',
    'string'               => 'これは文字列でなければなりません。',
    'timezone'             => 'これは有効なタイムゾーンでなければなりません。',
    'unique'               => 'このフィールドはすでに存在しています。',
    'uploaded'             => 'ファイルのアップロードに失敗しました。',
    'url'                  => 'この形式は無効です。',
    'uuid' => 'The :attribute must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
