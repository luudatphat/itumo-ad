@extends('layouts.master')
@section('title','配送依頼')
@section('sekai_content')
    <section>
        <header><h2>配送依頼画面</h2></header>
        <article>
            @if(session('message'))
                <div class="alert alert-success alert_success_company_japan">{{session('message')}}</div>
            @endif
            <div class="div-first-create-send-product" style="">

                <div class="text-center div-in-create-send-product" style="">
                    <p style="" class="p-first-create-send-product"><img style="vertical-align: top" src="images/icon_triangle_warning.jpg" width="26" height="23" alt=""> COUXU配送手数料 = 500円</p>
                    <p class="p-create-send-product" >※配送サービスは株式会社BENLYと連携しております。</p>
                    <p class="p-create-send-product" >お申込み後は、BENLY社からお見積もりが届きますのでご確認ください。</p>
                </div>
            </div>
            <form id="create-send-product" class="content-form-01 mgB-60" action="{{ url('/create-send-product') }}" method="POST">
                @csrf
                <h3 class="title-h3 mgL-12">宛先情報</h3>
                <div class="content-group-controls-01">
                    <table>
                        <tr>
                            <td>会社名<strong>必須</strong></td>
                            <td>
                                <input type="text" name="company_name" value="{{old('company_name')}}" placeholder="会社名を入力してください">
                                @if ($errors->has('company_name'))
                                    <strong>{{ $errors->first('company_name') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>苗字<strong>必須</strong></td>
                            <td>
                                <input type="text" name="last_name" value="{{old('last_name')}}" placeholder="苗字を入力してください">
                                @if ($errors->has('last_name'))
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>名前<strong>必須</strong></td>
                            <td>
                                <input type="text" name="first_name" value="{{old('first_name')}}" placeholder="名前を入力してください">
                                @if ($errors->has('first_name'))
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>国<strong>必須</strong></td>
                            <td>
                                <input type="text" name="country" value="{{old('country')}}" placeholder="国を入力してください">
                                @if ($errors->has('country'))
                                    <strong>{{ $errors->first('country') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>都市<strong>必須</strong></td>
                            <td>
                                <input type="text" name="city" value="{{old('city')}}" placeholder="都市を入力してください">
                                @if ($errors->has('city'))
                                    <strong>{{ $errors->first('city') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>住所<strong>必須</strong></td>
                            <td>
                                <input type="text" name="address" value="{{old('address')}}" placeholder="住所を入力してください">
                                @if ($errors->has('address'))
                                    <strong>{{ $errors->first('address') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <h3 class="title-h3 mgL-12">配送物詳細</h3>
                <div class="content-group-controls-01">
                    <table id="divshipping1">
                        <caption>１箱当たり</caption>
                        <tr>
                            <td>重量 / g<strong>必須</strong></td>
                            <td>
                                <input type="text" name="weight_1" value="{{old('weight_1')}}" placeholder="重量 / gを入力してください">
                                @if ($errors->has('weight_1'))
                                    <strong>{{ $errors->first('weight_1') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>サイズ縦 / cm<strong>必須</strong></td>
                            <td>
                                <input type="text" name="vertical_1" value="{{old('vertical_1')}}" placeholder="サイズ縦 / cmを入力してください">
                                @if ($errors->has('vertical_1'))
                                    <strong>{{ $errors->first('vertical_1') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>サイズ横 / cm<strong>必須</strong></td>
                            <td>
                                <input type="text" name="width_1" value="{{old('width_1')}}" placeholder="サイズ横 / cmを入力してください">
                                @if ($errors->has('width_1'))
                                    <strong>{{ $errors->first('width_1') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>サイズ高さ / cm<strong>必須</strong></td>
                            <td>
                                <input type="text" name="height_1" value="{{old('height_1')}}" placeholder="サイズ高さ / cmを入力してください">
                                @if ($errors->has('height_1'))
                                    <strong>{{ $errors->first('height_1') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>箱 個数<strong>必須</strong></td>
                            <td>
                                <input type="text" name="box_number_1" value="{{old('box_number_1')}}" placeholder="箱 個数を入力してください">
                                @if ($errors->has('box_number_1'))
                                    <strong>{{ $errors->first('box_number_1') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="text-right"><button type="button" data-id="2" class="btndivshipping">+</button></td>
                        </tr>
                    </table>
                    <?php for ($i = 2; $i <= 10; $i++){ ?>
                    <table id="divshipping<?php echo $i; ?>" style="{{($errors->has('weight_'.$i) || $errors->has('vertical_'.$i) || $errors->has('width_'.$i) || $errors->has('height_'.$i) || $errors->has('box_number_'.$i))? '':'display: none'}}">
                        <caption><?php echo $i; ?>箱当たり</caption>
                        <tr>
                            <td>重量 / g<strong>必須</strong></td>
                            <td>
                                <input type="text" name="weight_<?php echo $i; ?>" value="{{old('weight_'.$i)}}" placeholder="重量 / gを入力してください">
                                @if ($errors->has('weight_'.$i))
                                    <strong>{{ $errors->first('weight_'.$i) }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>サイズ縦 / cm<strong>必須</strong></td>
                            <td>
                                <input type="text" name="vertical_<?php echo $i; ?>" value="{{old('vertical_'.$i)}}" placeholder="サイズ縦 / cmを入力してください">
                                @if ($errors->has('vertical_'.$i))
                                    <strong>{{ $errors->first('vertical_'.$i) }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>サイズ横 / cm<strong>必須</strong></td>
                            <td>
                                <input type="text" name="width_<?php echo $i; ?>" value="{{old('width_'.$i)}}" placeholder="サイズ横 / cmを入力してください">
                                @if ($errors->has('width_'.$i))
                                    <strong>{{ $errors->first('width_'.$i) }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>サイズ高さ / cm<strong>必須</strong></td>
                            <td>
                                <input type="text" name="height_<?php echo $i; ?>" value="{{old('height_'.$i)}}" placeholder="サイズ高さ / cmを入力してください">
                                @if ($errors->has('height_'.$i))
                                    <strong>{{ $errors->first('height_'.$i) }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>箱 個数<strong>必須</strong></td>
                            <td>
                                <input type="text" name="box_number_<?php echo $i; ?>" value="{{old('box_number_'.$i)}}" placeholder="箱 個数を入力してください">
                                @if ($errors->has('box_number_'.$i))
                                    <strong>{{ $errors->first('box_number_'.$i) }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="text-right"><button type="button" data-id="<?php echo $i+1; ?>" class="btndivshipping">+</button></td>
                        </tr>
                    </table>
                    <?php } ?>
                </div>
                <!-- end .content-group-controls-01 -->
                <h3 class="title-h3 mgL-12">配送物情報</h3>
                <div class="content-group-controls-01">
                    <table id="divreport1">
                        <tr>
                            <td>商品カテゴリ<strong>必須</strong></td>
                            <td>
                                <select name="goods_category_1" class="w-330">
                                    @foreach($mtb_genres as $lgc)
                                        <option value="{{$lgc->id}}" {{ (old('goods_category_1') != NULL && old('goods_category_1') == $lgc->id)? 'selected' :''}} >{{$lgc->content}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('goods_category_1'))
                                    <br><strong>{{ $errors->first('goods_category_1') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>商品名<strong>必須</strong></td>
                            <td>
                                <input type="text" name="product_name_1" value="{{old('product_name_1')}}" placeholder="商品名を入力してください">
                                @if ($errors->has('product_name_1'))
                                    <strong>{{ $errors->first('product_name_1') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>商品単価<strong>必須</strong></td>
                            <td>
                                <input type="text" name="price_1" class="price" id="price_1" data-id="1" value="{{old('price_1')}}" placeholder="商品単価を入力してください">
                                @if ($errors->has('price_1'))
                                    <strong>{{ $errors->first('price_1') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>配送個数<strong>必須</strong></td>
                            <td>
                                <input type="text" name="quantity_1" class="quantity" id="quantity_1" data-id="1" value="{{old('quantity_1')}}" placeholder="配送個数を入力してください">
                                @if ($errors->has('quantity_1'))
                                    <strong>{{ $errors->first('quantity_1') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>合計売上<strong>必須</strong></td>
                            <td>
                                <input type="text" name="total_revenue_1" value="{{old('total_revenue_1')}}" id="total_revenue_1" placeholder="合計売上を入力してください" disabled>
                                @if ($errors->has('total_revenue_1'))
                                    <strong>{{ $errors->first('total_revenue_1') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="text-right"><button type="button" data-id="2" class="btndivreport">+</button></td>
                        </tr>
                    </table>

                    <?php for ($i = 2; $i <= 10; $i++){ ?>
                    <table id="divreport<?php echo $i?>" style="{{($errors->has('product_name_'.$i) || $errors->has('price_'.$i) || $errors->has('quantity_'.$i) || $errors->has('total_revenue_'.$i))? '':'display: none'}}">
                        <tr>
                            <td>商品カテゴリ<strong>必須</strong></td>
                            <td>
                                <select name="goods_category_<?php echo $i; ?>" class="w-330">
                                    @foreach($mtb_genres as $lgc)
                                        <option value="{{$lgc->id}}" {{ (old('goods_category'.$i) != NULL && old('goods_category'.$i) == $lgc->id)? 'selected' :''}} >{{$lgc->content}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('goods_category_'.$i))
                                    <br><strong>{{ $errors->first('goods_category_'.$i) }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>商品名<strong>必須</strong></td>
                            <td>
                                <input type="text" name="product_name_<?php echo $i; ?>" value="{{old('product_name_'.$i)}}" placeholder="商品名を入力してください">
                                @if ($errors->has('product_name_'.$i))
                                    <strong>{{ $errors->first('product_name_'.$i) }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>商品単価<strong>必須</strong></td>
                            <td>
                                <input type="text" name="price_<?php echo $i; ?>" class="price" id="price_<?php echo $i; ?>" data-id="<?php echo $i; ?>" value="{{old('price_'.$i)}}" placeholder="商品単価を入力してください">
                                @if ($errors->has('price_'.$i))
                                    <strong>{{ $errors->first('price_'.$i) }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>配送個数<strong>必須</strong></td>
                            <td>
                                <input type="text" name="quantity_<?php echo $i; ?>" class="quantity" id="quantity_<?php echo $i; ?>" data-id="<?php echo $i; ?>" value="{{old('quantity_'.$i)}}" placeholder="配送個数を入力してください">
                                @if ($errors->has('quantity_'.$i))
                                    <strong>{{ $errors->first('quantity_'.$i) }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>合計売上<strong>必須</strong></td>
                            <td>
                                <input type="text" name="total_revenue_<?php echo $i; ?>" id="total_revenue_<?php echo $i; ?>" value="{{old('total_revenue_'.$i)}}" placeholder="合計売上を入力してください" disabled>
                                @if ($errors->has('total_revenue_'.$i))
                                    <strong>{{ $errors->first('total_revenue_'.$i) }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="text-right"><button type="button" data-id="<?php echo $i+1; ?>" class="btndivreport">+</button></td>
                        </tr>
                    </table>
                    <?php } ?>
                </div>
                <!-- end .content-group-controls-01 -->
                <h3 class="title-h3 mgL-12">備考</h3>
                <div class="content-group-controls-01">
                    <table>
                        <tr>
                            <td>ご希望納期</td>
                            <td>
                                <input type="text" name="requested_delivery_date" value="{{old('requested_delivery_date')}}" class="date-delivery">
                                <?php
//                                    echo '<strong class="fs-13">← カレンダーポップアップ</strong>';
                                ?>
                                <br>
                                @if ($errors->has('requested_delivery_date'))
                                    <strong>{{ $errors->first('requested_delivery_date') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                その他ご相談やご希望があればご記載下さい
                                <textarea name="consultation" class="mgT-12 height-170"></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="text-center">
                    <a href="{{url("send-product")}}"><button type="button" class="back-list">キャンセル</button></a>
                    <button type="submit" class="btn-submit mgL-16">依頼</button>
                </div>
            </form>
        </article>
    </section>

    <script>
        $(document).ready(function () {
            $('.price, .quantity').keyup(function () {
                var id = $(this).data('id');
                $('#total_revenue_'+id).val($('#price_'+id).val() * $('#quantity_'+id).val());
            });

            $('.date-delivery').datepicker({ dateFormat: 'yy-mm-dd' });
        });

        /*Confirm when submit form*/
        $("#create-send-product").on("submit", function(){
            return confirm("配送依頼を送信してもよろしいですか。");
        });

        function addElement(element, limit, content, id, index) {
            if (index > limit) {
                return false;
            }
            if($(element).parents('.content-group-controls-01').children('#'+ id +'-'+index).length > 0) {
                //$('#DeliveryDetail-'+index).remove();
                //console.log(parseInt(index));
                $(element).html('+');
                for( i = parseInt(index); i < limit; i++ ) {
                    //console.log(i);
                    $('#'+ id +'-'+ i).remove();
                }
            } else {
                $(element).html('-');
                $(element).parents('.content-group-controls-01').append(content);
            }
        }

        function addShippingInformation(elm) {
            var index = $(elm).data('index') + 1;
            var id = 'ShippingInformation';
            var content = '<table id="'+ id +'-'+ index +'">'
                    +'<tr>'
                    +'<td>商品カテゴリ<strong>必須</strong></td>'
                    +'<td>'
                    +'<select name="goods_category_'+ index +'" class="w-330">'
                    +'@foreach($mtb_genres as $lgc)'
                    +' <option value="{{$lgc->id}}" {{ (old("goods_category_'+ index +'") != NULL && old("goods_category_'+ index +'") == $lgc->id)? "selected" :""}} >{{$lgc->content}}</option>'
                    +'@endforeach'
                    +'</select>'
                    +' @if ($errors->has("goods_category_'+ index +'"))'
                    +'<br><strong>{{ $errors->first("goods_category_'+ index +'") }}</strong>'
                    +'@endif'
                    +'</td>'
                    +'</tr>'
                    +'<tr>'
                    +'<td>商品名<strong>必須</strong></td>'
                    +'<td>'
                    +'<input type="text" name="product_name_'+ index +'" value="{{old("product_name_'+ index +'")}}" placeholder="商品名を入力してください">'
                    +'@if ($errors->has("product_name_'+ index +'"))'
                    +'<strong>{{ $errors->first("product_name_'+ index +'") }}</strong>'
                    +'@endif'
                    +'</td>'
                    +'</tr>'
                    +'<tr>'
                    +'<td>商品単価<strong>必須</strong></td>'
                    +'<td>'
                    +'<input type="text" name="price_'+ index +'" value="{{old("price_'+ index +'")}}" placeholder="商品単価を入力してください">'
                    +'@if ($errors->has("price_'+ index +'"))'
                    +'<strong>{{ $errors->first("price_'+ index +'") }}</strong>'
                    +'@endif'
                    +'</td>'
                    +'</tr>'
                    +'<tr>'
                    +'<td>配送個数<strong>必須</strong></td>'
                    +'<td>'
                    +'<input type="text" name="quantity_'+ index +'" value="{{old("quantity_'+ index +'")}}" placeholder="配送個数を入力してください">'
                    +'@if ($errors->has("quantity_'+ index +'"))'
                    +'<strong>{{ $errors->first("quantity_'+ index +'") }}</strong>'
                    +'@endif'
                    +'</td>'
                    +'</tr>'
                    +'<tr>'
                    +'<td>合計売上<strong>必須</strong></td>'
                    +'<td>'
                    +'<input type="text" name="total_revenue_'+ index +'" value="{{old("total_revenue_'+ index +'")}}" placeholder="合計売上を入力してください">'
                    +'@if ($errors->has("total_revenue_'+ index +'"))'
                    +'<strong>{{ $errors->first("total_revenue_'+ index +'") }}</strong>'
                    +'@endif'
                    +'</td>'
                    +'</tr>'
                    +'<tr>'
                    +'<td></td>'
                    +'<td class="text-right"><button type="button" data-index="'+ index +'" onclick="addShippingInformation(this)">+</button></td>'
                    +'</tr>'
                    +'</table>';
            addElement(elm, 7, content, id, index);
        }

        function addDeliveryDetails(element) {
            var index = $(element).data('index') + 1;
            var id = 'DeliveryDetail';
            //console.log(index);
            var content = '<table id="'+ id +'-'+ index +'">'
                    +'<caption>'+index+' 箱当たり</caption>'
                    +'<tr>'
                    +'<td>重量 / g<strong>必須</strong></td>'
                    +'<td>'
                    +'<input type="text" name="weight_'+ index +'" value="{{old("weight_'+ index +'")}}" placeholder="重量 / gを入力してください">'
                    +'@if ($errors->has("weight_'+ index +'"))'
                    +'<strong>{{ $errors->first("weight_'+ index +'") }}</strong>'
                    +'@endif'
                    +'</td>'
                    +'</tr>'
                    +'<tr>'
                    +'<td>サイズ縦 / cm<strong>必須</strong></td>'
                    +'<td>'
                    +'<input type="text" name="vertical_'+ index +'" value="{{old("vertical_'+ index +'")}}" placeholder="サイズ縦 / cmを入力してください">'
                    +'@if ($errors->has("vertical_'+ index +'"))'
                    +'<strong>{{ $errors->first("vertical_'+ index +'") }}</strong>'
                    +'@endif'
                    +'</td>'
                    +'</tr>'
                    +'<tr>'
                    +'<td>サイズ横 / cm<strong>必須</strong></td>'
                    +'<td>'
                    +'<input type="text" name="width_1" value="{{old("width_'+ index +'")}}" placeholder="サイズ横 / cmを入力してください">'
                    +'@if ($errors->has("width_'+ index +'"))'
                    +'<strong>{{ $errors->first("width_'+ index +'") }}</strong>'
                    +'@endif'
                    +'</td>'
                    +'</tr>'
                    +'<tr>'
                    +'<td>サイズ高さ / cm<strong>必須</strong></td>'
                    +'<td>'
                    +'<input type="text" name="height_'+ index +'" value="{{old("height_'+ index +'")}}" placeholder="サイズ高さ / cmを入力してください">'
                    +'@if ($errors->has("height_1"))'
                    +'<strong>{{ $errors->first("height_'+ index +'") }}</strong>'
                    +'@endif'
                    +'</td>'
                    +'</tr>'
                    +'<tr>'
                    +'<td>箱 個数<strong>必須</strong></td>'
                    +'<td>'
                    +'<input type="text" name="height_'+ index +'" value="{{old("box_number_'+ index +'")}}" placeholder="箱 個数を入力してください">'
                    +'@if ($errors->has("box_number_'+ index +'"))'
                    +'<strong>{{ $errors->first("box_number_'+ index +'") }}</strong>'
                    +'@endif'
                    +'</td>'
                    +'</tr>'
                    +'<tr>'
                    +'<td></td>'
                    +'<td class="text-right"><button type="button" data-index="'+ index +'" onclick="addDeliveryDetails(this)">+</button></td>'
                    +'</tr>'
                    +'</table>';
            //console.log($(element).parents('.content-group-controls-01').children('#DeliveryDetail-'+index).length);
            addElement(element, 10, content, id, index);
        }
    </script>
@endsection