@extends('layouts.master')
@section('title','配送一覧')
@section('sekai_content')
    <meta name="_token" content="{{ csrf_token() }}">

    <section class="fixed-min-width">
        <header><h2>配送一覧</h2></header>
        <article>
            @if(Session::has('flash_messages'))
                <div class="alert alert-{!! Session::get('flash_level') !!} alert_success_company_japan">
                    {!! Session::get('flash_messages') !!}
                </div>
            @endif
            <form class="form-sort form-sort-01 fix-form-01" action="{{ url('/send-product') }}" method="POST">
                @csrf
                <div class="content-group-control">
                    <div class="clearfix bottom-form pdT-0 pdLR-0">
                        <div class="pull-left">
                            <span class="title-control">状態</span>
                            <select name="status" id="status" class="mgR-37" >
                                <option value="">全て</option>
                                <option value="1" {{ (isset($status) && $status == '1')?'selected':''}}>OK</option>
                                <option value="2" {{ (isset($status) && $status == '2')?'selected':''}}>レビュー待ち</option>
                            </select>
                            <span class="title-control">キーワード</span>
                            <input class="input-last fix-w-input-02" type="text" placeholder="キーワードを入力して下さい" name="search" id="search" value="{{ isset($search)?$search:'' }}">
                            <span class="text-center"><button class="fix-width-btn-05" type="submit">検索</button></span>
                        </div>
                    </div>
                </div>
                <!-- end .content-group-control -->
            </form>

                <div class="row">
                    <div class="col-md-12 text-right fix-top-layout fix-bottom-layout">
                        <a href="{{url('/create-send-product')}}" class="btn btn-primary fix-width-btn-06 fix-width-btn-07">成約報告新規作成</a>
                    </div>
                </div>

            <table class="content-list-table-02 fix-table-01 result-table fix-table-02" style="white-space: nowrap">
                <thead>
                <tr>
                    <th id="created_at" class="text-center col-sort w-150">依頼日</th>
                    <th id="company_name" class="text-center col-sort">会社名</th>
                    <th id="last_name" class="text-center col-sort">名字</th>
                    <th id="first_name" class="text-center col-sort">名前</th>
                    <th id="country" class="text-center col-sort">国</th>
                    <th id="city" class="text-center col-sort">都市名</th>
                    <th id="address" class="text-center col-sort">住所</th>
                    <th class="text-center">ステータス</th>
                    <th class="text-center w-125">アクション</th>
                </tr>
                </thead>

                <tbody>
                @forelse ($products as $product)
                    <tr>
                        <td>{{ isset($product->created_at)?date_format(date_create($product->created_at), 'Y/m/d'):'' }}</td>
                        <td>{{ isset($product->company_name)?$product->company_name:'' }}</td>
                        <td>{{ isset($product->last_name)?$product->last_name:'' }}</td>
                        <td>{{ isset($product->first_name)?$product->first_name:'' }}</td>
                        <td>{{ isset($product->country)?$product->country:'' }}</td>
                        <td>{{ isset($product->city)?$product->city:'' }}</td>
                        <td>{{ isset($product->address)?$product->address:'' }}</td>
                        <td>
                            @if($product->status == 1)
                                {{ 'OK' }}
                            @else
                                {{ 'レビュー待ち' }}
                            @endif
                        </td>
                        <td class="text-center" style="vertical-align: middle">
                            <a class="btn btn-primary fix-btn-01" href="{{ url('/detail-send-product') }}/{{$product->id}}">詳細</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="9">データなし。</td>
                    </tr>
                @endforelse
                </tbody>
            </table>

            <nav class="text-right total-records pagination_div show_record">
                <p class="text-right">Records {{ ($products->currentPage() -1) * $products->perPage() + 1 }} -
                    {{ ($products->lastPage() == $products->currentPage()) ? $products->total() : ($products->currentPage() * $products->perPage()) }}
                    of {{ $products->total() }}</p>
                {{$products->links('pagination::bootstrap-4')}}
            </nav>

        </article>
    </section>

    <script type="text/javascript">
        $('.col-sort').click(function(){
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });
//            $('.col_sort').css({'background': '#3097D1'});
//            $(this).css({'background': '#3097D1'});
//            $('.col_sort').children('.fa').css({'display':'inline'});
            var page = {{$current_page}};
            var search = $('#search').val();
            var status = $('#status').val();
            if($(this).hasClass('down')) {
                $(this).removeClass('down');
                $(this).children('.fa-caret-down').css({'display': 'none'});
                $(this).children('.fa-caret-up').css({'display': 'inline'});
                //Sort ASC
                var current_token = '{{csrf_token()}}';
                var column = $(this).attr('id');
                $('#column_dl').val(column);
                $('#sort_dl').val("asc");
                $.ajax({
                    url: '/send-product-sort?page=' + page,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    data: {search: search, status: status, column: column, sort: 'asc', current_page: page,  fuel_csrf_token: current_token},
                    success: function( data, textStatus, jQxhr ){
                        var result = JSON.parse(data);
//                        $('.result-table tbody').html(data);
                        $('.result-table tbody').html(result['content']);
                        $('.pagination_div').html(result['pagination']);
                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        console.log( errorThrown );
                    }
                });

            }else {
                $(this).addClass('down');
                $(this).children('.fa-caret-up').css({'display': 'none'});
                $(this).children('.fa-caret-down').css({'display': 'inline'});
                //Sort DESC
                var current_token = '{{csrf_token()}}';
                var column =$(this).attr('id');
                $('#column_dl').val(column);
                $('#sort_dl').val("desc");
                $.ajax({
                    url: '/send-product-sort?page=' + page,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    data: {search: search, status: status,  column: column, sort: 'desc', current_page: page ,  fuel_csrf_token: current_token},
                    success: function( data, textStatus, jQxhr ){
                        var result = JSON.parse(data);
                        $('.result-table tbody').html(result['content']);
                        $('.pagination_div').html(result['pagination']);
//                        $('.result-table tbody').html(data);
                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        console.log( errorThrown );
                    }
                });
            }
        });

        $('.col-sort').click(function (e) {
            e.preventDefault();
            if($(this).hasClass('select-sort')) {
                $(this).removeClass('select-sort');
                return false;
            }
            $('.col-sort').removeClass('select-sort');
            $(this).addClass('select-sort');
        });
    </script>

@endsection