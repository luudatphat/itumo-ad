@extends('layouts.master')
@section('title','配送依頼画面')
@section('sekai_content')
    <section>
        <header><h2>配送依頼画面</h2></header>
        <article>
            @if(session('message'))
                <div class="alert alert-success alert_success_company_japan">{{session('message')}}</div>
            @endif
            <form id="detail-send-product" class="content-form-01 mgB-60" action="{{ url('/send-product-update') }}" method="POST">
                @csrf
                <input type="hidden" name="delivery_request_id" value="{{$product->id}}">
                <h3 class="title-h3 mgL-12">宛先情報</h3>
                <div class="content-group-controls-01">
                    <table>
                        <tr>
                            <td>会社名<strong>必須</strong></td>
                            <td>
                                <input type="text" name="company_name" value="{{$product->company_name}}" {{$product->status == '1' ? 'disabled' : ''}} placeholder="会社名を入力してください">
                                @if ($errors->has('company_name'))
                                    <strong>{{ $errors->first('company_name') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>苗字<strong>必須</strong></td>
                            <td>
                                <input type="text"  name="last_name" value="{{$product->last_name}}" {{$product->status == '1' ? 'disabled' : ''}} placeholder="苗字を入力してください">
                                @if ($errors->has('last_name'))
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>名前<strong>必須</strong></td>
                            <td>
                                <input type="text" name="first_name" value="{{$product->first_name}}" {{$product->status == '1' ? 'disabled' : ''}} placeholder="名前を入力してください">
                                @if ($errors->has('first_name'))
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>国<strong>必須</strong></td>
                            <td>
                                <input type="text" name="country" value="{{$product->country}}" {{$product->status == '1' ? 'disabled' : ''}} placeholder="国を入力してください">
                                @if ($errors->has('country'))
                                    <strong>{{ $errors->first('country') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>都市<strong>必須</strong></td>
                            <td>
                                <input type="text" name="city" value="{{$product->city}}" {{$product->status == '1' ? 'disabled' : ''}} placeholder="都市を入力してください">
                                @if ($errors->has('city'))
                                    <strong>{{ $errors->first('city') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>住所<strong>必須</strong></td>
                            <td>
                                <input type="text" name="address" value="{{$product->address}}" {{$product->status == '1' ? 'disabled' : ''}} placeholder="住所を入力してください">
                                @if ($errors->has('address'))
                                    <strong>{{ $errors->first('address') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <h3 class="title-h3 mgL-12">配送物詳細</h3>
                <div class="content-group-controls-01">
                    <?php for ($i = 1; $i <= 10; $i++){ ?>
                    <table id="divshipping<?php echo $i; ?>" style="{{($errors->has('weight_'.$i) || $errors->has('vertical_'.$i) || $errors->has('width_'.$i) || $errors->has('height_'.$i) || $errors->has('box_number_'.$i) || $i == 1 || isset($shipping_info[$i-1])) ? '':'display: none'}}">
                        <caption><?php echo $i; ?>箱当たり</caption>
                        <tr>
                            <td>重量 / g<strong>必須</strong></td>
                            <td>
                                <input type="text" {{$product->status == '1' ? 'disabled' : ''}} name="weight_{{$i}}" value="{{ isset($shipping_info[$i-1])? $shipping_info[$i-1]['weight'] : '' }}" placeholder="重量 / gを入力してください">
                                @if ($errors->has('weight_'.$i))
                                    <strong>{{ $errors->first('weight_'.$i) }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>サイズ縦 / cm<strong>必須</strong></td>
                            <td>
                                <input type="text" {{$product->status == '1' ? 'disabled' : ''}} name="vertical_{{$i}}" value="{{isset($shipping_info[$i-1])? $shipping_info[$i-1]['vertical'] : ''}}" placeholder="サイズ縦 / cmを入力してください">
                                @if ($errors->has('vertical_'.$i))
                                    <strong>{{ $errors->first('vertical_'.$i) }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>サイズ横 / cm<strong>必須</strong></td>
                            <td>
                                <input type="text" {{$product->status == '1' ? 'disabled' : ''}} name="width_{{$i}}" value="{{isset($shipping_info[$i-1])? $shipping_info[$i-1]['width'] : ''}}" placeholder="サイズ横 / cmを入力してください">
                                @if ($errors->has('width_'.$i))
                                    <strong>{{ $errors->first('width_'.$i) }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>サイズ高さ / cm<strong>必須</strong></td>
                            <td>
                                <input type="text" {{$product->status == '1' ? 'disabled' : ''}} name="height_{{$i}}" value="{{isset($shipping_info[$i-1])? $shipping_info[$i-1]['height'] : ''}}" placeholder="サイズ高さ / cmを入力してください">
                                @if ($errors->has('height_'.$i))
                                    <strong>{{ $errors->first('height_'.$i) }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>箱 個数<strong>必須</strong></td>
                            <td>
                                <input type="text" {{$product->status == '1' ? 'disabled' : ''}} name="box_number_{{$i}}" value="{{isset($shipping_info[$i-1])? $shipping_info[$i-1]['box_number'] : ''}}" placeholder="箱 個数を入力してください">
                                @if ($errors->has('box_number_'.$i))
                                    <strong>{{ $errors->first('box_number_'.$i) }}</strong>
                                @endif
                            </td>
                        </tr>
                        @if($product->status != '1')
                        <tr>
                            <td></td>
                            <td class="text-right"><button type="button" data-id="<?php echo $i+1; ?>" class="btndivshipping">+</button></td>
                        </tr>
                        @endif
                    </table>
                    <?php } ?>
                </div>
                <!-- end .content-group-controls-01 -->
                <h3 class="title-h3 mgL-12">配送物情報</h3>
                <div class="content-group-controls-01">
                    <?php for ($i = 1; $i <= 10; $i++){ ?>
                            <table id="divreport<?php echo $i?>" style="{{(isset($delivery_details[$i-1]) || $errors->has('product_name_'.$i) || $errors->has('price_'.$i) || $errors->has('quantity_'.$i) || $errors->has('total_revenue_'.$i)) || $i == 1 ? '':'display: none'}}">
                            <tr>
                                <td>商品カテゴリ<strong>必須</strong></td>
                                <td>
                                    <select name="goods_category_<?php echo $i; ?>" class="w-330" {{$product->status == '1' ? 'disabled' : ''}}>
                                        @foreach($mtb_genres as $lgc)
                                            <option value="{{$lgc->id}}" {{ (isset($delivery_details[$i-1]) && $delivery_details[$i-1]['mtb_genre_id'] == $lgc->id)? 'selected' : ''}} >{{$lgc->content}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('goods_category_'.$i))
                                        <br><strong>{{ $errors->first('goods_category_'.$i) }}</strong>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>商品名<strong>必須</strong></td>
                                <td>
                                    <input type="text" {{$product->status == '1' ? 'disabled' : ''}}  name="product_name_<?php echo $i?>" id="inpn" value="{{isset($delivery_details[$i-1])? $delivery_details[$i-1]['product_name'] : '' }}" placeholder="商品名を入力してください">
                                    @if ($errors->has('product_name_'.$i))
                                        <strong>{{ $errors->first('product_name_'.$i) }}</strong>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>商品単価<strong>必須</strong></td>
                                <td>
                                    <input type="text" class="price" {{$product->status == '1' ? 'disabled' : ''}} name="price_<?php echo $i?>" id="price_<?php echo $i?>" data-id="<?php echo $i?>" value="{{isset($delivery_details[$i-1])? $delivery_details[$i-1]['item_price'] : '' }}" placeholder="商品単価を入力してください">
                                    @if ($errors->has('price_'.$i))
                                        <strong>{{ $errors->first('price_'.$i) }}</strong>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>配送個数<strong>必須</strong></td>
                                <td>
                                    <input type="text" class="quantity" {{$product->status == '1' ? 'disabled' : ''}} name="quantity_<?php echo $i?>" id="quantity_<?php echo $i?>" data-id="<?php echo $i?>" value="{{isset($delivery_details[$i-1])? $delivery_details[$i-1]['number'] : '' }}" placeholder="配送個数を入力してください">
                                    @if ($errors->has('quantity_'.$i))
                                        <strong>{{ $errors->first('quantity_'.$i) }}</strong>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>合計売上<strong>必須</strong></td>
                                <td>
                                    <input type="text" name="total_revenue_<?php echo $i?>" id="total_revenue_<?php echo $i?>" value="{{(isset($delivery_details[$i-1]) ? ($delivery_details[$i-1]['item_price']*$delivery_details[$i-1]['number']) : '' )}}" placeholder="合計売上を入力してください" disabled>
                                    @if ($errors->has('total_revenue_'.$i))
                                        <strong>{{ $errors->first('total_revenue_'.$i) }}</strong>
                                    @endif
                                </td>
                            </tr>
                            @if($product->status != '1')
                            <tr>
                                <td></td>
                                <td class="text-right"><button type="button" data-id="<?php echo $i+1; ?>" class="btndivreport">+</button></td>
                            </tr>
                            @endif
                        </table>
                    <?php } ?>
                </div>
                <!-- end .content-group-controls-01 -->
                <h3 class="title-h3 mgL-12">備考</h3>
                <div class="content-group-controls-01">
                    <table>
                        <tr>
                            <td>ご希望納期</td>
                            <td>
                                <input type="text" name="requested_delivery_date" value="{{date_format(date_create($product['requested_delivery_date']),'Y-m-d')}}" class="date-delivery">
                                <strong class="fs-13">← カレンダーポップアップ</strong>
                                <br>
                                @if ($errors->has('requested_delivery_date'))
                                    <strong>{{ $errors->first('requested_delivery_date') }}</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                その他ご相談やご希望があればご記載下さい
                                <textarea {{$product->status == '1' ? 'disabled' : ''}} name="consultation" class="mgT-12 height-170">{{ $product['consultation'] }}</textarea>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="text-center">
                    <a href="{{url("send-product")}}"><button type="button" class="back-list">キャンセル</button></a>
                    @if($product->status != 1)
                    <button type="submit" class="btn-submit mgL-16">依頼</button>
                    @endif
                </div>
            </form>
        </article>
    </section>
<script>
    $(document).ready(function () {
        /*Confirm when submit form*/
        $("#detail-send-product").on("submit", function(){
            return confirm("配送依頼を送信してもよろしいですか。");
        });

        $('.price, .quantity').keyup(function () {
            var id = $(this).data('id');
            $('#total_revenue_'+id).val($('#price_'+id).val() * $('#quantity_'+id).val());
        });

        $('.date-delivery').datepicker({ dateFormat: 'yy-mm-dd' });
    });
</script>
@endsection