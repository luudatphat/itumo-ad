@extends('layouts.master')
@section('title','翻訳リクエストの詳細')
@section('sekai_content')
<section>
    <header><h2>翻訳依頼</h2></header>
    @if(session('message'))
    <div class="alert alert-success alert_success_company_japan">{{session('message')}}</div>
    @endif
    <article>
        <form method="post" action="">
            @csrf
        <input type="hidden" name="translate_id" value="{{$translate->id}}">
        <div class="title-control title-create-translate">リクエストしたコンテンツ</div>
        <textarea {{$translate->status != config('constants.STATUS_TRANSLATE_NOT_YET_REQUEST') ? 'readonly': ''}} class="form-control" name="content_required" id="content_required" rows="10">{{$translate->content_required}}</textarea>
        @if ($errors->has('content_required'))
            <span class="text-danger">
            <strong>{{ $errors->first('content_required') }}</strong>
        </span>
        @endif
        @if($translate->status == config('constants.STATUS_TRANSLATE_OK'))
            <div class="title-control title-create-translate mgT-33">翻訳されたコンテンツ</div>
            <textarea readonly class="form-control" name="content_result" id="content_result" rows="10">{{$translate->content_result}}</textarea>
        @endif
        <div class="content-2col mgT-25">
            <div class="a-col">
                <div class="title-create-translate">文字数カウント</div>
                <div>
                    <input class="form-control" name="number_character" id="number_character" value="{{$translate->word_count}}" readonly>
                </div>
            </div>
            <div class="a-col">
                <div class="title-create-translate">翻訳費用（税抜き）</div>
                <div>
                    <input class="form-control" name="translate_price" id="translate_price" value="{{$translate->translate_price}}" readonly>
                </div>
            </div>
        </div>
        <div class="border-create-translate"></div>
        <div class="text-center mgT-27 mgB-40">
            <a href="{{url('/translate-list')}}"><button type="button" class="detail-back-translate">翻訳一覧に戻る</button></a>
            @if($translate->status == config('constants.STATUS_TRANSLATE_NOT_YET_REQUEST'))
                <input type="submit" name="btnSave" class="save-translate" value="セーブ">
                <input type="submit" name="btnRequest" class="submit-translate" value="依頼する">
            @endif
        </div>
        </form>
    </article>
</section>

<script>
    $(document).ready(function () {
        $('#content_required').bind("keyup change mouseup mousedown paste",function () {
            var elem = $(this);
            setTimeout(function() {
                var content = elem.val().replace(/(\r\n|\n|\r|\s)/gm,"").trim();
                $('#number_character').val(content.length);
                $('#translate_price').val(content.length*{{env('TRANSLATE_PRICE_PER_WORD')}})
            }, 100);
        });
    })
</script>
@endsection