@extends('layouts.master')
@section('title','詳細メール')
@section('sekai_content')
    <section>
        <header><h2>詳細メール</h2></header>
        <article>
            <table class="table-type-01">
                <tr>
                    <th>登録日</th>
                    <td>{{ date_format(date_create($mail->created_at),'Y-m-d') }}</td>
                </tr>
                <tr>
                    <th>レコード番号</th>
                    <td><a href="/buyer_detail/{{ $mail->attack_id }}">{{ isset($mail->attack->request->id) ? $mail->attack->request->id : "" }}</a></td>
                </tr>
                <tr>
                    <th>企業名</th>
                    <td>{{ $mail->attack->request->buyer->company_name }}</td>
                </tr>
                <tr>
                    <th>担当者名</th>
                    <td>{{ $mail->supplier->contact_name_kanji }}</td>
                </tr>
                <tr>
                    <th>件名</th>
                    <td>{{$mail->title}}</td>
                </tr>
                <tr>
                    <th>本文</th>
                    <td>{!!nl2br(e($mail->content))!!}</td>
                </tr>
                <tr>
                    <th>添付ファイル</th>
                    <td>
                        <ul>
                            @foreach ($attachment_lists as $item)
                                @if (is_null($item['deleted_at']))
                                    <li><a href="{{ "/mail-download/".$item['id'] }}">{{ $item['file_name'] }}</a></li>
                                @else
                                    <li>{{ $item['file_name'] }}</li>
                                @endif
                            @endforeach
                        </ul>
                    </td>
                </tr>
            </table>
            <div class="text-center">
                <a href="{{url('mail-list')}}" class=""><button type="button" class="button-back">送信済みメール一覧に戻る</button></a>
            </div>
        </article>
    </section>
@endsection