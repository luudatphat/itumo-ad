@extends('layouts.master')
@section('title','送信済みメール一覧')
@section('sekai_content')
    <meta name="_token" content="{{ csrf_token() }}">

    <section>
        <header><h2>送信メール一覧</h2></header>
        <article>

            <form class="form-sort form-sort-01 fix-form-01" action="{{ url('/mail-list') }}" method="POST">
                @csrf
                <div class="content-group-control">
                                <div class="fix-display fix-mg-r-02">
                                    <span class="title-control">キーワード</span>
                                    <input class="input-last fix-w-input-01" type="text" placeholder="キーワードを入力して下さい" name="search" id="search" value="{{ isset($search)?$search:'' }}">
                                    <span class="text-center bottom-form"><button class="fix-width-btn-04" type="submit">検索</button></span>
                                </div>
                </div>
                <!-- end .content-group-control -->
            </form>

            <table class="content-list-table-02 fix-table-01 result-table fix-table-02" style="white-space: nowrap">
                <thead>
                <tr>
                    <th id="id" class="text-center col-sort w-100">NO.</th>
                    <th id="created_at" class="text-center col-sort">登録日</th>
                    <th id="attack_id" class="text-center col-sort">レコード番号</th>
                    <th class="text-center">企業名</th>
                    <th class="text-center">件名</th>
                    <th class="text-center w-125">詳細</th>
                </tr>
                </thead>

                <tbody>
                @forelse($mail_lists as $mail)
                    <tr>
                        <td>{{ $mail->id }}</td>
                        <td>{{ $mail->created_at }}</td>
                        <td><a href="/buyer_detail/{{ $mail->attack_id }}">{{ isset($mail->attack->request->id) ? $mail->attack->request->id : "" }}</a></td>
                        <!-- an_tnh_sc_136_start -->
                        <td>
                            @if ( is_object( $mail->attack->request->buyer ) )
                                {{ $mail->attack->request->buyer->company_name }}
                            @endif
                        </td>
                        <!-- an_tnh_sc_136_end -->
                        <td>{{ $mail->title }}</td>
                        <td class="text-center" style="vertical-align: middle">
                            <a class="btn btn-primary fix-btn-01" href="{!! url('mail-detail/'.$mail->id) !!}">詳細</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6">データなし。</td>
                    </tr>
                @endforelse
                </tbody>
            </table>

            <nav class="text-right total-records pagination_div show_record">
                <p class="text-right">Records {{ ($mail_lists->currentPage() -1) * $mail_lists->perPage() + 1 }} -
                    {{ ($mail_lists->lastPage() == $mail_lists->currentPage()) ? $mail_lists->total() : ($mail_lists->currentPage() * $mail_lists->perPage()) }}
                    of {{ $mail_lists->total() }}</p>
                {{$mail_lists->links('pagination::bootstrap-4')}}
            </nav>

        </article>
    </section>
    <script type="text/javascript">
        $('.col-sort').click(function(){
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });
//            $('.col-sort').css({'background': '#3097D1'});
//            $(this).css({'background': '#3097D1'});
//            $('.col-sort').children('.fa').css({'display':'inline'});
            var page = {{$current_page}};
            var search = $('#search').val();
            if($(this).hasClass('down')) {
                $(this).removeClass('down');
                $(this).children('.fa-caret-down').css({'display': 'none'});
                $(this).children('.fa-caret-up').css({'display': 'inline'});
                //Sort ASC
                var current_token = '{{csrf_token()}}';
                var column = $(this).attr('id');
                $('#column_dl').val(column);
                $('#sort_dl').val("asc");
                $.ajax({
                    url: '/mail-sort?page=' + page,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    data: {search: search, column: column, sort: 'asc', current_page: page,  fuel_csrf_token: current_token},
                    success: function( data, textStatus, jQxhr ){
                        var result = JSON.parse(data);
                        $('.result-table tbody').html(result['content']);
                        $('.pagination_div').html(result['pagination']);
                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        console.log( errorThrown );
                    }
                });

            }else {
                $(this).addClass('down');
                $(this).children('.fa-caret-up').css({'display': 'none'});
                $(this).children('.fa-caret-down').css({'display': 'inline'});
                //Sort DESC
                var current_token = '{{csrf_token()}}';
                var column =$(this).attr('id');
                $('#column_dl').val(column);
                $('#sort_dl').val("desc");
                $.ajax({
                    url: '/mail-sort?page=' + page,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    data: {search: search,  column: column, sort: 'desc', current_page: page ,  fuel_csrf_token: current_token},
                    success: function( data, textStatus, jQxhr ){
                        var result = JSON.parse(data);
                        $('.result-table tbody').html(result['content']);
                        $('.pagination_div').html(result['pagination']);
                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        console.log( errorThrown );
                    }
                });
            }
        });
    </script>

    <script type="application/javascript">
        $(document).ready(function (e) {
            $('.col-sort').click(function (e) {
                e.preventDefault();
                if($(this).hasClass('select-sort')) {
                    $(this).removeClass('select-sort');
                    return false;
                }
                $('.col-sort').removeClass('select-sort');
                $(this).addClass('select-sort');
            })
        });
    </script>
@endsection