<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style-fix.css">
    <script src="/js/app.js"></script>
    <?php include resource_path('views/layouts/juiser.php'); ?>
    <?php include resource_path('views/layouts/push-notification.php'); ?>
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body id="index">

<header class="clearfix">
    <h1 id="logo" class="pull-left"><a href="#"><img src="/images/logo_new.png" width="220" height="34" alt=""></a></h1>
    <ul class="pull-right">
        <li><a href="{{ url('/login') }}">ログイン</a></li>
        <li><a href="{{ url('/register') }}">法人登録</a></li>
    </ul>
</header>

@yield('content')

<!-- Scripts -->

</body>
</html>
