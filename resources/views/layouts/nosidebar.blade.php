<!DOCTYPE html>

<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex,nofollow">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"><![endif]-->
    <link rel="icon" href="{{ url('favicon.ico') }}">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/style.css?date_change=20190218094910">
    <link rel="stylesheet" href="/css/sekai-connect.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/jquery-ui/jquery-ui.css">
    <link rel="stylesheet" href="/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="/css/navbar.css">
    <link rel="stylesheet" href="/css/quill.snow.css">
    <link rel="stylesheet" href="/css/style-fix.css">

    <script src="/js/jquery-3.1.1.js"></script>
    <script src="/js/app.js"></script>
    <script src="/jquery-ui/jquery-ui.js"></script>
    <script src="/jquery-ui/datepicker-ja.js"></script>
    <script src="/js/myScript.js"></script>
    <script src="/js/jquery.dataTables.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/listgroup.min.js"></script>
    {{--JavaScript google chart--}}
    <script type="text/javascript" src="/js/loader.js"></script>
    <!-- Scripts -->
    <?php include resource_path('views/layouts/juiser.php'); ?>
    <?php include resource_path('views/layouts/push-notification.php'); ?>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#datepicker").datepicker({
                showButtonPanel: true,
                minDate: 0
            });

            $('#btndivreport4').click(function(e){
                $('#divreport4').toggle('1000');
            });
            $('#btndivreport5').click(function(e){
                $('#divreport5').toggle('1000');
            });
            $('#btndivreport6').click(function(e){
                $('#divreport6').toggle('1000');
            });
            $('#btndivreport7').click(function(e){
                $('#divreport7').toggle('1000');
            });

            $('.btndivreport').click(function (e) {
                var id = $(this).data('id');
                $('#divreport' + id).toggle('1000');
            });

            $('.btndivshipping').click(function (e) {
                var id = $(this).data('id');
                $('#divshipping' + id).toggle('1000');
            });

            $('.alert_success_company_japan').delay(3000).slideUp();
            $('.alert_success_company_japan_reset_password').delay(10000).slideUp();

            /*Active current menu*/
            var current_url = window.location.href;
            $.each($("aside a[href='" + current_url + "']"), function () {
                if ($(this).attr('class') != 'po_a') {
                    $(this).parent().addClass('po_active');
                    $(this).parents('.panel-collapse').prev().click();
                    if($('.po_active').parents('.dropdown')) {
                        //$('.po_active').parents('.dropdown').css('background', 'red');
                        $('.po_active').parents('.dropdown').children().children('i').toggleClass("rotate down");
                    }
                }
            });
            //alert($('.po_active img').attr('src'));
            if($('.po_active img').length>0) {
                $('.po_active img').attr('src', $('.po_active img').attr('src').replace('_noactive', '_active'));
            }

            $("aside .dropdown").click(function(){
                $(this).children().children('i').toggleClass("rotate down");
            });

            for (var i = 1; i <= 4; i++) {
                $('#images' + i).click(function () {
                    var src = $(this).attr('src');
                    $('#imagesmain').fadeOut('fast').attr('src', src).stop(true, true).hide().fadeIn(1000);
                });
            }
        });

    </script>
    <title>@yield('title')</title>

</head>
<body>
<header class="clearfix">
    <h1 id="logo" class="pull-left"><a href="#"><img src="/images/logo_new.png" width="220" height="34" alt=""></a></h1>
    <ul class="pull-right">
        <li><a href="{{ url('/login') }}">ログイン</a></li>
        <li><a href="{{ url('/register') }}">法人登録</a></li>
    </ul>
</header>
<div id="container" class="fix-container-01">
    <article class="clearfix">
        <!-- Main Content -->
        <div class="container">
            @yield('sekai_content')
        </div>
    </article>
    <footer>
        <ul>
            <li>© 2017 sekai-connect</li>
        </ul>
    </footer>
</div>
<!-- end #container -->
</body>
</html>