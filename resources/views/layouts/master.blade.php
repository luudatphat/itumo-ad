<!DOCTYPE html>

<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex,nofollow">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"><![endif]-->
    <link rel="icon" href="{{ url('favicon.ico') }}">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/style.css?date_change=20190218094910">
    <link rel="stylesheet" href="/css/sekai-connect.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/jquery-ui/jquery-ui.css">
    <link rel="stylesheet" href="/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="/css/navbar.css">
    <link rel="stylesheet" href="/css/quill.snow.css">
    <link rel="stylesheet" href="/css/style-fix.css">

    <script src="/js/jquery-3.1.1.js"></script>
    <script src="/js/app.js"></script>
    <script src="/jquery-ui/jquery-ui.js"></script>
    <script src="/jquery-ui/datepicker-ja.js"></script>
    <script src="/js/myScript.js"></script>
    <script src="/js/jquery.dataTables.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/listgroup.min.js"></script>
    {{--JavaScript google chart--}}
    <script type="text/javascript" src="/js/loader.js"></script>
    <!-- Scripts -->

    <script type="text/javascript">
        $(document).ready(function () {

            $("#datepicker").datepicker({
                showButtonPanel: true,
                minDate: 0
            });

            $('#btndivreport4').click(function(e){
                $('#divreport4').toggle('1000');
            });
            $('#btndivreport5').click(function(e){
                $('#divreport5').toggle('1000');
            });
            $('#btndivreport6').click(function(e){
                $('#divreport6').toggle('1000');
            });
            $('#btndivreport7').click(function(e){
                $('#divreport7').toggle('1000');
            });
            $('#btndivreport8').click(function(e){
                $('#divreport8').toggle('1000');
            });
            $('#btndivreport9').click(function(e){
                $('#divreport9').toggle('1000');
            });
            $('#btndivreport10').click(function(e){
                $('#divreport10').toggle('1000');
            });
            $('#btndivreport11').click(function(e){
                $('#divreport11').toggle('1000');
            });
            $('#btndivreport12').click(function(e){
                $('#divreport12').toggle('1000');
            });
            $('#btndivreport13').click(function(e){
                $('#divreport13').toggle('1000');
            });
            $('#btndivreport14').click(function(e){
                $('#divreport14').toggle('1000');
            });
            $('#btndivreport15').click(function(e){
                $('#divreport15').toggle('1000');
            });

            if ($('#right-create-report').find('input').parent().hasClass('has-error') || $('#right-create-report').find('select').parent().hasClass('has-error')) {
                var j = 0;
                for (var i = 4; i <= 15; i++) {
                    if (
                        $('#divreport'+i).find('input').parent().hasClass('has-error')
                        || $('#divreport'+i).find('select').parent().hasClass('has-error')
                        || $('#divreport'+i).find('input').val() != ''
                        || $('#divreport'+i).find('select').val() != ''
                    ) {
                        j = i;
                    }
                }
                for (var i = 4; i <= j; i++) {
                    $('#divreport'+i).attr('style', '');
                }
            }
            $('.btndivreport').click(function (e) {
                var id = $(this).data('id');
                $('#divreport' + id).toggle('1000');
            });

            $('.btndivshipping').click(function (e) {
                var id = $(this).data('id');
                $('#divshipping' + id).toggle('1000');
            });

            $('.alert_success_company_japan').delay(3000).slideUp();

            /*Active current menu*/
            var current_url = window.location.href;
            $.each($("aside a[href='" + current_url + "']"), function () {
                if ($(this).attr('class') != 'po_a') {
                    $(this).parent().addClass('po_active');
                    $(this).parents('.panel-collapse').prev().click();
                    if($('.po_active').parents('.dropdown')) {
                        //$('.po_active').parents('.dropdown').css('background', 'red');
                        $('.po_active').parents('.dropdown').children().children('i').toggleClass("rotate down");
                    }
                }
            });
            //alert($('.po_active img').attr('src'));
            if($('.po_active img').length>0) {
                $('.po_active img').attr('src', $('.po_active img').attr('src').replace('_noactive', '_active'));
            }

            $("aside .dropdown").click(function(){
                $(this).children().children('i').toggleClass("rotate down");
            });

            for (var i = 1; i <= 4; i++) {
                $('#images' + i).click(function () {
                    var src = $(this).attr('src');
                    $('#imagesmain').fadeOut('fast').attr('src', src).stop(true, true).hide().fadeIn(1000);
                });
            }
        });
    </script>

    <?php include resource_path('views/layouts/googleanalytics.php'); ?>
    <?php include resource_path('views/layouts/juiser.php'); ?>
    <?php include resource_path('views/layouts/push-notification.php'); ?>
    <title>@yield('title')</title>

</head>
<body>
<header class="clearfix">
    <h1 id="logo" class="pull-left"><a href="#"><img src="/images/logo_new.png" width="220" height="34" alt=""></a></h1>
    <ul class="pull-right">
        <li class="header"><a href="{!! url('/profile') !!}">{{\Illuminate\Support\Facades\Auth::user()->supplier->company_name_jp}}</a></li>
        <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-user"></i><i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li><a href="{{url('/profile')}}">基本情報登録</a></li>
                <li><a href="{{url('/contact')}}">サポート窓口</a></li>
                <li><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">ログアウト</a>
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </li>
    </ul>
</header>

<div id="container">
    <aside>
        <!-- uncomment code for absolute positioning tweek see top comment in css -->
        <!-- <div class="absolute-wrapper"> </div> -->
        <!-- Menu -->
        <div class="side-menu">

            <nav class="navbar navbar-default" role="navigation">
                <!-- Main Menu -->
                <div class="side-menu-container">
                    <ul class="nav navbar-nav">
                        {{--<p id="name" style="    color: #fff;background: #555;padding: 10px;">--}}
                        {{--{{\Illuminate\Support\Facades\Auth::user()->supplier->company_name_jp}}--}}
                        {{--</p>--}}
                        @if( $free_user == 1 ) 
                            <li><a href="{{url('/free/home')}}"><img src="/images/icon_01_noactive.png"> ホーム</a></li>
                            <li><a href="{{url('/free/news')}}"><img src="/images/icon_02_noactive.png"> お知らせ</a></li>
                            <li><a href="{{url('/free/request_list')}}"><img src="/images/icon_03_noactive.png"> 調達依頼一覧</a></li>
                            <li><a href="{{url('/free/attack_list')}}"><img src="/images/icon_04_noactive.png"> アタックリスト</a></li>
                            <li><a href="{{url('/free/mail-list') }}"><img src="/images/icon_05_noactive.png"> 送信済みメール</a></li>
                        @else
                            <li><a href="{{url('/home')}}"><img src="/images/icon_01_noactive.png"> ホーム</a></li>
                            <li><a href="{{url('/news')}}"><img src="/images/icon_02_noactive.png"> お知らせ</a></li>
                            <li><a href="{{url('/request_list')}}"><img src="/images/icon_03_noactive.png"> 調達依頼一覧</a></li>
                            <li><a href="{{url('/attack_list')}}"><img src="/images/icon_04_noactive.png"> アタックリスト</a></li>
                            <li><a href="{{url('/mail-list') }}"><img src="/images/icon_05_noactive.png"> 送信済みメール</a></li>
                        @endif
                        

                        {{--<li><a href="{{url('profile')}}"> <i class="fa fa-globe fa-lg" aria-hidden="true"> 基本情報登録</i></a></li>--}}
                                <!-- Dropdown-->


                        <?php
                        //                            echo '<li class="panel panel-default dropdown" id="dropdown">
                        //                            <a data-toggle="collapse" href="#dropdown-lvl1">
                        //                                <img src="/images/icon_06_noactive.png">商品登録 <i class="fa fa-chevron-up" aria-hidden="true"></i>
                        //                            </a>
                        //
                        //                            <!-- Dropdown level 1 -->
                        //                            <div id="dropdown-lvl1" class="panel-collapse collapse">
                        //                                <div class="panel-body">
                        //                                    <ul class="nav navbar-nav">
                        //                                        <li><a href="{{ url('/product_list') }}">商品一覧</a></li>
                        //                                        <li><a href="{{ url('/create_product') }}">カタログ作成</a></li>
                        //                                    </ul>
                        //                                </div>
                        //                            </div>
                        //                        </li>';
                        ?>


                                <!-- Dropdown-->

                        <?php
                        //                            echo '<li class="panel panel-default dropdown" id="dropdown1">
                        //                            <a data-toggle="collapse" href="#dropdown-lvl2">
                        //                                <img src="/images/icon_07_noactive.png"> 価格表 <i class="fa fa-chevron-up" aria-hidden="true"></i>
                        //                            </a>
                        //
                        //                            <!-- Dropdown level 1 -->
                        //                            <div id="dropdown-lvl2" class="panel-collapse collapse">
                        //                                <div class="panel-body">
                        //                                    <ul class="nav navbar-nav">
                        //                                        <li><a href="{{ url('/price_list') }}">価格表一覧</a></li>
                        //                                        <li><a href="{{ url('/create_new_price_list_detail') }}">価格表作成</a></li>
                        //                                    </ul>
                        //                                </div>
                        //                            </div>
                        //                        </li>';
                        ?>


                                <!-- Dropdown-->
                        <li class="panel panel-default dropdown" id="dropdown2">
                            <a data-toggle="collapse" href="#dropdown-lvl3"><img src="/images/icon_08_noactive.png"> メールテンプレート <i class="fa fa-chevron-up" aria-hidden="true"></i>
                            </a>

                            <!-- Dropdown level 1 -->
                            <div id="dropdown-lvl3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        @if( $free_user == 1 ) 
                                            <li><a href="{{ url('/free/template_mail_list') }}">メールテンプレート一覧</a></li>
                                            <li><a href="{{ url('/free/template_mail_create') }}">メールテンプレート作成</a></li>
                                        @else
                                            <li><a href="{{ url('/template_mail_list') }}">メールテンプレート一覧</a></li>
                                            <li><a href="{{ url('/template_mail_create') }}">メールテンプレート作成</a></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <!-- Dropdown-->
                        @if( $free_user != 1 ) 
                            <li class="panel panel-default dropdown" id="dropdown3">
                                <a data-toggle="collapse" href="#dropdown-lvl4"><img src="/images/icon_09_noactive.png"> 成約報告 <i class="fa fa-chevron-up" aria-hidden="true"></i>
                                </a>

                                <!-- Dropdown level 1 -->
                                <div id="dropdown-lvl4" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="nav navbar-nav">
                                            <li><a href="{{ url('/report_list') }}">成約一覧</a></li>
                                            <li><a href="{{ url('/report_create') }}">成約報告作成</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        @endif
                        <!-- Dropdown-->
                        <li class="panel panel-default dropdown" id="dropdown4">
                            <a data-toggle="collapse" href="#dropdown-lvl5">
                                <img src="/images/icon_10_noactive.png"> 翻訳依頼 <i class="fa fa-chevron-up" aria-hidden="true"></i>
                            </a>
                            <!-- Dropdown level 1 -->
                            <div id="dropdown-lvl5" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        @if( $free_user == 1 ) 
                                            <li><a href="{{ url('/free/translate-list') }}">翻訳一覧</a></li>
                                            <li><a href="{{ url('/free/create-translate-request') }}">翻訳依頼</a></li>
                                        @else
                                            <li><a href="{{ url('/translate-list') }}">翻訳一覧</a></li>
                                            <li><a href="{{ url('/create-translate-request') }}">翻訳依頼</a></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <!-- Dropdown-->

                        <!-- an_tnh_sc_94_sc_155_start -->
                        @if ( $free_user == 1 )
                            <li><a style="padding-left:5px; text-align: center;" href="https://change-plan.jpsupplier.commerce-sourcing.com/" target="_blank">有料プランに切り替える</a></li>
                        @endif
                        <!-- an_tnh_sc_94_sc_155_start -->

                        <?php
                        //                            echo '<li class="panel panel-default dropdown" id="dropdown5">
                        //                            <a data-toggle="collapse" href="#dropdown-lvl6"><img src="/images/icon_11_noactive.png"> 配送依頼 <i class="fa fa-chevron-up" aria-hidden="true"></i>
                        //                            </a>
                        //                            <!-- Dropdown level 1 -->
                        //                            <div id="dropdown-lvl6" class="panel-collapse collapse">
                        //                                <div class="panel-body">
                        //                                    <ul class="nav navbar-nav">
                        //                                        <li><a href="{{ url('/send-product') }}">配送一覧</a></li>
                        //                                        <li><a href="{{ url('/create-send-product') }}">配送依頼</a></li>
                        //                                    </ul>
                        //                                </div>
                        //                            </div>
                        //                        </li>';
                        ?>


                        {{--<li><a href="#">利用マニュアル</a></li>--}}
                        {{--<li><a href="/contact">サポート窓口</a></li>--}}
                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav>
        </div>
    </aside>
    <article class="clearfix">
        <!-- Main Content -->
        <div class="container">
            @yield('sekai_content')
        </div>
    </article>
    <footer>
        <ul>
            <li>© 2017 sekai-connect</li>
        </ul>
    </footer>
</div>
<!-- end #container -->
</body>
</html>