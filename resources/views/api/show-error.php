<style type="text/css">
    #waku{
        text-align:center;
    }
    ul.has_error{
        border:1px solid #f00;
        padding:10px;
        color:#f00;
        list-style:none;
        font-size:13px;
        line-height:1.5;
        display:inline-block;
        text-align:center
    }
    ul.has_error li{
        text-align:left;
    }
    ul.has_error em{
        font-style:normal;
    }
    .section_not_avail {
        background: repeat scroll 0 0 #F9F9F9;
        border-radius: 10px 10px 10px 10px;
        box-shadow: 0 2px 3px 0 #BBBBBB;
        margin-top: 30px;
        padding: 200px 0 200px 0;
    }
    .img_completion, .img_not_avail {
        /* width: 80px; */
        text-align: center;
        margin: 0 auto;
    }
    body {
        font-family: "ヒラギノ角ゴ Pro W3", "Hiragino Kaku Gothic Pro", "メイリオ", Meiryo, Osaka, "ＭＳ Ｐゴシック", "MS PGothic", sans-serif;
    }
</style>

<div id="container">
    <div id="content" class="structre">
        <div class="section_not_avail clearfix">
            <div class="img_not_avail">
                <img src="/images/i01_ng.png"><br>
                <?php
                if(isset($errors) && count($errors) > 0)
                {
                ?>
                <h3>Error found in submitted data</h3>
                <?php } ?>
                <?php
                if(isset($errors_domain))
                {
                ?>
                <p class="msg_not_avail">Cannot post from this domain</p>
                <?php } ?>
            </div>
            <?php
            if(isset($errors) && count($errors) > 0)
            {
            ?>
            <div id="waku">
                <ul class="has_error">
                    <?php
                    if(isset($errors['176815']['required']))
                    {
                    ?>
                    <li><strong>Company name<span class="komejirushi">*</span>: </strong><em>Required</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['176815']['max']))
                    {
                        ?>
                        <li><strong>Company name<span class="komejirushi">*</span>: </strong><em> > 64 character</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['176816']))
                    {
                        ?>
                        <li><strong>Title <span class="komejirushi">*</span>: </strong><em>Required</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['176817']))
                    {
                        ?>
                        <li><strong>First name<span class="komejirushi">*</span>: </strong><em>Required</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['176818']))
                    {
                        ?>
                        <li><strong>Last name<span class="komejirushi">*</span>: </strong><em>Required</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['country_required']))
                    {
                        ?>
                        <li><strong>Country (日本語入力)<span class="komejirushi">*</span>: </strong><em>Required</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['country']))
                    {
                        ?>
                        <li><strong>Country (日本語入力)<span class="komejirushi">*</span>: </strong><em>Country isn't exists</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['176820']))
                    {
                        ?>
                        <li><strong>Address<span class="komejirushi">*</span>: </strong><em>Required</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['176821']['required']))
                    {
                        ?>
                        <li><strong>Phone number<span class="komejirushi">*</span>: </strong><em>Required</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['176821']['numeric']))
                    {
                        ?>
                        <li><strong>Phone number<span class="komejirushi">*</span>: </strong><em>Numeric</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['176823']['required']))
                    {
                        ?>
                        <li><strong>E-Mail<span class="komejirushi">*</span>: </strong><em>Required</em></li>
                    <?php }else{ ?>

                        <?php
                        if(isset($errors['176823']['unique']))
                        {
                            ?>
                            <li><strong>E-Mail<span class="komejirushi">*</span>: </strong><em>Exist</em></li>
                        <?php }else {
                            ?>

                            <?php
                            if(isset($errors['176823']['email']))
                            {
                                ?>
                                <li><strong>E-Mail<span class="komejirushi">*</span>: </strong><em>Not match email format</em></li>
                            <?php } ?>

                            <?php
                        }
                        ?>

                    <?php

                    } ?>

                    <?php
                    if(isset($errors['176824']))
                    {
                        ?>
                        <li><strong>SNS tool<span class="komejirushi">*</span>: </strong><em>Required</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['176826']['required']))
                    {
                        ?>
                        <li><strong>Website URL<span class="komejirushi">*</span>: </strong><em>Required</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['176826']['url']))
                    {
                        ?>
                        <li><strong>Website URL<span class="komejirushi">*</span>: </strong><em>Not match url format</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['176827']))
                    {
                        ?>
                        <li><strong>Business category<span class="komejirushi">*</span>: </strong><em>Required</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['176828']['required']))
                    {
                        ?>
                        <li><strong>Handled goods<span class="komejirushi">*</span>: </strong><em>Required</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['176828']['options']))
                    {
                        ?>
                        <li><strong>Handled goods<span class="komejirushi">*</span>: </strong><em>Only allowed to choose 3 options</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['DB']))
                    {
                        ?>
                        <li><strong>Handled goods<span class="komejirushi">*</span>: </strong><em><?php echo $errors['DB']; ?></em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['something']))
                    {
                        ?>
                        <li><strong>Handled goods<span class="komejirushi">*</span>: </strong><em>Have some errors. Please try again later.</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['179568']['not_exist']))
                    {
                        ?>
                        <li><strong>E-Mail<span class="komejirushi">*</span>: </strong><em>Not exist</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['179568']['not_active']))
                    {
                        ?>
                        <li><strong>E-Mail<span class="komejirushi">*</span>: </strong><em>Not active</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['179540']))
                    {
                        ?>
                        <li><strong>Please choose an option<span class="komejirushi">*</span>: </strong><em>Required</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['179547']))
                    {
                        ?>
                        <li><strong>Category of desired product<span class="komejirushi">*</span>: </strong><em>Required</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['179542']['year']))
                    {
                        ?>
                        <li><strong>Year<span class="komejirushi">*</span>: </strong><em>Required</em></li>
                    <?php } ?>
                    <?php
                    if(isset($errors['179542']['month']))
                    {
                        ?>
                        <li><strong>Month<span class="komejirushi">*</span>: </strong><em>Required</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['179542']['day']))
                    {
                        ?>
                        <li><strong>Day<span class="komejirushi">*</span>: </strong><em>Required</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['179543']))
                    {
                        ?>
                        <li><strong>Preferred supply period<span class="komejirushi">*</span>: </strong><em>Required</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['179544']['numeric']))
                    {
                        ?>
                        <li><strong>Ideal buying price<span class="komejirushi">*</span>: </strong><em>Numeric</em></li>
                    <?php } ?>
                    <?php
                    if(isset($errors['179544']['large1']))
                    {
                        ?>
                        <li><strong>Ideal buying price<span class="komejirushi">*</span>: </strong><em>Min is 1</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['179545']['numeric']))
                    {
                        ?>
                        <li><strong>Planned retail price<span class="komejirushi">*</span>: </strong><em>Numeric</em></li>
                    <?php } ?>
                    <?php
                    if(isset($errors['179545']['large1']))
                    {
                        ?>
                        <li><strong>Planned retail price<span class="komejirushi">*</span>: </strong><em>Min is 1</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['179546']['numeric']))
                    {
                        ?>
                        <li><strong>Quantity<span class="komejirushi">*</span>: </strong><em>Numeric</em></li>
                    <?php } ?>
                    <?php
                    if(isset($errors['179546']['large1']))
                    {
                        ?>
                        <li><strong>Quantity<span class="komejirushi">*</span>: </strong><em>Min is 1</em></li>
                    <?php } ?>

                    <?php
                    if(isset($errors['date']['format']))
                    {
                        ?>
                        <li><strong>Deadline<span class="komejirushi">*</span>: </strong><em>Deadline isn't format</em></li>
                    <?php } ?>
                    <?php
                    if(isset($errors['date']['greater']))
                    {
                        ?>
                        <li><strong>Deadline<span class="komejirushi">*</span>: </strong><em>Deadline must greater today</em></li>
                    <?php } ?>

                </ul>
            </div>
            <?php } ?>
        </div>
    </div>
</div>