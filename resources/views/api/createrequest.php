
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="css/welcart_25hoursbuyers/style.css">
    <title>Detailed information | 25HOURS BUYERS.com</title>
    <?php include resource_path('views/layouts/juiser.php'); ?>
    <?php include resource_path('views/layouts/push-notification.php'); ?>
</head>
<body>

<div id="mainContent">
    <div class="wrap">

        <div id="content" style="width: auto;float: none">
            <section id="page">

                <h2 id="pageTtl">
                    Product enquiry form
                </h2>

                <h3 class="pageLabel">
                    Please provide us with more information so that we can respond faster and match you with the relevant supplier.
                </h3>

                <div id="form-layout">

                    <form action="/create-request-by-form" method="post">
                        <table>
                            <tr>
                                <th><label for="questions_179568">Email address</label></th>
                                <td><input style="ime-mode: disabled;" type="text" name="questions[179568]" class="inputTextSingle" id="questions_179568"  value="" /></td>
                            </tr>
                            <tr>
                                <th><label for="questions_179540">Please choose an option <span class="komejirushi">(required)</span></label></th>
                                <td><select name="questions[179540]" id="questions_179540">
                                        <option value="" selected="selected">-----</option>
                                        <option value="1066905">I need information</option>
                                        <option value="1066906">I want to purchase</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <th><label for="questions_179547">Category of desired product <span class="komejirushi">(required)</span></label></th>
                                <td><select name="questions[179547]" id="questions_179547">
                                        <option value="" selected="selected">-----</option>
                                        <option value="1066909">Food</option>
                                        <option value="1066910">Health &amp; Beauty</option>
                                        <option value="1066911">Beverage</option>
                                        <option value="1066912">Electronics</option>
                                        <option value="1066913">Fashion</option>
                                        <option value="1066914">Japanese Product</option>
                                        <option value="1066915">Gift & Novelty</option>
                                        <option value="1066916">Second Hand</option>
                                        <option value="1066917">Stationery</option>
                                        <option value="1066918">Daily / Household</option>
                                        <option value="1066919">Industrial Goods</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <th><label for="questions_179541">Product name/description</label></th>
                                <td><textarea rows="8" cols="32" name="questions[179541]" id="questions_179541"></textarea></td>
                            </tr>
                            <tr>
                                <th><label for="questions_179542">Deadline <span class="komejirushi">(required)</span></label></th>
                                <td><select name="questions[179542][year]" id="questions_179542_year" class="auto">
                                        <option value="" selected="selected"></option>
                                        <option value="2020">2020</option>
                                        <option value="2019">2019</option>
                                        <option value="2018">2018</option>
                                        <option value="2017">2017</option>
                                        <option value="2016">2016</option>
                                        <option value="2015">2015</option>
                                    </select> / <select name="questions[179542][month]" id="questions_179542_month" class="auto">
                                        <option value="" selected="selected"></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select> / <select name="questions[179542][day]" id="questions_179542_day" class="auto">
                                        <option value="" selected="selected"></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <th><label for="questions_179543">Preferred supply period <span class="komejirushi">(required)</span></label></th>
                                <td><ul class="radio_list"><li><input name="questions[179543]" type="radio" value="1066907" id="questions_179543_1066907" checked="checked" />&nbsp;<label for="questions_179543_1066907">Short-term</label></li>
                                        <li><input name="questions[179543]" type="radio" value="1066908" id="questions_179543_1066908" />&nbsp;<label for="questions_179543_1066908">Long-term</label></li></ul></td>
                            </tr>
                            <tr>
                                <th><label for="questions_179544">Ideal buying price</label></th>
                                <td><input type="text" name="questions[179544]" class="inputTextSingle" id="questions_179544" /></td>
                            </tr>
                            <tr>
                                <th><label for="questions_179545">Planned retail price</label></th>
                                <td><input type="text" name="questions[179545]" class="inputTextSingle" id="questions_179545" /></td>
                            </tr>
                            <tr>
                                <th><label for="questions_179546">Quantity</label></th>
                                <td><input style="ime-mode: disabled;" type="text" name="questions[179546]" class="inputTextSingle" id="questions_179546" /></td>
                            </tr>
                            <tr>
                                <th><label for="questions_179567">Customer category</label></th>
                                <td><ul class="checkbox_list"><li><input name="questions[179567][]" type="checkbox" value="1066967" id="questions_179567_1066967" />&nbsp;<label for="questions_179567_1066967">Trader</label></li>
                                        <li><input name="questions[179567][]" type="checkbox" value="1066968" id="questions_179567_1066968" />&nbsp;<label for="questions_179567_1066968">Wholesaler</label></li>
                                        <li><input name="questions[179567][]" type="checkbox" value="1066969" id="questions_179567_1066969" />&nbsp;<label for="questions_179567_1066969">Retailer</label></li>
                                        <li><input name="questions[179567][]" type="checkbox" value="1066970" id="questions_179567_1066970" />&nbsp;<label for="questions_179567_1066970">EC site</label></li>
                                        <li><input name="questions[179567][]" type="checkbox" value="1066971" id="questions_179567_1066971" />&nbsp;<label for="questions_179567_1066971">Company</label></li>
                                        <li><input name="questions[179567][]" type="checkbox" value="1066972" id="questions_179567_1066972" />&nbsp;<label for="questions_179567_1066972">Consumer</label></li>
                                        <li><input name="questions[179567][]" type="checkbox" value="1066973" id="questions_179567_1066973" />&nbsp;<label for="questions_179567_1066973">Manufacturer</label></li></ul></td>
                            </tr>
                        </table>
                        <input type="submit" value="Send" class="submitBtn">
                    </form>
                </div>

            </section>
        </div>
    </div>
</div>
</body>
</html>