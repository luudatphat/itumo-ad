@extends('layouts.master')
@section('title','調達依頼一覧')
@section('sekai_content')
    <?php if(!empty(old('genre_id'))) dd(old('genre_id')) ?>
    <section>
        <header><h2>調達依頼一覧</h2></header>
        <article>
            <form class="form-sort" action="{{url('request_list')}}" method="post">
                @csrf
                <div class="content-2col">
                    <div class="a-col">
                        <div class="title-control">カテゴリ</div>
                        <div class="select-multi">
                            @if(count($mtb_genres) > 0)
                                @foreach($mtb_genres as $genre)
                                    <input type="checkbox" name="genre_id[]" id="genre_id[]{{ $genre->id }}" value="{{ $genre->id }}" {{(isset($genre_id) && in_array($genre->id,$genre_id))?'checked':''}}><label for="genre_id[]{{ $genre->id }}">{{ $genre->content }}</label>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="a-col">
                        <div class="title-control">国</div>
                        <div name="" class="select-multi">
                            @foreach($countries as $country)
                                <input type="checkbox" name="country[]" value="{{ $country->id }}" id="country[]{{ $country->id }}" {{(isset($country_id) && in_array($country->id,$country_id))?'checked':''}}><label for="country[]{{ $country->id }}">{{ $country->country_name }}</label>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- end .content-2col -->
                <div class="content-group-control">

                    <?php
//                        echo '<span class="fix-display">
//                        <span class="title-control">評価</span>
//
//                        <select name="rating" id="rating">
//                            <option value="">--- Select ---</option>
//                            <option {{isset($rating) && $rating == 3 ? 'selected' : ''}} value="3">★★★</option>
//                            <option {{isset($rating) && $rating == 2 ? 'selected' : ''}} value="2">★★</option>
//                            <option {{isset($rating) && $rating == 1 ? 'selected' : ''}} value="1">★</option>
//                        </select>
//                    </span>';
                        ?>
                    <span class="fix-display">
                        <span class="title-control">依頼日</span>
                        <span class="calender-picker"><input type="text" name="from_date" id="from_date" value="{{isset($from_date) ? $from_date : ''}}"></span> - <span class="calender-picker"><input type="text" name="to_date" value="{{isset($to_date) ? $to_date: ''}}" id="to_date"></span>
                    </span>
                    <span class="fix-display">
                        <span class="title-control">キーワード</span>
                        <input class="input-last" type="text" name="search" id="search" placeholder="キーワードを入力して下さい" value="{{isset($search) ? $search : ''}}">
                    </span>
                </div>
                <div class="text-center bottom-form"><button type="submit">検索</button></div>
                <!-- end .content-group-control -->
            </form>
            <table class="content-list-table-02 result-table" style="min-width: 1200px;">
                <thead>
                <tr>
                    <th class="col-sort col-request" id="requests.id" width="11.9%">レコード番号</th>
                    <th class="col-sort col-date" id="date" width="11.9%">依頼日</th>
                    <!-- <th class="fix-width col-sort" id="expire_date">提案納期</th> -->
                    <th class="col-sort col-country" id="country" width="6.8%">国</th>
                    <th class="col-sort col-mtb-genre" id="mtb_genre_id" width="11%">欲しいカテゴリ</th>
                    <th class="col-sort col-product-description-jp" id="product_description_jp" width="18.5%">調達依頼内容</th>
                    <th class="col-sort col-company-strength" id="company_strength" width="26.4%">企業としての強み</th>
                    <!-- <th class="fix-width col-sort" id="sale_country">販路国</th> -->
                    <th class="col-button-attack">アタックリスト</th>
                </tr>
                </thead>
                <tbody>

                @forelse ($request_list as $request)
                    <tr>
                        <td>{{$request->id}}</td>
                        <td>{{date_format(date_create($request->date),'Y年m月d日')}}</td>
                        <!-- <td>{{date_format(date_create($request->expire_date),'Y年m月d日')}}</td> -->
                        <td>{{$request->buyer->countries->country_name}}</td>
                        <td>{{$request->mtb_genre->content}}</td>
                        <td>{!!nl2br(e($request->product_description_jp))!!}</td>
                        <td>{!!nl2br(e($request->buyer->company_strength))!!}</td>
                        <!-- <td>{{$request->buyer->sale_country}}</td> -->
                        <td>
                            <input type="hidden" id="request_id" value="{{$request->id}}">
                            @if ($request->supplier_id == NULL)
                                <button class="btn-add" name="btnAttack">追加する</button>
                            @else
                                <button class="btn-added" disabled>追加済み</button>
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7">データなし。</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <nav class="text-right pagination-div">
                {{$request_list->links('pagination::bootstrap-4')}}
            </nav>
        </article>
    </section>
    <script>
        $(document).ready(function () {
            //$('input[name=from_date], input[name=to_date]').datepicker();
        })
        $('.col-sort').click(function(){
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });
            var page = {{$current_page}};
            var search = $('#search').val();
            var rating = $('#rating').val();
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            var genre_id = $("input[name='genre_id[]']").map(function(){
                if($(this).is(":checked")){
                    return $(this).val();
                }
            }).get();
            var country = $("input[name='country[]']").map(function(){
                if($(this).is(":checked")){
                    return $(this).val();
                }
            }).get();
            var column = $(this).attr('id');

            if($(this).hasClass('select-sort')) {
                $(this).removeClass('down');
                $(this).children('.fa-caret-down').css({'display': 'none'});
                $(this).children('.fa-caret-up').css({'display': 'inline'});
                //Sort ASC
                $('#column_dl').val(column);
                $('#sort_dl').val("asc");
                $.ajax({
                    url: '/list_request_sort?page=' + page,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    data: {
                        search: search,
                        rating: rating,
                        from_date: from_date,
                        to_date: to_date,
                        genre_id: genre_id,
                        country: country,
                        column: column,
                        sort: 'asc',
                        current_page: page,
                        _token: "{{csrf_token()}}"
                    },
                    success: function( data, textStatus, jQxhr ){
                        var result = JSON.parse(data);
//                        $('.result-table tbody').html(data);
                        $('.result-table tbody').html(result['content']);
                        $('.pagination-div').html(result['pagination']);
                        $("button[name=btnAttack]").off().click(function () {
                            if (confirm("{{trans('messages.attack_confirm')}}")) {
                                var request_id = $(this).siblings("#request_id").val();
                                var token = "{{csrf_token()}}";
                                var thisElement = $(this);
                                $.ajax({
                                    url:"/new_attack",
                                    type:"POST",
                                    dataType:"json",
                                    data: {
                                        request_id: request_id,
                                        _token: token,
                                    },
                                    success: function (result) {
                                        if(result) {
                                            thisElement.after('<button class="btn-added" disabled>追加済み</button>');
                                            thisElement.remove();
                                            alert('{{trans("messages.attack_success")}}')
                                        } else {
                                            alert("{{trans('messages.error')}}");
                                        }
                                    },
                                    error: function () {
                                        alert("{{trans('messages.error')}}");
                                    }
                                });
                            }
                        });
                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        console.log( errorThrown );
                    }
                });

            }else {
                $(this).addClass('down');
                $(this).children('.fa-caret-up').css({'display': 'none'});
                $(this).children('.fa-caret-down').css({'display': 'inline'});
                //Sort DESC
                $('#column_dl').val(column);
                $('#sort_dl').val("desc");
                $.ajax({
                    url: '/list_request_sort?page=' + page,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    data: {
                        search: search,
                        rating: rating,
                        from_date: from_date,
                        to_date: to_date,
                        genre_id: genre_id,
                        country: country,
                        column: column,
                        sort: 'desc',
                        current_page: page,
                        _token: "{{csrf_token()}}"
                    },
                    success: function( data, textStatus, jQxhr ){
                        var result = JSON.parse(data);
                        $('.result-table tbody').html(result['content']);
                        $('.pagination-div').html(result['pagination']);$("button[name=btnAttack]").off().click(function () {
                            if (confirm("{{trans('messages.attack_confirm')}}")) {
                                var request_id = $(this).siblings("#request_id").val();
                                var token = "{{csrf_token()}}";
                                var thisElement = $(this);
                                $.ajax({
                                    url:"/new_attack",
                                    type:"POST",
                                    dataType:"json",
                                    data: {
                                        request_id: request_id,
                                        _token: token,
                                    },
                                    success: function (result) {
                                        if(result) {
                                            thisElement.after('<button class="btn-added" disabled>追加済み</button>');
                                            thisElement.remove();
                                            alert('{{trans("messages.attack_success")}}')
                                        } else {
                                            alert("{{trans('messages.error')}}");
                                        }
                                    },
                                    error: function () {
                                        alert("{{trans('messages.error')}}");
                                    }
                                });
                            }
                        });
                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        console.log( errorThrown );
                    }
                });
            }
        });
        $("button[name=btnAttack]").click(function () {
            if (confirm("{{trans('messages.attack_confirm')}}")) {
                var request_id = $(this).siblings("#request_id").val();
                var token = "{{csrf_token()}}";
                var thisElement = $(this);
                $.ajax({
                    url:"/new_attack",
                    type:"POST",
                    dataType:"json",
                    data: {
                        request_id: request_id,
                        _token: token,
                    },
                    success: function (result) {
                        if(result) {
                            thisElement.after('<button class="btn-added" disabled>追加済み</button>');
                            thisElement.remove();
                            alert('{{trans("messages.attack_success")}}')
                        } else {
                            alert("{{trans('messages.error')}}");
                        }
                    },
                    error: function () {
                        alert("{{trans('messages.error')}}");
                    }
                });
            }
        });
    </script>

    <script type="application/javascript">
        $(document).ready(function (e) {
            'use strict';
//            $('.calender-picker input').datepicker({
//                showOn: "button",
//                buttonImage: "images/icon_datepicker.png",
//                buttonImageOnly: true,
//                buttonText: "Select date"
//            });
            var dateFormat = "mm/dd/yy",
                    from = $( "#from_date" )
                            .datepicker({
                                defaultDate: "+1w",
                                changeMonth: true,
                                showOn: "button",
                                buttonImage: "images/icon_datepicker.png",
                                buttonImageOnly: true,
                                buttonText: "Select date"
                            })
                            .on( "change", function() {
                                to.datepicker( "option", "minDate", getDate( this ) );
                            }),
                    to = $( "#to_date" ).datepicker({
                                defaultDate: "+1w",
                                changeMonth: true,
                                showOn: "button",
                                buttonImage: "images/icon_datepicker.png",
                                buttonImageOnly: true,
                                buttonText: "Select date"
                            })
                            .on( "change", function() {
                                from.datepicker( "option", "maxDate", getDate( this ) );
                            });

            function getDate( element ) {
                var date;
                try {
                    date = $.datepicker.parseDate( dateFormat, element.value );
                } catch( error ) {
                    date = null;
                }

                return date;
            }

            $('.col-sort').click(function (e) {
                e.preventDefault();
                if($(this).hasClass('select-sort')) {
                    $(this).removeClass('select-sort');
                    return false;
                }
                $('.col-sort').removeClass('select-sort');
                $(this).addClass('select-sort');
            })
        });
    </script>
@endsection