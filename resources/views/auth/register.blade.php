@extends('layouts.nosidebar')
@section('title', '法人登録')
@section('sekai_content')
<h2 class="h2-01">法人登録</h2>
<form class="form-content-01" role="form" method="POST" action="{{ url('/register') }}">
    @csrf
    <table class="content-control-01">
        <tr>
            <td><label for="email">貴社名 (日本語)</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="text" name="company_name_jp" id="name" value="{{ old('company_name_jp') }}"  autofocus>
                @if ($errors->has('company_name_jp'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('company_name_jp') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="zip_code">郵便番号</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="text" name="zip_code" id="zip_code" value="{{ old('zip_code') }}"  autofocus>
                @if ($errors->has('zip_code'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('zip_code') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="province">都道府県</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="text" name="province" id="province" value="{{ old('province') }}"  autofocus>
                @if ($errors->has('province'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('province') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="address">市町村区以下</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="text" name="address" id="address" value="{{ old('address') }}"  autofocus>
                @if ($errors->has('address'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="representative_mobile_number">代表電話番号</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="text" name="representative_mobile_number" id="representative_mobile_number" value="{{ old('representative_mobile_number') }}"  autofocus>
                @if ($errors->has('representative_mobile_number'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('representative_mobile_number') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="representative_mobile_number">代表メールアドレス / ID</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="text" name="email" id="email" value="{{ old('email') }}"  autofocus>
                @if ($errors->has('email'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="company_website">ホームページ</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="text" name="company_website" id="company_website" value="{{ old('company_website') }}"  autofocus>
                @if ($errors->has('company_website'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('company_website') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="representative_name_kanji">代表者名（漢字）</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="text" name="representative_name_kanji" id="representative_name_kanji" value="{{ old('representative_name_kanji') }}"  autofocus>
                @if ($errors->has('representative_name_kanji'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('representative_name_kanji') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="representative_name_kana">代表者名（カナ）</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="text" name="representative_name_kana" id="representative_name_kana" value="{{ old('representative_name_kana') }}"  autofocus>
                @if ($errors->has('representative_name_kana'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('representative_name_kana') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="contact_name_kanji">ご担当者名</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="text" name="contact_name_kanji" id="contact_name_kanji" value="{{ old('contact_name_kanji') }}"  autofocus>
                @if ($errors->has('contact_name_kanji'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('contact_name_kanji') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="contact_name_kana">ご担当者名（カナ）</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="text" name="contact_name_kana" id="contact_name_kana" value="{{ old('contact_name_kana') }}"  autofocus>
                @if ($errors->has('contact_name_kana'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('contact_name_kana') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="contact_email_address">ご担当者メールアドレス</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="text" name="contact_email_address" id="contact_email_address" value="{{ old('contact_email_address') }}"  autofocus>
                @if ($errors->has('contact_email_address'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('contact_email_address') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="contact_mobile_number">ご担当者 携帯電話番号</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="text" name="contact_mobile_number" id="contact_mobile_number" value="{{ old('contact_mobile_number') }}"  autofocus>
                @if ($errors->has('contact_mobile_number'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('contact_mobile_number') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="password">パスワード</label><span class="text-danger mg-l-22">6文字以上にしてください</span></td>
            <td>
                <input type="password" name="password" id="password"  autofocus>
                @if ($errors->has('password'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="password_confirmation">再パスワード</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="password" name="password_confirmation" id="password_confirmation"  autofocus>
                @if ($errors->has('password_confirmation'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </td>
        </tr>

    </table>

    <!-- an_tnh_sc_130_start -->
    <center><input class="btn btn-primary button_create_list_user" value="登録" data-toggle="modal" style="width:100px;" data-target="#myModal2" type="button" onclick="get_product_list()"></center>
    <div class="modal fade" id="myModal2" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">こちらの内容で登録してよいですか？</h4>
                </div>
                
                <div class="text-center" style="margin-bottom: 10px;margin-top:10px;">
                    <button type="submit" class="btn btn-primary" style="width:15%; min-height:40px;height:40px;">OK</button>
                   
                    <input type="button" value="キャンセル" style="width:15%;padding:0px;" class="btn btn-warning"
                           data-dismiss="modal">
                </div>
            </div>
        </div>
    </div>
    <!-- an_tnh_sc_130_start -->
    
</form>
@endsection
