@extends('layouts.nosidebar')
@section('title', 'パスワードリセット要求')
<!-- Main Content -->
@section('sekai_content')
    @if(Session::has('flash_messages'))
        <div class="alert alert-{!! Session::get('flash_level') !!} alert_success_company_japan_reset_password">
            {!! Session::get('flash_messages') !!}
        </div>
    @endif
    <h2 class="h2-01">パスワードリセット要求</h2>
    <form class="form-content-01" role="form" method="POST" action="{{ url('/password/email') }}">
        @csrf
        <table class="content-control-01">
            <tr>
                <td><label for="email">登録したメールアドレス</label><span class="text-danger mg-l-22">必須</span></td>
                <td>
                    <input type="email" name="email" id="email" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </td>
            </tr>
        </table>
        <div class="text-center"><button type="submit" class="w-250 min-height-45 fs-15">パスワードをリセットする</button></div>
    </form>
@endsection
