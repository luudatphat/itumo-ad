@extends('layouts.app')
@section('title','ログイン')
@section('content')

    <div id="container-login">
        <video autoplay loop id="index-video">
            <source src="video/index.webm" type="video/webm">
            <source src="video/index.mp4" type="video/mp4">
        </video>
        <div class="content-form">
            <p class="logo-login-page"><img src="/images/logo_login_page.png" width="322" height="228" alt=""></p>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">

                <!-- an_tnh_sc_130_start -->
                @if(Session::has('register_success'))
                    <p class="p_reg_success">{!! session('register_success') !!}</p>
                    <p style="display:none;">{{ Session::pull('register_success') }}</p>
                @endif
                <!-- an_tnh_sc_130_start -->

                @csrf
                <p class="@if($errors->has('email')) {{ ' has-error' }} @elseif(Session::has('warning')) {{ ' has-error' }} @endif ">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="メールアドレス" required autofocus>
                </p>
                @if ($errors->has('email'))
                    <p class="error-message">
                        <strong>{{ $errors->first('email') }}</strong>
                    </p>
                @endif
                @if(Session::has('warning'))
                    <p class="error-message">
                        <strong>{{session('warning')}}</strong>
                    </p>
                @endif
                <p class="{{ $errors->has('password') ? ' has-error' : '' }}"><input type="password" placeholder="パスワード" id="password" class="form-control" name="password" required></p>
                @if ($errors->has('password'))
                    <p class="error-message">
                        <strong>{{ $errors->first('password') }}</strong>
                    </p>
                @endif
                <p><input type="submit" value="ログイン"></p>
            </form>
            <p><a href="{{url('/register')}}">登録をしていない方</a></p>
            <p class="fix-margin-01"><a href="{{url('/password/reset')}}">パスワードを忘れた方</a></p>
            <p class="mg-0"><small>© 2017 sekai-connect</small></p>
        </div>
    </div>
    <!-- End #container -->

@endsection
