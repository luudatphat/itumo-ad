@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <h1 class="h1 bg-primary text-center h1_list_user">ユーザ作成</h1>
        <form action="">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="email">社名 <span class="text-danger">必須</span></label>
                                <input type="email" class="form-control" id="email" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">担当者の氏 <span class="text-danger">必須</span></label>
                                <input type="text" class="form-control" id="email" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">担当者の名 <span class="text-danger">必須</span></label>
                                <input type="text" class="form-control" id="email" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">メールアドレス <span class="text-danger">必須</span></label>
                                <input type="text" class="form-control" id="email" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">固定電話  <span class="text-danger">必須</span></label>
                                <input type="text" class="form-control" id="email" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="email">ホームページ <span class="text-primary">任意</span></label>
                                <input type="email" class="form-control" id="email" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="content">お取り扱い商品/ご相談内容 <span class="text-primary">任意</span>500文字以内で入力してくださ</label>
                                <textarea id="content" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="checkbox" name="checkbox" id="c1">
                                <label for="c1">プライバシーポリシーに同意する <span class="text-danger">必須</span></label>
                            </div>
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input class="btn btn-primary" type="submit" name="submit" id="c1" value="Register">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection