@extends('layouts.master')
@section('sekai_content')
    <div class="container-fluid">

        <div class="">
            <h1 class="h1 bg-primary text-center h1_list_user">ユーザ一覧</h1>
            <div class="row">
                <div class="col-md-6 text-left">
                    <form class="form-group">
                        <div class="row">
                            <div class="col-md-5 h1_list_user">
                                <input class="form-control" type="text" placeholder="検索..." name="search">
                            </div>
                            <div class="col-md-1 h1_list_user">
                                <input type="submit" class="btn btn-primary" value="検索">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6 text-right">
                    {{--<button class="btn btn-primary button_create_list_user  ">新規作成</button>--}}
                    <a class="btn btn-primary button_create_list_user" href="{!! url('/create_user') !!}">新規作成</a>
                </div>
            </div>
            <table class="table table-hover table-bordered table-striped ">
                <thead>
                <tr class="bg-primary text-center">
                    <th class="text-center">No.</th>
                    <th class="text-center">苗字</th>
                    <th class="text-center">名前</th>
                    <th class="text-center">ロール</th>
                    <th class="text-center">メールアドレス</th>
                    <th class="text-center">アクション</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>Duong</td>
                    <td>Tran</td>
                    <td>Admin</td>
                    <td>duongtran@gmail.com</td>
                    <td class="text-center">
                        <a class="btn btn-primary button_create_list_user" href="{!! url('/detail_user') !!}">Detail</a>
                        <button onclick="return confirm('Are you sure?');" class="btn btn-danger">Delete</button>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>佐藤</td>
                    <td>みちよし</td>
                    <td>Normal</td>
                    <td>Sato-michiyoshi@yahoo.co.jp</td>
                    <td class="text-center">
                        <a class="btn btn-primary button_create_list_user" href="{!! url('/detail_user') !!}">Detail</a>
                        <button onclick="return confirm('Are you sure?');" class="btn btn-danger">Delete</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection