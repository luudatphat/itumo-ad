@extends('layouts.master')
@section('title','サプライヤー企業 詳細情報')
@section('sekai_content')
<section>
    <header><h2>サプライヤー企業 詳細情報</h2></header>
    <article>
        <form action="" method="post" name="frmEditCompanyJapan">
            @csrf
            <div class="row">
                @if(session('message'))
                <div class="alert alert-success alert_success_company_japan">{{session('message')}}</div>
                @endif
                <div class="row ">
                    <div class="col-md-6 form-group{{ $errors->has('txtCompanyNameJapan') ? ' has-error' : '' }}">
                        <label for="id">貴社名 (日本語) </label>
                        <input disabled type="text" class="form-control" name="txtCompanyNameJapan" value="{{ old('txtCompanyNameJapan',isset($suppliers) ? $suppliers->company_name_jp : '') }}">
                        @if ($errors->has('txtCompanyNameJapan'))
                        <div class="col-md-6">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('txtCompanyNameJapan') }}</strong>
                                    </span>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-6 form-group{{ $errors->has('txtCompanyNameEnglish') ? ' has-error' : '' }}">
                        <label for="id">貴社名 (英語) </label>
                        <input disabled type="text" class="form-control" name="txtCompanyNameEnglish" value="{{ old('txtCompanyNameEnglish',isset($suppliers) ? $suppliers->company_name_en : '') }}">
                        @if ($errors->has('txtCompanyNameEnglish'))
                        <div class="col-md-6">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('txtCompanyNameEnglish') }}</strong>
                                    </span>
                        </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2 form-group{{ $errors->has('txtZipCode') ? ' has-error' : '' }}">
                        <label for="id">郵便番号 </label>
                        <input disabled type="text" class="form-control" name="txtZipCode" value="{{ old('txtZipCode',isset($suppliers) ? $suppliers->zip_code : '') }}">
                        @if ($errors->has('txtZipCode'))
                        <div class="col-md-12">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('txtZipCode') }}</strong>
                                    </span>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-2 form-group{{ $errors->has('txtProvince') ? ' has-error' : '' }}">
                        <label for="id">都道府県 </label>
                        <input disabled type="text" class="form-control" name="txtProvince" value="{{ old('txtProvince',isset($suppliers) ? $suppliers->province : '') }}">
                        @if ($errors->has('txtProvince'))
                        <div class="col-md-12">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('txtProvince') }}</strong>
                                    </span>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-8 form-group{{ $errors->has('txtAddress') ? ' has-error' : '' }}">
                        <label for="id">市町村区以下 </label>
                        <input disabled type="text" class="form-control" name="txtAddress" value="{{ old('txtAddress',isset($suppliers) ? $suppliers->address : '') }}">
                        @if ($errors->has('txtAddress'))
                        <div class="col-md-12">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('txtAddress') }}</strong>
                                    </span>
                        </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 form-group{{ $errors->has('txtCompanyPhoneNumber') ? ' has-error' : '' }}">
                        <label for="id">代表電話番号 </label>
                        <input disabled type="text" class="form-control" name="txtCompanyPhoneNumber" value="{{ old('txtCompanyPhoneNumber',isset($suppliers) ? $suppliers->company_phone_number : '') }}">
                        @if ($errors->has('txtCompanyPhoneNumber'))
                        <div class="col-md-12">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('txtCompanyPhoneNumber') }}</strong>
                                    </span>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-3 form-group{{ $errors->has('txtCompanyFaxNumber') ? ' has-error' : '' }}">
                        <label for="id">代表FAX番号 </label>
                        <input disabled type="text" class="form-control" name="txtCompanyFaxNumber" value="{{ old('txtCompanyFaxNumber',isset($suppliers) ? $suppliers->company_fax_number : '') }}">
                        @if ($errors->has('txtCompanyFaxNumber'))
                        <div class="col-md-12">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('txtCompanyFaxNumber') }}</strong>
                                    </span>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-6 form-group{{ $errors->has('txtCompanyEmailAddress') ? ' has-error' : '' }}">
                        <label for="id">代表メールアドレス (ない場合はinfo@couxu.jpを入力)</label>
                        <input type="email" class="form-control" name="txtCompanyEmailAddress" value="{{ old('txtCompanyEmailAddress',isset($suppliers) ? $suppliers->company_email_address : '') }}">
                        @if ($errors->has('txtCompanyEmailAddress'))
                        <div class="col-md-12">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('txtCompanyEmailAddress') }}</strong>
                                    </span>
                        </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 form-group{{ $errors->has('txtCompanyWebsite') ? ' has-error' : '' }}">
                        <label for="id">コーポレートウェブサイト </label>
                        <input type="text" class="form-control" name="txtCompanyWebsite" value="{{ old('txtCompanyWebsite',isset($suppliers) ? $suppliers->company_website : '') }}">
                        @if ($errors->has('txtCompanyWebsite'))
                        <div class="col-md-12">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('txtCompanyWebsite') }}</strong>
                                    </span>
                        </div>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="row">
                    <div class="col-md-3 form-group{{ $errors->has('txtRepresentativeNameKanji') ? ' has-error' : '' }}">
                        <label for="id">代表者名 (漢字) </label>
                        <input disabled type="text" class="form-control" name="txtRepresentativeNameKanji" value="{{ old('txtRepresentativeNameKanji',isset($suppliers) ? $suppliers->representative_name_kanji : '') }}">
                        @if ($errors->has('txtRepresentativeNameKanji'))
                        <div class="col-md-12">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('txtRepresentativeNameKanji') }}</strong>
                                    </span>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-3 form-group{{ $errors->has('txtRepresentativeNameKana') ? ' has-error' : '' }}">
                        <label for="id">代表者名 (カナ) </label>
                        <input disabled type="text" class="form-control" name="txtRepresentativeNameKana" value="{{ old('txtRepresentativeNameKana',isset($suppliers) ? $suppliers->representative_name_kana : '') }}">
                        @if ($errors->has('txtRepresentativeNameKana'))
                        <div class="col-md-12">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('txtRepresentativeNameKana') }}</strong>
                                    </span>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-3 form-group{{ $errors->has('txtRepresentativeEmailAddress') ? ' has-error' : '' }}">
                        <label for="id">代表者 メールアドレス</label>
                        <input disabled type="email" class="form-control" name="txtRepresentativeEmailAddress"
                               value="{{ old('txtRepresentativeEmailAddress',isset($suppliers) ? $suppliers->representative_email_address : '') }}">
                        @if ($errors->has('txtRepresentativeEmailAddress'))
                        <div class="col-md-12">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('txtRepresentativeEmailAddress') }}</strong>
                                    </span>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-3 form-group{{ $errors->has('txtRepresentativeMobileNumber') ? ' has-error' : '' }}">
                        <label for="id">代表者 携帯電話番号</label>
                        <input type="text" class="form-control" name="txtRepresentativeMobileNumber"
                               value="{{ old('txtRepresentativeMobileNumber',isset($suppliers) ? $suppliers->representative_mobile_number : '') }}">
                        @if ($errors->has('txtRepresentativeMobileNumber'))
                        <div class="col-md-12">
                            <span class="help-block">
                                <strong>{{ $errors->first('txtRepresentativeMobileNumber') }}</strong>
                            </span>
                        </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 form-group{{ $errors->has('txtContactNameKanji') ? ' has-error' : '' }}">
                        <label for="id">ご担当者名 (漢字) </label>
                        <input type="text" class="form-control" name="txtContactNameKanji" value="{{ old('txtContactNameKanji',isset($suppliers) ? $suppliers->contact_name_kanji : '') }}">
                        @if ($errors->has('txtContactNameKanji'))
                        <div class="col-md-12">
                            <span class="help-block">
                                <strong>{{ $errors->first('txtContactNameKanji') }}</strong>
                            </span>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-3 form-group{{ $errors->has('txtContactNameKana') ? ' has-error' : '' }}">
                        <label for="id">ご担当者名 (カナ) </label>
                        <input type="text" class="form-control" name="txtContactNameKana" value="{{ old('txtContactNameKana',isset($suppliers) ? $suppliers->contact_name_kana : '') }}">
                        @if ($errors->has('txtContactNameKana'))
                        <div class="col-md-12">
                            <span class="help-block">
                                <strong>{{ $errors->first('txtContactNameKana') }}</strong>
                            </span>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-3 form-group{{ $errors->has('txtContactEmailAddress') ? ' has-error' : '' }}">
                        <label for="id">ご担当者者 メールアドレス</label>
                        <input type="email" class="form-control" name="txtContactEmailAddress" value="{{ old('txtContactEmailAddress',isset($suppliers) ? $suppliers->contact_email_address : '') }}">
                        @if ($errors->has('txtContactEmailAddress'))
                        <div class="col-md-12">
                            <span class="help-block">
                                <strong>{{ $errors->first('txtContactEmailAddress') }}</strong>
                            </span>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-3 form-group{{ $errors->has('txtContactMobileNumber') ? ' has-error' : '' }}">
                        <label for="id">ご担当者 携帯電話番号</label>
                        <input type="text" class="form-control" name="txtContactMobileNumber" value="{{ old('txtContactMobileNumber',isset($suppliers) ? $suppliers->contact_mobile_number : '') }}">
                        @if ($errors->has('txtContactMobileNumber'))
                        <div class="col-md-12">
                            <span class="help-block">
                                <strong>{{ $errors->first('txtContactMobileNumber') }}</strong>
                            </span>
                        </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 form-group{{ $errors->has('txtContactSkypeID') ? ' has-error' : '' }}">
                        <label for="id">ご担当者 Skype ID</label>
                        <input type="text" class="form-control" name="txtContactSkypeID" value="{{ old('txtContactSkypeID',isset($suppliers) ? $suppliers->contact_skype_id : '') }}">
                        @if ($errors->has('txtContactSkypeID'))
                        <div class="col-md-12">
                            <span class="help-block">
                                <strong>{{ $errors->first('txtContactSkypeID') }}</strong>
                            </span>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-3 form-group{{ $errors->has('txtContactLineID') ? ' has-error' : '' }}">
                        <label for="id">ご担当者 LINE ID</label>
                        <input type="text" class="form-control" name="txtContactLineID" value="{{ old('txtContactLineID',isset($suppliers) ? $suppliers->contact_line_id : '') }}">
                        @if ($errors->has('txtContactLineID'))
                        <div class="col-md-12">
                            <span class="help-block">
                                <strong>{{ $errors->first('txtContactLineID') }}</strong>
                            </span>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-3 form-group{{ $errors->has('txtContactWechatID') ? ' has-error' : '' }}">
                        <label for="id">ご担当者 WeChat ID</label>
                        <input type="text" class="form-control" name="txtContactWechatID" value="{{ old('txtContactWechatID',isset($suppliers) ? $suppliers->contact_wechat_id : '') }}">
                        @if ($errors->has('txtContactWechatID'))
                        <div class="col-md-12">
                            <span class="help-block">
                                <strong>{{ $errors->first('txtContactWechatID') }}</strong>
                            </span>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-3 form-group{{ $errors->has('txtContactWhatsappID') ? ' has-error' : '' }}">
                        <label for="id">ご担当者 WhatsApp ID</label>
                        <input type="text" class="form-control" name="txtContactWhatsappID" value="{{ old('txtContactWhatsappID',isset($suppliers) ? $suppliers->contact_whatsapp_id : '') }}">
                        @if ($errors->has('txtContactWhatsappID'))
                        <div class="col-md-12">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('txtContactWhatsappID') }}</strong>
                                    </span>
                        </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 form-group{{ $errors->has('txtContactGmailAddress') ? ' has-error' : '' }}">
                        <label for="id">ご担当者 Google アカウント</label>
                        <input type="email" class="form-control" name="txtContactGmailAddress" value="{{ old('txtContactGmailAddress',isset($suppliers) ? $suppliers->contact_gmail_address : '') }}">
                        @if ($errors->has('txtContactGmailAddress'))
                        <div class="col-md-12">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('txtContactGmailAddress') }}</strong>
                                    </span>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-3 form-group{{ $errors->has('txtContactFacebook') ? ' has-error' : '' }}">
                        <label for="id">ご担当者 Facebook</label>
                        <input type="text" class="form-control" name="txtContactFacebook" value="{{ old('txtContactFacebook',isset($suppliers) ? $suppliers->contact_facebook : '') }}">
                        @if ($errors->has('txtContactFacebook'))
                        <div class="col-md-12">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('txtContactFacebook') }}</strong>
                                    </span>
                        </div>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="row{{ $errors->has('cbl_genres') ? ' has-error' : '' }}">
                    <div class="col-md-12 form-group">
                        <label for="cbl_genres[]" class="form-group">ご対応可能ジャンル (複数選択可) </label>
                        <div class="row row_detail_company_japan ">
                            @foreach($mtb_genres as $value)
                            <div class="checkbox col-md-4" id="checkbox_list_detail" style="margin: 0px"><label>
                                    <input type="checkbox" value="{{$value->id}}" name="cbl_genres[{{$value->id}}]"
                                           @if(old('cbl_genres', []) != NULL)
                                    @if(array_key_exists($value->id, old('cbl_genres', []))) checked @endif
                                    @else
                                    @foreach($mtb_genres_id as $val)
                                    @if($value->id == $val->mtb_genre_id)
                                    {{ 'checked' }}
                                    @endif
                                    @endforeach
                                    @endif
                                    >{{ $value->content }}
                                </label></div>
                            @endforeach
                            @if ($errors->has('cbl_genres'))
                            <div class="col-md-12">
                                <span class="help-block">
                                                <strong>{{ $errors->first('cbl_genres') }}</strong>
                                            </span>
                            </div>
                            @endif
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-group{{ $errors->has('txtCompanyStrength') ? ' has-error' : '' }}">
                        <label for="id">これが得意！！企業の強み </label>
                        <textarea class="form-control" rows="5" name="txtCompanyStrength">{{ old('txtCompanyStrength',isset($suppliers->company_strength) ? $suppliers->company_strength : '') }}</textarea>
                        @if ($errors->has('txtCompanyStrength'))
                        <div class="col-md-12">
                            <span class="help-block">
                                <strong>{{ $errors->first('txtCompanyStrength') }}</strong>
                            </span>
                        </div>
                        @endif
                    </div>
                </div>

                <div class="row row_detail_foreign">
                    <input type="hidden" name="hidden_id" value="{{ $suppliers->id }}">
                    <div class="col-md-12 text-center">
                        <a href="{{url('/')}}"><button type="button" class="back-template-mail-create">キャンセル</button></a>
                        <button class="submit-template-mail-create">更新</button>
                    </div>
                </div>
                
            </div>
        </form>
    </article>
</section>
@endsection