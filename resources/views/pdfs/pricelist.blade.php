<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        * {
            font-family: ipag;
        }
        table {
            text-align: center;
        }
    </style>
</head>

<?php

$style = [
    /* Layout ------------------------------ */

    'body' => 'margin: 0; padding: 0; width: 100%; background-color: #F2F4F6;font-size: 10px',
    'email-wrapper' => 'width: 100%; margin: 0; padding: 0; background-color: #F2F4F6;',

    /* Masthead ----------------------- */

    'email-masthead' => 'padding: 25px 0; text-align: center;',
    'email-masthead_name' => 'font-size: 16px; font-weight: bold; color: #2F3133; text-decoration: none; text-shadow: 0 1px 0 white;',

    'email-body' => 'width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;',
    'email-body_inner' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0;',
    'email-body_cell' => 'padding: 35px;',

    'email-footer' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0; text-align: center;',
    'email-footer_cell' => 'color: #AEAEAE; padding: 35px; text-align: center;',

    /* Body ------------------------------ */

    'body_action' => 'width: 100%; margin: 30px auto; padding: 0; text-align: center;',
    'body_sub' => 'margin-top: 25px; padding-top: 25px; border-top: 1px solid #EDEFF2;',

    /* Type ------------------------------ */

    'anchor' => 'color: #3869D4;',
    'header-1' => 'margin-top: 0; color: #2F3133; font-size: 19px; font-weight: bold; text-align: left;',
    'paragraph' => 'margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;',
    'paragraph-sub' => 'margin-top: 0; color: #74787E; font-size: 12px; line-height: 1.5em;',
    'paragraph-center' => 'text-align: center;',

    /* Buttons ------------------------------ */

    'button' => 'display: block; display: inline-block; width: 200px; min-height: 20px; padding: 10px;
                 background-color: #3869D4; border-radius: 3px; color: #ffffff; font-size: 15px; line-height: 25px;
                 text-align: center; text-decoration: none; -webkit-text-size-adjust: none;',

    'button--green' => 'background-color: #22BC66;',
    'button--red' => 'background-color: #dc4d2f;',
    'button--blue' => 'background-color: #3869D4;',
];
?>

<body style="{{ $style['body'] }};">
<h1>{{$price_list->price_list_name}}</h1>
<table>
    <thead>
    <tr class="bg-primary text-center">
        <th class="text-center" width="10">No.</th>
        {{--<th class="text-center">商品写真</th>--}}
        <th class="text-center">英文商品名</th>
        <th class="text-center">ケース入数</th>
        <th class="text-center">MOQ/Piece</th>
        <th class="text-center">商品価格</th>
        <th class="text-center">ケース当たりSize 縦 (CM)</th>
        <th class="text-center">ケース当たりSize 横 (CM)</th>
        <th class="text-center">ケース当たりSize 高さ(CM)</th>
        <th class="text-center">ケース当たり重量</th>
    </tr>
    </thead>
    <tbody>
    @foreach($price_list->price_list_details as $item)
    <tr>
        <td>{{$loop->iteration}}</td>
        {{--<td><img src="{{'data:image/jpg;base64,'.$item->product->product_image_main_media_file->content}}" width="50px" height="50px"></td>--}}
        <td>{{$item->name_product_en}}</td>
        <td>{{$item->price_list_fob_price}}</td>
        <td>{{$item->price_list_moq}}</td>
        <td>{{$item->price_list_input}}</td>
        <td>{{$item->price_list_length}}</td>
        <td>{{$item->price_list_width}}</td>
        <td>{{$item->price_list_height}}</td>
        <td>{{$item->product->packing_weight}}</td>
    </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>