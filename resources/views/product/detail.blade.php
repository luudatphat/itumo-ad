@extends('layouts.master')
@section('title','カタログ作成')
@section('sekai_content')
<div class="container-fluid">
    <h1 class="h1 bg-primary text-center h1_list_user">カタログ作成</h1>
    @if(session('message'))
        <div class="alert alert-success alert_success_company_japan">{{session('message')}}</div>
    @endif
    <div class="row">
        <div class="col-md-4 text-center">
            <div class="row">
                <div class="col-md-12" id="divimages">
                    <img id="imagesmain" crossOrigin="Anonymous" src="{{ isset($product->product_image_main_media_file) ? 'data:image/jpg;base64,' . $product->product_image_main_media_file->content : '/images/not_available.jpg' }}" class="img-responsive" width="100%" >
                </div>

                <div class="clearfix"></div>
                <div class="col-md-3">
                    <img id="images1" crossOrigin="Anonymous" src="{{ isset($product->product_image_main_media_file) ? 'data:image/jpg;base64,' . $product->product_image_main_media_file->content : '/images/not_available.jpg' }}" class="img-responsive img-circle img-border" width="100%" >
                </div>
                <div class="col-md-3">
                    <img id="images2" crossOrigin="Anonymous" src="{{ isset($product->product_image_2) ? 'data:image/jpg;base64,' . $product->product_image_2_media_file->content : '/images/not_available.jpg' }}" class="img-responsive img-circle img-border" width="100%" >
                </div>
                <div class="col-md-3">
                    <img id="images3" crossOrigin="Anonymous" src="{{ isset($product->product_image_3) ? 'data:image/jpg;base64,' . $product->product_image_3_media_file->content : '/images/not_available.jpg' }}" class="img-responsive img-circle img-border" width="100%" >
                </div>
                <div class="col-md-3">
                    <img id="images4" crossOrigin="Anonymous" src="{{ isset($product->product_image_4) ? 'data:image/jpg;base64,' . $product->product_image_4_media_file->content : '/images/not_available.jpg' }}" class="img-responsive img-circle img-border" width="100%" >
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="row row_detail_foreign">
                <div class="col-md-4">
                    <label for="company_name">商品名 </label>
                </div>
                <div class="col-md-8">
                    {{$product->product_name}}
                </div>
            </div>

            <div class="row row_detail_foreign">
                <div class="col-md-4">
                    <label for="company_name">商品掲載代行依頼 </label>
                </div>
                <div class="col-md-8">
                    @if($product->flag_send_example_request == 0)
                    はい。
                    @else
                    いいえ。
                    @endif
                </div>
            </div>

            <div class="row row_detail_foreign">
                <div class="col-md-4">
                    <label for="company_name">最終顧客年齢層 </label>
                </div>
                <div class="col-md-8">
                    @foreach($product->mtb_date_ranges as $date_ranges)
                    {{$date_ranges->content}}<br>
                    @endforeach
                </div>
            </div>

            <div class="row row_detail_foreign">
                <div class="col-md-4">
                    <label for="company_name">今の販売チャネル </label>
                </div>
                <div class="col-md-8">
                    @foreach($product->mtb_sale_channels as $sale_channel)
                    {{$sale_channel->content}}<br>
                    @endforeach
                </div>
            </div>

            <div class="row row_detail_foreign">
                <div class="col-md-4">
                    <label for="company_name">商品売場動画URL </label>
                </div>
                <div class="col-md-8">
                    <a href="{{$product->product_store_video_url}}">{{$product->product_store_video_url}}</a>
                </div>
            </div>

            <div class="row row_detail_foreign">
                <div class="col-md-4">
                    <label for="company_name">商品説明動画URL </label>
                </div>
                <div class="col-md-8">
                    <a href="{{$product->product_description_url}}">{{$product->product_description_url}}</a>
                </div>
            </div>

            <div class="row row_detail_foreign">
                <div class="col-md-4">
                    <label for="company_name">最終顧客性別 </label>
                </div>
                <div class="col-md-8">
                    @if($product->end_customer_gender == 1)
                    男
                    @elseif($product->end_customer_gender == 2)
                    女
                    @else
                    男
                    <br>
                    女
                    @endif
                </div>
            </div>

            <div class="row row_detail_foreign">
                <div class="col-md-4">
                    <label for="company_name">バイヤーに対するリクエスト </label>
                </div>
                <div class="col-md-8">
                    @foreach($product->mtb_request_to_buyers as $request_to_buyer)
                    {{$request_to_buyer->content}}<br>
                    @endforeach
                </div>
            </div>

            <div class="row row_detail_foreign">
                <div class="col-md-4">
                    <label for="company_name">商品説明文 </label>
                </div>
                <div class="col-md-8">
                    {{$product->product_description}}
                </div>
            </div>

            <div class="row row_detail_foreign">
                <div class="col-md-4">
                    <label for="company_name">商品１個当たり FOBプライス </label>
                </div>
                <div class="col-md-8">
                    {{$product->fob_price}}
                </div>
            </div>

            <div class="row row_detail_foreign">
                <div class="col-md-4">
                    <label for="company_name">MOQ(最低取引数量) </label>
                </div>
                <div class="col-md-8">
                    {{$product->moq}}
                </div>
            </div>
        </div>

        <!--
            <div class="col-md-1">
            {{--<a class="btn btn-primary" href="{{ url('/product_edit/'.$product->id) }}">編集</a>--}}
        </div>
        -->

        <div class="clearfix"></div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="row row_detail_foreign">
                        <div class="col-md-4">
                            <label for="company_name">１個当たりの梱包資材込サイズ【縦】 </label>
                        </div>
                        <div class="col-md-8">
                            {{$product->length}}
                        </div>
                    </div>

                    <div class="row row_detail_foreign">
                        <div class="col-md-4">
                            <label for="company_name">１個当たりの梱包資材込サイズ【横】 </label>
                        </div>
                        <div class="col-md-8">
                            {{$product->width}}
                        </div>
                    </div>

                    <div class="row row_detail_foreign">
                        <div class="col-md-4">
                            <label for="company_name">１個当たりの梱包資材込サイズ【高さ】 </label>
                        </div>
                        <div class="col-md-8">
                            {{$product->height}}
                        </div>
                    </div>

                    <div class="row row_detail_foreign">
                        <div class="col-md-4">
                            <label for="company_name">１個当たりの梱包資材込重量 </label>
                        </div>
                        <div class="col-md-8">
                            {{$product->packing_weight}}
                        </div>
                    </div>

                    <div class="row row_detail_foreign">
                        <div class="col-md-4">
                            <label for="company_name">商品販売開始時期 </label>
                        </div>
                        <div class="col-md-8">
                            {{$product->sale_date}}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row row_detail_foreign">
                        <div class="col-md-4">
                            <label for="company_name">既存販売地域 </label>
                        </div>
                        <div class="col-md-8">
                            {{$product->sale_area}}
                        </div>
                    </div>

                    <div class="row row_detail_foreign">
                        <div class="col-md-4">
                            <label for="company_name">展開NG国 </label>
                        </div>
                        <div class="col-md-8">
                            {{$product->deploy_ng_country}}
                        </div>
                    </div>

                    <div class="row row_detail_foreign">
                        <div class="col-md-4">
                            <label for="company_name">サンプル価格 </label>
                        </div>
                        <div class="col-md-8">
                            {{$product->example_price}}
                        </div>
                    </div>

                    <div class="row row_detail_foreign">
                        <div class="col-md-4">
                            <label for="company_name">競合と比較して差別化出来るポイント </label>
                        </div>
                        <div class="col-md-8">
                            {{$product->different_point}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row text-center" style="margin-bottom: 20px">
                <a href="{{url('product_list')}}" class="btn btn-warning">キャンセル</a>
            </div>
        </div>
    </div>
</div>
@endsection