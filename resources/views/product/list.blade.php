@extends('layouts.master')
@section('title','商品一覧')
@section('sekai_content')
<meta name="_token" content="{{ csrf_token() }}">

<section>
    <header><h2>商品一覧</h2></header>
    <article>
        <form class="form-sort form-sort-01 fix-form-01" action="{{url('/product_list')}}" method="POST">
            @csrf
            <div class="content-group-control">
                <div class="clearfix bottom-form pdT-0">
                    <div class="pull-left">
                        <span class="title-control">キーワード</span>
                        <input class="input-last fix-w-input-01" type="text" placeholder="キーワードを入力して下さい" name="search" id="search" value="{{ isset($search)?$search:'' }}">
                        <span class="text-center"><button class="fix-width-btn-05" type="submit">検索</button></span>
                    </div>
                </div>
            </div>
            <!-- end .content-group-control -->
        </form>
        <div class="row">
            <div class="col-md-12 text-right fix-top-layout fix-bottom-layout">
                <a href="{{url('/create_product')}}" class="btn btn-primary fix-width-btn-06 fix-width-btn-07">製品を作成する</a>
            </div>
        </div>
        <table class="content-list-table-02 fix-table-01_fix_01 result-table fix-table-02" style="white-space: nowrap">
            <thead>
            <tr>
                <th id="created_at" class="text-center col-sort w-150">登録日</th>
                <th id="product_name" class="text-center col-sort">商品名</th>
                <th class="text-center">商品写真</th>
                <th class="text-center">商品説明文</th>
                <th id="example_price" class="text-center col-sort">サンプル価格</th>
                <th class="text-center w-125">詳細</th>
            </tr>
            </thead>

            <tbody>
            @forelse ($products as $product)
                <tr>
                    <td >{{date_format(date_create($product->created_at),'Y-m-d')}}</td>
                    <td >{{$product->product_name}}</td>
                    <td width="200px;" class='td-price-list'><img crossOrigin="Anonymous"
                                                                  src="{{url('/image/'.$product->product_image_main)}}"
                                                                  width="100%"/>
                    </td>
                    <td >
                        {!!nl2br(e($product->product_description))!!}
                    </td>
                    <td class="text-td" >
                        {{$product->example_price}}
                    </td>
                    <td class="text-center" style="vertical-align: middle">
                        <a class="btn btn-primary fix-btn-01" href="{{ url('/product_detail/'.$product->id) }}">詳細</a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="6">データなし。</td>
                </tr>
            @endforelse
            </tbody>
        </table>

        <nav class="text-right total-records pagination_div show_record">
            <p class="text-right">Records {{ ($products->currentPage() -1) * $products->perPage() + 1 }} -
                {{ ($products->lastPage() == $products->currentPage()) ? $products->total() : ($products->currentPage() * $products->perPage()) }}
                of {{ $products->total() }}</p>
            {{$products->links('pagination::bootstrap-4')}}
        </nav>

    </article>
</section>

<script type="text/javascript">
    $('.col-sort').click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
//        $('.col_sort').css({'background': '#3097D1'});
//        $(this).css({'background': '#3097D1'});
//        $('.col_sort').children('.fa').css({'display':'inline'});
        var page = {{$current_page}};
        var search = $('#search').val();
        if($(this).hasClass('down')) {
            $(this).removeClass('down');
            $(this).children('.fa-caret-down').css({'display': 'none'});
            $(this).children('.fa-caret-up').css({'display': 'inline'});
            //Sort ASC
            var current_token = '{{csrf_token()}}';
            var column = $(this).attr('id');
            $('#column_dl').val(column);
            $('#sort_dl').val("asc");
            $.ajax({
                url: '/list_product_sort?page=' + page,
                dataType: 'text',
                type: 'post',
                contentType: 'application/x-www-form-urlencoded',
                data: {search: search, column: column, sort: 'asc', current_page: page,  fuel_csrf_token: current_token},
                success: function( data, textStatus, jQxhr ){
                    var result = JSON.parse(data);
                    $('.result-table tbody').html(result['content']);
                    $('.pagination_div').html(result['pagination']);
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                }
            });

        }else {
            $(this).addClass('down');
            $(this).children('.fa-caret-up').css({'display': 'none'});
            $(this).children('.fa-caret-down').css({'display': 'inline'});
            //Sort DESC
            var current_token = '{{csrf_token()}}';
            var column =$(this).attr('id');
            $('#column_dl').val(column);
            $('#sort_dl').val("desc");
            $.ajax({
                url: '/list_product_sort?page=' + page,
                dataType: 'text',
                type: 'post',
                contentType: 'application/x-www-form-urlencoded',
                data: {search: search,  column: column, sort: 'desc', current_page: page ,  fuel_csrf_token: current_token},
                success: function( data, textStatus, jQxhr ){
                    var result = JSON.parse(data);
                    $('.result-table tbody').html(result['content']);
                    $('.pagination_div').html(result['pagination']);
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                }
            });
        }
    });

    $('.col-sort').click(function (e) {
        e.preventDefault();
        if($(this).hasClass('select-sort')) {
            $(this).removeClass('select-sort');
            return false;
        }
        $('.col-sort').removeClass('select-sort');
        $(this).addClass('select-sort');
    });
</script>
@endsection