@extends('layouts.master')
@section('title','カタログ作成')
@section('sekai_content')
    <div class="container-fluid">
        <h1 class="h1 bg-primary text-center h1_list_user">カタログ作成</h1>
        @if(session('message'))
        <div class="alert alert-success alert_success_company_japan">{{session('message')}}</div>
        @endif
        <form class="form-group" action="{{ url('/update_product') }}" enctype="multipart/form-data" method="post">
            @csrf
            <input type="hidden" name="id" value="{{isset($product)?$product->id:''}}">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row vertical-align {{$errors->has('flag_send_example') ? 'has-error' : ''}}">
                        <div class="col-md-4">
                            <label>当社に商品掲載代行を依頼する <span class="text-danger">必須</span></label>
                        </div>
                        <div class="col-md-8">
                            <div class="radio">
                                <label><input type="radio" name="flag_send_example" value="1" {{ ((old('flag_send_example') !== NULL) && (old('flag_send_example') == 1)) ? 'checked' : (isset($product->flag_send_example_request) && ($product->flag_send_example_request == 1)) ? 'checked' : '' }}>はい</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="flag_send_example" value="2" {{ ((old('flag_send_example') !== NULL) && (old('flag_send_example') == 2)) ? 'checked' : (isset($product->flag_send_example_request) && ($product->flag_send_example_request == 2)) ? 'checked' : '' }}>いいえ</label>
                            </div>
                            <div class="note_product">
                                掲載の代行を希望する方は、下記｢当社に商品掲載代行を依頼する｣の｢はい｣の項目にチェックをしていただいた上で、以下項目を日本語でご入力下さい。<br>
                                また商品掲載の代行は、1商品につき10,000円で代行いたします。
                            </div>
                        </div>
                        @if ($errors->has('flag_send_example'))
                        <span class="help-block">
                            <strong>{{ $errors->first('flag_send_example') }}</strong>
                        </span>
                        @endif
                    </div>
                    <hr>
                    <div class="row vertical-align {{$errors->has('product_name') ? 'has-error' : ''}}">
                        <div class="col-md-4">
                            <label for="">商品名 <span class="text-danger">必須</span></label>
                        </div>
                        <div class="col-md-8">
                             <input type="text" class="form-control" name="product_name" value="{{ old('product_name',isset($product->product_name) ? $product->product_name : '') }}">
                            @if ($errors->has('product_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('product_name') }}</strong>
                            </span>
                            @endif
                            <div class="note_product">
                                例) COUXU moisture cream
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row vertical-align {{$errors->has('picture_main') ? 'has-error' : ''}}">
                        <div class="col-md-4">
                            <label for="">商品画像【メイン画像】 <span class="text-danger">必須</span></label>
                        </div>
                        <div class="col-md-8">
                            <input type="file" class="form-control" name="picture_main" accept="image/*">
                            @if ($errors->has('picture_main'))
                            <span class="help-block">
                                <strong>{{ $errors->first('picture_main') }}</strong>
                            </span>
                            @endif
                            <div class="note_product">
                                ※ 最大5MBまで添付できます。<br>
                                ※ 画像サイズは500×500(pixel)でご用意下さい。<br>
                                ※ 画像背景は白で作成して下さい。<br>
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="row vertical-align {{$errors->has('picture_2') ? 'has-error' : ''}}">
                        <div class="col-md-4">
                            <label for="">商品画像２ </label>
                        </div>
                        <div class="col-md-8">
                            <input type="file" class="form-control" name="picture_2" accept="image/*">
                            @if ($errors->has('picture_2'))
                            <span class="help-block">
                                <strong>{{ $errors->first('picture_2') }}</strong>
                            </span>
                            @endif
                            <div class="note_product">
                                ※ 最大5MBまで添付できます。<br>
                                ※ 画像サイズは500×500(pixel)でご用意下さい。<br>
                                ※ 画像背景は白で作成して下さい。 <br>
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="row vertical-align {{$errors->has('picture_3') ? 'has-error' : ''}}">
                        <div class="col-md-4">
                            <label for="">商品画像3 </label>
                        </div>
                        <div class="col-md-8">
                            <input type="file" class="form-control" name="picture_3" accept="image/*">
                            @if ($errors->has('picture_3'))
                            <span class="help-block">
                                <strong>{{ $errors->first('picture_3') }}</strong>
                            </span>
                            @endif
                            <div class="note_product">
                                ※ 最大5MBまで添付できます。<br>
                                ※ 画像サイズは500×500(pixel)でご用意下さい。<br>
                                ※ 画像背景は白で作成して下さい。 <br>
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="row vertical-align {{$errors->has('picture_4') ? 'has-error' : ''}}">
                        <div class="col-md-4">
                            <label for="">商品画像4 </label>
                        </div>
                        <div class="col-md-8">
                            <input type="file" class="form-control" name="picture_4" accept="image/*">
                            @if ($errors->has('picture_4'))
                            <span class="help-block">
                                <strong>{{ $errors->first('picture_4') }}</strong>
                            </span>
                            @endif
                            <div class="note_product">
                                ※ 最大5MBまで添付できます。<br>
                                ※ 画像サイズは500×500(pixel)でご用意下さい。<br>
                                ※ 画像背景は白で作成して下さい。 <br>
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="row vertical-align {{$errors->has('product_store_video_url') ? 'has-error' : ''}}">
                        <div class="col-md-4">
                            <label for="">商品売場動画URL</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="product_store_video_url" value="{{ old('product_store_video_url',isset($product->product_store_video_url) ? $product->product_store_video_url : '') }}">
                            @if ($errors->has('product_store_video_url'))
                            <span class="help-block">
                                <strong>{{ $errors->first('product_store_video_url') }}</strong>
                            </span>
                            @endif
                            <div class="note_product">
                                商品が販売されている店舗があれば、その様子を動画にてご準備下さい。<br>
                                ※ YouTube動画のアップ方法についてはこちらをご参照下さい。<br>
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="row vertical-align {{$errors->has('product_description_url') ? 'has-error' : ''}}">
                        <div class="col-md-4">
                            <label for="">商品説明動画URL</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="product_description_url" value="{{ old('product_description_url',isset($product->product_description_url) ? $product->product_description_url : '') }}">
                            @if ($errors->has('product_description_url'))
                            <span class="help-block">
                                <strong>{{ $errors->first('product_description_url') }}</strong>
                            </span>
                            @endif
                            <div class="note_product">
                                ※ YouTube動画のアップ方法についてはこちらをご参照下さい。
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="row {{$errors->has('product_description') ? 'has-error' : ''}} vertical-align">
                        <div class="col-md-4 ">
                            <label for="">商品説明文 <span class="text-danger">必須</span></label>
                        </div>
                        <div class="col-md-8">
                            <textarea name="product_description" class="form-control" rows="10">{{ old('product_description',isset($product->product_description) ? $product->product_description : '') }}</textarea>
                            @if ($errors->has('product_description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('product_description') }}</strong>
                            </span>
                            @endif
                            <div class="note_product">
                                例） Usage：Moisture cream for hand, face, and body.<br>
                                (使用目的：体、顔、手用のモイスチャークリームです。) <br>
                                How to use：Apply a peal of 1 cm of cream to face. <br>
                                (使い方：1cmのパール粒大のクリームを顔に塗ってください。) <br>
                                Use nightly. (夜にご使用ください。) <br>
                                Prohibitions：Drink,Eat, <br>
                                (禁止事項：飲む、食べる) <br>
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="row {{$errors->has('fob_price') ? 'has-error' : ''}} vertical-align">
                        <div class="col-md-4 ">
                            <label for="">商品１個当たり FOBプライス <span class="text-danger">必須</span></label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="fob_price" value="{{ old('fob_price',isset($product->fob_price) ? $product->fob_price : '') }}">
                            @if ($errors->has('fob_price'))
                            <span class="help-block">
                                <strong>{{ $errors->first('fob_price') }}</strong>
                            </span>
                            @endif
                            <div class="note_product">
                                （入力単位：JPY）<br>
                                FOBプライスとは？：日本の港まで運賃込の取引条件。FOBでの取引では、保険料や日本国外への運賃は含まれません。なので、カタログや価格表に記載する際の価格などに使用されます。
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="row {{$errors->has('moq') ? 'has-error' : ''}} vertical-align">
                        <div class="col-md-4 ">
                            <label for="">MOQ(最低取引数量) <span class="text-danger">必須</span></label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="moq" value="{{ old('moq',isset($product->moq) ? $product->moq : '') }}">
                            @if ($errors->has('moq'))
                            <span class="help-block">
                                <strong>{{ $errors->first('moq') }}</strong>
                            </span>
                            @endif
                            <div class="note_product">
                                （入力単位：個）
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="row {{$errors->has('length') ? 'has-error' : ''}} vertical-align">
                        <div class="col-md-4 ">
                            <label for="">１個当たりの梱包資材込サイズ　【縦】<span class="text-danger">必須</span></label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="length" value="{{ old('length',isset($product->length) ? $product->length : '') }}">
                            @if ($errors->has('length'))
                            <span class="help-block">
                                <strong>{{ $errors->first('length') }}</strong>
                            </span>
                            @endif
                            <div class="note_product">
                                （入力単位：mm）
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="row {{$errors->has('width') ? 'has-error' : ''}} vertical-align">
                        <div class="col-md-4 ">
                            <label for="">１個当たりの梱包資材込サイズ　【横】<span class="text-danger">必須</span></label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="width" value="{{ old('width',isset($product->width) ? $product->width : '') }}">
                            @if ($errors->has('width'))
                            <span class="help-block">
                                <strong>{{ $errors->first('width') }}</strong>
                            </span>
                            @endif
                            <div class="note_product">
                                （入力単位：mm）
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="row {{$errors->has('height') ? 'has-error' : ''}} vertical-align">
                        <div class="col-md-4 ">
                            <label for="">１個当たりの梱包資材込サイズ　【高さ】<span class="text-danger">必須</span></label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="height" value="{{ old('height',isset($product->height) ? $product->height : '') }}">
                            @if ($errors->has('height'))
                            <span class="help-block">
                                <strong>{{ $errors->first('height') }}</strong>
                            </span>
                            @endif
                            <div class="note_product">
                                （入力単位：mm）
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="row {{$errors->has('packing_weight') ? 'has-error' : ''}} vertical-align">
                        <div class="col-md-4 ">
                            <label for="">１個当たりの梱包資材込重量 </label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="packing_weight" value="{{ old('packing_weight',isset($product->packing_weight) ? $product->packing_weight : '') }}">
                            @if ($errors->has('packing_weight'))
                            <span class="help-block">
                                <strong>{{ $errors->first('packing_weight') }}</strong>
                            </span>
                            @endif
                            <div class="note_product">
                                （入力単位：g）
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="row {{$errors->has('sale_date') ? 'has-error' : ''}} vertical-align">
                        <div class="col-md-4 ">
                            <label for="">商品販売開始時期  <span class="text-danger">必須</span></label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="sale_date" id="sale_date" value="{{ old('sale_date',isset($product->sale_date) ? $product->sale_date : '') }}">
                            @if ($errors->has('sale_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('sale_date') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="note_product">

                        </div>
                    </div>
                    <hr>
                    <div class="row {{$errors->has('sale_area') ? 'has-error' : ''}} vertical-align">
                        <div class="col-md-4 ">
                            <label for="">既存販売地域  <span class="text-danger">必須</span></label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="sale_area" value="{{ old('sale_area',isset($product->sale_area) ? $product->sale_area : '') }}">
                            @if ($errors->has('sale_area'))
                            <span class="help-block">
                                <strong>{{ $errors->first('sale_area') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="note_product"></div>
                    </div>
                    <hr>
                    <div class="row {{$errors->has('deploy_ng_country') ? 'has-error' : ''}} vertical-align">
                        <div class="col-md-4 ">
                            <label for="">展開NG国  <span class="text-danger">必須</span></label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="deploy_ng_country" value="{{ old('deploy_ng_country',isset($product->deploy_ng_country) ? $product->deploy_ng_country : '') }}">
                            @if ($errors->has('deploy_ng_country'))
                            <span class="help-block">
                                <strong>{{ $errors->first('deploy_ng_country') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="note_product"></div>
                    </div>
                    <hr>
                    <div class="row {{$errors->has('example_price') ? 'has-error' : ''}} vertical-align">
                        <div class="col-md-4 ">
                            <label for="">サンプル価格  <span class="text-danger">必須</span></label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="example_price" value="{{ old('example_price',isset($product->example_price) ? $product->example_price : '') }}">
                            @if ($errors->has('example_price'))
                            <span class="help-block">
                                <strong>{{ $errors->first('example_price') }}</strong>
                            </span>
                            @endif
                            <div class="note_product">
                                （入力単位：JPY）<br>
                                サンプル価格を安くするほど、海外企業が気軽にサンプル購買ができるため、商談回数は上がりやすくなっている傾向にあります。高く設定するほど、「本気で購買したい」海外企業のみのサンプル購買となる傾向にあります。
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="row {{$errors->has('different_point') ? 'has-error' : ''}} vertical-align">
                        <div class="col-md-4 ">
                            <label for="">競合と比較して差別化出来るポイント <span class="text-danger">必須</span></label>
                        </div>
                        <div class="col-md-8">
                            <textarea name="different_point" class="form-control" rows="10">{{ old('different_point',isset($product->different_point) ? $product->different_point : '') }}</textarea>
                            @if ($errors->has('different_point'))
                            <span class="help-block">
                                <strong>{{ $errors->first('different_point') }}</strong>
                            </span>
                            @endif
                            <div class="note_product">
                                貴社の商品がなぜ現在顧客から評価されているか？通常は〇〇〇だけど、この商品は△△△だからいい！というような貴社のセールスポイントを記載して下さい。<br>
                                例) A lot of skin-care product includes emulsifier, but there is no emulsifier in this cream <br>
                                (通常のスキンケア商品は乳化剤が含まれていますが、こちらには乳化剤は入っていません。) <br>
                                This is all natural material. It is good for sensitive skin. <br>
                                (無添加化粧品で、敏感肌にも優しいクリームです。) <br>
                                You can whitening the face healthy. <br>
                                (健康な美肌美白作りができます。) <br>
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="row {{$errors->has('end_user_date_range') ? 'has-error' : ''}} vertical-align">
                        <div class="col-md-4 ">
                            <label for="">最終顧客年齢層 <span class="text-danger">必須</span></label>
                        </div>
                        <div class="col-md-8">
                            @foreach($mtb_date_ranges as $item)
                            <div class="checkbox">
                                <label>
                                <input type="checkbox" value="{{$item->id}}" name="end_user_date_range[{{$item->id}}]"
                                       @if(old('end_user_date_range', []) != NULL)
                                       @if(array_key_exists($item->id, old('end_user_date_range', []))) {{ 'checked' }} @endif
                                @else
                                    @foreach($list_end_user_date_range as $val)
                                        @if($item->id == $val->id)
                                            {{ 'checked' }}
                                                @endif
                                            @endforeach
                                        @endif
                                >{{$item->content}}</label>
                            </div>
                            @endforeach
                            @if ($errors->has('end_user_date_range'))
                            <span class="help-block">
                                <strong>{{ $errors->first('end_user_date_range') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <hr>
                    <div class="row {{$errors->has('end_customer_gender') ? 'has-error' : ''}} vertical-align">
                        <div class="col-md-4 ">
                            <label for="">最終顧客性別 <span class="text-danger">必須</span></label>
                        </div>
                        <div class="col-md-8">
                            <div class="checkbox">
                                <label><input {{ old('end_customer_gender') !== NULL ? (in_array(1, old('end_customer_gender')) ? 'checked' : '') : (isset($product->end_customer_gender) && ($product->end_customer_gender == 1 || $product->end_customer_gender == 3)) ? 'checked' : '' }} type="checkbox" name="end_customer_gender[]" value="1">男</label>
                            </div>
                            <div class="checkbox">
                                <label><input {{ old('end_customer_gender') !== NULL ? (in_array(2, old('end_customer_gender')) ? 'checked' : '') : (isset($product->end_customer_gender) && ($product->end_customer_gender == 2 || $product->end_customer_gender == 3)) ? 'checked' : '' }} type="checkbox" name="end_customer_gender[]" value="2">女</label>
                            </div>
                            @if ($errors->has('end_customer_gender'))
                            <span class="help-block">
                                <strong>{{ $errors->first('end_customer_gender') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <hr>
                    <div class="row {{$errors->has('sale_channels') ? 'has-error' : ''}} vertical-align">
                        <div class="col-md-4 ">
                            <label for="">今の販売チャネル <span class="text-danger">必須</span></label>
                        </div>
                        <div class="col-md-8">
                            @foreach($mtb_sale_channels as $item)
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="{{$item->id}}" name="sale_channels[{{$item->id}}]"
                                    @if(old('sale_channels', []) != NULL)
                                        @if(array_key_exists($item->id, old('sale_channels', []))) {{ 'checked' }} @endif
                                            @else
                                        @foreach($list_sale_channels as $val)
                                            @if($item->id == $val->id)
                                                {{ 'checked' }}
                                                    @endif
                                                @endforeach
                                            @endif
                                    >{{$item->content}}</label>
                            </div>
                            @endforeach
                            @if ($errors->has('sale_channels'))
                            <span class="help-block">
                                <strong>{{ $errors->first('sale_channels') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <hr>
                    <div class="row {{$errors->has('request_to_buyer') ? 'has-error' : ''}} vertical-align">
                        <div class="col-md-4 ">
                            <label for="">バイヤーに対するリクエスト <span class="text-danger">必須</span></label>
                        </div>
                        <div class="col-md-8">
                            @foreach($mtb_request_to_buyers as $item)
                            <div class="checkbox">
                                <label>

                                    <input type="checkbox" value="{{$item->id}}" name="request_to_buyer[{{$item->id}}]"
                                    @if(old('request_to_buyer', []) != NULL)
                                        @if(array_key_exists($item->id, old('request_to_buyer', []))) {{ 'checked' }} @endif
                                            @else
                                        @foreach($list_request_to_buyer as $val)
                                            @if($item->id == $val->id)
                                                {{ 'checked' }}
                                                    @endif
                                                @endforeach
                                            @endif
                                    >{{$item->content}}</label>
                            </div>
                            @endforeach
                            @if ($errors->has('request_to_buyer'))
                            <span class="help-block">
                                <strong>{{ $errors->first('request_to_buyer') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!-- Button submit -->
            <div class="row text-center">
                <a href="{{url('product_list')}}" class="btn btn-warning">キャンセル</a>
                <input id="" class="btn btn-primary" type="submit" name="submit" value="送信する">
            </div>
        </form>
    </div>
@endsection