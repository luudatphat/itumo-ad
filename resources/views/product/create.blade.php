@extends('layouts.master')
@section('title','カタログ作成')
@section('sekai_content')
    <section>
        <header><h2>成約報告新規作成</h2></header>
        @if(session('message'))
            <div class="alert alert-success alert_success_company_japan">{{session('message')}}</div>
        @endif
        <article class="mgT-30">
            <p class="text-center mgB-45">下記、商品掲載登録フォームに、必須項目は必ずご入力のうえ送信して下さい。 ご入力いただく内容は、商品の価値を海外企業様にご理解いただくための大切な情報となります。<br>
                この情報次第でサンプルの購買をするか、しないかが判断されますので、可能な限り詳細な情報を記載していただきますよう宜しくお願い致します。</p>
            <form class="content-form-01 mgB-60" action="" enctype="multipart/form-data" method="post">
                @csrf
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">当社に商品掲載代行を依頼する<strong>必須</strong></span></td>
                            <td>
                                <label><input type="radio" name="flag_send_example" value="1" checked>はい</label><br>
                                <label><input type="radio" name="flag_send_example" value="2" {{old('flag_send_example')==2 ? 'checked' : '' }}>いいえ</label>
                                <div class="note_product">
                                    掲載の代行を希望する方は、下記｢当社に商品掲載代行を依頼する｣の｢はい｣の項目にチェックをしていただいた上で、以下項目を日本語でご入力下さい。<br>
                                    また商品掲載の代行は、1商品につき10,000円で代行いたします。
                                </div>
                                @if ($errors->has('flag_send_example'))
                                    <strong>{{ $errors->first('flag_send_example') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">商品名<strong>必須</strong></span></td>
                            <td>
                                <input type="text" name="product_name" value="{{old('product_name')}}" placeholder="商品名を入力してください">
                                <div class="note_product">
                                    例) COUXU moisture cream
                                </div>
                                @if ($errors->has('product_name'))
                                    <strong>{{ $errors->first('product_name') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">商品画像【メイン画像】<strong>必須</strong></span></td>
                            <td class="content-input-file">
                                <input type="file" id="picture_main" name="picture_main" value="{{old('picture_main')}}" onchange="$('#picture-main-url-1').html($(this).val())">
                                <label for="picture_main">ファイルを選択</label><span id="picture-main-url-1">選択されていません</span>
                                <div class="note_product">
                                    ※ 最大5MBまで添付できます。<br>
                                    ※ メイン画像はファイル名をmainとして下さい。<br>
                                    ※ 画像サイズは500×500(pixel)でご用意下さい。<br>
                                    ※ 画像背景は白で作成して下さい。
                                </div>
                                @if ($errors->has('picture_main'))
                                    <strong>{{ $errors->first('picture_main') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">商品画像２</span></td>
                            <td class="content-input-file">
                                <input type="file" id="picture_2" name="picture_2" value="{{old('picture_2')}}" onchange="$('#picture-main-url-2').html($(this).val())">
                                <label for="picture_2">ファイルを選択</label><span id="picture-main-url-2">選択されていません</span>
                                <div class="note_product">
                                    ※ 最大5MBまで添付できます。<br>
                                    ※ 画像サイズは500×500(pixel)でご用意下さい。<br>
                                    ※ 画像背景は白で作成して下さい。
                                </div>
                                @if ($errors->has('picture_2'))
                                    <strong>{{ $errors->first('picture_2') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">商品画像3</span></td>
                            <td class="content-input-file">
                                <input type="file" id="picture_3" name="picture_3" value="{{old('picture_3')}}" onchange="$('#picture-main-url-3').html($(this).val())">
                                <label for="picture_3">ファイルを選択</label><span id="picture-main-url-3">選択されていません</span>
                                <div class="note_product">
                                    ※ 最大5MBまで添付できます。<br>
                                    ※ 画像サイズは500×500(pixel)でご用意下さい。<br>
                                    ※ 画像背景は白で作成して下さい。
                                </div>
                                @if ($errors->has('picture_3'))
                                    <strong>{{ $errors->first('picture_3') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">商品画像4</span></td>
                            <td class="content-input-file">
                                <input type="file" id="picture_4" name="picture_4" value="{{old('picture_4')}}" onchange="$('#picture-main-url-4').html($(this).val())">
                                <label for="picture_4">ファイルを選択</label><span id="picture-main-url-4">選択されていません</span>
                                <div class="note_product">
                                    ※ 最大5MBまで添付できます。<br>
                                    ※ 画像サイズは500×500(pixel)でご用意下さい。<br>
                                    ※ 画像背景は白で作成して下さい。
                                </div>
                                @if ($errors->has('picture_4'))
                                    <strong>{{ $errors->first('picture_4') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">商品売場動画URL</span></td>
                            <td>
                                <input type="text" name="product_store_video_url" value="{{old('product_store_video_url')}}" placeholder="商品売場動画URLを入力してください">
                                <div class="note_product">
                                    商品が販売されている店舗があれば、その様子を動画にてご準備下さい。<br>
                                    ※ YouTube動画のアップ方法については<a href="#">こちらをご参照</a>下さい。
                                </div>
                                @if ($errors->has('product_store_video_url'))
                                    <strong>{{ $errors->first('product_store_video_url') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">商品説明動画URL</span></td>
                            <td>
                                <input type="text" name="product_description_url" value="{{old('product_description_url')}}" placeholder="商品説明動画URLを入力してください">
                                <div class="note_product">
                                    ※ YouTube動画のアップ方法については<a href="#">こちらをご参照</a>下さい。
                                </div>
                                @if ($errors->has('product_description_url'))
                                    <strong>{{ $errors->first('product_description_url') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">商品説明文<strong>必須</strong></span></td>
                            <td>
                                <textarea name="product_description" rows="10">{{old('product_description')}}</textarea>
                                <div class="note_product">
                                    例） Usage：Moisture cream for hand, face, and body.<br>
                                    (使用目的：体、顔、手用のモイスチャークリームです。)<br>
                                    How to use：Apply a peal of 1 cm of cream to face.<br>
                                    (使い方：1cmのパール粒大のクリームを顔に塗ってください。)<br>
                                    Use nightly. (夜にご使用ください。)<br>
                                    Prohibitions：Drink,Eat,<br>
                                    (禁止事項：飲む、食べる)
                                </div>
                                @if ($errors->has('product_description'))
                                    <strong>{{ $errors->first('product_description') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">商品１個当たり FOBプライス<strong>必須</strong></span></td>
                            <td>
                                <input type="text" name="fob_price" value="{{old('fob_price')}}" placeholder="商品１個当たり FOBプライスを入力してください">
                                <div class="note_product">
                                    （入力単位：JPY）<br>
                                    FOBプライスとは？：日本の港まで運賃込の取引条件。FOBでの取引では、保険料や日本国外への運賃は含まれません。なので、カタログや価格表に記載する際の価格などに使用されます。
                                </div>
                                @if ($errors->has('fob_price'))
                                    <strong>{{ $errors->first('fob_price') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">MOQ(最低取引数量)<strong>必須</strong></span></td>
                            <td>
                                <input type="text" name="moq" value="{{old('moq')}}" placeholder="MOQ(最低取引数量)を入力してください">
                                <div class="note_product">
                                    （入力単位：個）
                                </div>
                                @if ($errors->has('moq'))
                                    <strong>{{ $errors->first('moq') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">１個当たりの梱包資材込サイズ【縦】<strong>必須</strong></span></td>
                            <td>
                                <input type="text" name="length" value="{{old('length')}}" placeholder="１個当たりの梱包資材込サイズ【縦】を入力してください">
                                <div class="note_product">
                                    （入力単位：mm）
                                </div>
                                @if ($errors->has('length'))
                                    <strong>{{ $errors->first('length') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">１個当たりの梱包資材込サイズ【横】<strong>必須</strong></span></td>
                            <td>
                                <input type="text" name="width" value="{{old('width')}}" placeholder="１個当たりの梱包資材込サイズ【横】を入力してください">
                                <div class="note_product">
                                    （入力単位：mm）
                                </div>
                                @if ($errors->has('width'))
                                    <strong>{{ $errors->first('width') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">１個当たりの梱包資材込サイズ【高さ】<strong>必須</strong></span></td>
                            <td>
                                <input type="text" name="height" value="{{old('height')}}" placeholder="１個当たりの梱包資材込サイズ【高さ】を入力してください">
                                <div class="note_product">
                                    （入力単位：mm）
                                </div>
                                @if ($errors->has('height'))
                                    <strong>{{ $errors->first('height') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">１個当たりの梱包資材込重量</span></td>
                            <td>
                                <input type="text" name="packing_weight" value="{{old('packing_weight')}}" placeholder="１個当たりの梱包資材込重量を入力してください">
                                <div class="note_product">
                                    （入力単位：g）
                                </div>
                                @if ($errors->has('packing_weight'))
                                    <strong>{{ $errors->first('packing_weight') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">商品販売開始時期<strong>必須</strong></span></td>
                            <td>
                                <span class="calender-picker">
                                    <input type="text" name="sale_date" value="{{old('sale_date')}}">
                                </span>
                                @if ($errors->has('sale_date'))
                                    <strong>{{ $errors->first('sale_date') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">既存販売地域<strong>必須</strong></span></td>
                            <td>
                                <input type="text" name="sale_area" value="{{old('sale_area')}}" placeholder="既存販売地域を入力してください">
                                @if ($errors->has('sale_area'))
                                    <strong>{{ $errors->first('sale_area') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">展開NG国<strong>必須</strong></span></td>
                            <td>
                                <input type="text" name="deploy_ng_country" value="{{old('deploy_ng_country')}}" placeholder="展開NG国を入力してください">
                                @if ($errors->has('deploy_ng_country'))
                                    <strong>{{ $errors->first('deploy_ng_country') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">サンプル価格<strong>必須</strong></span></td>
                            <td>
                                <input type="text" name="example_price" value="{{old('example_price')}}" placeholder="商品１個当たり FOBプライスを入力してください">
                                <div class="note_product">
                                    （入力単位：JPY）<br>
                                    サンプル価格を安くするほど、海外企業が気軽にサンプル購買ができるため、商談回数は上がりやすくなっている傾向にあります。高く設定するほど、「本気で購買したい」海外企業のみのサンプル購買となる傾向にあります。
                                </div>
                                @if ($errors->has('example_price'))
                                    <strong>{{ $errors->first('example_price') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">競合と比較して差別化出来るポイント<strong>必須</strong></span></td>
                            <td>
                                <textarea name="different_point" rows="10">{{old('different_point')}}</textarea>
                                <div class="note_product">
                                    貴社の商品がなぜ現在顧客から評価されているか？通常は〇〇〇だけど、この商品は△△△だからいい！というような貴社のセールスポイントを記載して下さい。<br>
                                    例) A lot of skin-care product includes emulsifier, but there is no emulsifier in this cream<br>
                                    (通常のスキンケア商品は乳化剤が含まれていますが、こちらには乳化剤は入っていません。)<br>
                                    This is all natural material. It is good for sensitive skin.<br>
                                    (無添加化粧品で、敏感肌にも優しいクリームです。)<br>
                                    You can whitening the face healthy.<br>
                                    (健康な美肌美白作りができます。)
                                </div>
                                @if ($errors->has('different_point'))
                                    <strong>{{ $errors->first('different_point') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>

                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">最終顧客年齢層<strong>必須</strong></span></td>
                            <td>
                                @foreach($mtb_date_ranges as $item)
                                    <div class="checkbox">
                                        <label><input {{ old('end_user_date_range') !== NULL ? (in_array($item->id, old('end_user_date_range')) ? 'checked' : '') : '' }} type="checkbox" name="end_user_date_range[]" value="{{$item->id}}">{{$item->content}}</label>
                                    </div>
                                @endforeach
                                @if ($errors->has('end_user_date_range'))
                                    <strong>{{ $errors->first('end_user_date_range') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">最終顧客性別<strong>必須</strong></span></td>
                            <td>
                                <div class="checkbox">
                                    <label><input {{ old('end_customer_gender') !== NULL ? (in_array(1, old('end_customer_gender')) ? 'checked' : '') : '' }} type="checkbox" name="end_customer_gender[]" value="1">男</label>
                                </div>
                                <div class="checkbox">
                                    <label><input {{ old('end_customer_gender') !== NULL ? (in_array(2, old('end_customer_gender')) ? 'checked' : '') : '' }} type="checkbox" name="end_customer_gender[]" value="2">女</label>
                                </div>
                                @if ($errors->has('end_customer_gender'))
                                    <strong>{{ $errors->first('end_customer_gender') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">今の販売チャネル<strong>必須</strong></span></td>
                            <td>
                                @foreach($mtb_sale_channels as $item)
                                    <div class="checkbox">
                                        <label><input {{ old('sale_channels') !== NULL ? (in_array($item->id, old('sale_channels')) ? 'checked' : '') : '' }} type="checkbox" name="sale_channels[]" value="{{$item->id}}">{{$item->content}}</label>
                                    </div>
                                @endforeach
                                @if ($errors->has('sale_channels'))
                                    <strong>{{ $errors->first('sale_channels') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="content-group-controls-01 fix-table-03">
                    <table>
                        <tr>
                            <td><span class="title-control">バイヤーに対するリクエスト<strong>必須</strong></span></td>
                            <td>
                                @foreach($mtb_request_to_buyers as $item)
                                    <div class="checkbox">
                                        <label><input {{ old('request_to_buyer') !== NULL ? (in_array($item->id, old('request_to_buyer')) ? 'checked' : '') : '' }} type="checkbox" name="request_to_buyer[]" value="{{$item->id}}">{{$item->content}}</label>
                                    </div>
                                @endforeach
                                @if ($errors->has('request_to_buyer'))
                                    <strong>{{ $errors->first('request_to_buyer') }}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end .content-group-controls-01 -->
                <div class="text-center"><button type="submit" class="button-type-01">送信する</button></div>
            </form>
        </article>
    </section>
    <script>
        $(document).ready(function () {
            $('.calender-picker input').datepicker({
                changeMonth: true,
                showOn: "button",
                buttonImage: "images/icon_datepicker.png",
                buttonImageOnly: true,
                buttonText: "Select date",
                dateFormat: 'yy-mm-dd'
            })
        })
    </script>
    {{--<div class="container-fluid">--}}
        {{--<h1 class="h1 bg-primary text-center h1_list_user">カタログ作成</h1>--}}
        {{--@if(session('message'))--}}
        {{--<div class="alert alert-success alert_success_company_japan">{{session('message')}}</div>--}}
        {{--@endif--}}
        {{--<form class="form-group" action="" enctype="multipart/form-data" method="post">--}}
            {{--{{csrf_field()}}--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-10 col-md-offset-1">--}}
                    {{--<div class="row vertical-align {{$errors->has('flag_send_example') ? 'has-error' : ''}}">--}}
                        {{--<div class="col-md-4">--}}
                            {{--<label>当社に商品掲載代行を依頼する <span class="text-danger">必須</span></label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<div class="radio">--}}
                                {{--<label><input type="radio" name="flag_send_example" value="1" checked>はい</label>--}}
                            {{--</div>--}}
                            {{--<div class="radio">--}}
                                {{--<label><input type="radio" name="flag_send_example" value="2" {{old('flag_send_example')==2 ? 'checked' : '' }}>いいえ</label>--}}
                            {{--</div>--}}
                            {{--<div class="note_product">--}}
                                {{--掲載の代行を希望する方は、下記｢当社に商品掲載代行を依頼する｣の｢はい｣の項目にチェックをしていただいた上で、以下項目を日本語でご入力下さい。<br>--}}
                                {{--また商品掲載の代行は、1商品につき10,000円で代行いたします。--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--@if ($errors->has('flag_send_example'))--}}
                        {{--<span class="help-block">--}}
                            {{--<strong>{{ $errors->first('flag_send_example') }}</strong>--}}
                        {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row vertical-align {{$errors->has('product_name') ? 'has-error' : ''}}">--}}
                        {{--<div class="col-md-4">--}}
                            {{--<label for="">商品名 <span class="text-danger">必須</span></label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                             {{--<input type="text" class="form-control" name="product_name" value="{{old('product_name')}}">--}}
                            {{--@if ($errors->has('product_name'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('product_name') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--<div class="note_product">--}}
                                {{--例) COUXU moisture cream--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row vertical-align {{$errors->has('picture_main') ? 'has-error' : ''}}">--}}
                        {{--<div class="col-md-4">--}}
                            {{--<label for="">商品画像【メイン画像】 <span class="text-danger">必須</span></label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<input type="file" class="form-control" name="picture_main" accept="image/*">--}}
                            {{--@if ($errors->has('picture_main'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('picture_main') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--<div class="note_product">--}}
                                {{--※ 最大5MBまで添付できます。<br>--}}
                                {{--※ 画像サイズは500×500(pixel)でご用意下さい。<br>--}}
                                {{--※ 画像背景は白で作成して下さい。<br>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row vertical-align {{$errors->has('picture_2') ? 'has-error' : ''}}">--}}
                        {{--<div class="col-md-4">--}}
                            {{--<label for="">商品画像２ </label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<input type="file" class="form-control" name="picture_2" accept="image/*">--}}
                            {{--@if ($errors->has('picture_2'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('picture_2') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--<div class="note_product">--}}
                                {{--※ 最大5MBまで添付できます。<br>--}}
                                {{--※ 画像サイズは500×500(pixel)でご用意下さい。<br>--}}
                                {{--※ 画像背景は白で作成して下さい。 <br>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row vertical-align {{$errors->has('picture_3') ? 'has-error' : ''}}">--}}
                        {{--<div class="col-md-4">--}}
                            {{--<label for="">商品画像3 </label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<input type="file" class="form-control" name="picture_3" accept="image/*">--}}
                            {{--@if ($errors->has('picture_3'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('picture_3') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--<div class="note_product">--}}
                                {{--※ 最大5MBまで添付できます。<br>--}}
                                {{--※ 画像サイズは500×500(pixel)でご用意下さい。<br>--}}
                                {{--※ 画像背景は白で作成して下さい。 <br>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row vertical-align {{$errors->has('picture_4') ? 'has-error' : ''}}">--}}
                        {{--<div class="col-md-4">--}}
                            {{--<label for="">商品画像4 </label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<input type="file" class="form-control" name="picture_4" accept="image/*">--}}
                            {{--@if ($errors->has('picture_4'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('picture_4') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--<div class="note_product">--}}
                                {{--※ 最大5MBまで添付できます。<br>--}}
                                {{--※ 画像サイズは500×500(pixel)でご用意下さい。<br>--}}
                                {{--※ 画像背景は白で作成して下さい。 <br>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row vertical-align {{$errors->has('product_store_video_url') ? 'has-error' : ''}}">--}}
                        {{--<div class="col-md-4">--}}
                            {{--<label for="">商品売場動画URL</label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<input type="text" class="form-control" name="product_store_video_url" value="{{old('product_store_video_url')}}">--}}
                            {{--@if ($errors->has('product_store_video_url'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('product_store_video_url') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--<div class="note_product">--}}
                                {{--商品が販売されている店舗があれば、その様子を動画にてご準備下さい。<br>--}}
                                {{--※ YouTube動画のアップ方法についてはこちらをご参照下さい。<br>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row vertical-align {{$errors->has('product_description_url') ? 'has-error' : ''}}">--}}
                        {{--<div class="col-md-4">--}}
                            {{--<label for="">商品説明動画URL</label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<input type="text" class="form-control" name="product_description_url" value="{{old('product_description_url')}}">--}}
                            {{--@if ($errors->has('product_description_url'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('product_description_url') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--<div class="note_product">--}}
                                {{--※ YouTube動画のアップ方法についてはこちらをご参照下さい。--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row {{$errors->has('product_description') ? 'has-error' : ''}} vertical-align">--}}
                        {{--<div class="col-md-4 ">--}}
                            {{--<label for="">商品説明文 <span class="text-danger">必須</span></label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<textarea name="product_description" class="form-control" rows="10">{{old('product_description')}}</textarea>--}}
                            {{--@if ($errors->has('product_description'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('product_description') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--<div class="note_product">--}}
                                {{--例） Usage：Moisture cream for hand, face, and body.<br>--}}
                                {{--(使用目的：体、顔、手用のモイスチャークリームです。) <br>--}}
                                {{--How to use：Apply a peal of 1 cm of cream to face. <br>--}}
                                {{--(使い方：1cmのパール粒大のクリームを顔に塗ってください。) <br>--}}
                                {{--Use nightly. (夜にご使用ください。) <br>--}}
                                {{--Prohibitions：Drink,Eat, <br>--}}
                                {{--(禁止事項：飲む、食べる) <br>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row {{$errors->has('fob_price') ? 'has-error' : ''}} vertical-align">--}}
                        {{--<div class="col-md-4 ">--}}
                            {{--<label for="">商品１個当たり FOBプライス <span class="text-danger">必須</span></label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<input type="text" class="form-control" name="fob_price" value="{{old('fob_price')}}">--}}
                            {{--@if ($errors->has('fob_price'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('fob_price') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--<div class="note_product">--}}
                                {{--（入力単位：JPY）<br>--}}
                                {{--FOBプライスとは？：日本の港まで運賃込の取引条件。FOBでの取引では、保険料や日本国外への運賃は含まれません。なので、カタログや価格表に記載する際の価格などに使用されます。--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row {{$errors->has('moq') ? 'has-error' : ''}} vertical-align">--}}
                        {{--<div class="col-md-4 ">--}}
                            {{--<label for="">MOQ(最低取引数量) <span class="text-danger">必須</span></label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<input type="text" class="form-control" name="moq" value="{{old('moq')}}">--}}
                            {{--@if ($errors->has('moq'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('moq') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--<div class="note_product">--}}
                                {{--（入力単位：個）--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row {{$errors->has('length') ? 'has-error' : ''}} vertical-align">--}}
                        {{--<div class="col-md-4 ">--}}
                            {{--<label for="">１個当たりの梱包資材込サイズ　【縦】<span class="text-danger">必須</span></label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<input type="text" class="form-control" name="length" value="{{old('length')}}">--}}
                            {{--@if ($errors->has('length'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('length') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--<div class="note_product">--}}
                                {{--（入力単位：mm）--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row {{$errors->has('width') ? 'has-error' : ''}} vertical-align">--}}
                        {{--<div class="col-md-4 ">--}}
                            {{--<label for="">１個当たりの梱包資材込サイズ　【横】<span class="text-danger">必須</span></label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<input type="text" class="form-control" name="width" value="{{old('width')}}">--}}
                            {{--@if ($errors->has('width'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('width') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--<div class="note_product">--}}
                                {{--（入力単位：mm）--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row {{$errors->has('height') ? 'has-error' : ''}} vertical-align">--}}
                        {{--<div class="col-md-4 ">--}}
                            {{--<label for="">１個当たりの梱包資材込サイズ　【高さ】<span class="text-danger">必須</span></label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<input type="text" class="form-control" name="height" value="{{old('height')}}">--}}
                            {{--@if ($errors->has('height'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('height') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--<div class="note_product">--}}
                                {{--（入力単位：mm）--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row {{$errors->has('packing_weight') ? 'has-error' : ''}} vertical-align">--}}
                        {{--<div class="col-md-4 ">--}}
                            {{--<label for="">１個当たりの梱包資材込重量 </label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<input type="text" class="form-control" name="packing_weight" value="{{old('packing_weight')}}">--}}
                            {{--@if ($errors->has('packing_weight'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('packing_weight') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--<div class="note_product">--}}
                                {{--（入力単位：g）--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row {{$errors->has('sale_date') ? 'has-error' : ''}} vertical-align">--}}
                        {{--<div class="col-md-4 ">--}}
                            {{--<label for="">商品販売開始時期  <span class="text-danger">必須</span></label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<input type="text" class="form-control" name="sale_date" id="sale_date" value="{{old('sale_date')}}">--}}
                            {{--@if ($errors->has('sale_date'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('sale_date') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                        {{--<div class="note_product">--}}

                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row {{$errors->has('sale_area') ? 'has-error' : ''}} vertical-align">--}}
                        {{--<div class="col-md-4 ">--}}
                            {{--<label for="">既存販売地域  <span class="text-danger">必須</span></label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<input type="text" class="form-control" name="sale_area" value="{{old('sale_area')}}">--}}
                            {{--@if ($errors->has('sale_area'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('sale_area') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                        {{--<div class="note_product"></div>--}}
                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row {{$errors->has('deploy_ng_country') ? 'has-error' : ''}} vertical-align">--}}
                        {{--<div class="col-md-4 ">--}}
                            {{--<label for="">展開NG国  <span class="text-danger">必須</span></label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<input type="text" class="form-control" name="deploy_ng_country" value="{{old('deploy_ng_country')}}">--}}
                            {{--@if ($errors->has('deploy_ng_country'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('deploy_ng_country') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                        {{--<div class="note_product"></div>--}}
                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row {{$errors->has('example_price') ? 'has-error' : ''}} vertical-align">--}}
                        {{--<div class="col-md-4 ">--}}
                            {{--<label for="">サンプル価格  <span class="text-danger">必須</span></label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<input type="text" class="form-control" name="example_price" value="{{old('example_price')}}">--}}
                            {{--@if ($errors->has('example_price'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('example_price') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--<div class="note_product">--}}
                                {{--（入力単位：JPY）<br>--}}
                                {{--サンプル価格を安くするほど、海外企業が気軽にサンプル購買ができるため、商談回数は上がりやすくなっている傾向にあります。高く設定するほど、「本気で購買したい」海外企業のみのサンプル購買となる傾向にあります。--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row {{$errors->has('different_point') ? 'has-error' : ''}} vertical-align">--}}
                        {{--<div class="col-md-4 ">--}}
                            {{--<label for="">競合と比較して差別化出来るポイント <span class="text-danger">必須</span></label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<textarea name="different_point" class="form-control" rows="10">{{old('different_point')}}</textarea>--}}
                            {{--@if ($errors->has('different_point'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('different_point') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--<div class="note_product">--}}
                                {{--貴社の商品がなぜ現在顧客から評価されているか？通常は〇〇〇だけど、この商品は△△△だからいい！というような貴社のセールスポイントを記載して下さい。<br>--}}
                                {{--例) A lot of skin-care product includes emulsifier, but there is no emulsifier in this cream <br>--}}
                                {{--(通常のスキンケア商品は乳化剤が含まれていますが、こちらには乳化剤は入っていません。) <br>--}}
                                {{--This is all natural material. It is good for sensitive skin. <br>--}}
                                {{--(無添加化粧品で、敏感肌にも優しいクリームです。) <br>--}}
                                {{--You can whitening the face healthy. <br>--}}
                                {{--(健康な美肌美白作りができます。) <br>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row {{$errors->has('end_user_date_range') ? 'has-error' : ''}} vertical-align">--}}
                        {{--<div class="col-md-4 ">--}}
                            {{--<label for="">最終顧客年齢層 <span class="text-danger">必須</span></label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--@foreach($mtb_date_ranges as $item)--}}
                            {{--<div class="checkbox">--}}
                                {{--<label><input {{ old('end_user_date_range') !== NULL ? (in_array($item->id, old('end_user_date_range')) ? 'checked' : '') : '' }} type="checkbox" name="end_user_date_range[]" value="{{$item->id}}">{{$item->content}}</label>--}}
                            {{--</div>--}}
                            {{--@endforeach--}}
                            {{--@if ($errors->has('end_user_date_range'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('end_user_date_range') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row {{$errors->has('end_customer_gender') ? 'has-error' : ''}} vertical-align">--}}
                        {{--<div class="col-md-4 ">--}}
                            {{--<label for="">最終顧客性別 <span class="text-danger">必須</span></label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--<div class="checkbox">--}}
                                {{--<label><input {{ old('end_customer_gender') !== NULL ? (in_array(1, old('end_customer_gender')) ? 'checked' : '') : '' }} type="checkbox" name="end_customer_gender[]" value="1">男</label>--}}
                            {{--</div>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label><input {{ old('end_customer_gender') !== NULL ? (in_array(2, old('end_customer_gender')) ? 'checked' : '') : '' }} type="checkbox" name="end_customer_gender[]" value="2">女</label>--}}
                            {{--</div>--}}
                            {{--@if ($errors->has('end_customer_gender'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('end_customer_gender') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row {{$errors->has('sale_channels') ? 'has-error' : ''}} vertical-align">--}}
                        {{--<div class="col-md-4 ">--}}
                            {{--<label for="">今の販売チャネル <span class="text-danger">必須</span></label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--@foreach($mtb_sale_channels as $item)--}}
                            {{--<div class="checkbox">--}}
                                {{--<label><input {{ old('sale_channels') !== NULL ? (in_array($item->id, old('sale_channels')) ? 'checked' : '') : '' }} type="checkbox" name="sale_channels[]" value="{{$item->id}}">{{$item->content}}</label>--}}
                            {{--</div>--}}
                            {{--@endforeach--}}
                            {{--@if ($errors->has('sale_channels'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('sale_channels') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--<div class="row {{$errors->has('request_to_buyer') ? 'has-error' : ''}} vertical-align">--}}
                        {{--<div class="col-md-4 ">--}}
                            {{--<label for="">バイヤーに対するリクエスト <span class="text-danger">必須</span></label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8">--}}
                            {{--@foreach($mtb_request_to_buyers as $item)--}}
                            {{--<div class="checkbox">--}}
                                {{--<label><input {{ old('request_to_buyer') !== NULL ? (in_array($item->id, old('request_to_buyer')) ? 'checked' : '') : '' }} type="checkbox" name="request_to_buyer[]" value="{{$item->id}}">{{$item->content}}</label>--}}
                            {{--</div>--}}
                            {{--@endforeach--}}
                            {{--@if ($errors->has('request_to_buyer'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('request_to_buyer') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- Button submit -->--}}
            {{--<div class="row text-center">--}}
                {{--<a href="{{url('product_list')}}" class="btn btn-warning">キャンセル</a>--}}
                {{--<input id="" class="btn btn-primary" type="submit" name="submit" value="送信する">--}}
            {{--</div>--}}
        {{--</form>--}}
    {{--</div>--}}
@endsection