@extends('layouts.master')
@section('title','価格表一覧')
@section('sekai_content')

    <section>
        <header><h2>価格表一覧</h2></header>
        @if(session('message'))
            <div class="alert alert-success alert_success_company_japan">{{session('message')}}</div>
        @endif
        <article>
            <div class="row">
                <div class="col-md-12 text-right fix-top-layout">
                    <a class="btn btn-primary button_create_list_user fix-width-btn-01 fix-width-btn-07" href="{{url('/create_new_price_list_detail')}}">成約報告新規作成</a>
                </div>
            </div>
            <table class="content-list-table-02 fix-table-01 result-table fix-table-02 text-center" style="white-space: nowrap">
                <thead>
                <tr>
                    <th class="text-center w-100">NO.</th>
                    <th class="text-center">価格表名</th>
                    <th class="text-center">最終更新日</th>
                    <th class="text-center w-150">編集</th>
                    <th class="text-center w-150">削除</th>
                    <th class="text-center w-250">Excelダウンロード</th>
                    <th class="text-center w-250">PDFダウンロード</th>
                </tr>
                </thead>

                <tbody>
                @forelse ($price_list as $key => $list)
                    <tr>
                        <td ><span>{{++$key}}</span><input type="hidden" value="{{$list->id}}"></td>
                        <td >{{$list->price_list_name}}</td>
                        <td >
                            <?php
                            $str = $list->updated_at;
                            $str = substr($str , 0 , strpos($str , ":")-3);
                            $str = explode("-", $str);
                            $final = $str[0]."年".$str[1]."月".$str[2]."日";
                            echo $final;
                            ?>
                        </td>
                        <td ><a class="btn btn-primary button_create_list_user fix-width-btn-03 fix-bg-color fix-btn-size-01" href="/edit_price_list_detail/{{$list->id}}">編集する</a></td>
                        <td ><a class="btn btn-danger button_create_list_user fix-width-btn-03 fix-btn-size-02" href="/delete_price_list/{{$list->id}}" onclick=" return question()">削除</a></td>
                        <td ><a class="btn btn-primary fix-btn-01 fix-btn-size-03" href="/download_file_excel/{{$list->id}}">Excelダウンロード</a></td>
                        <td ><a class="btn btn-primary fix-btn-01 fix-btn-size-04" href="/download_file_pdf/{{$list->id}}">PDFダウンロード</a></td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan="7">データなし。</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            <nav class="text-right total-records pagination_div show_record">
                <p class="text-right">Records {{ ($price_list->currentPage() -1) * $price_list->perPage() + 1 }} -
                    {{ ($price_list->lastPage() == $price_list->currentPage()) ? $price_list->total() : ($price_list->currentPage() * $price_list->perPage()) }}
                    of {{ $price_list->total() }}</p>
                {{$price_list->links('pagination::bootstrap-4')}}
            </nav>
        </article>
    </section>

    <script type="text/javascript">
        $(document).ready(function () {
            if($('#price_list_detail > tbody > tr').length > 0){
                $('#price_list_detail').DataTable({
                    "searching": false,
                    "bInfo": false,
                    "bLengthChange": false,
                    "pageLength": 5
                });
            }
        });

        /*Question when delete*/
        function question() {
            var r = confirm("削除してもよろしいですか？");
            if(r == true){
                return true;
            }
            else{
                return false;
            }
        }
        /*End Question when delete*/

    </script>
@endsection