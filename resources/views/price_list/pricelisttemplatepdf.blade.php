<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        * {
            font-family: ipag;
        }
        table {
            text-align: center;
            border-collapse: collapse;
        }
        table, th, td {
            border: 1px solid black;
        }
        td{
            wrap-text : true;
        }
    </style>
</head>

<?php

$style = [
        "table" => "border-collapse: collapse;border: 1px solid #000000;"
];
?>


<body>
<h1>価格表一覧</h1>
<table>

    <tr>
        <th>価格表名</th>
        <th>最終更新日</th>
    </tr>

    <tbody>
    <tr>
        <td align="center">{{$price_list[0]->price_list_name}}</td>
        <td align="center">
            <?php
            $str = $price_list[0]->updated_at;
            $str = substr($str , 0 , strpos($str , ":")-3);
            $str = explode("-", $str);
            $final = $str[0]."年".$str[1]."月".$str[2]."日";
            echo $final;
            ?>
        </td>
    </tr>
    </tbody>
</table>
<table>
    <tr>
        {{--<th>商品写真</th>--}}
        <th>英文商品名</th>
        <th>ケース入数</th>
        <th>MOQ/Piece</th>
        <th>商品価格</th>
        <th>ケース当たり<br/> Size 縦 (CM)</th>
        <th>ケース当たり<br/> Size 横 (CM)</th>
        <th>ケース当たり<br/> Size 高さ(CM)</th>
        <th>ケース当たり重量</th>
    </tr>
    <tbody>
    @foreach($price_list_detail as $key => $list)
        <tr>
            {{--<td height="100px"><img src="{{'data:image/jpg;base64,'.$list->image}}" width="50px" height="50px"></td>--}}
            <td align="center">{{$list->name_product_en}}</td>
            <td align="center">{{$list->price_list_fob_price}}</td>
            <td align="center">{{$list->price_list_moq}}</td>
            <td align="center">{{$list->price_list_input}}</td>
            <td align="center">{{$list->price_list_length}}</td>
            <td align="center">{{$list->price_list_width}}</td>
            <td align="center">{{$list->price_list_height}}</td>
            <td align="center">{{$list->price_list_packing_weight}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>
