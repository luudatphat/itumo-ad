@extends('layouts.master')
@section('title','価格表作成画面')
@section('sekai_content')
    <div class="container-fluid">
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
        <h1 class="h1 bg-primary text-center h1_list_user">価格表作成画面</h1>

        <div class="alert alert-success message-div" id="message" style="display: none;"></div>

        <form id="myForm">
            @csrf
            <div class="row">
                <div class="col-md-2" style="width: 80px;margin-top: 8px;">
                    <div style="width: 80px;">
                        <label for="email">価格表名</label>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <input type="text" class="form-control" name="name_price_list" placeholder=""
                               value="{{$price_list[0]->price_list_name}}" maxlength="255">
                        <input type="hidden" class="form-control" id="id_price_list" placeholder=""
                               value="{{$price_list[0]->id}}">
                        <input type="hidden" class="form-control" id="no_next" value="{{count($price_list_detail)+1}}">
                        <input type="hidden" class="form-control" id="no_current" placeholder="">
                    </div>
                </div>
            </div>
            <div id="div-price-list-detail">
                <table class="table table-hover table-bordered table-striped" id="price_list_detail">
                    <thead>
                    <tr class="bg-primary text-center">
                        <th class="text-center" width="10">No.</th>
                        <th class="text-center">商品写真</th>
                        <th class="text-center">英文商品名</th>
                        <th class="text-center">ケース入数</th>
                        <th class="text-center">MOQ/Piece</th>
                        <th class="text-center">商品価格</th>
                        <th class="text-center">ケース当たりSize 縦 (CM)</th>
                        <th class="text-center">ケース当たりSize 横 (CM)</th>
                        <th class="text-center">ケース当たりSize 高さ(CM)</th>
                        <th class="text-center">ケース当たり重量</th>
                        <th class="text-center"></th>
                        <th class="text-center"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($price_list_detail as $key => $list)
                        <tr id="product-list-detail-{{$key+1}}">
                            <td class='td-price-list'><span>{{$key+1}}</span><input type="hidden"
                                                                                    value="{{$list->product_id}}"
                                                                                    id="id_product_{{$key+1}}"/><input
                                        type="hidden" id="price_list_detail_id_{{$key+1}}" value="{{$list->id}}"/></td>
                            <td width="100px;" class='td-price-list'><img crossOrigin="Anonymous"
                                                                          src="data:image/jpg;base64,{{$list->image}}"
                                                                          width="100%"/> <input type="hidden"
                                                                                                value="{{$key+1}}"
                                                                                                id="id_value_{{$key+1}}"/>
                            </td>
                            <td width="120px;" class='td-price-list'>
                                <p id="p_name_en_{{$key+1}}">{{$list->name_product_en}}</p>
                                <input type="hidden" class="name" style="width: 150px;" id="name_en_{{$key+1}}"
                                       name="name-en-{{$key+1}}" maxlength="255" value="{{$list->name_product_en}}"/>
                            </td>
                            <td width="50px;" class='td-price-list'>
                                <p id="p_fobprice_{{$key+1}}">{{$list->price_list_fob_price}}</p>
                                <input type="hidden" class="number-digit-{{$key+1}}"
                                                                           style="width:50px;" id="fobprice_{{$key+1}}"
                                                                           name="fobprice-{{$key+1}}"
                                                                           value="{{$list->price_list_fob_price}}"
                                                                           maxlength="11"></td>
                            <td width="80px;" class='td-price-list'>
                                <p id="p_moq_{{$key+1}}">{{$list->price_list_moq}}</p>
                                <input type="hidden" class="number-digit-{{$key+1}}"
                                                                           style="width: 80px;" id="moq__{{$key+1}}"
                                                                           name="moq--{{$key+1}}"
                                                                           value="{{$list->price_list_moq}}" maxlength="11"/>
                            </td>
                            <td width="50px;" class='td-price-list'>
                                <p id="p_price_input_{{$key+1}}">{{$list->price_list_input}}</p>
                                <input type="hidden" class="number-digit-{{$key+1}}"
                                                                           style='width: 50px;'
                                                                           id="price_input_{{$key+1}}"
                                                                           name="price-input-{{$key+1}}"
                                                                           value="{{$list->price_list_input}}"
                                       maxlength="11" /></td>
                            <td width="50px;" class='td-price-list'>
                                <p id="p_length_{{$key+1}}">{{$list->price_list_length}}</p>
                                <input type="hidden" class="number"
                                                                           style="width: 50px;" id="length__{{$key+1}}"
                                                                           name="length--{{$key+1}}"
                                                                           value="{{$list->price_list_length}}"
                                       maxlength="11" /></td>
                            <td width="50px;" class='td-price-list'>
                                <p id="p_with_{{$key+1}}">{{$list->price_list_width}}</p>
                                <input type="hidden" class="number"
                                                                           style="width: 50px;" id="with__{{$key+1}}"
                                                                           name="with--{{$key+1}}"
                                                                           value="{{$list->price_list_width}}"
                                       maxlength="11" /></td>
                            <td width="50px;" class='td-price-list'>
                                <p id="p_height_{{$key+1}}">{{$list->price_list_height}}</p>
                                <input type="hidden" class="number"
                                                                           style="width: 50px;" id="height__{{$key+1}}"
                                                                           name="height--{{$key+1}}"
                                                                           value="{{$list->price_list_height}}"
                                       maxlength="11" /></td>
                            <td width="50px;" class='td-price-list'>
                                <p id="p_packing_weight_{{$key+1}}">{{$list->price_list_packing_weight}}</p>
                                <input type="hidden" class="number-digit-{{$key+1}}"
                                                                           style="width: 50px;"
                                                                           id="packing_weight__{{$key+1}}"
                                                                           name="packing-weight--{{$key+1}}"
                                                                           value="{{$list->price_list_packing_weight}}"
                                       maxlength="11"/></td>
                            <td width="50px;" class='td-price-list'><input
                                        class="btn btn-primary button_create_list_user"
                                        style="width: 50px;display: none;" value="保存" type="submit"
                                        id="save_price_list_detail_{{$key+1}}"
                                        onclick="return set_current_save_no({{$key+1}})"/><input
                                        class="btn btn-primary button_create_list_user" style="width: 50px;" value="編集"
                                        id="edit_price_list_detail_{{$key+1}}"
                                        onclick="edit_price_list_detail({{$key+1}})" type="button"/></td>
                            <td width="50px;" class='td-price-list'><input
                                        class="btn btn-danger button_create_list_user" style="width: 50px;" value="削除"
                                        onclick="delete_product_list_detail({{$key+1}})" type="button"/></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-inline">
                        <a class="btn btn-warning button_create_list_user" href="/price_list">リセット</a>
                        <input class="btn btn-primary button_create_list_user" value="追加" data-toggle="modal"
                               data-target="#myModal2" type="button" onclick="get_product_list()">
                        <input class="btn btn-primary button_create_list_user" value="別の名前で保存" onclick="check_all()" type="button"/>
                    </div>
                    <div class="modal fade" id="myModal2" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title"></h4>
                                    <div class="row col-md-12">
                                        <div class="col-md-2" style="margin-top: 8px;width: 80px;">
                                            <div>
                                                <label for="email">製品名</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="button" class="form-control" placeholder="" value=""
                                                       id="search_string">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row text-center">
                                        <input type="button" class="btn btn-primary button_create_list_user" value="検索"
                                               onclick="get_product_list()">
                                    </div>
                                </div>

                                <div class="modal-body">
                                    <table class="table table-hover table-bordered table-striped" id="product_list">
                                        <thead>
                                        <tr class="bg-primary text-center">
                                            <th class="text-center sel-product-list">Sel</th>
                                            <th class="text-center name-product-list">製品名</th>
                                            <th class="text-center image-product-list">商品写真</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-center" style="margin-bottom: 10px;">
                                    <input type="button" name="reset" value="リセット" class="btn btn-warning"
                                           data-dismiss="modal">
                                    <input name="submit" value="保存" class="btn btn-primary button_create_list_user"
                                           onclick="add_product_detail()" style="width: 80px;" type="button">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="myModal3" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title"></h4>
                                    <div class="row col-md-12">
                                        <div class="col-md-2" style="width: 80px;margin-top: 8px;">
                                            <div style="width: 80px;">
                                                <label for="email">価格表名</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="" value=""
                                                       id="name_price_list_new" name="name_price_list_new"
                                                       onchange="change_name()">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center" style="margin-bottom: 10px;">
                                    <input type="button" name="reset" value="リセット" class="btn btn-warning"
                                           data-dismiss="modal">
                                    <input value="保存" class="btn btn-primary button_create_list_user"
                                           style="width: 80px;" id="save_different" disabled onclick="save_diff_name()" type="button">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#myForm").validate({
                rules: {
                    name_price_list: {
                        required: true
                    },
                },
                messages:{
                    name_price_list: {
                        required: "テキストを入力してください。",
                    }
                },
                submitHandler: function (form) {
                    var no_current = $("#no_current").val();
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    var dt = {
                        '_token': CSRF_TOKEN,
                        'price_list_name': $("input[name='name_price_list']").val(),
                        'id_price_list': $("#id_price_list").val(),
                        'price_list_detail': get_list_detail_price(no_current)
                    };
                    $.ajax({
                        type: "POST",
                        url: "/create_new_price_list_detail",
                        data: dt,
                        dataType: "json",
                        success: function (data) {
                            $("#id_price_list").val(data.result.price_list_id);
                            $("#price_list_detail_id_" + no_current).val(data.result.price_list_detail_id);
                            $("#message").text(data.result.msg);
                            $("#message").css("display", "block");
                            $('#message').delay(3000).slideUp();
                            $("#p_name_en_" + no_current).show();
                            $("#p_name_en_" + no_current).text($("#name_en_" + no_current).val());
                            $("#name_en_" + no_current).attr("type", "hidden");

                            $("#p_fobprice_" + no_current).show();
                            $("#p_fobprice_" + no_current).text($("#fobprice_" + no_current).val());
                            $("#fobprice_" + no_current).attr("type", "hidden");

                            $("#p_moq_" + no_current).show();
                            $("#p_moq_" + no_current).text($("#moq__" + no_current).val());
                            $("#moq__" + no_current).attr("type", "hidden");

                            $("#p_price_input_" + no_current).show();
                            $("#p_price_input_" + no_current).text($("#price_input_" + no_current).val());
                            $("#price_input_" + no_current).attr("type", "hidden");

                            $("#p_length_" + no_current).show();
                            $("#p_length_" + no_current).text($("#length__" + no_current).val());
                            $("#length__" + no_current).attr("type", "hidden");

                            $("#p_with_" + no_current).show();
                            $("#p_with_" + no_current).text($("#with__" + no_current).val());
                            $("#with__" + no_current).attr("type", "hidden");

                            $("#p_height_" + no_current).show();
                            $("#p_height_" + no_current).text($("#height__" + no_current).val());
                            $("#height__" + no_current).attr("type", "hidden");

                            $("#p_packing_weight_" + no_current).show();
                            $("#p_packing_weight_" + no_current).text($("#packing_weight__" + no_current).val());
                            $("#packing_weight__" + no_current).attr("type", "hidden");

                            $("#save_price_list_detail_" + no_current).css("display", "none");
                            $("#edit_price_list_detail_" + no_current).css("display", "");
                        }
                    });
                }
            });
        });
        var dt = [];
        /*clear table product list and check box checked*/
        $(".button_create_list_user").click(function () {
            var table = $('#product_list').DataTable();
            table.clear().draw();
            table.destroy();
            dt = [];
        });
        /*end clear table product list*/

        /*get search product list*/
        function get_product_list() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var dt = {
                'id': get_list_id(),
                'search_string': $("#search_string").val(),
                '_token': CSRF_TOKEN
            };
            $.ajax({
                type: "POST",
                url: "/get_product_list",
                data: dt,
                dataType: "json",
                beforeSend : function () {
                    $(".modal-content").css('cursor', 'progress');
                },
                complete: function () {
                    $(".modal-content").css('cursor', 'default');
                },
                success: function (data) {
                    if(data.msg!=""){
                        alert(data.msg);
                    }
                    else{
                        var output = "";
                        $.each(data.result, function (key, row) {
                            output += "<tr>";
                            output += "<td><input type='checkbox' onchange='change_check_box(" + (key + 1) + ")' id='check-box-" + (key + 1) + "'/> <input type='hidden' id='product-id-" + (key + 1) + "' value='" + row['id'] + "' /><input type='hidden' id='moq_" + (key + 1) + "' value='" + row['moq'] + "' /><input type='hidden' id='fob_price_" + (key + 1) + "' value='" + row['fob_price'] + "' /><input type='hidden' id='length_" + (key + 1) + "' value='" + row['length'] + "' /><input type='hidden' id='width_" + (key + 1) + "' value='" + row['width'] + "' /><input type='hidden' id='height_" + (key + 1) + "' value='" + row['height'] + "' /><input type='hidden' id='packing_weight_" + (key + 1) + "' value='" + row['packing_weight'] + "' /></td>";
                            output += "<td class='text-center'>" + row['product_name'] + "</td>";
                            output += "<td class='text-center' width='300'> <input type='hidden' id='product-img-" + (key + 1) + "' value='" + row['product_image_main'] + "'/><img crossOrigin='Anonymous' src='data:image/jpg;base64," + row['product_image_main'] + "' width='10%'/></td>";
                            output += "</tr>";
                        });
                        $("#product_list > tbody").html(output);
                        if ($("#product_list > tbody > tr").length > 0) {
                            $('#product_list').DataTable({
                                "searching": false,
                                "bInfo": false,
                                'retrieve': true,
                                "bLengthChange": false,
                                "pageLength": 5
                            });
                        }
                    }
                }
            });
        }
        /*end get search product list*/

        /*get id and image product*/
        function change_check_box(key) {
            var list = {
                "id": $("#product-id-" + key).val(),
                "image": $("#product-img-" + key).val(),
                "moq": $("#moq_" + key).val(),
                'fob_price': $("#fob_price_" + key).val(),
                'length': $("#length_" + key).val(),
                'width': $("#width_" + key).val(),
                'height': $("#height_" + key).val(),
                'packing_weight': $("#packing_weight_" + key).val()
            };

            if ($("#check-box-" + key).prop("checked")) {
                dt[key] = list;
            }
            else {
                delete dt[key];
            }
        }
        /*end get id and image product*/

        /*add product detail*/
        function add_product_detail() {
            var output = "";
            var no_next = parseInt($("#no_next").val());
            if (no_next) {
                no = no_next;
            }
            else {
                no = 1;
            }
            var length = $("#price_list_detail >tbody >tr").length;
            dt.forEach(function (row, key) {
                if (length >= 50) {
                    $("#message").text("50製品の制限");
                    $("#message").css("display", "block");
                    $('#message').delay(3000).slideUp();
                    $("#myModal2").modal('hide');
                    return false;
                }
                output += "<tr id='product-list-detail-" + no + "'>";
                output += "<td class='td-price-list'><span>" + (length + 1) + "</span><input type='hidden' value='" + row['id'] + "' id='id_product_" + no + "'/><input type='hidden'  id='price_list_detail_id_" + no + "'/></td>";
                output += "<td width='100px;' class='td-price-list'><img crossOrigin='Anonymous' src='data:image/jpg;base64," + row['image'] + "' width='100%'/><input type='hidden' value='" + no + "' id='id_value_" + no + "'/></td>";
                output += "<td width='120px;' class='td-price-list'><p id='p_name_en_" + no + "'></p><input type='text' class='name name-en-" + no + "' style='width: 150px;' id='name_en_" + no + "' name='name-en-" + no + "' maxlength='255'/></td>";
                if(row['fob_price']&&row['fob_price']!='null'){
                    output += "<td width='50px;' class='td-price-list'><p id='p_fobprice_" + no + "'></p><input type='text' class='number-digit-" + no + "' style='width:50px;' id='fobprice_" + no + "' name='fobprice-" + no + "' value='" + row['fob_price'] + "' maxlength='11'/></td>";
                }
                else{
                    output += "<td width='50px;' class='td-price-list'><p id='p_fobprice_" + no + "'></p><input type='text' class='number-digit-" + no + "' style='width:50px;' id='fobprice_" + no + "' name='fobprice-" + no + "' value='0' maxlength='11'/></td>";
                }
                if(row['moq']&&row['moq']!='null'){
                    output += "<td width='80px;' class='td-price-list'><p id='p_moq_" + no + "'></p><input type='text' class='number-digit-" + no + "' style='width: 80px;' id='moq__" + no + "' name='moq--" + no + "' value='" + row['moq'] + "' maxlength='11' /></td>";
                }
                else{
                    output += "<td width='80px;' class='td-price-list'><p id='p_moq_" + no + "'></p><input type='text' class='number-digit-" + no + "' style='width: 80px;' id='moq__" + no + "' name='moq--" + no + "' value='0' maxlength='11' /></td>";
                }
                output += "<td width='50px;' class='td-price-list'><p id='p_price_input_" + no + "'></p><input type='text' class='number-digit-" + no + "' style='width: 50px;' id='price_input_" + no + "' name='price-input-" + no + "' maxlength='11'/></td>";
                if(row['length']&&row['length']!='null'){
                    output += "<td width='50px;' class='td-price-list'><p id='p_length_" + no + "'></p><input type='text' class='number number-" + no + "' style='width: 50px;' id='length__" + no + "' name='length--" + no + "' value='" + row['length'] + "' maxlength='11'/></td>";
                }
                else{
                    output += "<td width='50px;' class='td-price-list'><p id='p_length_" + no + "'></p><input type='text' class='number number-" + no + "' style='width: 50px;' id='length__" + no + "' name='length--" + no + "' value='0' maxlength='11'/></td>";
                }
                if(row['width']&&row['width']!='null'){
                    output += "<td width='50px;' class='td-price-list'><p id='p_with_" + no + "'></p><input type='text' class='number number-" + no + "' style='width: 50px;' id='with__" + no + "' name='with--" + no + "' value='" + row['width'] + "' maxlength='11'/></td>";
                }
                else{
                    output += "<td width='50px;' class='td-price-list'><p id='p_with_" + no + "'></p><input type='text' class='number number-" + no + "' style='width: 50px;' id='with__" + no + "' name='with--" + no + "' value='0' maxlength='11'/></td>";
                }
                if(row['height']&&row['height']!='null'){
                    output += "<td width='50px;' class='td-price-list'><p id='p_height_" + no + "'></p><input type='text' class='number number-" + no + "' style='width: 50px;' id='height__" + no + "' name='height--" + no + "' value='" + row['height'] + "' maxlength='11'/></td>";
                }             else{
                    output += "<td width='50px;' class='td-price-list'><p id='p_height_" + no + "'></p><input type='text' class='number number-" + no + "' style='width: 50px;' id='height__" + no + "' name='height--" + no + "' value='0' maxlength='11'/></td>";
                }
                if(row['packing_weight']&&row['packing_weight']!='null'){
                    output += "<td width='50px;' class='td-price-list'><p id='p_packing_weight_" + no + "'></p><input type='text' class='number-digit-" + no + "' style='width: 50px;' id='packing_weight__" + no + "' name='packing-weight--" + no + "' value='" + row['packing_weight'] + "' maxlength='11'/></td>";
                }
                else{
                    output += "<td width='50px;' class='td-price-list'><p id='p_packing_weight_" + no + "'></p><input type='text' class='number-digit-" + no + "' style='width: 50px;' id='packing_weight__" + no + "' name='packing-weight--" + no + "' value='0' maxlength='11'/></td>";
                }
                output += "<td width='50px;' class='td-price-list'><input class='btn btn-primary button_create_list_user' style='width: 50px;'  value='保存' type='submit' onclick='return set_current_save_no(" + no + ")' id='save_price_list_detail_" + no + "'/><input class='btn btn-primary button_create_list_user' style='width: 50px;display: none;'  value='編集' id='edit_price_list_detail_" + no + "' onclick='edit_price_list_detail(" + no + ")' type='button'/></td>";
                output += "<td width='50px;' class='td-price-list'><input class='btn btn-danger button_create_list_user' style='width: 50px;'  value='削除' onclick='delete_product_list_detail(" + no + ")' type='button'/></td>";
                output += "</tr>";
                no = no + 1;
                length = length + 1;
            });
            $("#price_list_detail >tbody").append(output);
            $("#no_next").val(no);
            $("#myModal2").modal('hide');
        }
        /*end add product detail*/

        /*product list detail delete*/
        function delete_product_list_detail(no) {
            var r = confirm("削除してもよろしいですか？");
            if (r == true) {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                var id_price_list_detail = $("#price_list_detail_id_" + no).val();
                var id_price_list = $("#id_price_list").val();
                var n_table = $("#price_list_detail > tbody > tr").length;
                if (id_price_list) {
                    var dt = {
                        'id': id_price_list_detail,
                        'length_table': n_table,
                        'price_list_id': id_price_list,
                        '_token': CSRF_TOKEN
                    };
                    $.ajax({
                        type: "POST",
                        url: "/delete_price_list_detail",
                        data: dt,
                        dataType: "json",
                        success: function (data) {
                            if (id_price_list_detail) {
                                $("#message").text(data.result.msg);
                                $("#message").css("display", "block");
                                $('#message').delay(3000).slideUp();
                            }
                        }
                    });
                }
                if (n_table == 1) {
                    location.href = "/price_list";
                }
                $("#product-list-detail-" + no).remove();
                var no_1 = 1;
                $("#price_list_detail > tbody > tr").each(function () {
                    var id_tr = $(this).attr("id");
                    $("#" + id_tr + " > td:nth-child(1) > span").text(no_1);
                    no_1 = no_1 + 1;
                });
            }
        }
        /*end product list detail delete*/

        /*get list id product list detail*/
        function get_list_id() {
            var id_list = "";
            $("#price_list_detail > tbody > tr").each(function () {
                var id_tr = $(this).attr("id");
                id_list = id_list + $("#" + id_tr + " > td:nth-child(1) > input").val() + ",";
            });
            return id_list;
        }
        /*end get list id product list detail*/

        /*change button after save*/
        function edit_price_list_detail(no) {
            $("#p_name_en_" + no).hide();
            $("#name_en_" + no).attr("type", "text");
            $("#p_fobprice_" + no).hide();
            $("#fobprice_" + no).attr("type", "text");
            $("#p_moq_" + no).hide();
            $("#moq__" + no).attr("type", "text");
            $("#p_price_input_" + no).hide();
            $("#price_input_" + no).attr("type", "text");
            $("#p_length_" + no).hide();
            $("#length__" + no).attr("type", "text");
            $("#p_with_" + no).hide();
            $("#with__" + no).attr("type", "text");
            $("#p_height_" + no).hide();
            $("#height__" + no).attr("type", "text");
            $("#p_packing_weight_" + no).hide();
            $("#packing_weight__" + no).attr("type", "text");
            $("#save_price_list_detail_" + no).css("display", "");
            $("#edit_price_list_detail_" + no).css("display", "none");
        }
        /*end change button after save*/

        /*get current no save */
        function set_current_save_no(no) {
            jQuery.validator.addMethod("required", jQuery.validator.methods.required,
                    "テキストを入力してください。");
            $("#no_current").val(no);
            /*check name -en*/
            jQuery.validator.addClassRules('name-en-' + no, {
                required: true /*,
                 other rules */
            });
            /*check name -en*/
            /*check number float and required*/
            jQuery.validator.addClassRules('number-' + no, {
                required: true,
                number: true
                /*other rules */
            });
            /*check number float and required*/
            /*check number digit and required*/
            jQuery.validator.addClassRules('number-digit-' + no, {
                required: true,
                digits: true
                /*other rules */
            });
            /*check number digit and required*/
            return true;
        }
        /*end get current no save */

        /*get data price list detail of 1 row*/
        function get_list_detail_price(no) {
            var dt = {
                'price_list_detail_id': $("#price_list_detail_id_" + no).val(),
                'product_id': $("#id_product_" + no).val(),
                'name_product_en': $("#name_en_" + no).val(),
                'price_list_fob_price': $("#fobprice_" + no).val(),
                'price_list_moq': $("#moq__" + no).val(),
                'price_list_input': $("#price_input_" + no).val(),
                'price_list_length': $("#length__" + no).val(),
                'price_list_width': $("#with__" + no).val(),
                'price_list_height': $("#height__" + no).val(),
                'price_list_packing_weight': $("#packing_weight__" + no).val()
            };
            return dt;
        }
        /*end get data price list detail of 1 row*/

        /*show modal save name difference*/
        function check_all() {
            $("#myModal3").modal('show');
        }
        /*end show modal save name difference*/

        /*remove disabled button save and add*/
        function change_name() {
            var name = $("#name_price_list_new").val();
            if (name != "") {
                $("#save_different").removeAttr('disabled');
            }
            else {
                $("#save_different").attr('disabled', true);
            }
        }
        /*end remove disabled button save and add*/

        function save_diff_name() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var dt = {
                '_token': CSRF_TOKEN,
                'new_price_list_name': $("#name_price_list_new").val(),
                'id_price_list': $("#id_price_list").val()
            };
            $.ajax({
                type: "POST",
                url: "/save_diff_name",
                data: dt,
                dataType: "json",
                success: function (data) {
                    if (data.id_price_list) {
                        location.href = "/edit_price_list_detail/" + data.id_price_list;
                    }
                }
            });
        }
    </script>
@endsection