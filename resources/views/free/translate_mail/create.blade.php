@extends('layouts.master')
@section('title','翻訳依頼')
@section('sekai_content')
<section>
    <div class="popup_part_1" style="width: 100%;opacity: 0.9;top: 20px;left: 0;position: absolute;background-color: #313131;z-index: 999;">    
        <div style="position: absolute;left: 5%;top: 35%;color: black;font-weight: bold;font-size: 30px;width: 90%;text-align:center;">
            <p>こちらの機能は有料プランに切り替えると利用することができます</p>
        </div>
    </div>
    <header><h2>翻訳依頼</h2></header>
    @if(session('message'))
        <div class="alert alert-success alert_success_company_japan">{{session('message')}}</div>
    @endif
    <article>
        <form class="" method="post" action="">
            @csrf
        <div class="title-control title-create-translate">コンテンツをリクエストする</div>
        <textarea disabled class="form-control" name="content_required" id="content_required" rows="10" placeholder="コンテンツを入力してください"></textarea>
        @if ($errors->has('content_required'))
            <span class="text-danger">
                <strong>{{ $errors->first('content_required') }}</strong>
            </span>
        @endif
        <div class="content-2col mgT-25">
            <div class="a-col">
                <div class="title-create-translate">文字数カウント</div>
                <div>
                    <input disabled class="form-control" name="number_character" id="number_character" value="" readonly>
                </div>
            </div>
            <div class="a-col">
                <div class="title-create-translate">翻訳費用（税抜き）</div>
                <div>
                    <input disabled class="form-control" name="translate_price" id="translate_price" value="" readonly>
                </div>
            </div>
        </div>
        <div class="border-create-translate"></div>
        <div class="text-center mgT-27 mgB-40">
            <a disabled href="{{url('/free/translate-list')}}"><button disabled type="button" class="back-translate">キャンセル</button></a>
            <input type="submit" disabled name="btnSave" class="save-translate" value="セーブ">
            <input type="submit" disabled name="btnRequest" class="submit-translate" value="送信する">
        </div>
        </form>
    </article>
</section>

<script>
$(document).ready(function () {
    var _height_part_1 = jQuery( 'section' ).height();
    jQuery( '.popup_part_1' ).css( 'height', _height_part_1 + 25);
    $('#content_required').bind("keyup change mouseup mousedown paste",function () {
        var elem = $(this);
        setTimeout(function() {
            var content = elem.val().trim();
            $('#number_character').val(content.length);
            $('#translate_price').val(content.length * {{env('TRANSLATE_PRICE_PER_WORD')}})
        }, 100);
    });
})
</script>
@endsection