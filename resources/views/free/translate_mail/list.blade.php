@extends('layouts.master')
@section('title','翻訳一覧')
@section('sekai_content')
<meta name="_token" content="{{ csrf_token() }}">

<section>
    <header><h2>翻訳一覧</h2></header>
    <article>
        @if(Session::has('flash_messages'))
            <div class="alert alert-{!! Session::get('flash_level') !!} alert_success_company_japan">
                {!! Session::get('flash_messages') !!}
            </div>
        @endif
        <form class="form-sort form-sort-01 fix-form-01" action="{{ url('/free/translate-list') }}" method="POST">
            @csrf
            <div class="content-group-control">
                <div class="clearfix bottom-form pdT-0">
                    <div class="pull-left">
                        <span class="title-control">キーワード</span>
                        <input class="input-last fix-w-input-01" type="text" placeholder="キーワードを入力して下さい" name="search" id="search" value="{{ isset($search)?$search:'' }}">
                        <span class="text-center"><button class="fix-width-btn-05" type="submit">検索</button></span>
                    </div>

                    <div class="text-center pull-right"><a href="{{url('/free/create-translate-request')}}" class="btn btn-primary fix-width-btn-06">翻訳要求を作成する</a></div>
                </div>
            </div>
            <!-- end .content-group-control -->
        </form>

        <table class="content-list-table-02 fix-table-01 result-table fix-table-02">
            <thead>
            <tr>
                <th id="id" class="text-center col-sort w-100">NO.</th>
                <th id="created_at" class="text-center col-sort" style="width: 170px;">日付を送信します</th>
                <th class="text-center" style="min-width: 450px;">必要なコンテンツ</th>
                <th class="text-center" style="min-width: 450px;">コンテンツ結果</th>
                <th class="text-center fix-width">ステータス</th>
                <th class="text-center" style="width: 200px">アクション</th>
            </tr>
            </thead>

            <tbody>
            @foreach($translate_list as $translate)
                <tr>
                    <td>{{ $translate->id }}</td>
                    <td>{{ $translate->created_at }}</td>
                    <td>{!! nl2br(e($translate->content_required)) !!}</td>
                    <td>
                        @if($translate->status == config('constants.STATUS_TRANSLATE_OK'))
                            {!! nl2br(e($translate->content_result)) !!}
                        @endif
                    </td>
                    <td>
                        @if(isset($translate->status))
                            @if($translate->status == config('constants.STATUS_TRANSLATE_OK'))
                                {{ '翻訳完了' }}
                            @elseif($translate->status == config('constants.STATUS_TRANSLATE_WAITING_TRANSLATE'))
                                {{ '翻訳依頼中' }}
                            @elseif($translate->status == config('constants.STATUS_TRANSLATE_NOT_YET_REQUEST'))
                                {{ '編集中' }}
                            @endif
                        @endif
                    </td>
                    <td class="text-center" style="vertical-align: middle">
                        @if($translate->status == config('constants.STATUS_TRANSLATE_NOT_YET_REQUEST'))
                            <form action="{{url('/free/delete-translate-request')}}" method="POST">
                                <a class="btn btn-primary fix-btn-01" href="{!! url('/free/translate-detail/'.$translate->id) !!}">詳細</a>
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="translate_id" value="{{$translate->id}}" >
                                <input type="submit" class="btn btn-danger fix-width-btn-03" name="btnDelete" value="削除" onclick="return confirm_delete_translate()">
                            </form>
                        @else
                            <a class="btn btn-primary fix-btn-01" href="{!! url('/free/translate-detail/'.$translate->id) !!}">詳細</a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <nav class="text-right total-records pagination_div show_record">
            <p class="text-right">Records {{ ($translate_list->currentPage() -1) * $translate_list->perPage() + 1 }} -
                {{ ($translate_list->lastPage() == $translate_list->currentPage()) ? $translate_list->total() : ($translate_list->currentPage() * $translate_list->perPage()) }}
                of {{ $translate_list->total() }}</p>
            {{$translate_list->links('pagination::bootstrap-4')}}
        </nav>

    </article>
</section>

<script type="text/javascript">
    $('.col-sort').click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
//        $('.col_sort').css({'background': '#3097D1'});
//        $(this).css({'background': '#3097D1'});
//        $('.col_sort').children('.fa').css({'display':'inline'});
        var page = {{$current_page}};
        var search = $('#search').val();
        var status = $('#status').val();
        if($(this).hasClass('down')) {
            $(this).removeClass('down');
            $(this).children('.fa-caret-down').css({'display': 'none'});
            $(this).children('.fa-caret-up').css({'display': 'inline'});
            //Sort ASC
            var current_token = '{{csrf_token()}}';
            var column = $(this).attr('id');
            $('#column_dl').val(column);
            $('#sort_dl').val("asc");
            $.ajax({
                url: '/translate-sort?page=' + page,
                dataType: 'text',
                type: 'post',
                contentType: 'application/x-www-form-urlencoded',
                data: {search: search, status: status, column: column, sort: 'asc', current_page: page,  fuel_csrf_token: current_token},
                success: function( data, textStatus, jQxhr ){
                    var result = JSON.parse(data);
                    $('.result-table tbody').html(result['content']);
                    $('.pagination_div').html(result['pagination']);
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                }
            });

        }else {
            $(this).addClass('down');
            $(this).children('.fa-caret-up').css({'display': 'none'});
            $(this).children('.fa-caret-down').css({'display': 'inline'});
            //Sort DESC
            var current_token = '{{csrf_token()}}';
            var column =$(this).attr('id');
            $('#column_dl').val(column);
            $('#sort_dl').val("desc");
            $.ajax({
                url: '/translate-sort?page=' + page,
                dataType: 'text',
                type: 'post',
                contentType: 'application/x-www-form-urlencoded',
                data: {search: search, status: status,  column: column, sort: 'desc', current_page: page ,  fuel_csrf_token: current_token},
                success: function( data, textStatus, jQxhr ){
                    var result = JSON.parse(data);
                    $('.result-table tbody').html(result['content']);
                    $('.pagination_div').html(result['pagination']);
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                }
            });
        }
    });
    function confirm_delete_translate () {
        var result = confirm("{{trans('messages.delete_translate_request_confirm')}}");
        if (result) {
            return true;
        }
        return false;
    }

    $('.col-sort').click(function (e) {
        e.preventDefault();
        if($(this).hasClass('select-sort')) {
            $(this).removeClass('select-sort');
            return false;
        }
        $('.col-sort').removeClass('select-sort');
        $(this).addClass('select-sort');
    })

</script>
@endsection