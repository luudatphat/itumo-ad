@extends('layouts.nosidebar')
@section('title', '法人登録')
@section('sekai_content')
<h2 class="h2-01">法人登録</h2>
<form class="form-content-01" role="form" method="POST" action="{{ url('/register') }}">
    @csrf
    <table class="content-control-01">
        <tr>
            <td><label for="email">貴社名 (日本語)</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="text" name="company_name_jp" id="name" value="{{ old('company_name_jp') }}"  autofocus>
                @if ($errors->has('company_name_jp'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('company_name_jp') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="representative_mobile_number">代表電話番号</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="text" name="representative_mobile_number" id="representative_mobile_number" value="{{ old('representative_mobile_number') }}"  autofocus>
                @if ($errors->has('representative_mobile_number'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('representative_mobile_number') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="representative_mobile_number">代表メールアドレス / ID</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="text" name="email" id="email" value="{{ old('email') }}"  autofocus>
                @if ($errors->has('email'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <!-- an_tnh_sc_94_sc_151_start -->
            <td><label for="company_website">ホームページ</label></td>
            <!-- an_tnh_sc_94_sc_151_end -->
            <td>
                <input type="text" name="company_website" id="company_website" value="{{ old('company_website') }}"  autofocus>
                @if ($errors->has('company_website'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('company_website') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="contact_name_kanji">ご担当者名</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="text" name="contact_name_kanji" id="contact_name_kanji" value="{{ old('contact_name_kanji') }}"  autofocus>
                @if ($errors->has('contact_name_kanji'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('contact_name_kanji') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="contact_name_kanji">招待コード</label></td>
            <td>
                <input type="number" name="invitation_code" id="invitation_code" value="{{ old('invitation_code') }}"  autofocus>
                @if ($errors->has('invitation_code'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('invitation_code') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="password">パスワード</label><span class="text-danger mg-l-22">6文字以上にしてください</span></td>
            <td>
                <input type="password" name="password" id="password"  autofocus>
                @if ($errors->has('password'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td><label for="password_confirmation">再パスワード</label><span class="text-danger mg-l-22">必須</span></td>
            <td>
                <input type="password" name="password_confirmation" id="password_confirmation"  autofocus>
                @if ($errors->has('password_confirmation'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </td>
        </tr>
    </table>
    <center>
        <p><b><a href="{{ url('/terms-of-service') }}" target="_blank">セカイコネクト利用規約</a>の内容をご確認の上、「同意する」チェックボックスにチェックを入れてください。</b></p>
        <div class="checkbox">
          <label onclick="disabled_submit()"><input id="_cb" style="width:15px;height:15px;" type="checkbox" value=""><b>セカイコネクト利用規約に同意する</b></label>
        </div>
    </center>
    <input type="hidden" name="register_free" value="register_free" />
    <center>
        <input class="btn btn-primary button_create_list_user" disabled value="登録" data-toggle="modal" style="width:100px;" data-target="#myModal2" type="button">
    </center>
    <div class="modal fade" id="myModal2" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">こちらの内容で登録してよいですか？</h4>
                </div>
                <div class="text-center" style="margin-bottom: 10px;margin-top:10px;">
                    <button type="submit" class="btn btn-primary" style="width:15%; min-height:40px;height:40px;">OK</button>
                    <input type="button" value="キャンセル" style="width:15%;padding:0px;" class="btn btn-warning" data-dismiss="modal">
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    function disabled_submit() {
        if ( jQuery('#_cb').is(":checked") ) {
            jQuery( '.button_create_list_user' ).removeAttr( 'disabled' );
        } else {
            jQuery( '.button_create_list_user' ).attr( 'disabled', 'disabled' );
        }
    }
</script>
@endsection
