@extends('layouts.master')
@section('title','アタックリスト')
@section('sekai_content')
    <style>
        h2 { margin-top: 10px; border-left: none; }
    </style>
    <section>
        <h2><b>デモの情報を表示しています</b></h2>
        <header><h2>アタックリスト</h2></header>
        <article>
            <form class="form-sort form-sort-01" method="post" action="{{url('free/attack_list')}}">
                @csrf
                <div class="content-group-control">
                                <span class="fix-display fix-mg-r-01">
                                    <span class="title-control">依頼日</span>
                                    <span class="calender-picker">
                                        <input type="text" name="from_date" id="from_date" value="{{isset($from_date) ? $from_date : ''}}">
                                    </span> - <span class="calender-picker">
                                        <input type="text" name="to_date" value="{{isset($to_date) ? $to_date: ''}}" id="to_date">
                                        </span>
                                </span>
                                <span class="fix-display fix-mg-r-02">
                                    <span class="title-control">キーワード</span>
                                    <input class="input-last"  type="text" id="search" name="search" value="{{isset($search) ? $search : ''}}" placeholder="キーワードを入力して下さい">
                                </span>
                                <span class="fix-display">
                                    <span class="title-control">状態</span>
                                    <select name="status" id="status">
                                        <option value="">ステータスを選択する</option>
                                        @foreach($mtb_attack_status as $item)
                                            <option {{$status == $item->id ? 'selected' : ''}} value="{{$item->id}}">{{$item->content}}</option>
                                        @endforeach
                                    </select>
                                </span>
                </div>
                <div class="text-center bottom-form"><button type="submit">検索</button></div>
                <!-- end .content-group-control -->
            </form>

            <table class="content-list-table-02 fix-table-01 result-table">
                <thead>
                <tr>
                    <!-- <th class="col-sort" id="request_id">レコード番号</th> -->
                    <th class="fix-col-btn">詳細</th>
                    <th class="fix-col-btn">ステータス</th>
                    <!-- <th class="col-sort fix-col-date" id="requests.date">依頼日</th> -->
                    <th class="col-sort fix-col-date" id="requests.expire_date">提案納期</th>
                    <th class="col-sort" id="buyers.country">国</th>
                    <th class="col-sort" id="buyers.sale_country">企業名</th>
                    <th class="col-sort fix-width-product-description" id="mtb_genres.content">調達依頼内容</th>
                    <th class="fix-col-checkbox">代理店契約</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($attack_list as $attack)
                    <tr>
                        <!-- <td>{{$attack->request->id}}</td> -->
                        <td>
                            <a href="/free/buyer_detail/{{$attack->id}}"><button class="btn-add">詳細</button></a>
                        </td>
                        <td>
                            {{$attack->mtb_attack_status->content}}
                        </td>
                        <!-- <td>{{date_format(date_create($attack->request->date),'Y-m-d')}}</td> -->
                        <td>{{date_format(date_create($attack->request->expire_date),'Y-m-d')}}</td>
                        <td>{{$attack->request->buyer->countries->country_name}}</td>
                        <td>{{ isset($attack->request->buyer->company_name)?$attack->request->buyer->company_name:'' }}</td>
                        <td>{{$attack->request->product_description_jp}}</td>
                        <td class="text-center">
                            <input name="status_agency" class="a_id" type="checkbox" value="{{ $attack->id }}" {{ ($attack->status_agency == 1) ? "checked" : "" }} >
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="9">データなし。</td>
                    </tr>
                @endforelse
                </tbody>
            </table>

            <nav class="text-right total-records pagination_div show_record">
                <p class="text-right">Records {{ ($attack_list->currentPage() -1) * $attack_list->perPage() + 1 }} -
                    {{ ($attack_list->lastPage() == $attack_list->currentPage()) ? $attack_list->total() : ($attack_list->currentPage() * $attack_list->perPage()) }}
                    of {{ $attack_list->total() }}</p>
                {{$attack_list->links('pagination::bootstrap-4')}}
            </nav>

        </article>
    </section>
    <script>
        $(document).ready(function () {
            //$('#from_date,#to_date').datepicker();

            $('body').delegate('.a_id','change',(function () {
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });
                var a_id = $(this).val();
                $.ajax({
                    url: '/change_status_agency',
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    data: {
                        attack_id: a_id,
                        _token: "{{csrf_token()}}"
                    },
                    success: function( data, textStatus, jQxhr ){
                        // var result = JSON.parse(data);
//                        $('.result-table tbody').html(data);
                        $('.result-table tbody #status_agency').html(data);

                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        console.log( errorThrown );
                    }
                });
            }));

            $('.col-sort').click(function(){
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });
                var page = {{$current_page}};
                var search = $('#search').val();
                var status = $('#status').val();
                var from_date = $('#from_date').val();
                var to_date = $('#to_date').val();
                var column = $(this).attr('id');

                if($(this).hasClass('down')) {
                    $(this).removeClass('down');
                    $(this).children('.fa-caret-down').css({'display': 'none'});
                    $(this).children('.fa-caret-up').css({'display': 'inline'});
                    //Sort ASC
                    $('#column_dl').val(column);
                    $('#sort_dl').val("asc");
                    $.ajax({
                        url: '/free/list_attack_sort?page=' + page,
                        dataType: 'text',
                        type: 'post',
                        contentType: 'application/x-www-form-urlencoded',
                        data: {
                            search: search,
                            status: status,
                            from_date: from_date,
                            to_date: to_date,
                            column: column,
                            sort: 'asc',
                            current_page: page,
                            _token: "{{csrf_token()}}"
                        },
                        success: function( data, textStatus, jQxhr ){
                            var result = JSON.parse(data);
//                        $('.result-table tbody').html(data);
                            $('.result-table tbody').html(result['content']);
                            $('.pagination_div').html(result['pagination']);
                        },
                        error: function( jqXhr, textStatus, errorThrown ){
                            console.log( errorThrown );
                        }
                    });

                }else {
                    $(this).addClass('down');
                    $(this).children('.fa-caret-up').css({'display': 'none'});
                    $(this).children('.fa-caret-down').css({'display': 'inline'});
                    //Sort DESC
                    $('#column_dl').val(column);
                    $('#sort_dl').val("desc");
                    $.ajax({
                        url: '/free/list_attack_sort?page=' + page,
                        dataType: 'text',
                        type: 'post',
                        contentType: 'application/x-www-form-urlencoded',
                        data: {
                            search: search,
                            status: status,
                            from_date: from_date,
                            to_date: to_date,
                            column: column,
                            sort: 'desc',
                            current_page: page,
                            _token: "{{csrf_token()}}"
                        },
                        success: function( data, textStatus, jQxhr ){
                            var result = JSON.parse(data);
                            $('.result-table tbody').html(result['content']);
                            $('.pagination_div').html(result['pagination']);
                        },
                        error: function( jqXhr, textStatus, errorThrown ){
                            console.log( errorThrown );
                        }
                    });
                }
            });
        });
    </script>

    <script type="application/javascript">
        $(document).ready(function (e) {
            'use strict';
//            $('.calender-picker input').datepicker({
//                showOn: "button",
//                buttonImage: "images/icon_datepicker.png",
//                buttonImageOnly: true,
//                buttonText: "Select date"
//            });
            var _url = window.location.host;
            var dateFormat = "mm/dd/yy",
                    from = $( "#from_date" )
                            .datepicker({
                                defaultDate: "+1w",
                                changeMonth: true,
                                showOn: "button",
                                buttonImage: 'http://' + _url + "/images/icon_datepicker.png",
                                buttonImageOnly: true,
                                buttonText: "Select date"
                            })
                            .on( "change", function() {
                                to.datepicker( "option", "minDate", getDate( this ) );
                            }),
                    to = $( "#to_date" ).datepicker({
                                defaultDate: "+1w",
                                changeMonth: true,
                                showOn: "button",
                                buttonImage: 'http://' + _url + "/images/icon_datepicker.png",
                                buttonImageOnly: true,
                                buttonText: "Select date"
                            })
                            .on( "change", function() {
                                from.datepicker( "option", "maxDate", getDate( this ) );
                            });

            function getDate( element ) {
                var date;
                try {
                    date = $.datepicker.parseDate( dateFormat, element.value );
                } catch( error ) {
                    date = null;
                }

                return date;
            }

            $('.col-sort').click(function (e) {
                e.preventDefault();
                if($(this).hasClass('select-sort')) {
                    $(this).removeClass('select-sort');
                    return false;
                }
                $('.col-sort').removeClass('select-sort');
                $(this).addClass('select-sort');
            })
        });
    </script>
@endsection