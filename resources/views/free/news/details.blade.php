@extends('layouts.master')
@section('title','ニュース詳細')
@section('sekai_content')

    <section>
        <header class="title-top">カリキュラム　営業</header>
        <article>
            <div class="content-post">
                <div class="ql-editor">
                    <h1>@php echo isset($detail_news[0])?$detail_news[0]->title:''; @endphp</h1>
                    @php echo isset($detail_news[0])?$detail_news[0]->content:''; @endphp
                </div>
                <!-- end .ql-editor -->
            </div>
            <!-- end .content-post -->
        </article>
    </section>
    <script>
        $('p > s, p > u').parent().addClass('quote');
    </script>
@endsection