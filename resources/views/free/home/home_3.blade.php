
<div class="content-charts-01">

<div class="title-chart-01 clearfix">活動推移
    <div class="pull-right">
        <span class="mgR-16">期間選択</span>
        <input type="text" name="graph_3_start" id="graph_3_start" class="graph_3_start"><span class="mgLR-20">-</span><input type="text" class="graph_3_end" name="graph_3_end" id="graph_3_end" value="">
        <button type="button" id="graph_3_load">適用</button>
    </div>
</div>

<!-- end .title-chart-01 -->
<div class="content-group-button" id="attack_status_graph_fix">
    <button type="button" class="active" data-value="1">入札</button>
    <button type="button" data-value="2">提案</button>
    <button type="button" data-value="3">進展あり返信</button>
    <button type="button" data-value="4">サンプル</button>
    <button type="button" data-value="5" style="display:none;">受注</button>
    <button type="button" data-value="6" style="display:none;">リピート</button>
</div>
<!-- end .content-group-button -->

        <div id="chart_div" class="content-chart-01"></div>
</div>

    <div class="content-table-01">
        <table>
            <thead>
            <tr id="graph_3_table_title">
                <?php /* <td>活動推移</td> */ ?>
            </tr>
            </thead>
            <tbody>
            <tr id="graph_3_table_count">
                <?php /* <td>貴社</td> */ ?>
            </tr>
            <?php
//            echo "
//            <tr id="graph_3_table_data">
//                <td>発展率</td>
//            </tr>";
            ?>
            </tbody>
        </table>
    </div>

    @if(!empty($arr_status_count_by_country))
        <div class="title-chart-01 clearfix">国別活動ステータス</div>

        <div class="content-table-01">
            <table>
                <thead>
                <tr>
                    <th >提案している国</th>
                    <th >提案</th>
                    <th >進展あり返信</th>
                    <th >進展なし返信</th>
                    <th >無償サンプル</th>
                    <th >有償サンプル</th>
                    <th >受注</th>
                    <th >リピート成約</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($arr_status_count_by_country))
                    @foreach ($countries as $country)
                        @if(isset($arr_status_count_by_country[$country->id]))
                            <tr>
                                <td >{{$country->country_name}}</td>
                                <td >{{isset($arr_status_count_by_country[$country->id]['suggest']) ? $arr_status_count_by_country[$country->id]['suggest'] : 0}}</td>
                                <td >{{isset($arr_status_count_by_country[$country->id]['response_yes']) ? $arr_status_count_by_country[$country->id]['response_yes'] : 0}}</td>
                                <td >{{isset($arr_status_count_by_country[$country->id]['response_no']) ? $arr_status_count_by_country[$country->id]['response_no'] : 0}}</td>
                                <td >{{isset($arr_status_count_by_country[$country->id]['send_example_free']) ? $arr_status_count_by_country[$country->id]['send_example_free'] : 0}}</td>
                                <td >{{isset($arr_status_count_by_country[$country->id]['send_example_charge']) ? $arr_status_count_by_country[$country->id]['send_example_charge'] : 0}}</td>
                                <td >{{isset($arr_status_count_by_country[$country->id]['order']) ? $arr_status_count_by_country[$country->id]['order'] : 0}}</td>
                                <td >{{isset($arr_status_count_by_country[$country->id]['repeat']) ? $arr_status_count_by_country[$country->id]['repeat'] : 0}}</td>
                            </tr>
                        @endif
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    @endif
<script type="text/javascript">
    $(document).ready(function () {

        // Set up datepicker
        var number_period = 3;
        var current_date = new Date();
        var five_month_ago = new Date();
        five_month_ago.setMonth(five_month_ago.getMonth()-4);
        pickmeup('.sekai-calendar', {
            flat      : true,
            date      : [five_month_ago, current_date],
            class_name: 'calendar',
            mode      : 'range',
            calendars : 3,
            format    : 'Y/m/d',
        });

        pickmeup('.graph_3_start', {
            position       : 'bottom',
            hide_on_select : true,
            format    : 'Y/m/d',
            date: five_month_ago.getFullYear()+'/'+(five_month_ago.getMonth()+1)+'/'+five_month_ago.getDate()
        });

        pickmeup('.graph_3_end', {
            position       : 'bottom',
            hide_on_select : true,
            format    : 'Y/m/d',
            date: current_date.getFullYear()+'/'+(current_date.getMonth()+1)+'/'+current_date.getDate()
        });
        var temp_start_date = $(".graph_3_start").val();
        var temp_end_date = $(".graph_3_end").val();

        $(".sekai-calendar").on('pickmeup-change', function (e) {
            $(".graph_3_start").val(e.detail.formatted_date[0]);
            $(".graph_3_end").val(e.detail.formatted_date[1]);
            temp_start_date = e.detail.formatted_date[0];
            temp_end_date = e.detail.formatted_date[1];
        });

        $(".graph_3_end, .graph_3_start").on('pickmeup-change', function (e) {
            var startDate = $(".graph_3_start").val();
            var endDate = $(".graph_3_end").val();

            var startDateGraph = new Date(startDate);
            var endDateGraph = new Date(endDate);
            var diffMonth = monthDiff(startDateGraph, endDateGraph);
            if(diffMonth >= 12){
                alert("12ヶ月以内で統計してください。");
                $(".graph_3_start").val(temp_start_date);
                $(".graph_3_end").val(temp_end_date);
                return;
            }
            if(endDateGraph.getTime() <  startDateGraph.getTime()){
                alert("開始日は終了日前にする必要があります。");
                $(".graph_3_start").val(temp_start_date);
                $(".graph_3_end").val(temp_end_date);
                return;
            }
            pickmeup(".sekai-calendar").set_date([startDate, endDate]);
            temp_start_date = startDate;
            temp_end_date = endDate;
        });
        // Change datepicker
        $('#period').change(function () {
            number_period = $('#period').val();
            pickmeup('.sekai-calendar').destroy();
            pickmeup('.sekai-calendar', {
                flat      : true,
                date      : [
                    new Date()
                ],
                class_name: 'calendar',
                mode      : 'range',
                calendars : number_period,
                format    : 'Y/m/d',
            });
            pickmeup(".sekai-calendar").set_date([$(".graph_3_start").val(), $(".graph_3_end").val()]);
        });

        //Graph
        /*load on start*/
        loadGraph();
        /*load on click*/
        $('#graph_3_load').click(loadGraph);

        $('.content-group-button button').click(function() {
            $(this).parent().children().removeClass('active');
            $(this).addClass('active');
        });
        /*load graph function*/
        function loadGraph() {
            var start_date = $('#graph_3_start').val();
            var end_date = $('#graph_3_end').val();
            var attack_status = $('#attack_status_graph_fix .active').attr('data-value');
            //var attack_status = $('#attack_status_graph_3').val();
            $.ajax({
                url: '{{url("/load_graph_3")}}',
                method: 'POST',
                dataType: 'json',
                data: {
                    '_token' : '{{csrf_token()}}',
                    'start_date' : start_date,
                    'end_date' : end_date,
                    'attack_status' : attack_status,
                },
                success: function (result) {
                    var arr = [
                        ['月', '件', { role: 'tooltip' }, '件', { role: 'tooltip' }],
                    ];
                    var start_date_object = new Date(start_date);
                    var end_date_object = new Date(end_date);
                    var diff_month = monthDiff(start_date_object, end_date_object);
                    var start_month = start_date_object.getMonth() + 1;
                    var start_year = start_date_object.getFullYear();
                    var current_month = start_month;
                    var table_title = ''; // '<th>活動推移</th>';
                    var table_count = ''; // '<td>貴社</td>';
                    var table_data = ''; // '<td>発展率</td>';
                    //var title_chart = '';
                    for (i = start_month; i <= start_month + diff_month; i++) {
                        var current_status = (typeof result['current_status_data'][start_year*100+current_month] != 'undefined') ? result['current_status_data'][start_year*100+current_month] : 0;
                        /*create graph data array*/
                        arr.push([
                            (current_month) + '月' + start_year + '年',
                            (typeof result['current_status_data'][start_year*100+current_month] != 'undefined') ? result['current_status_data'][start_year*100+current_month] : 0,
                            (current_month) + '月\n' + current_status + '件',
                            (typeof result['arr_average'][start_year*100+current_month] != 'undefined') ? result['arr_average'][start_year*100+current_month] : 0,
                            (current_month) + '月\n' + ((typeof result['arr_average'][start_year*100+current_month] != 'undefined') ? (result['arr_average'][start_year*100+current_month]).toFixed(1) : 0) + '件'
                        ]);
                        /*create analyze table data*/
                        table_title += '<th>'+current_month+'月'+start_year+'年</th>';
                        table_count += '<td>'+current_status+'</td>';

                        if (typeof result['previous_status_data'][start_year*100+current_month] != "undefined") {
                            table_data += '<td>'+((current_status/result['previous_status_data'][start_year*100+current_month])*100).toFixed(2)+'%</td>';
                        } else {
                            table_data += '<td>-</td>';
                        }

                        current_month++;
                        if (current_month > 12) {
                            current_month = 1;
                            start_year++;
                        }
                    }
                    $('#graph_3_table_title').html(table_title);
                    $('#graph_3_table_count').html(table_count);
//                    $('#graph_3_table_data').html(table_data);
                    google.charts.setOnLoadCallback(function(){
                        drawVisualization(arr, "chart_div");
                    });
                }
            });
        }
        /*Calculate Difference in Months between two dates*/
        function monthDiff(d1, d2) {
            var months;
            months = (d2.getFullYear() - d1.getFullYear()) * 12;
            months -= d1.getMonth();
            months += d2.getMonth();
            return months <= 0 ? 0 : months;
        }
    });
</script>
