@extends('layouts.master')
@section('title','メールテンプレート作成')
@section('sekai_content')
<section>
    <div class="popup_part_1" style="width: 100%;opacity: 0.9;top: 20px;left: 0;position: absolute;background-color: #313131;z-index: 999;">    
        <div style="position: absolute;left: 5%;top: 35%;color: black;font-weight: bold;font-size: 30px;width: 90%;text-align:center;">
            <p>こちらの機能は有料プランに切り替えると利用することができます</p>
        </div>
    </div>
    <header><h2>メールテンプレート作成</h2></header>
    @if(session('message'))
        <div class="alert alert-success alert_success_company_japan">{{session('message')}}</div>
    @endif
    <article>
        <form action="" method="post" class="form-group" enctype="multipart/form-data">
        @csrf
        <div class="content-group-controls-01 mgB-20">
            <table class="template-mail-create">
                <tr>
                    <td><span>テンプレート名 </span><strong>必須</strong></td>
                    <td>
                        <input disabled type="text" class="form-control" name="template_name" placeholder="テンプレート名を入力してください" value="{{old('template_name')}}">
                        @if ($errors->has('template_name'))
                            <span class="text-danger error">
                            <strong>{{ $errors->first('template_name') }}</strong>
                            </span>
                        @endif
                    </td>
                    <td class="content-icon-01">
                        <button disabled type="button" style="background-color: white; border: none;" data-toggle="modal"
                                data-target="#myModal1">
                            <img src="images/icon_question_01.png" alt="">
                        </button>
                    </td>
                    <div class="modal fade" id="myModal1" role="dialog">
                    <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                    <button disabled type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.header_template_mail_name') }}</h4>
                    </div>
                    <div class="modal-body">
                    <p>{{ trans('messages.detail_update_template_name') }}</p>
                    </div>
                    <div class="modal-footer">
                    <button disabled type="button" class="btn btn-default" data-dismiss="modal">Close
                    </button>
                    </div>
                    </div>
                    </div>
                    </div>
                </tr>
            </table>
        </div>
        <div class="content-group-controls-01 mgB-20">
            <table class="template-mail-create">
                <tr>
                    <td><span>メール件名 </span><strong>必須</strong></td>
                    <td>
                        <input disabled type="text" class="form-control" name="subject" placeholder="メール件名を入力してください" value="{{old('subject')}}">
                        @if ($errors->has('subject'))
                            <span class="text-danger error">
                            <strong>{{ $errors->first('subject') }}</strong>
                            </span>
                        @endif
                    </td>
                    <td class="content-icon-01">
                        <button disabled type="button" style="background-color: white; border: none;" data-toggle="modal"
                        data-target="#myModal2">
                            <img src="images/icon_question_01.png" alt="">
                        </button>
                    </td>
                    <div class="modal fade" id="myModal2" role="dialog">
                    <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                    <button disabled type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.header_template_mail_subject') }}</h4>
                    </div>
                    <div class="modal-body">
                    <p>{{ trans('messages.detail_update_subject') }}</p>
                    </div>
                    <div class="modal-footer">
                    <button disabled type="button" class="btn btn-default" data-dismiss="modal">Close
                    </button>
                    </div>
                    </div>
                    </div>
                    </div>
                </tr>
            </table>
        </div>
        <div class="content-group-controls-01 mgB-20">
            <table class="template-mail-create">
                <tr>
                    <td><span>添付ファイル </span></td>
                    <td>

                        <input disabled type="file" name="mail_attach_file[]" id="mail_attach_file">
                        <label for="mail_attach_file">ファイルを選択する</label>
                        <span id="show-file"></span>
                        <button disabled class="btn-danger-clear-file" type="button" id="clear">削除</button>
                        <!-- Choosen multile file -->
                        <div><i class="fa fa-plus-square-o" aria-hidden="true"></i> <a id="multile_file" style="text-decoration: underline;">別のファイルを選択する</a></div>
                        <?php for($i = 1; $i <= 4; $i++){ ?>
                        <div id="div_multile_file_<?php echo $i; ?>" style="display: none;">
                            <div>
                                <input disabled type="file" name="mail_attach_file[]" id="mail_attach_file_<?php echo $i; ?>">
                                <label for="mail_attach_file_<?php echo $i; ?>">ファイルを選択する</label>
                                <span id="show-file-<?php echo $i; ?>"></span>
                                <button disabled class="btn-danger-clear-file" type="button" id="clear-<?php echo $i; ?>">削除</button>
                            </div>
                        </div>
                        <?php } ?>
                        最大20MB

                        {{--<input disabled type="file" name="attach_file" id="attach_file" onchange="$('#show-file-id').html($(this).val())">--}}
                        {{--<label for="attach_file" class="mgR-16">ファイルを選択する</label><span id="show-file-id">選択されていません</span>--}}
                        {{--<br>--}}
                        {{--最大20MB--}}
                        {{--@if ($errors->has('attach_file'))--}}
                            {{--<span class="text-danger error">--}}
                            {{--<strong>{{ $errors->first('attach_file') }}</strong>--}}
                            {{--</span>--}}
                        {{--@endif--}}

                        @if ($errors->has('mail_attach_file'))
                            <span class="text-danger error">
                            <strong>{{ $errors->first('mail_attach_file') }}</strong>
                            </span>
                        @endif
                    </td>
                </tr>
            </table>
        </div>
        <div class="content-group-controls-01 mgB-20">
            <table class="template-mail-create">
                <tr>
                    <td><span>本文 </span><strong>必須</strong></td>
                    <td>
                        <textarea disabled name="contentd" placeholder="本文を入力してください" class="form-control" rows="5">{{old('contentd')}}</textarea>
                        @if ($errors->has('contentd'))
                            <span class="text-danger error">
                            <strong>{{ $errors->first('contentd') }}</strong>
                            </span>
                        @endif
                    </td>
                    <td class="content-icon-01">
                        <button disabled type="button" style="background-color: white; border: none;" data-toggle="modal"
                        data-target="#myModal3">
                            <img src="images/icon_question_01.png" alt="">
                        </button>
                    </td>
                    <div class="modal fade" id="myModal3" role="dialog">
                    <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                    <button disabled type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.header_template_mail_content') }}</h4>
                    </div>
                    <div class="modal-body">
                    <p>{{ trans('messages.detail_update_content') }}</p>
                    </div>
                    <div class="modal-footer">
                    <button disabled type="button" class="btn btn-default" data-dismiss="modal">Close
                    </button>
                    </div>
                    </div>
                    </div>
                    </div>
                </tr>
            </table>
        </div>
        <div class="text-center mgT-25 mgB-40">
            <a disabled href="/free/template_mail_list"><button type="button" class="back-template-mail-create">キャンセル</button></a>
            <input disabled type="submit" name="btnRequest" class="submit-template-mail-create" value="保存する">
        </div>
        </form>
    </article>
</section>

<script>
    $(document).ready(function () {
        var _height_part_1 = jQuery( 'section' ).height();
        jQuery( '.popup_part_1' ).css( 'height', _height_part_1 + 25);
        /*Multil File*/
        $('#multile_file').click(function(e){
            if($('#div_multile_file_3').is(":visible")) {
                $('#div_multile_file_4').show();
            } else
            if($('#div_multile_file_2').is(":visible")) {
                $('#div_multile_file_3').show();
            } else
            if($('#div_multile_file_1').is(":visible")) {
                $('#div_multile_file_2').show();
            } else {
                $('#div_multile_file_1').show();
            }

        });
        /*End Multil File*/

        /*Reset File*/
        var test = $('#show-file').text();
        if(test == ''){
            $('#clear').hide();
        }else{
            $('#clear').show();
        }

        $('#mail_attach_file').change(function () {
            $('#show-file').html($('#mail_attach_file').val());
            var test = $('#show-file').text();
            if(test == ''){
                $('#clear').hide();
            }else{
                $('#clear').show();
            }
        });
        document.getElementById("clear").addEventListener("click", function () {
            document.getElementById("mail_attach_file").value = "";
            document.getElementById("show-file").innerHTML = "";
            $("#clear").hide();
        }, false);


        document.getElementById("clear-1").addEventListener("click", function () {
            document.getElementById("mail_attach_file_1").value = "";
            document.getElementById("show-file-1").innerHTML = "";
            $("#clear-1").hide();
        }, false);


        multile_file('#mail_attach_file_1','#show-file-1','#clear-1');
        multile_file('#mail_attach_file_2','#show-file-2','#clear-2');
        multile_file('#mail_attach_file_3','#show-file-3','#clear-3');
        multile_file('#mail_attach_file_4','#show-file-4','#clear-4');
        multile_file('#mail_attach_file_5','#show-file-5','#clear-5');

        clearFile('mail_attach_file_1','clear-1','show-file-1');
        clearFile('mail_attach_file_2','clear-2','show-file-2');
        clearFile('mail_attach_file_3','clear-3','show-file-3');
        clearFile('mail_attach_file_4','clear-4','show-file-4');
        clearFile('mail_attach_file_5','clear-5','show-file-5');

        function multile_file(mail_attach_file, show_file, clear) {
            var test = $(show_file).text();
            if(test == ''){
                $(clear).hide();
            }else{
                $(clear).show();
            }

            $(mail_attach_file).change(function () {
                $(show_file).html($(mail_attach_file).val());
                var test = $(show_file).text();
                if(test == ''){
                    $(clear).hide();
                }else{
                    $(clear).show();
                }
            });
        }

        function clearFile(mail_attach_file,clear,show_file) {
            var el = document.getElementById(clear);
            if(el){
                el.addEventListener("click", function () {
                    document.getElementById(mail_attach_file).value = "";
                    document.getElementById(show_file).innerHTML = "";
                    $("#"+clear).hide();
                }, false);
            }
        }
    });
</script>

@endsection
