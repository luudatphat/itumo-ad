@extends('layouts.master')
@section('title','メールテンプレート一覧')
@section('sekai_content')

    <section>
        <header><h2>メールテンプレート一覧</h2></header>
        @if(session('message'))
            <div class="alert alert-success">{{session('message')}}</div>
        @endif
        <article>
            <div class="row">
                <div class="col-md-12 text-left fix-top-layout">
                    <a class="btn btn-primary button_create_list_user fix-width-btn-01 fix-width-btn-07" href="/free/template_mail_create">新規作成</a>
                </div>
            </div>

            <table class="content-list-table-02 fix-table-01 result-table fix-table-02">
                <thead>
                <tr>
                    <th class="text-center fix-w-col-01">No.</th>
                    <th class="text-center">テンプレート名</th>

                    <th class="text-center fix-w-col-02"></th>
                    <th class="text-center fix-w-col-03"></th>
                    <th class="text-center fix-w-col-03"></th>
                </tr>
                </thead>
                <tbody>
                <?php if(count($template_mail_list) > 0){
                $current_page = $template_mail_list->currentPage();
                $k = $current_page==1?1:($current_page+1);
                ?>
                @foreach($template_mail_list  as $list)
                    <tr>
                        <td class="text-center"><?php echo $k;?></td>
                        <td class="text-center"><?php echo $list->template_name;?></td>
                        <td class="text-center">
                            <a class="btn btn-primary fix-width-btn-02" onclick="show_modal(<?php echo $k;?>)">プレビュー</a>
                            <div class="modal fade" id="myModal<?php echo $k;?>" role="dialog">
                                <div class="form-group">
                                    <div>
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="form-inline preview_class">
                                                        <label for="email">テンプレート名</label>
                                                        <input type="text" class="form-control" placeholder=""
                                                               value="<?php echo $list->template_name;?>" readonly>
                                                    </div>
                                                    <div class="form-inline preview_class">
                                                        <label for="email">メール件名</label>
                                                        <input type="text" class="form-control" placeholder=""
                                                               value="<?php echo $list->subject;?>" readonly>
                                                    </div>
                                                    <div class="form-inline preview_class">
                                                        <label for="email">添付ファイル</label>
                                                        <input type="text" class="form-control" placeholder=""
                                                               value="<?php
                                                               //echo $list->attach_file;
                                                                       $list_file_name = '';

//                                                                       if(isset($list->attach_file)){
//                                                                           $list_file_name.= $list->attach_file;
//                                                                           $list_file_name.=',';
//                                                                       }

                                                                       $list_file = $list->mail_attachmennt;
                                                                       if(isset($list_file) && count($list_file) > 0)
                                                                           {
                                                                               foreach ($list_file as $item){
                                                                                   if(isset($item->media_files)){
                                                                                       $list_file_name.= $item->media_files->file_name;
                                                                                       $list_file_name.=',';
                                                                                   }
                                                                               }
                                                                           }
                                                                       echo substr($list_file_name, 0, -1);
                                                               ?>" readonly>
                                                    </div>
                                                    <div class="form-inline preview_class">
                                                        <label for="email">本文</label>
                                                    <textarea type="text" class="form-control" placeholder=""
                                                              readonly rows="5">{{$list->content}}</textarea>
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">閉じる</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="td-price-list">
                            <a class="btn btn-primary button_create_list_user fix-width-btn-03 fix-bg-color fix-height-btn-01" href="/free/template_mail_detail/<?php echo $list->id;?>">編集</a>
                        </td>
                        <td class="td-price-list">
                            <a class="btn btn-danger button_create_list_user fix-width-btn-03 fix-height-btn-01" href="/free/template_mail_delete/<?php echo $list->id;?>" onclick="return question()">削除</a>
                        </td>
                    </tr>
                    <?php $k++;?>
                @endforeach
                <?php }
                else{
                ?>
                <tr>
                    <td colspan="5">データなし。</td>
                </tr>
                <?php
                }
                ?>

                </tbody>
            </table>

            <nav class="text-right total-records pagination_div show_record">
                <p class="text-right">Records {{ ($template_mail_list->currentPage() -1) * $template_mail_list->perPage() + 1 }} -
                    {{ ($template_mail_list->lastPage() == $template_mail_list->currentPage()) ? $template_mail_list->total() : ($template_mail_list->currentPage() * $template_mail_list->perPage()) }}
                    of {{ $template_mail_list->total() }}</p>
                {{$template_mail_list->links('pagination::bootstrap-4')}}
            </nav>

        </article>
    </section>

    <script type="text/javascript">
        /*Question before delete*/
        function question() {
            var r = confirm("削除してもよろしいですか？");
            if(r == true){
                return true;
            }
            else{
                return false;
            }
        }
        /*End Question before delete*/

        /*show modal after click*/
        function show_modal(id) {
            $("#myModal"+id).modal('show');
        }
        /*show modal after click*/
    </script>
@endsection