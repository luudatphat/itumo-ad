@extends('layouts.master')
@section('title','バイヤー企業 詳細情報')
@section('sekai_content')

    <section>
        <header><h2>バイヤー企業 詳細情報</h2></header>
        <article>
            <h3 class="title-h3">企業詳細情報</h3>
            <table class="table-type-01">

                <?php
                //                echo '@if ($attack->mtb_attack_status_id !== 1)';
                ?>


                <tr>
                    <th>企業名</th>
                    <td>{{ isset($buyer->company_name)?$buyer->company_name:'' }}</td>
                </tr>
                <tr>
                    <th>国</th>
                    <td>{{ isset($buyer->countries->country_name)?$buyer->countries->country_name:'' }}</td>
                </tr>
                <tr>
                    <th>住所</th>
                    <td>{{ isset($buyer->address)?$buyer->address:'' }}</td>
                </tr>
                <tr>
                    <th>電話番号</th>
                    <td>{{ isset($buyer->phone_number)?$buyer->phone_number:'' }}</td>
                </tr>
                <tr>
                    <th>携帯電話番号</th>
                    <td>{{ isset($buyer->cell_phone_number)?$buyer->cell_phone_number:'' }}</td>
                </tr>

                <?php
                //                        echo '<tr>
                //                    <th>メールアドレス</th>
                //                    <td>{{ isset($buyer->email_address)?$buyer->email_address:'' }}</td>
                //                </tr>';
                ?>
                <tr>
                    <th>SNS tool</th>
                    <td>{{ isset($buyer->mtb_sns_tool_id)?$buyer->mtb_sns_tool()->first()->content:'' }}</td>
                </tr>
                <tr>
                    <th>SNS tool ID/URL/Mobile number</th>
                    <td>{{ isset($buyer->sns_id)?$buyer->sns_id:'' }}</td>
                </tr>
                <tr>
                    <th>企業ウェブサイト</th>
                    <td><a target="_blank" href="{{ isset($buyer->company_website_url)?$buyer->company_website_url:'#' }}">{{ isset($buyer->company_website_url)?$buyer->company_website_url:'' }}</a>
                    </td>
                </tr>

                <?php
                //                        echo '@else
                //                    <tr>
                //                        <th>企業名</th>
                //                        <td>●●●●●</td>
                //                    </tr>
                //                    <tr>
                //                        <th>国</th>
                //                        <td>{{ isset($buyer->countries->country_name)?$buyer->countries->country_name:'' }}</td>
                //                    </tr>
                //                    <tr>
                //                        <th>電話番号</th>
                //                        <td>●●●●●</td>
                //                    </tr>
                //                    <tr>
                //                        <th>携帯電話番号</th>
                //                        <td>●●●●●</td>
                //                    </tr>
                //
                //
                ////                                            <tr>
                //                    //                        <th>メールアドレス</th>
                //                    //                        <td>●●●●●</td>
                //                    //                    </tr>;
                //
                //                    <tr>
                //                        <th>SNS tool</th>
                //                        <td>●●●●●</td>
                //                    </tr>
                //                    <tr>
                //                        <th>SNS tool ID/URL/Mobile number</th>
                //                        <td>●●●●●</td>
                //                    </tr>
                //                    <tr>
                //                        <th>企業ウェブサイト</th>
                //                        <td>●●●●●</td>
                //                    </tr>
                //                @endif';
                ?>
                <tr>
                    <th>敬称</th>
                    <td>{{ isset($buyer->mtb_title_id)?$buyer->mtb_title_id:'' }}</td>
                </tr>
                <tr>
                    <th>名</th>
                    <td>{{ isset($buyer->name)?$buyer->name:'' }}</td>
                </tr>
                <tr>
                    <th>姓</th>
                    <td>{{ isset($buyer->surname)?$buyer->surname:'' }}</td>
                </tr>
                <tr>
                    <th>仕入れ国</th>
                    <td>{{isset($buyer->purchasing_country)?$buyer->purchasing_country:''}}</td>
                </tr>
                <tr>
                    <th>販路国</th>
                    <td>{{isset($buyer->sale_country)?$buyer->sale_country:''}}</td>
                </tr>
                <tr>
                    <th>主要販売商品</th>
                    <td>{!!  isset($buyer->desired_product)? nl2br(e($buyer->desired_product)) : '' !!}</td>
                </tr>
                <tr>
                    <th>日本から仕入れたい理由</th>
                    <td>{!! isset($buyer->purchase_reason)?nl2br(e($buyer->purchase_reason)):'' !!}</td>
                </tr>
                <tr>
                    <th>企業としての強み</th>
                    <td>{!! isset($buyer->company_strength)?nl2br(e($buyer->company_strength)):'' !!}</td>
                </tr>
            </table>
            <h3 class="title-h3">情報のリクエスト</h3>
            <table class="table-type-01">
                <tr>
                    <th>ID</th>
                    <td>{{$attack->request->id}}</td>
                </tr>
                <tr>
                    <th>カテゴリ</th>
                    <td>{{$attack->request->mtb_genre->content}}</td>
                </tr>
                <tr>
                    <th>依頼詳細</th>
                    <td>{!! nl2br(e($attack->request->product_description_jp)) !!}</td>
                </tr>
            </table>
            <h3 class="title-h3">メール送信画面</h3>
            <form class="form-content-01" action="{{url('/send_email_to_buyer')}}" method="POST" enctype="multipart/form-data" id="send_mail_form">
                @csrf
                <input type="hidden" name="attack_id" value="{{$attack->id}}">
                <div class="content-box-form">
                    <table>
                        <tr>
                            <td><label class="title-control" for="sender">FROM</label></td>
                            <td>
                                <select name="sender" id="sender">
                                    <option value="">{{\Illuminate\Support\Facades\Auth::user()->email}}</option>
                                    @foreach ($sender_list as $sender)
                                        <option {{old('sender') == $sender->id ? 'selected' : ''}} value="{{$sender->id}}">{{$sender->email}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="title-control" for="mail_template">テンプレート</label></td>
                            <td>
                                <select name="mail_template" id="mail_template">
                                    <option value="">テンプレートを選択する</option>
                                    @foreach ($mail_templates as $mail_template)
                                        <option {{old('mail_template') == $mail_template->id ? 'selected' : ''}} value="{{$mail_template->id}}">{{$mail_template->template_name}}</option>
                                    @endforeach
                                </select>
                                <button type="button" id="load_template_mail">ロードする</button>
                            </td>
                        </tr>

                        <?php
                        //                            echo '<tr>
                        //                            <td><label class="title-control" for="price_list">価格表</label></td>
                        //                            <td>
                        //                                <select name="price_list" id="price_list">
                        //                                    <option value="">価格表を選択する</option>
                        //                                    @foreach($price_lists as $price_list)
                        //                                        <option {{old('price_list') == $price_list->id ? 'selected' : ''}} value="{{$price_list->id}}">{{$price_list->price_list_name}}</option>
                        //                                    @endforeach
                        //                                </select>
                        //                                <button type="button" id="load_price_list">ロードする</button>
                        //                            </td>
                        //                        </tr>';
                        ?>
                        <tr>
                            <td><label class="title-control" for="mail_title">件名</label></td>
                            <td>
                                <p class="{{ $errors->has('mail_title') ? ' has-error' : '' }}"><input type="text" placeholder="件名を入力して下さい" name="mail_title" id="mail_title" value="{{old('mail_title')}}"></p>
                                @if ($errors->has('mail_title'))
                                    <p class="error-message">
                                        <strong>{{ $errors->first('mail_title') }}</strong>
                                    </p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td><label class="title-control" for="mail_cc">CC</label></td>
                            <td>
                                <p class="{{ $errors->has('mail_cc') ? ' has-error' : '' }}"><input type="text" name="mail_cc" placeholder="" id="mail_cc" value="{{old('mail_cc')}}"></p>
                                @if ($errors->has('mail_cc'))
                                    <p class="error-message">
                                        <strong>{{ $errors->first('mail_cc') }}</strong>
                                    </p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td><label class="title-control" for="mail_content">本文</label></td>
                            <td>
                                <p class="{{ $errors->has('mail_content') ? ' has-error' : '' }}"><textarea name="mail_content" id="mail_content">{{old('mail_content')}}</textarea></p>
                                @if ($errors->has('mail_content'))
                                    <p class="error-message">
                                        <strong>{{ $errors->first('mail_content') }}</strong>
                                    </p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td><label class="title-control">添付ファイル</label></td>
                            <td>
                                <input type="file" name="mail_attach_file[]" id="mail_attach_file">
                                <label for="mail_attach_file">ファイルを選択する</label>
                                <span id="show-file"></span>
                                <button class="btn-danger-clear-file" type="button" id="clear">削除</button>
                                <!-- Choosen multile file -->
                                <div><i class="fa fa-plus-square-o" aria-hidden="true"></i> <a id="multile_file" style="text-decoration: underline;">別のファイルを選択する</a></div>
                                <?php for($i = 1; $i <= 4; $i++){ ?>
                                <div id="div_multile_file_<?php echo $i; ?>" style="display: none;">
                                    <div>
                                        <input type="file" name="mail_attach_file[]" id="mail_attach_file_<?php echo $i; ?>">
                                        <label for="mail_attach_file_<?php echo $i; ?>">ファイルを選択する</label>
                                        <span id="show-file-<?php echo $i; ?>"></span>
                                        <button class="btn-danger-clear-file" type="button" id="clear-<?php echo $i; ?>">削除</button>
                                    </div>
                                </div>
                            <?php } ?>
                            {{--<div><i class="fa fa-plus-square-o" aria-hidden="true"></i> <a style="text-decoration: underline;">別のファイルを選択する</a></div>--}}

                            <!-- End Choosen multile file -->
                                最大20MB
                                <br>
                                <div id="mail_attachment_list"></div>
                                <div id="price_list_list"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                @if(session('message'))
                    <div class="alert alert-success alert_success_company_japan" style="margin-top: 20px">{{session('message')}}</div>
                @endif
                <div class="group-control-01 text-center">
                    <button type="submit" name="submit">送信</button>
                </div>
                <!-- end .group-control -->
            </form>

            <div class="content-2col fix-content-01" id="content-memo">
                <div class="a-col">
                    <h3 class="title-h3">提案メモ</h3>
                    @if(session('message_memo'))
                        <div class="alert alert-warning alert_success_company_japan" style="margin-top: 20px">{{session('message_memo')}}</div>
                    @endif
                    <form action="{{url('/add_memo')}}" method="post">
                        @csrf
                        <input type="hidden" name="attack_id" value="{{$attack->id}}">
                        <p>
                            <select {{(($attack->mtb_attack_status_id == 9 || $attack->mtb_attack_status_id == 5 || $attack->mtb_attack_status_id == 3 || $attack->mtb_attack_status_id == 6 || $attack->mtb_attack_status_id == 7)?'disabled':'')}} name="attack_status">
                                <?php
                                switch ($attack->mtb_attack_status_id) {
                                case 1:
                                ?>
                                <option value="1">入札済み</option>
                                <option value="2">初回提案送信済み</option>
                                <?php
                                break;
                                case 2:
                                ?>
                                <option value="2">初回提案送信済み</option>
                                <?php // <option value="3">フィードバックなし</option> ?>
                                <option value="4">返信から進展有り</option>
                                <option value="5">進展なし返信</option>
                                <?php
                                break;
                                case 3:
                                ?>
                                <option value="5">進展なし返信</option>
                                <?php
                                break;
                                case 5:
                                ?>
                                <option value="5">進展なし返信</option>
                                <?php
                                break;
                                case 4:
                                ?>
                                <option value="4">返信から進展有り</option>
                                <option value="7">サンプル無償</option>
                                <option value="6">サンプル有償</option>
                                <?php
                                break;
                                case 7:
                                ?>
                                <option value="7">サンプル無償</option>
                                <?php //<option value="6">サンプル有償</option> ?>
                                <?php
                                break;
                                case 6:
                                ?>
                                <option value="6">サンプル有償</option>
                                <?php
                                break;
                                case 8:
                                ?>
                                <option value="8">受注</option>
                                <option value="9">リピート</option>
                                <?php
                                break;
                                case 9:
                                ?>
                                <option value="9">リピート</option>
                                <?php
                                }
                                ?>
                            </select>
                        </p>
                        <p class="form-group{{ $errors->has('memo-date-create') ? ' has-error' : '' }}">
                            <input style="width: 250px;" type="text" name="memo-date-create" value="{{old('memo-date-create')}}" class="memo-date-create form-control">
                            @if ($errors->has('memo-date-create'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('memo-date-create') }}</strong>
                                </span>
                            @endif
                        </p>

                        <p class="{{ $errors->has('memo') ? ' has-error' : '' }}">
                            <textarea name="memo" id="memo">{{ old('memo') }}</textarea>
                        </p>

                        @if ($errors->has('memo'))
                            <p class="error-message">
                                <strong>{{ $errors->first('memo') }}</strong>
                            </p>
                        @endif
                        <p class="text-center">
                            <button type="submit">保存</button>
                            @if(($attack->contract_report == NULL && in_array($attack->mtb_attack_status_id, [4, 7, 6])) || ($attack->mtb_attack_status_id == 9))
                                <a href="{{url('report_create?id='.$attack->id)}}" class="btn-fixing"><button type="button">成約報告をする</button></a>
                            @endif
                        </p>
                    </form>
                </div>
                <div class="a-col">
                    <h3 class="title-h3">提案メモ一覧</h3>
                    <table>
                        <tr>
                            <td>No.</td>
                            <td class="fix-width-col-01">日付</td>
                            <td>コンテンツ</td>
                        </tr>
                        @forelse ($memo_list as $memo)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{date_format(date_create($memo->created_at),'Y-m-d')}}</td>
                                <td>{!!nl2br(e($memo->content))!!}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3">データなし。</td>
                            </tr>
                        @endforelse
                    </table>
                </div>
            </div>
            <!-- end .content-2col -->
        </article>
    </section>
    <script>
        $(document).ready(function () {
            /*Date picker*/
            $('.memo-date-create').datepicker({ dateFormat: 'yy-mm-dd' });
            $(".memo-date-create").datepicker("setDate", new Date());
            /*Multil File*/
            $('#multile_file').click(function(e){
                if($('#div_multile_file_3').is(":visible")) {
                    $('#div_multile_file_4').show();
                } else
                if($('#div_multile_file_2').is(":visible")) {
                    $('#div_multile_file_3').show();
                } else
                if($('#div_multile_file_1').is(":visible")) {
                    $('#div_multile_file_2').show();
                } else {
                    $('#div_multile_file_1').show();
                }

            });
            /*End Multil File*/
            /*Confirm when submit form*/
            $("#send_mail_form").on("submit", function(){
                return confirm("メールを送信してもよろしいですか。");
            });

            /*load mail template*/
            $('#load_template_mail').click(function () {
                $('#mail_attachment_list').html('');
                var template_mail_id = $('#mail_template').val();
                if (template_mail_id !== '') {
                    $.ajax({
                        url:'/load_mail_template',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            attack_id: {{$attack->id}},
                            template_mail_id: template_mail_id,
                            _token: '{{csrf_token()}}'
                        },
                        success: function (result) {
                            $('#mail_title').val(result['template_mail']["subject"]);
                            $('#mail_content').html(result['template_mail']['content']);
                            var list_attach = result['list_attach'];
                            for(i = 0; i < list_attach.length; i++){
                                $('#mail_attachment_list').append("- "+ list_attach[i] + '<br>');
                            }
                        }
                    });
                }
            });
            /*load price list*/
            var price_list_list = {};
            $('#load_price_list').click(function () {
                var price_list_id = $('#price_list').val();
                if (price_list_id != '')
                {
                    /*Do not add if price list already added*/
                    if (typeof (price_list_list[price_list_id]) == "undefined")
                    {
                        $('#price_list_list').append('- '+$("#price_list option:selected").text()+'<br>');

                        price_list_list[price_list_id] = price_list_id;
                        // Add hidden input contain price list id
                        $('<input>').attr({
                            type: 'hidden',
                            name: 'price_list_id[]',
                            value: price_list_id
                        }).appendTo('#send_mail_form');
                    }
                }
            });
            /*Reset File*/
            var test = $('#show-file').text();
            if(test == ''){
                $('#clear').hide();
            }else{
                $('#clear').show();
            }
            $('#mail_attach_file').change(function () {
                $('#show-file').html($('#mail_attach_file').val());
                var test = $('#show-file').text();
                if(test == ''){
                    $('#clear').hide();
                }else{
                    $('#clear').show();
                }
            });
            document.getElementById("clear").addEventListener("click", function () {
                document.getElementById("mail_attach_file").value = "";
                document.getElementById("show-file").innerHTML = "";
                $("#clear").hide();
            }, false);


            document.getElementById("clear-1").addEventListener("click", function () {
                document.getElementById("mail_attach_file_1").value = "";
                document.getElementById("show-file-1").innerHTML = "";
                $("#clear-1").hide();
            }, false);


            multile_file('#mail_attach_file_1','#show-file-1','#clear-1');
            multile_file('#mail_attach_file_2','#show-file-2','#clear-2');
            multile_file('#mail_attach_file_3','#show-file-3','#clear-3');
            multile_file('#mail_attach_file_4','#show-file-4','#clear-4');
            multile_file('#mail_attach_file_5','#show-file-5','#clear-5');

            clearFile('mail_attach_file_1','clear-1','show-file-1');
            clearFile('mail_attach_file_2','clear-2','show-file-2');
            clearFile('mail_attach_file_3','clear-3','show-file-3');
            clearFile('mail_attach_file_4','clear-4','show-file-4');
            clearFile('mail_attach_file_5','clear-5','show-file-5');

            function multile_file(mail_attach_file, show_file, clear) {
                var test = $(show_file).text();
                if(test == ''){
                    $(clear).hide();
                }else{
                    $(clear).show();
                }

                $(mail_attach_file).change(function () {
                    $(show_file).html($(mail_attach_file).val());
                    var test = $(show_file).text();
                    if(test == ''){
                        $(clear).hide();
                    }else{
                        $(clear).show();
                    }
                });

//                document.getElementById(clear.substr(0, 1)).addEventListener("click", function () {
//                    document.getElementById(mail_attach_file.substr(0, 1)).value = "";
//                    document.getElementById(show_file.substr(0, 1)).innerHTML = "";
//                    $(clear).hide();
//                }, false);

//                $(clear).click(function () {
//                    $(mail_attach_file).value = "";
//                    $(show_file).innerHTML = "";
//                    $(clear).hide();
//                });
            }

            function clearFile(mail_attach_file,clear,show_file) {
                var el = document.getElementById(clear);
                if(el){
                    el.addEventListener("click", function () {
                        document.getElementById(mail_attach_file).value = "";
                        document.getElementById(show_file).innerHTML = "";
                        $("#"+clear).hide();
                    }, false);
                }
            }
        });
    </script>
@endsection