@extends('layouts.master')
@section('title','メールテンプレート編集')
@section('sekai_content')
<section>
        <header><h2>メールテンプレート作成</h2></header>
        @if(session('message'))
            <div class="alert alert-success alert_success_company_japan">{{session('message')}}</div>
        @endif
        <article>
            <form action="" method="post" class="form-group" enctype="multipart/form-data">
                @csrf
                <div class="content-group-controls-01 mgB-20">
                    <table class="template-mail-create">
                        <tr>
                            <td><span>テンプレート名 </span><strong>必須</strong></td>
                            <td>
                                <input type="text" class="form-control" name="template_name" placeholder="テンプレート名を入力してください"
                                value="{{!empty(old('template_name'))?old('template_name'):((isset($template_mail_list)&&(count($template_mail_list)>0))?$template_mail_list[0]->template_name:'')}}">
                                <input type="hidden" value="{{(isset($template_mail_list)&&(count($template_mail_list)>0))?$template_mail_list[0]->id:''}}" name="id"/>
                                <input type="hidden" value="{{(isset($template_mail_list)&&(count($template_mail_list)>0))?$template_mail_list[0]->attach_file:''}}" name="post_attach" />
                                @if ($errors->has('template_name'))
                                    <span class="text-danger error">
                            <strong>{{ $errors->first('template_name') }}</strong>
                            </span>
                                @endif
                            </td>
                            <td class="content-icon-01">
                                <button type="button" style="background-color: white; border: none;" data-toggle="modal"
                                        data-target="#myModal1">
                                    <img src="{{ url('/images/icon_question_01.png') }}" alt="">
                                </button>
                            </td>
                            <div class="modal fade" id="myModal1" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">{{ trans('messages.header_template_mail_name') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>{{ trans('messages.detail_update_template_name') }}</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </tr>
                    </table>
                </div>
                <div class="content-group-controls-01 mgB-20">
                    <table class="template-mail-create">
                        <tr>
                            <td><span>メール件名 </span><strong>必須</strong></td>
                            <td>
                                <input type="text" class="form-control" name="subject" placeholder="メール件名を入力してください"
                                value="{{!empty(old('subject'))?old('subject'):((isset($template_mail_list)&&(count($template_mail_list)>0))?$template_mail_list[0]->subject:'')}}">
                                @if ($errors->has('subject'))
                                    <span class="text-danger error">
                            <strong>{{ $errors->first('subject') }}</strong>
                            </span>
                                @endif
                            </td>
                            <td class="content-icon-01">
                                <button type="button" style="background-color: white; border: none;" data-toggle="modal"
                                        data-target="#myModal2">
                                    <img src="{{ url('/images/icon_question_01.png') }}" alt="">
                                </button>
                            </td>
                            <div class="modal fade" id="myModal2" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">{{ trans('messages.header_template_mail_subject') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>{{ trans('messages.detail_update_subject') }}</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </tr>
                    </table>
                </div>
                <div class="content-group-controls-01 mgB-20">
                    <table class="template-mail-create">
                        <tr>
                            <td><span>添付ファイル </span></td>
                            <td>
                                @foreach($template_mail_list[0]->mail_attachmennt  as $item)
                                    <div id="div_multile_file_{{$item->media_files->id}}">
                                        <div id="div-old-{{$item->media_files->id}}">
                                            <span id="show-file-{{$item->media_files->id}}">
                                                <a href="{{ url('/mail-template-download/'.$item->media_files->id) }}">{{$item->media_files->file_name}}</a>
                                            </span>
                                            {{--<input type="hidden" class="hidden-media-id" value="{{$item->media_files->id}}">--}}
                                        </div>
                                    </div>
                                @endforeach
                                <input type="file" name="mail_attach_file[]" id="mail_attach_file">
                                <label for="mail_attach_file">ファイルを選択する</label>
                                <span id="show-file"></span>
                                <button class="btn-danger-clear-file" type="button" id="clear">削除</button>
                                <!-- Choosen multile file -->
                                <div><i class="fa fa-plus-square-o" aria-hidden="true"></i> <a id="multile_file" style="text-decoration: underline;">別のファイルを選択する</a></div>
                                <?php
//                                $count = count($template_mail_list[0]->mail_attachmennt);
                                for($i = 1; $i <= 4; $i++){
                                    ?>
                                <div id="div_multile_file_<?php echo $i; ?>" style="display: none;">
                                    <div>
                                        <input type="file" name="mail_attach_file[]" id="mail_attach_file_<?php echo $i; ?>">
                                        <label for="mail_attach_file_<?php echo $i; ?>">ファイルを選択する</label>
                                        <span id="show-file-<?php echo $i; ?>"></span>
                                        <button class="btn-danger-clear-file" type="button" id="clear-<?php echo $i; ?>">削除</button>
                                    </div>
                                </div>
                                <?php } ?>
                                最大20MB
                                @if ($errors->has('mail_attach_file'))
                                    <span class="text-danger error">
                            <strong>{{ $errors->first('mail_attach_file') }}</strong>
                            </span>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-group-controls-01 mgB-20">
                    <table class="template-mail-create">
                        <tr>
                            <td><span>本文 </span><strong>必須</strong></td>
                            <td>
                                <textarea name="contentd" class="form-control"
                                rows="5">{{!empty(old('contentd'))?old('contentd'):((isset($template_mail_list)&&(count($template_mail_list)>0))?$template_mail_list[0]->content:'')}}</textarea>
                                @if ($errors->has('contentd'))
                                    <span class="text-danger error">
                            <strong>{{ $errors->first('contentd') }}</strong>
                            </span>
                                @endif
                            </td>
                            <td class="content-icon-01">
                                <button type="button" style="background-color: white; border: none;" data-toggle="modal"
                                        data-target="#myModal3">
                                    <img src="{{ url('/images/icon_question_01.png') }}" alt="">
                                </button>
                            </td>
                            <div class="modal fade" id="myModal3" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">{{ trans('messages.header_template_mail_content') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>{{ trans('messages.detail_update_content') }}</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </tr>
                    </table>
                </div>
                <div class="text-center mgT-25 mgB-40">
                    <a href="/template_mail_list"><button type="button" class="back-template-mail-create">キャンセル</button></a>
                    <input type="submit" name="btnRequest" class="submit-template-mail-create" value="保存する">
                </div>
            </form>
        </article>
    </section>

<script>
    $(document).ready(function () {
        /*Multil File*/
        $('#multile_file').click(function(e){
            if($('#div_multile_file_3').is(":visible")) {
                $('#div_multile_file_4').show();
            } else
            if($('#div_multile_file_2').is(":visible")) {
                $('#div_multile_file_3').show();
            } else
            if($('#div_multile_file_1').is(":visible")) {
                $('#div_multile_file_2').show();
            } else {
                $('#div_multile_file_1').show();
            }
        });
        /*End Multil File*/

        /*Reset File*/
        var test = $('#show-file').text();
        if(test == ''){
            $('#clear').hide();
        }else{
            $('#clear').show();
        }

        $('#mail_attach_file').change(function () {
            $('#show-file').html($('#mail_attach_file').val());
            var test = $('#show-file').text();
            if(test == ''){
                $('#clear').hide();
            }else{
                $('#clear').show();
            }
        });
        document.getElementById("clear").addEventListener("click", function () {
            document.getElementById("mail_attach_file").value = "";
            document.getElementById("show-file").innerHTML = "";
            $("#clear").hide();
        }, false);


        document.getElementById("clear-1").addEventListener("click", function () {
            document.getElementById("mail_attach_file_1").value = "";
            document.getElementById("show-file-1").innerHTML = "";
            $("#clear-1").hide();
        }, false);

        $('.class-clear-old').click(function () {
            var a = $(this).siblings(".hidden-media-id").val();
            $('#array-delete-media').val(function(i,val) {
                return val + (!val ? '' : ',') + a;
            });
            $('#div_multile_file_'+a).html("");
        });

        var count_media_files = $('#count-media-files').val();
       // alert(count_media_files);

        multile_file('#mail_attach_file_1','#show-file-1','#clear-1');
        multile_file('#mail_attach_file_2','#show-file-2','#clear-2');
        multile_file('#mail_attach_file_3','#show-file-3','#clear-3');
        multile_file('#mail_attach_file_4','#show-file-4','#clear-4');
        multile_file('#mail_attach_file_5','#show-file-5','#clear-5');

        clearFile('mail_attach_file_1','clear-1','show-file-1');
        clearFile('mail_attach_file_2','clear-2','show-file-2');
        clearFile('mail_attach_file_3','clear-3','show-file-3');
        clearFile('mail_attach_file_4','clear-4','show-file-4');
        clearFile('mail_attach_file_5','clear-5','show-file-5');

        function multile_file(mail_attach_file, show_file, clear) {
            var test = $(show_file).text();
            if(test == ''){
                $(clear).hide();
            }else{
                $(clear).show();
            }

            $(mail_attach_file).change(function () {
                $(show_file).html($(mail_attach_file).val());
                var test = $(show_file).text();
                if(test == ''){
                    $(clear).hide();
                }else{
                    $(clear).show();
                }
            });
        }

        function clearFile(mail_attach_file,clear,show_file) {
            var el = document.getElementById(clear);
            if(el){
                el.addEventListener("click", function () {
                    document.getElementById(mail_attach_file).value = "";
                    document.getElementById(show_file).innerHTML = "";
                    $("#"+clear).hide();
                }, false);
            }
        }



    });
</script>

@endsection
