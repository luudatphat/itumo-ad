@extends('layouts.master')
@section('title', 'サポート窓口')
@section('sekai_content')
    <h2>サポート窓口</h2>
    <div class="container container_contact">
        @if(session('message'))
        <div class="alert alert-success alert_success_company_japan">{{session('message')}}</div>
        @endif
        <form method="post" name="" action="{{ url('/contact') }}">
            @csrf
            <table class="table table-bordered table_contact_po" style="margin: auto; width: 600px;">
                <tr>
                    <td style="background-color: #fafafa; vertical-align: middle; width: 150px;" class="text-center">問い合わせ内容 <span class="text-danger">必須</span></td>
                    <td>
                        @foreach($support_categories as $item)
                        <div class="radio">
                            <label><input type="radio" value="{{$item->id}}" name="category" {{$loop->iteration == 1 ? "checked" : ""}}>{{$item->content}}</label>
                        </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #fafafa; vertical-align: middle; width: 150px;" class="text-center">詳細</td>
                    <td>
                        <textarea class="form-control" name="support_content" rows="10"></textarea>
                    </td>
                </tr>
            </table>
            <!-- Button submit -->
            <div class="row row_create_report text-center">
                <input id="btn_submit_po" class="btn btn-warning" type="submit" name="submit" value="送信する">
            </div>
        </form>
    </div>
@endsection