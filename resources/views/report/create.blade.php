@extends('layouts.master')
@section('title', '商品掲載登録')
@section('sekai_content')
    <section>
        <header><h2>成約報告新規作成</h2></header>
        @if(session('message'))
            <div class="alert alert-success alert_success_company_japan">{{session('message')}}</div>
        @endif
        <article>
            <form class="form-group" method="post" action="" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="row row_detail_foreign {{$errors->has('attack_id') ? 'has-error' : ''}}">
                            <div class="col-md-3">
                                <span class="title-report-create">アタックリスト レコード番号</span><strong
                                        class="require-report-create">必須</strong>
                            </div>
                            <div class="col-md-9">
                                <select name="attack_id" class="form-control select-create-report">
                                    <option value>-- 選択してください --</option>
                                    @forelse ($attack_list as $attack)
                                        <option {{(isset($attack_id) && $attack_id == $attack->id)?'selected':(old('attack_id')==$attack->id ? 'selected': '')}} value="{{$attack->id}}">
                                            {{ isset($attack->request->id) ? $attack->request->id : ''}}</option>
                                    @empty
                                    @endforelse
                                </select>
                                @if ($errors->has('attack_id'))
                                    <span class="help-block">
        <span>{{ $errors->first('attack_id') }}</span>
        </span>
                                @endif
                            </div>
                        </div>
                        <hr class="hr-dotted">
                        <div class="row row_detail_foreign {{$errors->has('invoice') ? 'has-error' : ''}}">
                            <div class="col-md-3 ">
                                <span class="title-report-create">インボイス  </span><strong
                                        class="require-report-create">必須</strong>
                            </div>
                            <div class="col-md-9 file-invoice-create-report">
                                <input type="file" name="invoice" value="{{old('invoice')}}" id="invoice"
                                       onchange="$('#show-invoice').html($(this).val())">
                                <label for="invoice" class="mgR-16">ファイルを選択する</label><span
                                        id="show-invoice">選択されていません</span>
                                @if ($errors->has('invoice'))
                                    <span class="help-block">
        <span>{{ $errors->first('invoice') }}</span>
        </span>
                                @endif
                            </div>
                        </div>
                        <hr class="hr-dotted">
                        <div class="row row_detail_foreign {{$errors->has('packing_list') ? 'has-error' : ''}}">
                            <div class="col-md-3 ">
                                <span class="title-report-create">パッキングリスト  </span>
                            </div>
                            <div class="col-md-9 file-packing-list-create-report">
                                <input type="file" name="packing_list" value="{{old('packing_list')}}" id="packing_list"
                                       onchange="$('#show-packing_list').html($(this).val())">
                                <label for="packing_list" class="mgR-16">ファイルを選択する</label><span id="show-packing_list">選択されていません</span>
                                @if ($errors->has('packing_list'))
                                    <span class="help-block">
        <span>{{ $errors->first('packing_list') }}</span>
        </span>
                                @endif
                            </div>
                        </div>
                        <hr class="hr-dotted">
                        <div class="row row_detail_foreign {{$errors->has('number_contact') ? 'has-error' : ''}}">
                            <!-- an_tnh_sc_126_start -->
                            <div class="col-md-3">
                                <span class="title-report-create">成約回数  </span><strong
                                        class="require-report-create">必須</strong>
                            </div>
                            <div class="col-md-9">
                                <select name="number_contact" class="form-control select-create-report">
                                <option value="">-- 選択してください --</option>
                                @for ($i = 1; $i <= 50; $i++)
                                    @if ( $i == 1 )
                                        <option value="{{$i}}">初回成約</option>
                                    @elseif ( $i == 50 )
                                        <option value="{{$i}}">{{$i}}回目以降成約</option>
                                    @else
                                        <option value="{{$i}}">{{$i}}回目成約</option>
                                    @endif
                                @endfor
                                </select>
                                @if ($errors->has('number_contact'))
                                    <span class="help-block">
                                        <span>{{ $errors->first('number_contact') }}</span>
                                    </span>
                                @endif
                            </div>
                            <!-- an_tnh_sc_126_end -->
                        </div>
                        <hr class="hr-dotted">
                        <div class="row row_detail_foreign">
                            <div class="col-md-3 vertical-align" style="height: 600px;">
                                <span class="title-report-create">商品情報  </span><strong
                                        class="require-report-create">必須</strong>
                            </div>
                            <div class="col-md-9">
                                <div class="row row_create_report">
                                    <div class="col-md-12 {{$errors->has('product_name_1') ? 'has-error' : ''}}">
                                        <label class="control-label fw-400">商品名 - ①</label>
                                        <input type="text" name="product_name_1" class="form-control" id="inpn"
                                               value="{{old('product_name_1')}}" placeholder="商品名を入力してください">
                                        @if ($errors->has('product_name_1'))
                                            <span class="help-block">
        <span>{{ $errors->first('product_name_1') }}</span>
        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row row_create_report">
                                    <div class="col-md-3 {{$errors->has('product_category_1') ? 'has-error' : ''}}">
                                        <label class="control-label fw-400">商品カテゴリー - ①</label>
                                        <select class="form-control" name="product_category_1">
                                            <option value="">-- 選択してください --</option>
                                            @forelse ($category_list as $category)
                                                <option {{(old('product_category_1') == $category->id) && ($category->id != 0) ? 'selected' : ''}} value="{{$category->id}}">{{$category->content}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                        @if ($errors->has('product_category_1'))
                                            <span class="help-block">
        <span>{{ $errors->first('product_category_1') }}</span>
        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 {{$errors->has('price_1') ? 'has-error' : ''}}">
                                        <label class="control-label fw-400">商品単価 - ①</label>
                                        <input type="text" name="price_1" class="form-control" id="inpn"
                                               value="{{old('price_1')}}" placeholder="商品単価を入力してください">
                                        @if ($errors->has('price_1'))
                                            <span class="help-block">
        <span>{{ $errors->first('price_1') }}</span>
        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 {{$errors->has('quantity_1') ? 'has-error' : ''}}">
                                        <label class="control-label fw-400">数量 - ①</label>
                                        <input type="text" name="quantity_1" class="form-control" id="inpn"
                                               value="{{old('quantity_1')}}" placeholder="数量を入力してください">
                                        @if ($errors->has('quantity_1'))
                                            <span class="help-block">
        <span>{{ $errors->first('quantity_1') }}</span>
        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 {{$errors->has('couxu_commission_1') ? 'has-error' : ''}}">
                                        <label class="control-label fw-400">COUXU手数料率 - ①</label>
                                        <input type="text" name="couxu_commission_1" class="form-control" id="inpn"
                                               value="{{old('couxu_commission_1')}}" placeholder="手数料率を入力してください">
                                        @if ($errors->has('couxu_commission_1'))
                                            <span class="help-block">
        <span>{{ $errors->first('couxu_commission_1') }}</span>
        </span>
                                        @endif
                                    </div>
                                </div>
                                <hr class="hr-dotted">
                                <div class="row row_create_report">
                                    <div class="col-md-12 {{ $errors->has('product_name_2') ? 'has-error' : ''}}">
                                        <label class="control-label fw-400">商品名 - ②</label>
                                        <input type="text" name="product_name_2" class="form-control" id="inpn"
                                               value="{{old('product_name_2')}}" placeholder="商品名を入力してください">
                                        @if ($errors->has('product_name_2'))
                                            <span class="help-block">
        <span>{{ $errors->first('product_name_2') }}</span>
        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row row_create_report">
                                    <div class="col-md-3 {{$errors->has('product_category_2') ? 'has-error' : ''}}">
                                        <label class="control-label fw-400">商品カテゴリー - ②</label>
                                        <select class="form-control" name="product_category_2">
                                            <option value="">-- 選択してください --</option>
                                            @forelse ($category_list as $category)
                                                <option {{(old('product_category_2') == $category->id) && ($category->id != 0) ? 'selected' : ''}} value="{{$category->id}}">{{$category->content}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                        @if ($errors->has('product_category_2'))
                                            <span class="help-block">
        <span>{{ $errors->first('product_category_2') }}</span>
        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 {{$errors->has('price_2')? 'has-error' : ''}}">
                                        <label class="control-label fw-400">商品単価 - ②</label>
                                        <input type="text" name="price_2" class="form-control" id="inpn"
                                               value="{{old('price_2')}}" placeholder="商品単価を入力してください">
                                        @if ($errors->has('price_2'))
                                            <span class="help-block">
        <span>{{ $errors->first('price_2') }}</span>
        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 {{$errors->has('quantity_2')? 'has-error' : ''}}">
                                        <label class="control-label fw-400">数量 - ②</label>
                                        <input type="text" name="quantity_2" class="form-control" id="inpn"
                                               value="{{old('quantity_2')}}" placeholder="数量を入力してください">
                                        @if ($errors->has('quantity_2'))
                                            <span class="help-block">
        <span>{{ $errors->first('quantity_2') }}</span>
        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 {{$errors->has('couxu_commission_2') ? 'has-error':''}}">
                                        <label class="control-label fw-400">COUXU手数料率 - ②</label>
                                        <input type="text" name="couxu_commission_2" class="form-control" id="inpn"
                                               value="{{old('couxu_commission_2')}}" placeholder="手数料率を入力してください">
                                        @if ($errors->has('couxu_commission_2'))
                                            <span class="help-block">
        <span>{{ $errors->first('couxu_commission_2') }}</span>
        </span>
                                        @endif
                                    </div>
                                </div>
                                <hr class="hr-dotted">
                                <div class="row row_create_report">
                                    <div class="col-md-12 {{ $errors->has('product_name_3') ? 'has-error' : ''}}">
                                        <label class="control-label fw-400">商品名 - ③</label>
                                        <input type="text" name="product_name_3" class="form-control" id="inpn"
                                               value="{{old('product_name_3')}}" placeholder="商品名を入力してください">
                                        @if ($errors->has('product_name_3'))
                                            <span class="help-block">
        <span>{{ $errors->first('product_name_3') }}</span>
        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row row_create_report">
                                    <div class="col-md-3 {{$errors->has('product_category_3') ? 'has-error' : ''}}">
                                        <label class="control-label fw-400">商品カテゴリー - ③</label>
                                        <select class="form-control" name="product_category_3">
                                            <option value="">-- 選択してください --</option>
                                            @forelse ($category_list as $category)
                                                <option {{(old('product_category_3') == $category->id) && ($category->id != 0) ? 'selected' : ''}} value="{{$category->id}}">{{$category->content}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                        @if ($errors->has('product_category_3'))
                                            <span class="help-block">
        <span>{{ $errors->first('product_category_3') }}</span>
        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 {{$errors->has('price_3')? 'has-error' : ''}}">
                                        <label class="control-label fw-400">商品単価 - ③</label>
                                        <input type="text" name="price_3" class="form-control" id="inpn"
                                               value="{{old('price_3')}}" placeholder="商品単価を入力してください">
                                        @if ($errors->has('price_3'))
                                            <span class="help-block">
        <span>{{ $errors->first('price_3') }}</span>
        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 {{$errors->has('quantity_3')? 'has-error' : ''}}">
                                        <label class="control-label fw-400"> 数量 - ③</label>
                                        <input type="text" name="quantity_3" class="form-control" id="inpn"
                                               value="{{old('quantity_3')}}" placeholder="数量を入力してください">
                                        @if ($errors->has('quantity_3'))
                                            <span class="help-block">
        <span>{{ $errors->first('quantity_3') }}</span>
        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 {{$errors->has('couxu_commission_3') ? 'has-error':''}}">
                                        <label class="control-label fw-400">COUXU手数料率 - ③</label>
                                        <input type="text" name="couxu_commission_3" class="form-control" id="inpn"
                                               value="{{old('couxu_commission_3')}}" placeholder="手数料率を入力してください">
                                        @if ($errors->has('couxu_commission_3'))
                                            <span class="help-block">
        <span>{{ $errors->first('couxu_commission_3') }}</span>
        </span>
                                        @endif
                                    </div>
                                </div>

                                <!-- button toggle -->
                                <div class="row row_create_report">
                                    <div class="col-md-12">
                                        <p id="btndivreport4"
                                           style="color: #888;line-height: 40px;background: #eee;border-radius: 4px;text-align: center;cursor: pointer;">
                                            ＋ 入力欄を追加</p>
                                    </div>
                                </div>
                                <!-- div 4-->
                                <div id="right-create-report">
                                <div style="display: none" id="divreport4">
                                    <div class="row row_create_report">
                                        <div class="col-md-12 {{ $errors->has('product_name_4') ? 'has-error' : ''}}">
                                            <label class="control-label fw-400">商品名 - ④</label>
                                            <input type="text" name="product_name_4" class="form-control" id="inpn"
                                                   value="{{old('product_name_4')}}" placeholder="商品名を入力してください">
                                            @if ($errors->has('product_name_4'))
                                                <span class="help-block">
        <span>{{ $errors->first('product_name_4') }}</span>
        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row row_create_report">
                                        <div class="col-md-3 {{$errors->has('product_category_4') ? 'has-error' : ''}}">
                                            <label class="control-label fw-400">商品カテゴリー - ④</label>
                                            <select class="form-control" name="product_category_4">
                                                <option value="">-- 選択してください --</option>
                                                @forelse ($category_list as $category)
                                                    <option {{(old('product_category_4') == $category->id) && ($category->id != 0) ? 'selected' : ''}} value="{{$category->id}}">{{$category->content}}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                            @if ($errors->has('product_category_4'))
                                                <span class="help-block">
        <span>{{ $errors->first('product_category_4') }}</span>
        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-3 {{$errors->has('price_4')? 'has-error' : ''}}">
                                            <label class="control-label fw-400">商品単価 - ④</label>
                                            <input type="text" name="price_4" class="form-control" id="inpn"
                                                   value="{{old('price_4')}}" placeholder="商品単価を入力してください">
                                            @if ($errors->has('price_4'))
                                                <span class="help-block">
        <span>{{ $errors->first('price_4') }}</span>
        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-3 {{$errors->has('quantity_4')? 'has-error' : ''}}">
                                            <label class="control-label fw-400">数量 - ④</label>
                                            <input type="text" name="quantity_4" class="form-control" id="inpn"
                                                   value="{{old('quantity_4')}}" placeholder="数量を入力してください">
                                            @if ($errors->has('quantity_4'))
                                                <span class="help-block">
        <span>{{ $errors->first('quantity_4') }}</span>
        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-3 {{$errors->has('couxu_commission_4') ? 'has-error':''}}">
                                            <label class="control-label fw-400">COUXU手数料率 - ④</label>
                                            <input type="text" name="couxu_commission_4" class="form-control" id="inpn"
                                                   value="{{old('couxu_commission_4')}}" placeholder="手数料率を入力してください">
                                            @if ($errors->has('couxu_commission_4'))
                                                <span class="help-block">
        <span>{{ $errors->first('couxu_commission_4') }}</span>
        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row row_create_report">
                                        <div class="col-md-12">
                                            <p id="btndivreport5"
                                               style="color: #888;line-height: 40px;background: #eee;border-radius: 4px;text-align: center;cursor: pointer;">
                                                ＋ 入力欄を追加</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- end div 4-->
                                <!-- div 5-->
                                <div style="display: none" id="divreport5">
                                    <div class="row row_create_report">
                                        <div class="col-md-12 {{ $errors->has('product_name_5') ? 'has-error' : ''}}">
                                            <label class="control-label fw-400">商品名 - ⑤</label>
                                            <input type="text" name="product_name_5" class="form-control" id="inpn"
                                                   value="{{old('product_name_5')}}" placeholder="商品名を入力してください">
                                            @if ($errors->has('product_name_5'))
                                                <span class="help-block">
        <span>{{ $errors->first('product_name_5') }}</span>
        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row row_create_report">
                                        <div class="col-md-3 {{$errors->has('product_category_5') ? 'has-error' : ''}}">
                                            <label class="control-label fw-400">商品カテゴリー - ⑤</label>
                                            <select class="form-control" name="product_category_5">
                                                <option value="">-- 選択してください --</option>
                                                @forelse ($category_list as $category)
                                                    <option {{(old('product_category_5') == $category->id) && ($category->id != 0) ? 'selected' : ''}} value="{{$category->id}}">{{$category->content}}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                            @if ($errors->has('product_category_5'))
                                                <span class="help-block">
        <span>{{ $errors->first('product_category_5') }}</span>
        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-3 {{$errors->has('price_5')? 'has-error' : ''}}">
                                            <label class="control-label fw-400">商品単価 - ⑤</label>
                                            <input type="text" name="price_5" class="form-control" id="inpn"
                                                   value="{{old('price_5')}}" placeholder="商品単価を入力してください">
                                            @if ($errors->has('price_5'))
                                                <span class="help-block">
        <span>{{ $errors->first('price_5') }}</span>
        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-3 {{$errors->has('quantity_5')? 'has-error' : ''}}">
                                            <label class="control-label fw-400">数量 - ⑤</label>
                                            <input type="text" name="quantity_5" class="form-control" id="inpn"
                                                   value="{{old('quantity_5')}}" placeholder="数量を入力してください">
                                            @if ($errors->has('quantity_5'))
                                                <span class="help-block">
        <span>{{ $errors->first('quantity_5') }}</span>
        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-3 {{$errors->has('couxu_commission_5') ? 'has-error':''}}">
                                            <label class="control-label fw-400">COUXU手数料率 - ⑤</label>
                                            <input type="text" name="couxu_commission_5" class="form-control" id="inpn"
                                                   value="{{old('couxu_commission_5')}}" placeholder="手数料率を入力してください">
                                            @if ($errors->has('couxu_commission_5'))
                                                <span class="help-block">
        <span>{{ $errors->first('couxu_commission_5') }}</span>
        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row row_create_report">
                                        <div class="col-md-12">
                                            <p id="btndivreport6"
                                               style="color: #888;line-height: 40px;background: #eee;border-radius: 4px;text-align: center;cursor: pointer;">
                                                ＋ 入力欄を追加</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- end div 5-->
                                <!-- div 6-->
                                <div style="display: none" id="divreport6">
                                    <div class="row row_create_report">
                                        <div class="col-md-12 {{ $errors->has('product_name_6') ? 'has-error' : ''}}">
                                            <label class="control-label fw-400">商品名 - ⑥</label>
                                            <input type="text" name="product_name_6" class="form-control" id="inpn"
                                                   value="{{old('product_name_6')}}" placeholder="商品名を入力してください">
                                            @if ($errors->has('product_name_6'))
                                                <span class="help-block">
        <span>{{ $errors->first('product_name_6') }}</span>
        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row row_create_report">
                                        <div class="col-md-3 {{$errors->has('product_category_6') ? 'has-error' : ''}}">
                                            <label class="control-label fw-400">商品カテゴリー - ⑥</label>
                                            <select class="form-control" name="product_category_6">
                                                <option value="">-- 選択してください --</option>
                                                @forelse ($category_list as $category)
                                                    <option {{(old('product_category_6') == $category->id) && ($category->id != 0) ? 'selected' : ''}} value="{{$category->id}}">{{$category->content}}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                            @if ($errors->has('product_category_6'))
                                                <span class="help-block">
        <span>{{ $errors->first('product_category_6') }}</span>
        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-3 {{$errors->has('price_6')? 'has-error' : ''}}">
                                            <label class="control-label fw-400">商品単価 - ⑥</label>
                                            <input type="text" name="price_6" class="form-control" id="inpn"
                                                   value="{{old('price_6')}}" placeholder="商品単価を入力してください">
                                            @if ($errors->has('price_6'))
                                                <span class="help-block">
        <span>{{ $errors->first('price_6') }}</span>
        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-3 {{$errors->has('quantity_6')? 'has-error' : ''}}">
                                            <label class="control-label fw-400">数量 - ⑥</label>
                                            <input type="text" name="quantity_6" class="form-control" id="inpn"
                                                   value="{{old('quantity_6')}}" placeholder="数量を入力してください">
                                            @if ($errors->has('quantity_6'))
                                                <span class="help-block">
        <span>{{ $errors->first('quantity_6') }}</span>
        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-3 {{$errors->has('couxu_commission_6') ? 'has-error':''}}">
                                            <label class="control-label fw-400">COUXU手数料率 - ⑥</label>
                                            <input type="text" name="couxu_commission_6" class="form-control" id="inpn"
                                                   value="{{old('couxu_commission_6')}}" placeholder="手数料率を入力してください">
                                            @if ($errors->has('quantity_6'))
                                                <span class="help-block">
        <span>{{ $errors->first('quantity_6') }}</span>
        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row row_create_report">
                                        <div class="col-md-12">
                                            <p id="btndivreport7"
                                               style="color: #888;line-height: 40px;background: #eee;border-radius: 4px;text-align: center;cursor: pointer;">
                                                ＋ 入力欄を追加</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- end div 6-->
                                <!-- div 7-->
                                <div style="display: none" id="divreport7">
                                    <div class="row row_create_report">
                                        <div class="col-md-12 {{ $errors->has('product_name_7') ? 'has-error' : ''}}">
                                            <label class="control-label fw-400">商品名 - ⑦</label>
                                            <input type="text" name="product_name_7" class="form-control" id="inpn"
                                                   value="{{old('product_name_7')}}" placeholder="商品名を入力してください">
                                            @if ($errors->has('product_name_7'))
                                                <span class="help-block">
        <span>{{ $errors->first('product_name_7') }}</span>
        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row row_create_report">
                                        <div class="col-md-3 {{$errors->has('product_category_7') ? 'has-error' : ''}}">
                                            <label class="control-label fw-400">商品カテゴリー - ⑦</label>
                                            <select class="form-control" name="product_category_7">
                                                <option value="">-- 選択してください --</option>
                                                @forelse ($category_list as $category)
                                                    <option {{(old('product_category_7') == $category->id) && ($category->id != 0) ? 'selected' : ''}} value="{{$category->id}}">{{$category->content}}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                            @if ($errors->has('product_category_7'))
                                                <span class="help-block">
        <span>{{ $errors->first('product_category_7') }}</span>
        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-3 {{$errors->has('price_7')? 'has-error' : ''}}">
                                            <label class="control-label fw-400">商品単価 - ⑦</label>
                                            <input type="text" name="price_7" class="form-control" id="inpn"
                                                   value="{{old('price_7')}}" placeholder="商品単価を入力してください">
                                            @if ($errors->has('price_7'))
                                                <span class="help-block">
        <span>{{ $errors->first('price_7') }}</span>
        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-3 {{$errors->has('quantity_7')? 'has-error' : ''}}">
                                            <label class="control-label fw-400">数量 - ⑦</label>
                                            <input type="text" name="quantity_7" class="form-control" id="inpn"
                                                   value="{{old('quantity_7')}}" placeholder="数量を入力してください">
                                            @if ($errors->has('quantity_7'))
                                                <span class="help-block">
        <span>{{ $errors->first('quantity_7') }}</span>
        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-3 {{$errors->has('couxu_commission_7') ? 'has-error':''}}">
                                            <label class="control-label fw-400">COUXU手数料率 - ⑦</label>
                                            <input type="text" name="couxu_commission_7" class="form-control" id="inpn"
                                                   value="{{old('couxu_commission_7')}}" placeholder="手数料率を入力してください">
                                            @if ($errors->has('couxu_commission_7'))
                                                <span class="help-block">
        <span>{{ $errors->first('couxu_commission_7') }}</span>
        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row row_create_report">
                                        <div class="col-md-12">
                                            <p id="btndivreport8"
                                               style="color: #888;line-height: 40px;background: #eee;border-radius: 4px;text-align: center;cursor: pointer;">
                                                ＋ 入力欄を追加</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- end div 7-->
                                <?php
                                    $character = [
                                        8 => '⑧',
                                        9 => '⑨',
                                        10 => '⑩',
                                        11 => '⑪',
                                        12 => '⑫',
                                        13 => '⑬',
                                        14 => '⑭',
                                        15 => '⑮'
                                    ];
                                ?>
                                <?php for($i=8;$i<=15;$i++){ ?>
                                <!-- div 8-->
                                <div style="display: none" id="divreport<?php echo $i; ?>">
                                    <div class="row row_create_report">
                                        <div class="col-md-12 {{ $errors->has('product_name_'.$i) ? 'has-error' : ''}}">
                                            <label class="control-label fw-400">商品名 - <?php echo $character[$i]; ?></label>
                                            <input type="text" name="product_name_<?php echo $i; ?>" class="form-control" id="inpn"
                                                   value="{{old('product_name_'.$i)}}" placeholder="商品名を入力してください">
                                            @if ($errors->has('product_name_'.$i))
                                                <span class="help-block">
        <span>{{ $errors->first('product_name_'.$i) }}</span>
        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row row_create_report">
                                        <div class="col-md-3 {{$errors->has('product_category_'.$i) ? 'has-error' : ''}}">
                                            <label class="control-label fw-400">商品カテゴリー - <?php echo $character[$i]; ?></label>
                                            <select class="form-control" name="product_category_<?php echo $i; ?>">
                                                <option value="">-- 選択してください --</option>
                                                @forelse ($category_list as $category)
                                                    <option {{(old('product_category_'.$i) == $category->id) && ($category->id != 0) ? 'selected' : ''}} value="{{$category->id}}">{{$category->content}}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                            @if ($errors->has('product_category_'.$i))
                                                <span class="help-block">
        <span>{{ $errors->first('product_category_'.$i) }}</span>
        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-3 {{$errors->has('price_'.$i)? 'has-error' : ''}}">
                                            <label class="control-label fw-400">商品単価 - <?php echo $character[$i]; ?></label>
                                            <input type="text" name="price_<?php echo $i; ?>" class="form-control" id="inpn"
                                                   value="{{old('price_'.$i)}}" placeholder="商品単価を入力してください">
                                            @if ($errors->has('price_'.$i))
                                                <span class="help-block">
        <span>{{ $errors->first('price_'.$i) }}</span>
        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-3 {{$errors->has('quantity_'.$i)? 'has-error' : ''}}">
                                            <label class="control-label fw-400">数量 - <?php echo $character[$i]; ?></label>
                                            <input type="text" name="quantity_<?php echo $i; ?>" class="form-control" id="inpn"
                                                   value="{{old('quantity_'.$i)}}" placeholder="数量を入力してください">
                                            @if ($errors->has('quantity_'.$i))
                                                <span class="help-block">
        <span>{{ $errors->first('quantity_'.$i) }}</span>
        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-3 {{$errors->has('couxu_commission_'.$i) ? 'has-error':''}}">
                                            <label class="control-label fw-400">COUXU手数料率 - <?php echo $character[$i]; ?></label>
                                            <input type="text" name="couxu_commission_<?php echo $i; ?>" class="form-control" id="inpn"
                                                   value="{{old('couxu_commission_'.$i)}}" placeholder="手数料率を入力してください">
                                            @if ($errors->has('couxu_commission_'.$i))
                                                <span class="help-block">
        <span>{{ $errors->first('couxu_commission_'.$i) }}</span>
        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <?php if($i < 15){ ?>
                                    <div class="row row_create_report">
                                        <div class="col-md-12">
                                            <p id="btndivreport<?php echo $i + 1; ?>"
                                               style="color: #888;line-height: 40px;background: #eee;border-radius: 4px;text-align: center;cursor: pointer;">
                                                ＋ 入力欄を追加</p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                                <!-- end div 8-->
                                </div>
                            </div>
                        </div>
                        <hr class="hr-dotted hr-dotted-last">
                        <!-- Button submit -->
                        <div class="row row_create_report text-center">
                            <input class="submit-report-create" type="submit" name="submit" value="送信する">
                        </div>
                    </div>
                </div>
            </form>
        </article>
    </section>
@endsection
