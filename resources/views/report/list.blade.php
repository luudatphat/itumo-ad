@extends('layouts.master')
@section('title','成約一覧')
@section('sekai_content')


    <section>
        <header><h2>成約一覧</h2></header>
        <article>

            <div class="row">
                <div class="col-md-12 text-right fix-top-layout">
                    <a class="btn btn-primary button_create_list_user fix-width-btn-01 fix-width-btn-07" href="{{url('/report_create')}}">成約報告新規作成</a>
                </div>
            </div>

            <table class="content-list-table-02 fix-table-01 result-table fix-table-02" style="white-space: nowrap">
                <thead>
                <tr>
                    <th class="text-center w-100">アタックリスト レコード番号</th>
                    <th class="text-center ">成約回数必須</th>
                    <th class="text-center ">商品情報</th>
                    <th class="text-center w-125">詳細</th>
                </tr>
                </thead>

                <tbody>
                @forelse ($report_list as $report)
                    <tr>
                        <td>{{ isset($report->attacks->request->id) ? $report->attacks->request->id : '' }}</td>
                        <!-- an_tnh_sc_126_start -->
                        <td>
                            <?php
                            if ( $report->number_of_contact == 1 ) {
                                echo '初回成約';
                            } elseif ( $report->number_of_contact == 50 ) {
                                echo $report->number_of_contact . '回目以降成約';
                            } else {
                                echo $report->number_of_contact . '回目成約';
                            }
                            ?>
                        </td>
                        <!-- an_tnh_sc_126_end -->
                        <td>
                            @foreach ($report->contract_report_products as $key => $product)
                                @if($key < 3)
                                    商品名: {{$product->product_name}} <br>
                                    商品カテゴリー: {{$product->mtb_genre->content}} <br>
                                    商品単価: {{$product->price}} YEN <br>
                                    数量: {{$product->quantity}} <br>
                                    COUXU手数料率: {{$product->couxu_commission_rate}}%   <br>
                                @endif
                            @endforeach
                        </td>
                        <td class="text-center" style="vertical-align: middle">
                            <a class="btn btn-primary fix-btn-01" href="{{ url('/report_detail/'.$report->id) }}">詳細</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4">データなし。</td>
                    </tr>
                @endforelse
                </tbody>
            </table>

            <nav class="text-right total-records pagination_div show_record">
                <p class="text-right">Records {{ ($report_list->currentPage() -1) * $report_list->perPage() + 1 }} -
                    {{ ($report_list->lastPage() == $report_list->currentPage()) ? $report_list->total() : ($report_list->currentPage() * $report_list->perPage()) }}
                    of {{ $report_list->total() }}</p>
                {{$report_list->links('pagination::bootstrap-4')}}
            </nav>

        </article>
    </section>

@endsection
