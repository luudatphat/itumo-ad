@extends('layouts.master')
@section('title','成約報告詳細')
@section('sekai_content')
    <section>
        <header><h2>成約報告詳細</h2></header>
        @if(session('message'))
            <div class="alert alert-success alert_success_company_japan">{{session('message')}}</div>
        @endif
        <article>
            <table class="table-type-01">
                <tr>
                    <th>アタックリスト レコード番号</th>
                    <td>
                        {{ isset($report_detail->attacks->request->id) ? $report_detail->attacks->request->id : '' }}
                    </td>
                </tr>
                <tr>
                    <th>インボイス</th>
                    <td>
                        <a href="{{url('/report_download/invoice/'.$report_detail->invoice_media_file->id)}}">{{$report_detail->invoice_media_file->file_name}}</a>
                    </td>
                </tr>
                <tr>
                    <th>パッキングリスト</th>
                    <td>
                        @if(isset($report_detail->packing_list_media_file))
                            <a href="{{url('/report_download/packing_list/'.$report_detail->packing_list_media_file->id)}}">{{$report_detail->packing_list_media_file->file_name}}</a>
                        @else
                            -----
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>成約回数</th>
                    <!-- an_tnh_sc_126_start -->
                    <td>
                        <?php
                        if ( $report_detail->number_of_contact == 1 ) {
                            echo '初回成約';
                        } else if ( $report_detail->number_of_contact == 50 ) {
                            echo $report_detail->number_of_contact . '回目以降成約';
                        } else {
                            echo $report_detail->number_of_contact . '回目成約';
                        }
                        ?>
                    </td>
                    <!-- an_tnh_sc_126_end -->
                </tr>
                <tr>
                    <th>商品情報</th>
                    <td>
                        <?php
                        $number_array = [
                            1 => '①',
                            2 => '②',
                            3 => '③',
                            4 => '④',
                            5 => '⑤',
                            6 => '⑥',
                            7 => '⑦',
                            8 => '⑧',
                            9 => '⑨',
                            10 => '⑩',
                            11 => '⑪',
                            12 => '⑫',
                            13 => '⑬',
                            14 => '⑭',
                            15 => '⑮'
                        ];
                        ?>
                        @foreach ($report_detail->contract_report_products as $product)
                            <div>
                                商品名 - {{$number_array[$loop->iteration]}} : {{$product->product_name}}<br>
                                商品カテゴリー - {{$number_array[$loop->iteration]}}: {{$product->mtb_genre->content}} <br>
                                商品単価 - {{$number_array[$loop->iteration]}}: {{$product->price}} YEN <br>
                                数量 - {{$number_array[$loop->iteration]}}: {{$product->quantity}} <br>
                                COUXU手数料率 - {{$number_array[$loop->iteration]}}: {{$product->couxu_commission_rate}}% <br>

                                @if(!$loop->last)
                                    <hr>
                                @endif
                            </div>
                        @endforeach
                    </td>
                </tr>
            </table>
            <div class="row">
                <div class="col-md-12 text-center">
                    <a href="{{url('report_list')}}" class="btn btn-danger btn-back-report-detail">キャンセル</a>
                    <a target="_blank" href="{{ url('report_create?id='.$report_detail->attack_id) }}" class="btn btn-success btn-create-report-detail">成約報告</a>
                </div>
            </div>
        </article>
    </section>
@endsection
