@extends('layouts.master')
@section('title','お知らせ')
@section('sekai_content')

    <section>
        <header><h2>お知らせ</h2></header>
        <article>
            <div class="banner-top"><img src="/images/banner_top.jpg" width="1340" height="380" alt="最新ニュース"></div>
            <div class="content-a-col">
                <div class="a-col">
                    <table>
                        <?php
                            $count = 0;
                        ?>
                        @foreach($list_news_other[6] as $item)
                            @if(isset($item))
                                    <?php
                                    $count += 1;
                                    if($count <= 5){
                                    ?>
                                <tr>
                                    <td>{{date_format(date_create($item['created_at']),'Y/m/d')}}</td>
                                    <td><a href="/news/details/{{$item['id']}}" target="_blank">{{$item['title']}}</a></td>
                                </tr>
                                <?php
                                            }
                                ?>
                            @endif
                        @endforeach
                    </table>
                </div>
            </div>
        </article>
    </section>

    <section>
        <header><h2>カリキュラム</h2></header>
        <article>
            <div class="banner-top"><img src="/images/banner_01.jpg" width="1340" height="380" alt="営業カリキュラム　- 海外輸出・貿易の基礎〜応用を学ぶ -　海外輸出・貿易を基礎から応用まで。最新の情報を、学習したい内容だけ効率的に学ぶことができます。 1 順番通り学ぶだけ すぐに使える能力が体系的に身につきます。 2 効率的に学習できる 学習したいカリキュラムだけ 効率よく学習できます。 3 コンテンツは常に最新 カリキュラムは常に最新の情 報に更新しています。"></div>
            <div class="box-link-on-page">カリキュラム一覧
                <ul>
                    @foreach($list_categories_news as $item)
                    <li><a href="#section-0{{$item->id}}">{{$item->content}}</a></li>
                    @endforeach
                </ul>
            </div>

            @foreach($list_categories_news as $item)

                <div id="section-0{{$item->id}}" class="contents-group">
                    <header><img src="/images/thumnail_category_0{{$item->id}}.jpg" width="1340" height="200" alt="{{$item->content}}"></header>
                    <div class="content-list-table">
                        <table>
                            <?php
                            $count = 0;
                            ?>
                            @if(isset($list_news_admin[$item->id]))
                                @foreach($list_news_admin[$item->id] as $item_post)
                                    @if(isset($item))
                                        <?php
                                        $count += 1;
                                        ?>
                                        <tr>
                                            <td class="active">
                                                <div class="Cell w40psubleft"><a href="/news/details/{{$item_post['id']}}" target="_blank"><?php echo $count ?>.  {{$item_post['title']}}</a></div>
                                            </td>
                                            <td class="active">
                                                <div class="Cell w40psub">
                                                    <a href="/news/details/{{$item_post['id']}}" target="_blank"><?php echo(strip_tags($item_post['content'])); ?></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif

                        </table>
                    </div>
                </div>
            @endforeach
        </article>
    </section>

    <section>
        <header><h2 class="icon-title">セカイコネクト</h2></header>
        <article>
            <h3>最新ニュース</h3>
            <div class="content-2col">
            @foreach($list_news_world_connect as $item)
                <div class="a-col">
                    <table>
                        <tr>
                            <td>{{isset($item['pubDate'])?date_format(date_create($item['pubDate']),'Y/m/d'):''}}</td>
                            <td><a href="{{$item['link']}}" target="_blank">{{$item['title']}}</a></td>
                        </tr>
                    </table>
                </div>
            @endforeach
            </div>
        </article>
    </section>

    <script type="application/javascript">
        $(document).ready(function (e) {
            'use strict';
            $('.box-link-on-page a').click(function (e) {
                e.preventDefault();
                var positionTop = $($(this).attr('href')).offset().top - 62;
                console.log(positionTop);
                $('html, body').animate({'scrollTop': positionTop}, 500);
            })
        });
    </script>

@endsection