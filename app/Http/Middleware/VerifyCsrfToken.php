<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
//        'create-buyer',
//        'create-request',
//        'create-buyer-by-form',
//        '/login-company'
        'create-buyer-by-form',
        'create-request-by-form'
    ];
}
