<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $arr_validation = [
            'attack_id' => 'required|numeric',
            'invoice' => 'required|max:5120',
            'packing_list' => 'max:5120',
            'number_contact' => 'required|numeric',

            'product_name_1' => 'required',
            'product_category_1' => 'required',
            'price_1' => 'required|numeric|nullable',
            'quantity_1' => 'required|numeric|nullable',
            'couxu_commission_1' => 'required|numeric|nullable',

            'product_name_2' => 'required_with:product_category_2,price_2,quantity_2,couxu_commission_2',
            'product_category_2' => 'required_with:product_name_2,price_2,quantity_2,couxu_commission_2',
            'price_2' => 'required_with:product_category_2,product_name_2,quantity_2,couxu_commission_2|numeric|nullable',
            'quantity_2' => 'required_with:product_category_2,price_2,product_name_2,couxu_commission_2|numeric|nullable',
            'couxu_commission_2' => 'required_with:product_category_2,price_2,quantity_2,product_name_2|numeric|nullable',

            'product_name_3' => 'required_with:product_category_3,price_3,quantity_3,couxu_commission_3',
            'product_category_3' => 'required_with:product_name_3,price_3,quantity_3,couxu_commission_3',
            'price_3' => 'required_with:product_category_3,product_name_3,quantity_3,couxu_commission_3|numeric|nullable',
            'quantity_3' => 'required_with:product_category_3,price_3,product_name_3,couxu_commission_3|numeric|nullable',
            'couxu_commission_3' => 'required_with:product_category_3,price_3,quantity_3,product_name_3|numeric|nullable',

            'product_name_4' => 'required_with:product_category_4,price_4,quantity_4,couxu_commission_4',
            'product_category_4' => 'required_with:product_name_4,price_4,quantity_4,couxu_commission_4',
            'price_4' => 'required_with:product_category_4,product_name_4,quantity_4,couxu_commission_4|numeric|nullable',
            'quantity_4' => 'required_with:product_category_4,price_4,product_name_4,couxu_commission_4|numeric|nullable',
            'couxu_commission_4' => 'required_with:product_category_4,price_4,quantity_4,product_name_4|numeric|nullable',

            'product_name_5' => 'required_with:product_category_5,price_5,quantity_5,couxu_commission_5',
            'product_category_5' => 'required_with:product_name_5,price_5,quantity_5,couxu_commission_5',
            'price_5' => 'required_with:product_category_5,product_name_5,quantity_5,couxu_commission_5|numeric|nullable',
            'quantity_5' => 'required_with:product_category_5,price_5,product_name_5,couxu_commission_5|numeric|nullable',
            'couxu_commission_5' => 'required_with:product_category_5,price_5,quantity_5,product_name_5|numeric|nullable',

            'product_name_6' => 'required_with:product_category_6,price_6,quantity_6,couxu_commission_6',
            'product_category_6' => 'required_with:product_name_6,price_6,quantity_6,couxu_commission_6',
            'price_6' => 'required_with:product_category_6,product_name_6,quantity_6,couxu_commission_6|numeric|nullable',
            'quantity_6' => 'required_with:product_category_6,price_6,product_name_6,couxu_commission_6|numeric|nullable',
            'couxu_commission_6' => 'required_with:product_category_6,price_6,quantity_6,product_name_6|numeric|nullable',

            'product_name_7' => 'required_with:product_category_7,price_7,quantity_7,couxu_commission_7',
            'product_category_7' => 'required_with:product_name_7,price_7,quantity_7,couxu_commission_7',
            'price_7' => 'required_with:product_category_7,product_name_7,quantity_7,couxu_commission_7|numeric|nullable',
            'quantity_7' => 'required_with:product_category_7,price_7,product_name_7,couxu_commission_7|numeric|nullable',
            'couxu_commission_7' => 'required_with:product_category_7,price_7,quantity_7,product_name_7|numeric|nullable',
        ];

        for($i = 8 ; $i <= 15; $i++){
            $arr = [
                'product_name_'.$i => 'required_with:product_category_'.$i.',price_'.$i.',quantity_'.$i.',couxu_commission_'.$i.'',
                'product_category_'.$i => 'required_with:product_name_'.$i.',price_'.$i.',quantity_'.$i.',couxu_commission_'.$i.'',
                'price_'.$i => 'required_with:product_category_'.$i.',product_name_'.$i.',quantity_'.$i.',couxu_commission_'.$i.'|numeric|nullable',
                'quantity_'.$i => 'required_with:product_category_'.$i.',price_'.$i.',product_name_'.$i.',couxu_commission_'.$i.'|numeric|nullable',
                'couxu_commission_'.$i => 'required_with:product_category_'.$i.',price_'.$i.',quantity_'.$i.',product_name_'.$i.'|numeric|nullable',
            ];
            $arr_validation = array_merge($arr_validation, $arr);
        }

        return $arr_validation;
    }

    public function messages(){
        $arr_messages =  [
            'product_name_2.required_with' => trans('validation.required'),
            'product_category_2.required_with' => trans('validation.required'),
            'price_2.required_with' => trans('validation.required'),
            'quantity_2.required_with' => trans('validation.required'),
            'couxu_commission_2.required_with' => trans('validation.required'),
            'product_name_3.required_with' => trans('validation.required'),
            'product_category_3.required_with' => trans('validation.required'),
            'price_3.required_with' => trans('validation.required'),
            'quantity_3.required_with' => trans('validation.required'),
            'couxu_commission_3.required_with' => trans('validation.required'),
            'product_name_4.required_with' => trans('validation.required'),
            'product_category_4.required_with' => trans('validation.required'),
            'price_4.required_with' => trans('validation.required'),
            'quantity_4.required_with' => trans('validation.required'),
            'couxu_commission_4.required_with' => trans('validation.required'),
            'product_name_5.required_with' => trans('validation.required'),
            'product_category_5.required_with' => trans('validation.required'),
            'price_5.required_with' => trans('validation.required'),
            'quantity_5.required_with' => trans('validation.required'),
            'couxu_commission_5.required_with' => trans('validation.required'),
            'product_name_6.required_with' => trans('validation.required'),
            'product_category_6.required_with' => trans('validation.required'),
            'price_6.required_with' => trans('validation.required'),
            'quantity_6.required_with' => trans('validation.required'),
            'couxu_commission_6.required_with' => trans('validation.required'),
            'product_name_7.required_with' => trans('validation.required'),
            'product_category_7.required_with' => trans('validation.required'),
            'price_7.required_with' => trans('validation.required'),
            'quantity_7.required_with' => trans('validation.required'),
            'couxu_commission_7.required_with' => trans('validation.required'),
        ];
        for($i = 8 ; $i <= 15; $i++){
            $arr = [
                'product_name_'.$i.'.required_with' => trans('validation.required'),
                'product_category_'.$i.'.required_with' => trans('validation.required'),
                'price_'.$i.'.required_with' => trans('validation.required'),
                'quantity_'.$i.'.required_with' => trans('validation.required'),
                'couxu_commission_'.$i.'.required_with' => trans('validation.required'),
            ];
            $arr_messages = array_merge($arr_messages, $arr);
        }

        return $arr_messages;
    }
}
