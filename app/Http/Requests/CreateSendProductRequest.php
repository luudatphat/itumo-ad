<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSendProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $arr_validation = [
            'last_name' => 'required|max:255',
            'first_name' => 'required|max:255',
            'address' => 'required|max:255',
            'city' => 'required|max:255',
            'country' => 'required|max:255',
            'company_name' => 'required|max:255',

            'product_name_1' => 'max:255|required',
            'price_1' => 'numeric|required',
            'quantity_1' => 'integer|required',

            'weight_1' => 'integer|required',
            'vertical_1' => 'integer|required',
            'width_1' => 'integer|required',
            'height_1' => 'integer|required',
            'box_number_1' => 'integer|required',
            'requested_delivery_date' => 'required|date_format:Y-m-d',
        ];
        for ($i = 2; $i <= 10; $i++) {
            $arr = [
                'weight_'.$i => 'integer|required_with:vertical_'.$i.',width_'.$i.',height_'.$i.',box_number_'.$i,
                'vertical_'.$i => 'integer|required_with:weight_'.$i.',width_'.$i.',height_'.$i.',box_number_'.$i,
                'width_'.$i => 'integer|required_with:vertical_'.$i.',weight_'.$i.',height_'.$i.',box_number_'.$i,
                'height_'.$i => 'integer|required_with:vertical_'.$i.',width_'.$i.',weight_'.$i.',box_number_'.$i,
                'box_number_'.$i => 'integer|required_with:vertical_'.$i.',width_'.$i.',height_'.$i.',weight_'.$i,

                'product_name_'.$i => 'max:255|required_with:price_'.$i.',quantity_'.$i,
                'price_'.$i => 'numeric|required_with:product_name_'.$i.',quantity_'.$i,
                'quantity_'.$i => 'integer|required_with:price_'.$i.',product_name_'.$i,
            ];
            $arr_validation = array_merge($arr_validation, $arr);
        }
        return $arr_validation;
    }
    public function messages()
    {
        $arr_messages = [];
        for ($i = 2; $i <= 10; $i++) {
            $arr = [
                'weight_'.$i.'.required_with' => trans('validation.required'),
                'vertical_'.$i.'.required_with' => trans('validation.required'),
                'width_'.$i.'.required_with' => trans('validation.required'),
                'height_'.$i.'.required_with' => trans('validation.required'),
                'box_number_'.$i.'.required_with' => trans('validation.required'),

                'product_name_'.$i.'.required_with' => trans('validation.required'),
                'price_'.$i.'.required_with' => trans('validation.required'),
                'quantity_'.$i.'.required_with' => trans('validation.required'),
            ];
            $arr_messages = array_merge($arr_messages, $arr);
        }
        return $arr_messages;
    }
}
