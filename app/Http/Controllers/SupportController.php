<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSupportRequest;
use App\Models\Mtb_Support_Category;
use App\Models\Support;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Config;

class SupportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('userDeleted');
    }

    public function create_support() {
        /* an_tnh_sc_94_start */
        $_free_user = Auth::user()->free_user;
        $data['free_user'] = $_free_user;
        /* an_tnh_sc_94_end */

        $data['support_categories'] = Mtb_Support_Category::all();

        return view('contact.create',$data);
    }

    public function create_support_handler(CreateSupportRequest $request) {
        $support = new Support();
        $support->mtb_support_category_id = $request->category;
        $support->content = $request->support_content;
        $supliers = Auth::user()->supplier;
        $support->supplier_id = $supliers->id;
        $support->status = config('constants.STATUS_SUPPORT_WAITING');

        if(isset($request->category)){
            if($request->category == 1){

                $data['mtb_support_category'] = '商談方法';

            }elseif($request->category == 2){

                $data['mtb_support_category'] = '商品掲載について';

            }else{

                $data['mtb_support_category'] = 'その他';

            }
        }else{
            $data['mtb_support_category'] = 'その他';
        }

        $data['content']  = $request->support_content;
        $data['company_name'] = $supliers->company_name_jp;
        $data['contact_name_kanji'] = $supliers->contact_name_kanji;

        Mail::raw($this->notificationSupport($data), function($message) use ($data)
        {
            $message->from(Config::get('mail.from.address'));
            $message->subject("【サポート窓口に問合せがありました】");
            $message->to('support@couxu.jp');
//            $message->to('sekai+330@staff.distance-c.asia');
        });

        if ($support->save()) {
            return back()->with('message',trans('messages.create_support_success'));
        } else {
            abort(500);
        }
    }

    private function notificationSupport($data){
        $text = "
企業名：".$data['company_name']."
担当者名：".$data['contact_name_kanji']."

《問い合わせ内容》
".$data['mtb_support_category']."

《詳細》
".$data['content'];
        return $text;
    }
}
