<?php
/**
 * Created by PhpStorm.
 * User: thuan.nguyen
 * Date: 12/14/16
 * Time: 4:42 PM
 *
 * Must have account to get token from my website
 * Must have client_id and client_secret from table oauth_clients
 * Config file index.php by add header on the top page (Note: must have Authorization)
 * If not working add header into .htaccess
 */
namespace App\Http\Controllers;

use App\Models\Buyer;
use App\Models\Country;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Validator;
use App\Models\Buyer_Business_Category;
use App\Models\Buyer_Handled_Good;
use App\Models\Request_Business_Category;
use GuzzleHttp;
use \App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Config;

class ApiController extends Controller {

    public function createCompanyForeign(Request $request){

        if($request->server->all()['HTTP_ORIGIN'] === 'http://25hours.sc.distance-c.com' ||
            $request->server->all()['HTTP_ORIGIN'] === 'https://25hours.sc.distance-c.com' ||
            $request->server->all()['HTTP_ORIGIN'] === 'http://sekai-connect.local'||
            $request->server->all()['HTTP_ORIGIN'] === 'http://supplier.sc.distance-c.com'||
            $request->server->all()['HTTP_ORIGIN'] === 'https://supplier.sc.distance-c.com' ||
            $request->server->all()['HTTP_ORIGIN'] === 'http://sekai-company.local'||
            $request->server->all()['HTTP_ORIGIN'] === 'https://sekai-company.local' ||
            $request->server->all()['HTTP_ORIGIN'] === 'http://sekai-connect.company'||
            $request->server->all()['HTTP_ORIGIN'] === 'https://sekai-connect.company'||
            $request->server->all()['HTTP_ORIGIN'] === 'http://jpsupplier.test.commerce-sourcing.com'||
            $request->server->all()['HTTP_ORIGIN'] === 'https://jpsupplier.test.commerce-sourcing.com' ||
            $request->server->all()['HTTP_ORIGIN'] === 'http://jpsupplier.commerce-sourcing.com'||
            $request->server->all()['HTTP_ORIGIN'] === 'https://jpsupplier.commerce-sourcing.com' ||
            $request->server->all()['HTTP_ORIGIN'] === 'http://25hoursbuyers.com'||
            $request->server->all()['HTTP_ORIGIN'] === 'https://25hoursbuyers.com'
        ){
            //Validate form
            $data['errors'] = Array();

            if ($request->country === "") {
                $data['errors']['country_required'] = 'Country is required';
            }

            if (isset($request->questions)) {
                $questions = $request->questions;
                if ($questions['176815'] === "") {
                    $data['errors']['176815']['required'] = 'Company name is required';
                }
                if (strlen($questions['176815']) > 64) {
                    $data['errors']['176815']['max'] = 'Company name is max 64 character';
                }
                if ($questions['176816'] === "") {
                    $data['errors']['176816'] = 'Title is required';
                }
                if ($questions['176817'] === "") {
                    $data['errors']['176817'] = 'First Name is required';
                }
                if ($questions['176818'] === "") {
                    $data['errors']['176818'] = 'Last Name is required';
                }
//                if ($questions['176819'] === "") {
//                    $data['errors']['176819'] = 'Country is required';
//                }
                if ($questions['176820'] === "") {
                    $data['errors']['176820'] = 'Address is required';
                }
                if ($questions['176821'] === "") {
                    $data['errors']['176821']['required'] = 'Phone number is required';
                }
//                if (!is_numeric($questions['176821'])) {
//                    $data['errors']['176821']['numeric'] = 'Phone number is number';
//                }
                if ($questions['176823'] === "") {
                    $data['errors']['176823']['required'] = 'E-Mail address is required';
                }
                if (!filter_var($questions['176823'], FILTER_VALIDATE_EMAIL)) {
                    $data['errors']['176823']['email'] = 'E-Mail address is email format';
                }

                if ($questions['176824'] === "") {
                    $data['errors']['176824'] = 'SNS tool is required';
                }
//                if ($questions['176826'] === "") {
//                    $data['errors']['176826']['required'] = 'Website URL is required';
//                }
//                if (!preg_match('/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}' . '((:[0-9]{1,5})?\\/.*)?$/i', $questions['176826'])) {
//                    $data['errors']['176826']['url'] = 'Website URL is not url format';
//                }
                if (!isset($questions['176827'])) {
                    $data['errors']['176827'] = 'Business category is required';
                }
                if (!isset($questions['176828'])) {
                    $data['errors']['176828']['required'] = 'Handled goods is required';
                }
            }

            $check_buyer = Buyer::where('email_address', $questions['176823'])->get();
            if (count($check_buyer) > 0) {
                $data['errors']['176823']['unique'] = 'E-Mail address exist';
            }

            if (count($data['errors']) > 0) {
                return view('api.show-error', $data);
            }

            //Transaction
            try {
                DB::beginTransaction();
                //add company foreign
                $new_company = new Buyer();
                $new_company->company_name = $request->questions['176815'];

                if ($request->questions['176816'] == '1059191') {
                    $new_company->mtb_title_id = 'Mr';
                } elseif ($request->questions['176816'] == '1059192') {
                    $new_company->mtb_title_id = 'Miss';
                } elseif ($request->questions['176816'] == '1059193') {
                    $new_company->mtb_title_id = 'Mrs';
                }elseif ($request->questions['176816'] == '29111994') {
                    $new_company->mtb_title_id = 'Ms';
                }
                else {
                    $new_company->mtb_title_id = '';
                }

                $new_company->name = $request->questions['176817'];
                $new_company->surname = $request->questions['176818'];

//                $country = Country::where('country_name', $request->questions['176819'])->get();
//                if (count($country) > 0) {
//                    $new_company->country = $country[0]['id'];
//                } else {
//                    $new_company->country = 246;
//                }

                $country = Country::where('country_code',$request->country)->first();
                if(empty($country)){
                    //return json_encode(array('status' => false, 'messages' => "Country isn't exists",'code' => '500'));
                    $data['errors']['country'] = "Country isn't exists";
                    return view('api.show-error', $data);
                }
                $new_company->country = $country->id;

                $new_company->address = $request->questions['176820'];
                $new_company->phone_number = $request->questions['176821'];
                $new_company->cell_phone_number = $request->questions['176822'];
                $new_company->email_address = $request->questions['176823'];

                if ($request->questions['176824'] == '1059194') {
                    $new_company->mtb_sns_tool_id = 1;
                }
                if ($request->questions['176824'] == '1059195') {
                    $new_company->mtb_sns_tool_id = 2;
                }
                if ($request->questions['176824'] == '1059196') {
                    $new_company->mtb_sns_tool_id = 3;
                }
                if ($request->questions['176824'] == '1059197') {
                    $new_company->mtb_sns_tool_id = 4;
                }
                if ($request->questions['176824'] == '1059198') {
                    $new_company->mtb_sns_tool_id = 5;
                }
                if ($request->questions['176824'] == '1059199') {
                    $new_company->mtb_sns_tool_id = 6;
                }
                if ($request->questions['176824'] == '1059200') {
                    $new_company->mtb_sns_tool_id = 7;
                }

                $new_company->sns_id = $request->questions['176825'];
                $new_company->company_skype_id = $request->questions['176834'];
                $new_company->company_website_url = $request->questions['176826'];
                $new_company->desired_product = $request->questions['176829'];
                $new_company->purchasing_country = '';
                $new_company->sale_country = '';
                $new_company->purchase_reason = '';
                $new_company->company_strength = '';
                $new_company->status = 2;
                $new_company->rating = 0;
                $new_company->year_month = (Carbon::now()->year) * 100 + Carbon::now()->month;
                $new_company->save();

                //add record into table buyer_business_category
                $buyer_business_categories_list = $request->questions['176827'];
                if (count($buyer_business_categories_list) > 0) {
                    foreach ($buyer_business_categories_list as $buyer_business_categories) {
                        $buyer_business_categories_item = new Buyer_Business_Category();
                        $buyer_business_categories_item->buyer_id = $new_company->id;

                        if ($buyer_business_categories == '1059201') {
                            $buyer_business_categories_item->mtb_business_category_id = 1;
                        }
                        if ($buyer_business_categories == '1059202') {
                            $buyer_business_categories_item->mtb_business_category_id = 7;
                        }
                        if ($buyer_business_categories == '1059203') {
                            $buyer_business_categories_item->mtb_business_category_id = 2;
                        }
                        if ($buyer_business_categories == '1059204') {
                            $buyer_business_categories_item->mtb_business_category_id = 3;
                        }
                        if ($buyer_business_categories == '1059205') {
                            $buyer_business_categories_item->mtb_business_category_id = 4;
                        }
                        if ($buyer_business_categories == '1059206') {
                            $buyer_business_categories_item->mtb_business_category_id = 8;
                        }

                        $buyer_business_categories_item->save();
                    }
                }

                //add record into table buyer_handled_good
                $buyer_good_category_list = $request->questions['176828'];
                $count_buyer_good_category = count($buyer_good_category_list);
                if ($count_buyer_good_category > 0) {
//                    if ($count_buyer_good_category > 3) {
//                        DB::rollBack();
//                        $data['errors']['176828']['options'] = 'Handled goods is only allowed to choose 3 options';
//                        return view('api.show-error', $data);
//                    }
                    $buyer_handled_good_item = '';
                    foreach ($buyer_good_category_list as $buyer_handled_good) {
                        $buyer_handled_good_item = new Buyer_Handled_Good();
                        $buyer_handled_good_item->buyer_id = $new_company->id;

                        if ($buyer_handled_good == '1059207') {
                            $buyer_handled_good_item->mtb_goods_category_id = 1;
                        }
                        if ($buyer_handled_good == '1059208') {
                            $buyer_handled_good_item->mtb_goods_category_id = 2;
                        }
                        if ($buyer_handled_good == '1059209') {
                            $buyer_handled_good_item->mtb_goods_category_id = 3;
                        }
                        if ($buyer_handled_good == '1059210') {
                            $buyer_handled_good_item->mtb_goods_category_id = 4;
                        }
                        if ($buyer_handled_good == '1059211') {
                            $buyer_handled_good_item->mtb_goods_category_id = 5;
                        }
                        if ($buyer_handled_good == '1059212') {
                            $buyer_handled_good_item->mtb_goods_category_id = 6;
                        }
                        if ($buyer_handled_good == '1059213') {
                            $buyer_handled_good_item->mtb_goods_category_id = 7;
                        }
                        if ($buyer_handled_good == '1059214') {
                            $buyer_handled_good_item->mtb_goods_category_id = 8;
                        }
                        if ($buyer_handled_good == '1059215') {
                            $buyer_handled_good_item->mtb_goods_category_id = 9;
                        }
                        if ($buyer_handled_good == '1059216') {
                            $buyer_handled_good_item->mtb_goods_category_id = 10;
                        }
                        if ($buyer_handled_good == '1059217') {
                            $buyer_handled_good_item->mtb_goods_category_id = 11;
                        }

                        $buyer_handled_good_item->save();
                    }
                }

                DB::commit();

                $data['company_name'] = $new_company->company_name;
                $data['contact_name'] = $new_company->name.' '.$new_company->surname;

                Mail::raw($this->notificationBuyerRegistion($data), function($message) use ($data)
                {
                    $message->from(Config::get('mail.from.address'));
                    $message->subject("【新規バイヤー登録がありました】");
                    $message->to('support@couxu.jp');
//            $message->to('sekai+330@staff.distance-c.asia');
                });
                if($request->server->all()['HTTP_ORIGIN'] === 'http://25hours.sc.distance-c.com' ||
                    $request->server->all()['HTTP_ORIGIN'] === 'https://25hours.sc.distance-c.com' ||
                    $request->server->all()['HTTP_ORIGIN'] === 'http://25hoursbuyers.com'||
                    $request->server->all()['HTTP_ORIGIN'] === 'https://25hoursbuyers.com'
                ){
                    return redirect($request->server->all()['HTTP_ORIGIN'].'/interim/');
                }else{
                    return redirect('/interim');
                }


            } catch (\Exception $e) {
                $data['errors']['DB'] = $e;
                return view('api.show-error', $data);
                DB::rollBack();
            }
        }else{
            $data['errors_domain'] = 'Have some errors. Please try again later.';
            return view('api.show-error', $data);
        }

        $data['errors']['something']= 'Have some errors. Please try again later.';
        return view('api.show-error', $data);

    }

    private function notificationBuyerRegistion($data){
        $text = "
企業名：".$data['company_name']."
担当者名：".$data['contact_name'];
        return $text;
    }

    public function createRequest(Request $request){

        if($request->server->all()['HTTP_ORIGIN'] === 'http://25hours.sc.distance-c.com' ||
            $request->server->all()['HTTP_ORIGIN'] === 'https://25hours.sc.distance-c.com' ||
            $request->server->all()['HTTP_ORIGIN'] === 'http://sekai-connect.local'||
            $request->server->all()['HTTP_ORIGIN'] === 'http://supplier.sc.distance-c.com'||
            $request->server->all()['HTTP_ORIGIN'] === 'https://supplier.sc.distance-c.com' ||
            $request->server->all()['HTTP_ORIGIN'] === 'http://sekai-company.local'||
            $request->server->all()['HTTP_ORIGIN'] === 'https://sekai-company.local' ||
            $request->server->all()['HTTP_ORIGIN'] === 'http://jpsupplier.test.commerce-sourcing.com'||
            $request->server->all()['HTTP_ORIGIN'] === 'https://jpsupplier.test.commerce-sourcing.com' ||
            $request->server->all()['HTTP_ORIGIN'] === 'http://jpsupplier.commerce-sourcing.com'||
            $request->server->all()['HTTP_ORIGIN'] === 'https://jpsupplier.commerce-sourcing.com' ||
            $request->server->all()['HTTP_ORIGIN'] === 'http://25hoursbuyers.com'||
            $request->server->all()['HTTP_ORIGIN'] === 'https://25hoursbuyers.com'
        ) {


            $data['errors'] = Array();
            //Validate form

            $email = $request->questions['179568'];
            $buyer_id = Buyer::where('email_address', $email)->first();
            if (empty($buyer_id)) {
                $data['errors']['179568']['not_exist'] = '';
                return view('api.show-error', $data);
                //return json_encode(array('status' => false, 'messages' => 'Email not exists in system','code' => '500'));
            }
            if ($buyer_id->status != 1) {
                $data['errors']['179568']['not_active'] = '';
                return view('api.show-error', $data);
                //return json_encode(array('status' => false, 'messages' => "Company isn’t active",'code' => '500'));
            }

            if (isset($request->questions)) {
                $questions = $request->questions;
                if ($questions['179568'] === "") {
                    $data['errors']['179568']['required'] = 'Email is required';
                }
                if (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', $questions['179568'])) {
                    $data['errors']['179568']['email_format'] = 'E-Mail address is email format';
                }
                if ($questions['179540'] === "") {
                    $data['errors']['179540'] = 'Type required is required';
                }
                if ($questions['179547'] === "") {
                    $data['errors']['179547'] = 'Goods category is required';
                }

                if ($questions['179542']['year'] === "") {
                    $data['errors']['179542']['year'] = 'Year is required';
                }
                if ($questions['179542']['month'] === "") {
                    $data['errors']['179542']['month'] = 'Month is required';
                }
                if ($questions['179542']['day'] === "") {
                    $data['errors']['179542']['day'] = 'Day is required';
                }
                if ($questions['179543'] === "") {
                    $data['errors']['179543'] = 'Preferred supply period is required';
                }

                if ($questions['179544'] != "") {
                    if (!is_numeric($questions['179544'])) {
                        $data['errors']['179544']['numeric'] = 'Ideal buying price must is numeric';
                    }
                    if ($questions['179544'] < 1) {
                        $data['errors']['179544']['large1'] = 'Ideal buying price min is 1';
                    }
                }

                if ($questions['179545'] != "") {
                    if (!is_numeric($questions['179545'])) {
                        $data['errors']['179545']['numeric'] = 'Planned retail price must is numeric';
                    }
                    if ($questions['179545'] < 1) {
                        $data['errors']['179545']['large1'] = 'Planned retail price min is 1';
                    }
                }

                if ($questions['179546'] != "") {
                    if (!is_numeric($questions['179546'])) {
                        $data['errors']['179546']['numeric'] = 'Quantity must is numeric';
                    }
                    if ($questions['179546'] < 1) {
                        $data['errors']['179546']['large1'] = 'Quantity min is 1';
                    }
                }
            }

            if (count($data['errors']) > 0) {
                return view('api.show-error', $data);
            }

            //Transaction
            try {
                DB::beginTransaction();
                //add record into table Request
                $new_request = new \App\Models\Request();

                $email = $request->questions['179568'];
                $buyer_id = Buyer::where('email_address', $email)->first();
                $new_request->buyer_id = $buyer_id->id;

                //$new_request->type_required = $request->questions['179540'];

                if ($request->questions['179540'] == '1066905') {
                    $new_request->type_required = 1;
                }
                if ($request->questions['179540'] == '1066906') {
                    $new_request->type_required = 2;
                }

                //$new_request->mtb_goods_category_id = $request->questions['179547'];

                if ($request->questions['179547'] == '1066909') {
                    $new_request->mtb_goods_category_id = 1;
                }
                if ($request->questions['179547'] == '1066910') {
                    $new_request->mtb_goods_category_id = 2;
                }
                if ($request->questions['179547'] == '1066911') {
                    $new_request->mtb_goods_category_id = 3;
                }
                if ($request->questions['179547'] == '1066912') {
                    $new_request->mtb_goods_category_id = 4;
                }
                if ($request->questions['179547'] == '1066913') {
                    $new_request->mtb_goods_category_id = 5;
                }
                if ($request->questions['179547'] == '1066914') {
                    $new_request->mtb_goods_category_id = 6;
                }
                if ($request->questions['179547'] == '1066915') {
                    $new_request->mtb_goods_category_id = 7;
                }
                if ($request->questions['179547'] == '1066916') {
                    $new_request->mtb_goods_category_id = 8;
                }
                if ($request->questions['179547'] == '1066917') {
                    $new_request->mtb_goods_category_id = 9;
                }
                if ($request->questions['179547'] == '1066918') {
                    $new_request->mtb_goods_category_id = 10;
                }
                if ($request->questions['179547'] == '1066919') {
                    $new_request->mtb_goods_category_id = 11;
                }

                if (empty($request->questions['179541'])) {
                    $new_request->product_description_en = '';
                }
                $new_request->product_description_en = $request->questions['179541'];
                $new_request->product_description_jp = '';

                $year = $request->questions['179542']['year'];
                $month = $request->questions['179542']['month'];
                $day = $request->questions['179542']['day'];
                if ($month < 10) {
                    $month = "0" . $month;
                }
                if ($day < 10) {
                    $day = "0" . $day;
                }

                $str_expire = $year . '-' . $month . '-' . $day;

                $data_time = date_format(date_create($str_expire), "Y-m-d");

                if (strcmp($str_expire, $data_time) == 0) {
                    $new_request->expire_date = $str_expire;
                } else {
                    //return json_encode(array('status' => false, 'messages' => "Deadline isn't format",'code' => '500'));
                    $data['errors']['date']['format'] = 'Deadline isn\'t format';
                    return view('api.show-error', $data);
                }

                $today = Carbon::today();

                if (strtotime($data_time) < strtotime($today)) {
                    //return json_encode(array('status' => false, 'messages' => 'Deadline must greater today','code' => '500'));
                    $data['errors']['date']['greater'] = 'Deadline must greater today';
                    return view('api.show-error', $data);
                }

//            $new_request->expire_date = date_format(date_create($request->expire_date),"Y-m-d");
                // $new_request->expire_date = $str_expire;


                //$new_request->supply_period = $request->questions['179543'];

                if ($request->questions['179543'] == '1066907') {
                    $new_request->supply_period = 1;
                }
                if ($request->questions['179543'] == '1066908') {
                    $new_request->supply_period = 2;
                }

                if (empty($request->questions['179544'])) {
                    $new_request->ideal_price = 0;
                } else {
                    $new_request->ideal_price = $request->questions['179544'];
                }


                if (empty($request->questions['179545'])) {
                    $new_request->retail_price = 0;
                } else {
                    $new_request->retail_price = $request->questions['179545'];
                }


                if (empty($request->questions['179546'])) {
                    $new_request->quantity = 0;
                } else {
                    $new_request->quantity = $request->questions['179546'];
                }

                $new_request->mtb_genre_id = 1;
                $new_request->date = date("Y-m-d");
                $new_request->status = 3;
                $new_request->year_month = (Carbon::now()->year) * 100 + Carbon::now()->month;

                $new_request->save();

                if(isset($request->questions['179567'])){
                    //add record into table buyer_business_category
                    $customer_category_list = $request->questions['179567'];
                    if ($request->questions['179567'] != null) {
                        foreach ($customer_category_list as $customer_category) {
                            $request_item = new Request_Business_Category();
                            $request_item->request_id = $new_request->id;

                            if ($customer_category == '1066967') {
                                $request_item->mtb_business_category_id = 1;
                            }
                            if ($customer_category == '1066968') {
                                $request_item->mtb_business_category_id = 2;
                            }
                            if ($customer_category == '1066969') {
                                $request_item->mtb_business_category_id = 3;
                            }
                            if ($customer_category == '1066970') {
                                $request_item->mtb_business_category_id = 4;
                            }
                            if ($customer_category == '1066971') {
                                $request_item->mtb_business_category_id = 5;
                            }
                            if ($customer_category == '1066972') {
                                $request_item->mtb_business_category_id = 6;
                            }
                            if ($customer_category == '1066973') {
                                $request_item->mtb_business_category_id = 7;
                            }

                            //$request_item->mtb_business_category_id = $customer_category;

                            $request_item->save();
                        }
                    }
                }

                DB::commit();

                if($request->server->all()['HTTP_ORIGIN'] === 'http://25hours.sc.distance-c.com' ||
                    $request->server->all()['HTTP_ORIGIN'] === 'https://25hours.sc.distance-c.com' ||
                    $request->server->all()['HTTP_ORIGIN'] === 'http://25hoursbuyers.com'||
                    $request->server->all()['HTTP_ORIGIN'] === 'https://25hoursbuyers.com'
                ){
                    //status messages code
                    return redirect($request->server->all()['HTTP_ORIGIN'].'/thanks/');
                }else{
                    return redirect('/thanks');
                }

            } catch (\Exception $e) {
                $data['errors']['DB'] = $e;
                return view('api.show-error', $data);
                DB::rollBack();
            }
        }else{
            $data['errors_domain'] = 'Have some errors. Please try again later.';
            return view('api.show-error', $data);
        }

        $data['errors']['something']= 'Have some errors. Please try again later.';
        return view('api.show-error', $data);
    }
}