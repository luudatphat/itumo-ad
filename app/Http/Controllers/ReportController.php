<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReportRequest;
use App\Models\Attack;
use App\Models\Contract_Report;
use App\Models\Contract_Report_Product;
use App\Models\Dasboard;
use App\Models\Dashboard_Row;
use App\Models\DashboardBuyerMaster;
use App\Models\DashboardSupplierMaster;
use App\Models\Media_File;
use App\Models\Memo;
use App\Models\Mtb_Genre;
use App\Models\Supplier;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Validator;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('userDeleted');
    }

    public function report_list()
    {
        //Get current supplier's report list
        $current_supplier_id = Auth::user()->supplier->id;
        /* an_tnh_sc_94_start */
        $_free_user = Auth::user()->free_user;
        if ( $_free_user == '1' ) {
            return Redirect::to( '/free/home' );
        }
        $data['free_user'] = $_free_user;
        /* an_tnh_sc_94_end */

        $data['report_list'] = Contract_Report::where('supplier_id', $current_supplier_id)->orderBy('id','desc')->paginate(env('PER_PAGES'));
        return view('report.list', $data);
    }

    public function create_report(Request $request)
    {
        $data = [];
        /* an_tnh_sc_94_start */
        $_free_user = Auth::user()->free_user;
        if ( $_free_user == '1' ) {
            return Redirect::to( '/free/home' );
        }
        $data['free_user'] = $_free_user;
        /* an_tnh_sc_94_end */

        if (isset($request->id)) {
            $data['attack_id'] = $request->id;
        }
        /*Get current supplier's attack list*/
        $current_supplier_id = Auth::user()->supplier->id;
        /*Get attack_id list that not yet have report*/
        #get attack_id list that already have report
        $reported_attack_ids = Contract_Report::where('supplier_id', $current_supplier_id)->pluck('attack_id')->toArray();
        $data['attack_list'] = Attack::where('supplier_id', $current_supplier_id)
//            ->whereNotIn('id',$reported_attack_ids)
            ->whereIn('mtb_attack_status_id',[config('constants.STATUS_PROCCESS'),
                config('constants.STATUS_SAMPLE_CHARGE'),
                config('constants.STATUS_SAMPLE_FREE'),
                config('constants.STATUS_ORDER'),
                config('constants.STATUS_REPEAT')])
            ->get();

        $data['category_list'] = Mtb_Genre::all();
        return view('report.create',$data);
    }

    public function create_report_handler(CreateReportRequest $request)
    {
        /*Check Attack status*/
        $attack = Attack::find($request->attack_id);
        if (!empty($attack) && in_array($attack->mtb_attack_status_id, [config('constants.STATUS_PROCCESS'),
                config('constants.STATUS_SAMPLE_CHARGE'),
                config('constants.STATUS_SAMPLE_FREE'),
                config('constants.STATUS_ORDER'),
                config('constants.STATUS_REPEAT')])) {
            /*Insert new report detail and report product to DB*/
            DB::beginTransaction();
            try
            {
                #insert invoice to DB
                if ($request->hasFile('invoice')) {
                    if ($request->invoice->isValid()) {
                        $invoice = $this->insert_file_to_db($request->invoice);
                    } else {
                        return view('errors.display', ['error_message' => trans('messages.file_upload_error')]);
                    }
                }
                #insert packing list to DB
                if ($request->hasFile('packing_list')) {
                    if ($request->packing_list->isValid()) {
                        $packing_list = $this->insert_file_to_db($request->packing_list);
                    } else {
                        return view('errors.display', ['error_message' => trans('messages.file_upload_error')]);
                    }
                }
                #Insert new report
                $contract_report = new Contract_Report();
                $contract_report->supplier_id = Auth::user()->supplier->id;
                $contract_report->attack_id = $request->attack_id;
                $contract_report->invoice = isset($invoice) ? $invoice->id : NULL;
                $contract_report->packing_list = isset($packing_list) ? $packing_list->id : NULL;
                $contract_report->number_of_contact = $request->number_contact;
                $contract_report->save();

                #Insert products to DB

                $total_money = 0;
                for ($i = 1; $i <= 15; $i++)
                {
                    // $request->has('product_name_'.$i) === true with $request->product_name_i = null ???
                    if($request->input('product_name_'.$i) !== null)
                    {
                        $contract_report_product = new Contract_Report_Product();
                        $contract_report_product->contract_report_id = $contract_report->id;
                        $contract_report_product->product_name = $request->input('product_name_'.$i);
                        $contract_report_product->mtb_genre_id = $request->input('product_category_'.$i);
                        $contract_report_product->price = $request->input('price_'.$i);
                        $contract_report_product->quantity = $request->input('quantity_'.$i);
                        $contract_report_product->couxu_commission_rate = $request->input('couxu_commission_'.$i);
                        $total_money += $request->input('price_'.$i) * $request->input('quantity_'.$i);
                        $contract_report_product->save();
                    }
                }

                /* Insert table Dashboard Supplier Master (update field Money) when create report */
                $year_current = Carbon::now()->format("Ym");
                $current_supplier_id = Auth::user()->supplier->id;
                $dashboard_supplier_masters = DashboardSupplierMaster::where('supplier_id','=',$current_supplier_id)->where('year_month', '=', $year_current)->first();
                if(empty($dashboard_supplier_masters)){
                    $dashboard_supplier_masters = new DashboardSupplierMaster();
                    $dashboard_supplier_masters->supplier_id = $current_supplier_id;
                    $dashboard_supplier_masters->year_month = $year_current;
                }

                // Cần confirm lại, có bỏ luồng từ 8->9 hay không?

                // 06/03/2017
                /* If number contact is 1, + money, else (2,3,4,5) + money_repeat*/
                if($request->number_contact == 1) {
                    $dashboard_supplier_masters->money += $total_money;
                }else{
                    $dashboard_supplier_masters->money_repeat += $total_money;
                }
                $dashboard_supplier_masters->save();
                /* End Insert table Dashboard Supplier Master*/

//                $dashboard_supplier_masters->money += $total_money;
//                $dashboard_supplier_masters->save();

                #change attack status
                // 06/03/2017
//                $attack->mtb_attack_status_id = ($request->number_contact == 1) ? config('constants.STATUS_ORDER') : config('constants.STATUS_REPEAT');

                if($attack->mtb_attack_status_id == config('constants.STATUS_ORDER')){
                    $attack->mtb_attack_status_id = config('constants.STATUS_REPEAT');
                }else if($attack->mtb_attack_status_id == config('constants.STATUS_REPEAT')){
                    $attack->mtb_attack_status_id = config('constants.STATUS_REPEAT');
                }else{
                    $attack->mtb_attack_status_id = config('constants.STATUS_ORDER');
                }

                $attack->save();
                /*Dash board*/
                $supplier_id = Auth::user()->supplier->id;
                $attack_date = strtotime($attack->created_at);
                $attack_year = date('Y',$attack_date);
                $attack_month = date('m', $attack_date);
                $attack_day = date('d', $attack_date);
                $attack_year_month_day = ($attack_year*100 + $attack_month)*100 + $attack_day;
                //dashboard_buyer_masters
                $dashboard_buyer_master = new DashboardBuyerMaster();
                $dashboard_buyer_master->request_id = $attack->request->id;
                $dashboard_buyer_master->supplier_id = $supplier_id;
                $dashboard_buyer_master->mtb_goods_category_id = $attack->request->mtb_goods_category_id;
                // 06/03/2017
//                $dashboard_buyer_master->mtb_attack_status_id = ($request->number_contact == 1) ? config('constants.STATUS_ORDER') : config('constants.STATUS_REPEAT');
                $dashboard_buyer_master->mtb_attack_status_id = config('constants.STATUS_ORDER');

                $dashboard_buyer_master->year_month = $attack->year_month;
                $dashboard_buyer_master->year_month_day = $attack_year_month_day;
                $dashboard_buyer_master->save();
                //dashboard_information_column
                $dashboard_information_column = Dasboard::where('request_id','=',$attack->request->id);
                if($request->number_contact == 1){
                    $dashboard_information_column->increment('status_order');
                }else{
                    $dashboard_information_column->increment('status_repeat');
                }
                //dashboard_information_row
                $dashboard_information_row = Dashboard_Row::where('supplier_id','=',$supplier_id);
                if($request->number_contact == 1){
                    $dashboard_information_row->increment('status_order');
                }else{
                    $dashboard_information_row->increment('status_repeat');
                }

                #Add memo
                $memo = new Memo();
                $memo->attack_id = $attack->id;
                $memo->content = ($request->number_contact == 1) ? '受注' : 'リピート';
                $memo->save();

                /*SC - 53*/
                $data["company_name_jp"]  = Auth::user()->supplier->company_name_jp;
                $data["number_attack"] = isset($attack->request_id) ? $attack->request_id : "";
                $data["number_contract"] = $request->number_contact;

                Mail::raw($this->notificationCreateReport($data), function($message) use ($data)
                {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->subject("【成約報告がありました！】セカイコネクト");
                    $message->to('support@couxu.jp');
                    //$message->bcc('phuc.nguyen@staff.distance-c.asia');
                });
                /*End SC - 53*/
                DB::commit();
                return redirect('/report_create')->with('message',trans('messages.create_report_success'));
            }
            catch (\Exception $e)
            {
                DB::rollback();
                abort(500, 'Unauthorized action.');
            }
        }
    }

    public function report_detail($id)
    {
        /* an_tnh_sc_94_start */
        $_free_user = Auth::user()->free_user;
        if ( $_free_user == '1' ) {
            return Redirect::to( '/free/home' );
        }
        $data['free_user'] = $_free_user;
        /* an_tnh_sc_94_end */
        
        $report_detail = Contract_Report::find($id);
        if (empty($report_detail)) {
            abort(404);
        }
        if ($report_detail->supplier_id != Auth::user()->supplier->id) {
            abort(403);
        }
        $data['report_detail'] = $report_detail;
        return view('report.detail', $data);
    }

    private function notificationCreateReport($data){
        $domain = \Illuminate\Support\Facades\Request::root();
        if($domain == Config::get('constants.DOMAIN_SUPPLIER_TEST')){
            $domain = Config::get('constants.DOMAIN_ADMIN_TEST');
        } else if($domain == Config::get('constants.DOMAIN_SUPPLIER_PRODUCT')){
            $domain = Config::get('constants.DOMAIN_ADMIN_PRODUCT');
        }
        $text = "企業名：".$data['company_name_jp']."
レコード番号：".$data['number_attack']."
成約回数：".$data['number_contract']. "
".$domain."/list-report.html";
        return $text;
    }
}
