<?php

namespace App\Http\Controllers\free;

use App\Models\Attack;
use App\Models\Country;
use App\Models\Mtb_Genre;
use App\Models\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Redirect;

class FreeRequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('userDeleted');
    }

    public function request_list(\Illuminate\Http\Request $request)
    {
        $_free_user = Auth::user()->free_user;
        if ( $_free_user != '1' ) {
            return Redirect::to( '/request_list' );
        }
        $data['free_user'] = $_free_user;
        //echo 'aaaaaaa';
        /*Get user product list*/
        $per_page = env('PER_PAGES');

        $current_page = $request->input('page') ? $request->input('page') : 0;
        $data['current_page'] = $current_page;
        $column = 'created_at';
        $sort = 'desc';
        if ($request->has(['column', 'sort'])) {
            $column   = $request->input('column');
            $sort     = $request->input('sort');
        }
        $pagination_url = 'request_list?';

        $genre_id = $request->input('genre_id');
        $data['genre_id'] = $genre_id;
        $country = $request->input('country');
        $data['country_id'] = $country;
        $rating = $request->input('rating');
        $data['rating'] = $rating;
        $from_date = $request->input('from_date');
        $data['from_date'] = $from_date;
        $to_date = $request->input('to_date');
        $data['to_date'] = $to_date;
        $search = trim($request->input('search'));
        $data['search'] = $search;

        $query = Request::where('requests.status', 'like' , '%4%' );
        /*Join to check if this request has been attacked or not*/
        $query->select('requests.*','attacks.supplier_id', 'buyers.country', 'buyers.sale_country', 'mtb_genres.content','countries.country_name');
        $query->join('buyers','requests.buyer_id','=','buyers.id');
        $query->join('mtb_genres','requests.mtb_genre_id','=','mtb_genres.id');
        $query->join('countries','buyers.country','=','countries.id');
        $query->leftJoin('attacks', function ($join) {
            $join->on('requests.id','=','attacks.request_id');
            $join->on('attacks.supplier_id','=', DB::raw(config('constants.FREE_DEFAULT_SUPPLIER_ID')));
        });
        $query->where('buyers.deleted_at', '=', null);

        if (empty($genre_id) && empty($country) && empty($rating) && empty($from_date) && empty($to_date) && empty($search)) {
            if ($column != '' && $sort != '') {
                $query->orderBy($column, $sort);
            }
        } else {
            /*search by text*/
            if (!empty($search) && $search != ''){
                $search_value = $search;
                $search_string = '%'.$search.'%';
                $query->where(function ($func_query) use ($search_string, $search_value) {
                    $func_query->where('date', 'like', $search_string)
                        ->orWhere('expire_date', 'like', $search_string)
                        ->orWhere('product_description_jp','like', $search_string)
                        ->orWhere('countries.country_name','like', $search_string)
                        ->orWhere('buyers.sale_country','like',$search_string)
                        ->orWhere('mtb_genres.content','like',$search_string)
                        ->orWhere('buyers.company_strength','like',$search_string);
                });
            }
            /*search by genre*/
            if (!empty($genre_id)) {
                $query->whereIn('mtb_genre_id',$genre_id);
                $pagination_url.= '&'.http_build_query(['genre_id' => $genre_id]);
            }
            /*search by country*/
            if (!empty($country)) {
                $query->whereIn('buyers.country',$country);
                $pagination_url.= '&'.http_build_query(['country' => $country]);
            }
            /*search by rating*/
            if (!empty($rating)) {
                $query->whereHas('buyer', function ($func_query) use ($rating) {
                    $func_query->where('rating','=',$rating);
                });
                $pagination_url.= '&rating='.$rating;
            }
            /*search by date*/
            if (!empty($from_date)) {
                $query->where('date','>=', date_format(date_create($from_date),'Y-m-d'));
                $pagination_url.= '&from_date='.$from_date;
            }
            if(!empty($to_date)) {
                $query->where('date','<=', date_format(date_create($to_date),'Y-m-d'));
                $pagination_url.= '&to_date='.$to_date;
            }
            if ($column != '' && $sort != '') {
                $query->orderBy($column, $sort);
            }
        }
//        die(var_dump($query->get()->toArray()));
        $request_list = $query->paginate($per_page);
        $pagination_url .= ($column !== '' && $sort !== '') ? '&column=' . $column . '&sort=' . $sort : null;
        $pagination_url .= ($search !== '' && !empty($search)) ? '&search=' . $search : null;
        $request_list->setPath($pagination_url);


        $data['mtb_genres'] = Mtb_Genre::all();
        $data['countries'] = Country::all();
        $data['request_list'] = $request_list;
        return view('free.request_list.list', $data);
    }

    public function sort(\Illuminate\Http\Request $request) {
        $data = '';
        $per_page = env('PER_PAGES');

        if($request->isMethod('post')){
            $search = $request->input('search');
            $data['search'] = $search;
            $genre_id = $request->input('genre_id');
            $data['genre_id'] = $genre_id;
            $country = $request->input('country');
            $data['country_id'] = $country;
            $from_date = $request->input('from_date');
            $data['from_date'] = $from_date;
            $to_date = $request->input('to_date');
            $data['to_date'] = $to_date;
            $rating = $request->input('rating');
            $data['rating'] = $rating;

            $current_page = $request->input('current_page') ? $request->input('current_page') : 0;
            $data['current_page'] = $current_page;
            $column   = $request->input('column');
            $sort     = $request->input('sort');

            $pagination_url = 'request_list?column=' . $column . '&sort=' . $sort;
            $query = Request::where('requests.status',1);
            /*Join to check if this request has been attacked or not*/
            $query->select('requests.*','attacks.supplier_id', 'buyers.country', 'buyers.sale_country', 'mtb_genres.content','countries.country_name');
            $query->join('buyers','requests.buyer_id','=','buyers.id');
            $query->join('mtb_genres','requests.mtb_genre_id','=','mtb_genres.id');
            $query->join('countries','buyers.country','=','countries.id');
            $query->leftJoin('attacks', function ($join) {
                $join->on('requests.id','=','attacks.request_id');
                $join->on('attacks.supplier_id','=', DB::raw(Auth::user()->supplier->id));
            });

            if (empty($genre_id) && empty($country) && empty($rating) && empty($from_date) && empty($to_date) && empty($search)) {
                if ($column != '' && $sort != '') {
                    $query->orderBy($column, $sort);
                }
            } else {
                /*search by text*/
                if (!empty($search) && $search != ''){
                    $search_value = $search;
                    $search_string = '%'.$search.'%';
                    $query->where(function ($func_query) use ($search_string, $search_value) {
                        $func_query->where('date', 'like', $search_string)
                            ->orWhere('expire_date', 'like', $search_string)
                            ->orWhere('product_description_jp','like', $search_string)
                            ->orWhere('countries.country_name','like', $search_string)
                            ->orWhere('buyers.sale_country','like',$search_string)
                            ->orWhere('mtb_genres.content','like',$search_string)
                            ->orWhere('buyers.company_strength','like',$search_string);
                    });
                }
                /*search by genre*/
                if (!empty($genre_id)) {
                    $query->whereIn('mtb_genre_id',$genre_id);
                    $pagination_url.= '&'.http_build_query(['genre_id' => $genre_id]);
                }
                /*search by country*/
                if (!empty($country)) {
                    $query->whereIn('buyers.country',$country);
                    $pagination_url.= '&'.http_build_query(['country' => $country]);
                }
                /*search by rating*/
                if (!empty($rating)) {
                    $query->whereHas('buyer', function ($func_query) use ($rating) {
                        $func_query->where('rating','=',$rating);
                    });
                    $pagination_url.= '&rating='.$rating;
                }
                /*search by date*/
                if (!empty($from_date)) {
                    $query->where('date','>=', date_format(date_create($from_date),'Y-m-d'));
                    $pagination_url.= '&from_date='.$from_date;
                }
                if(!empty($to_date)) {
                    $query->where('date','<=', date_format(date_create($to_date),'Y-m-d'));
                    $pagination_url.= '&to_date='.$to_date;
                }
                if ($column != '' && $sort != '') {
                    $query->orderBy($column, $sort);
                }
            }
            $request_list = $query->paginate($per_page);
            $pagination_url .= ($request->input('search') !== '' && !empty($request->input('search'))) ? '&search=' . $request->input('search') : null;
            $request_list->setPath($pagination_url);

            $data['$request_list'] = $request_list;
            $content = '';
            foreach ($request_list as $request)
            {
                $content.= '<tr>'.
                    '<td>'.$request->id.'</td>'.
                    '<td>'.date_format(date_create($request->date),'Y年m月d日').'</td>'.
//                    '<td>'.date_format(date_create($request->expire_date),'Y年m月d日').'</td>'.
                    '<td>'.$request->buyer->countries->country_name.'</td>'.
                    '<td>'.$request->mtb_genre->content.'</td>'.
                    '<td><div id="overflow">'.nl2br(htmlspecialchars($request->product_description_jp)).'</div></td>'.
                    '<td>'.$request->buyer->company_strength.'</td>'.
//                    '<td>'.$request->buyer->sale_country.'</td>'.
                    '<td style="vertical-align: middle">'.
                    '<input type="hidden" id="request_id" value="'.$request->id.'">';
                if($request->supplier_id == NULL){
                    $content.= '<button class="btn-add" name="btnAttack">追加する</button>';
                } else {
                    $content.= '<button class="btn-added" disabled>追加済み</button>';
                }
                $content.= '</td>'.
                    '</tr>';
            }
            if ($content == '') {
                $content = '<tr><td colspan="7">データなし。</td></tr>';
            }
            $data['content'] = $content;
            $data['pagination'] = $request_list->links()->toHtml();

            return json_encode( $data );
        }
    }
}
