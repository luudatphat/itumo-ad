<?php

namespace App\Http\Controllers\free;

use App\Models\Attack;
use App\Models\Country;
use App\Models\Dashboard_Row;
use App\Models\DashboardBuyerMaster;
use App\Models\DashboardSupplierMaster;
use App\Models\Mtb_Genre;
use App\Models\MtbNewsCategory;
use App\Models\News;
use App\Models\Supplier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Redirect;


class FreeHomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('userDeleted');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $_free_user = Auth::user()->free_user;
        if ( $_free_user != '1' ) {
            return Redirect::to( '/home' );
        }
        $data['free_user'] = $_free_user;
        // $supplier_id = Auth::user()->supplier->id;
        $supplier_id = config('constants.FREE_DEFAULT_SUPPLIER_ID');

        $attacks = Attack::where('supplier_id',$supplier_id)->get();


        $arr_status_count_by_country = [];
        $arr_status_count = [];
        $arr_status_count['suggest'] = 0;
        $arr_status_count['response_yes'] = 0;
        $arr_status_count['response_no'] = 0;
        $arr_status_count['send_example_free'] = 0;
        $arr_status_count['send_example_charge'] = 0;
        $arr_status_count['order'] = 0;
        $arr_status_count['repeat'] = 0;
        foreach ($attacks as $attack) {
            if(!empty($attack->request)) {
                $dashboard = DashboardBuyerMaster::where([
                    ['supplier_id', '=', $supplier_id],
                    ['request_id', '=', $attack->request->id]
                ])->get()->toArray();
                if (!empty($dashboard)) {
                    //Count number of each status in $dashboard
                    $dashboard_count = array_count_values((array_column($dashboard, 'mtb_attack_status_id')));
                    //Count status of each country
                    if (isset($arr_status_count_by_country[$attack->request->buyer->country])) {
                        if (isset($dashboard_count[config('constants.STATUS_SUGGESTSENT')]) && $dashboard_count[config('constants.STATUS_SUGGESTSENT')] > 0) {
                            $arr_status_count_by_country[$attack->request->buyer->country]['suggest'] += $dashboard_count[config('constants.STATUS_SUGGESTSENT')];
                            $arr_status_count_by_country[$attack->request->buyer->country]['response_no'] += (isset($dashboard_count[config('constants.STATUS_NOREPONSE')]) ? $dashboard_count[config('constants.STATUS_NOREPONSE')] : 0);
                            $arr_status_count_by_country[$attack->request->buyer->country]['response_yes'] += (isset($dashboard_count[config('constants.STATUS_PROCCESS')]) ? $dashboard_count[config('constants.STATUS_PROCCESS')] : 0);
                            $arr_status_count_by_country[$attack->request->buyer->country]['send_example_charge'] += (isset($dashboard_count[config('constants.STATUS_SAMPLE_CHARGE')]) ? $dashboard_count[config('constants.STATUS_SAMPLE_CHARGE')] : 0);
                            $arr_status_count_by_country[$attack->request->buyer->country]['send_example_free'] += (isset($dashboard_count[config('constants.STATUS_SAMPLE_FREE')]) ? $dashboard_count[config('constants.STATUS_SAMPLE_FREE')] : 0);
                            $arr_status_count_by_country[$attack->request->buyer->country]['order'] += (isset($dashboard_count[config('constants.STATUS_ORDER')]) ? $dashboard_count[config('constants.STATUS_ORDER')] : 0);
                            $arr_status_count_by_country[$attack->request->buyer->country]['repeat'] += (isset($dashboard_count[config('constants.STATUS_REPEAT')]) ? $dashboard_count[config('constants.STATUS_REPEAT')] : 0);
                        }
                    } else {
                        if (isset($dashboard_count[config('constants.STATUS_SUGGESTSENT')]) && $dashboard_count[config('constants.STATUS_SUGGESTSENT')] > 0) {
                            $arr_status_count_by_country[$attack->request->buyer->country]['suggest'] = $dashboard_count[config('constants.STATUS_SUGGESTSENT')];
                            $arr_status_count_by_country[$attack->request->buyer->country]['response_no'] = (isset($dashboard_count[config('constants.STATUS_NOREPONSE')]) ? $dashboard_count[config('constants.STATUS_NOREPONSE')] : 0);
                            $arr_status_count_by_country[$attack->request->buyer->country]['response_yes'] = (isset($dashboard_count[config('constants.STATUS_PROCCESS')]) ? $dashboard_count[config('constants.STATUS_PROCCESS')] : 0);
                            $arr_status_count_by_country[$attack->request->buyer->country]['send_example_charge'] = (isset($dashboard_count[config('constants.STATUS_SAMPLE_CHARGE')]) ? $dashboard_count[config('constants.STATUS_SAMPLE_CHARGE')] : 0);
                            $arr_status_count_by_country[$attack->request->buyer->country]['send_example_free'] = (isset($dashboard_count[config('constants.STATUS_SAMPLE_FREE')]) ? $dashboard_count[config('constants.STATUS_SAMPLE_FREE')] : 0);
                            $arr_status_count_by_country[$attack->request->buyer->country]['order'] = (isset($dashboard_count[config('constants.STATUS_ORDER')]) ? $dashboard_count[config('constants.STATUS_ORDER')] : 0);
                            $arr_status_count_by_country[$attack->request->buyer->country]['repeat'] = (isset($dashboard_count[config('constants.STATUS_REPEAT')]) ? $dashboard_count[config('constants.STATUS_REPEAT')] : 0);
                        }
                    }
                    //Count status
                    $arr_status_count['suggest'] += isset($dashboard_count[config('constants.STATUS_SUGGESTSENT')]) ? $dashboard_count[config('constants.STATUS_SUGGESTSENT')] : 0;
                    $arr_status_count['response_no'] += (isset($dashboard_count[config('constants.STATUS_NOREPONSE')]) ? $dashboard_count[config('constants.STATUS_NOREPONSE')] : 0);
                    $arr_status_count['response_yes'] += (isset($dashboard_count[config('constants.STATUS_PROCCESS')]) ? $dashboard_count[config('constants.STATUS_PROCCESS')] : 0);
                    $arr_status_count['send_example_charge'] += (isset($dashboard_count[config('constants.STATUS_SAMPLE_CHARGE')]) ? $dashboard_count[config('constants.STATUS_SAMPLE_CHARGE')] : 0);
                    $arr_status_count['send_example_free'] += (isset($dashboard_count[config('constants.STATUS_SAMPLE_FREE')]) ? $dashboard_count[config('constants.STATUS_SAMPLE_FREE')] : 0);
                    $arr_status_count['order'] += (isset($dashboard_count[config('constants.STATUS_ORDER')]) ? $dashboard_count[config('constants.STATUS_ORDER')] : 0);
                    $arr_status_count['repeat'] += (isset($dashboard_count[config('constants.STATUS_REPEAT')]) ? $dashboard_count[config('constants.STATUS_REPEAT')] : 0);
                }
            }
        }
        $data['arr_status_count_by_country'] = $arr_status_count_by_country;
        $data['arr_status_count'] = $arr_status_count;
        $data['countries'] = Country::all();

        $all_status_of_dashboard_row = Dashboard_Row::where('supplier_id','=',$supplier_id)->get();
        $total_status_bid = array_sum(array_column($all_status_of_dashboard_row->toArray(),'status_bid'));
        $total_status_suggestsent = array_sum(array_column($all_status_of_dashboard_row->toArray(),'status_suggestsent'));
        $total_status_noreponse = array_sum(array_column($all_status_of_dashboard_row->toArray(),'status_noreponse'));
        $total_status_proccess = array_sum(array_column($all_status_of_dashboard_row->toArray(),'status_proccess'));
        $total_status_sample_free = array_sum(array_column($all_status_of_dashboard_row->toArray(),'status_sample_free'));
        $total_status_sample_charge = array_sum(array_column($all_status_of_dashboard_row->toArray(),'status_sample_charge'));
        $total_status_order = array_sum(array_column($all_status_of_dashboard_row->toArray(),'status_order'));
        $total_status_repeat = array_sum(array_column($all_status_of_dashboard_row->toArray(),'status_repeat'));

        $data['arr_graph_tab2']['status_bid'] = $total_status_bid;
        $data['arr_graph_tab2']['status_suggestsent'] = $total_status_suggestsent;
        $data['arr_graph_tab2']['status_noreponse'] = $total_status_noreponse;
        $data['arr_graph_tab2']['status_proccess'] = $total_status_proccess;
        $data['arr_graph_tab2']['status_sample_free'] = $total_status_sample_free;
        $data['arr_graph_tab2']['status_sample_charge'] = $total_status_sample_charge;
        $data['arr_graph_tab2']['status_order'] = $total_status_order;
        $data['arr_graph_tab2']['status_repeat'] = $total_status_repeat;

        $all_record_dashboard_supplier_master = DashboardSupplierMaster::where('supplier_id','=',$supplier_id)->get();
        $total_money_order = array_sum(array_column($all_record_dashboard_supplier_master->toArray(),'money'));
        $total_money_repeat = array_sum(array_column($all_record_dashboard_supplier_master->toArray(),'money_repeat'));
        $data['arr_graph_tab2']['money_order'] = $total_money_order;
        $data['arr_graph_tab2']['money_repeat'] = $total_money_repeat;
        $data['categories']             = Mtb_Genre::all();
        // echo '<pre>'; print_r( $data ); echo '</pre>'; die();
        return view('free.home.home', $data);
    }

    /*Graph 3*/
    public function load_graph_3(Request $request) {
        /*array mapping post data to attack status id*/
        $arr_attack_status_mapping = [
            1 => config('constants.STATUS_BID'),
            2 => config('constants.STATUS_SUGGESTSENT'),
            3 => config('constants.STATUS_PROCCESS'),
            4 => 'send_sample',
            5 => config('constants.STATUS_ORDER'),
            6 => config('constants.STATUS_REPEAT'),
        ];
        /*convert date string to integer*/
        $start_date = strtotime($request->start_date);
        $start_date = (date('Y', $start_date)*100 + date('m', $start_date))*100 + date('d', $start_date);
        $end_date = strtotime($request->end_date);
        $end_date = (date('Y', $end_date)*100 + date('m', $end_date))*100 + date('d', $end_date);
        /*get current status data from DB*/
        $current_status_data = $this->getDashboardBuyerMasterByStatus($start_date, $end_date, $arr_attack_status_mapping[$request->attack_status]);
        /*get count record by year month*/
        $current_status_data_value = [];
        foreach ($current_status_data as $key=>$value){
            $current_status_data_value[] = ['year_month' => substr($value['year_month_day'], 0,6)];
        }
        $current_status_data = array_count_values(array_column($current_status_data_value, 'year_month'));

        /*get previous status data of this current status from DB*/
        if ($request->attack_status != 1) {
            //if current status is ORDER or REPEAT then previous status is PROCESS
            $previous_status = (($request->attack_status == 5) || ($request->attack_status == 6)) ? config('constants.STATUS_PROCCESS') : $arr_attack_status_mapping[$request->attack_status - 1];
            $previous_status_data = $this->getDashboardBuyerMasterByStatus($start_date, $end_date, $previous_status);
            /*get count record by year month*/
            $previous_status_data_value = [];
            foreach ($previous_status_data as $key=>$value){
                $previous_status_data_value[] = ['year_month' => substr($value['year_month_day'], 0,6)];
            }
            $previous_status_data = array_count_values(array_column($previous_status_data_value, 'year_month'));
        }
        $all_supplier_data = $this->getDashboardBuyerMasterByStatus($start_date, $end_date, $arr_attack_status_mapping[$request->attack_status], true);
        /*group by year month*/
        $arr_group_by_year_month = [];
        foreach ($all_supplier_data as $item) {
            $arr_group_by_year_month[substr($item['year_month_day'],0 ,6)][] = $item;
        }
        /*calculate average*/
        $arr_average = [];
        foreach ($arr_group_by_year_month as $year_month => $year_month_item) {
            $count = count($year_month_item);
            $count_supplier = count(array_unique(array_column($year_month_item, 'supplier_id')));
            $arr_average[$year_month] = $count/$count_supplier;
        }
        $response['current_status_data'] = $current_status_data;
        $response['previous_status_data'] = isset($previous_status_data) ? $previous_status_data : [];
        $response['arr_average'] = $arr_average;
        return json_encode($response);
    }

    protected function getDashboardBuyerMasterByStatus ($start_date, $end_date, $attack_status, $flag_get_all_supplier = false) {
        // $supplier_id = Auth::user()->supplier->id;
        $supplier_id = config('constants.FREE_DEFAULT_SUPPLIER_ID');
        $where = [
            ['year_month_day', '>=', $start_date],
            ['year_month_day', '<=', $end_date]
        ];
        if (!$flag_get_all_supplier) {
            $where[] = ['supplier_id', '=', $supplier_id];
        }
        $result = DashboardBuyerMaster::where($where);
        if ($attack_status == 'send_sample') {
            $result->where(function ($query) {
                $query->where('mtb_attack_status_id','=', config('constants.STATUS_SAMPLE_FREE'));
                $query->orWhere('mtb_attack_status_id','=', config('constants.STATUS_SAMPLE_CHARGE'));
            });
        } else {
            $result->where('mtb_attack_status_id','=', $attack_status);
        }
        $result->orderBy('year_month','asc');
        return $result->get()->toArray();
    }

    public function news(){
        $_free_user = Auth::user()->free_user;
        if ( $_free_user != '1' ) {
            return Redirect::to( '/news' );
        }
        $result['free_user'] = $_free_user;
        /*INCLUDE LIBRARY SIMPLE DOM HTML */
        include(app_path().'/functions/simple_html_dom.php');
        /*END INCLUDE LIBRARY SIMPLE DOM HTML */
        $ch = curl_init("http://world-conect.com/feed");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        $html = curl_exec($ch);
        curl_close($ch);
        $html = str_get_html($html);
        $data  = [];
        if($html){
            foreach ($html->find('item') as $item){
                $array = [];
                $array['title'] = $item->find('title', 0)->plaintext;
                $array['link'] = substr($item->find('comments' , 0)->plaintext , 0, strpos($item->find('comments' , 0)->plaintext , "#"));
                $array['pubDate'] = date('Y/m/d H:i:s' ,strtotime($item->find('pubDate', 0)->plaintext));
                $data[] = $array;
            }
        }
        $result["list_news_world_connect"] = $data;

        /*get categories*/
        $result["list_categories_news"]         = MtbNewsCategory::where([['content', '<>', 'Other'],['content', '<>', '無料会員向け']])->get();
        $result["list_categories_news_other"]   = MtbNewsCategory::where('content', 'Other')->get();
        /*end get categories*/
        /*get news admin*/
        $list_news = [];
        $list_admin_news = News::where('status' , 1)->orderBy('updated_at', 'desc')->get();
        foreach ($list_admin_news as $value){
            $list_news[$value->category_id][] =  $value->toArray();
        }
        $list_news_other = News::where('category_id', 7)->orderBy('updated_at', 'desc')->get();
        $list_news_free = [];
        foreach ( $list_news_other as $value ) {
            /* an_tnh_sc_94_sc_157_start */
            if ( $value->status == '1' ) {
                $list_news_free[] = $value->toArray();
            }
            /* an_tnh_sc_94_sc_157_end */
        }
        $result["list_news_admin"] = $list_news;
        $result["list_news_free"] = $list_news_free;
        /*end get news admin*/

        return view('free.news.new' , $result);
    }

    public function details($id){
        $data = [];
        $_free_user = Auth::user()->free_user;
        if ( $_free_user != '1' ) {
            return Redirect::to( '/news/details/' . $id );
        }
        $data['free_user'] = $_free_user;
        if(!empty($id)){
            $object = News::find($id);
            if (empty($object)) {
                abort(404);
            }
            $data['detail_news'] = News::where('id' , $id)->where('status' , 1)->get();
        }
        return view('free.news.details' , $data);
    }
}
