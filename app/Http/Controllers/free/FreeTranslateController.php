<?php

namespace App\Http\Controllers\free;

use App\Http\Requests\CreateTranslateRequest;
use App\Http\Requests\DeleteTranslateRequest;
use App\Models\Supplier;
use App\Models\Translate;
use Illuminate\Http\Request;
use App\Http\Requests\EditTranslateRequest;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Config;
use App\Http\Controllers\Controller;
use Redirect;

class FreeTranslateController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('userDeleted');
    }

    public function create(){
        $_free_user = Auth::user()->free_user;
        
        $data['free_user'] = $_free_user;
        return view('free.translate_mail.create', $data);
    }

    public function create_handler(CreateTranslateRequest $request){
        $content = trim($request->content_required);
        $word_count = mb_strlen(preg_replace( "/\r|\n|\s/", "", $content ));
        $translate_price = $word_count*env('TRANSLATE_PRICE_PER_WORD');
        $translate = new Translate();
        $translate->supplier_id = Auth::user()->supplier->id;
        $translate->content_required = $content;
        /*save with status = 2 if user request and 3 if save*/
        if (isset($request->btnSave)) {
            $translate->status = config('constants.STATUS_TRANSLATE_NOT_YET_REQUEST');
            $message = trans('messages.create_translate_request_success');
        } else {
            $translate->status = config('constants.STATUS_TRANSLATE_WAITING_TRANSLATE');
            $message = trans('messages.send_translate_request_success');

        }
        $translate->word_count = $word_count;
        $translate->translate_price = $translate_price;
        if ($translate->save()) {

            if($translate->status == config('constants.STATUS_TRANSLATE_WAITING_TRANSLATE')){
                $data['company_name_jp']  = Auth::user()->supplier->company_name_jp;
                $data['word_count']  = $translate->word_count;
                $data['translate_price']  = $translate->translate_price;
                $data['content_required']  = $translate->content_required;

                Mail::raw($this->notificationSupplierSendTranselate($data), function($message) use ($data)
                {
                    $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $message->subject("【翻訳依頼】セカイコネクト");
                    $message->to('support@couxu.jp');
                    //$message->bcc('nhhthuanck@gmail.com');
                });
            }

            return back()->with('message',$message);
        } else {
            abort(500);
        }
    }

    private function notificationSupplierSendTranselate($data){
        $text = "・企業名：".$data['company_name_jp']."
・文字数：".$data['word_count']."
・翻訳費用：".$data['translate_price']."
【文章】
".$data['content_required'];
        return $text;
    }

    public function edit_translate($translate_id){
        $_free_user = Auth::user()->free_user;
        if ( $_free_user != '1' ) {
            return Redirect::to( '/translate-detail/' . $translate_id );
        }
        $data['free_user'] = $_free_user;
        $translate = Translate::find($translate_id);
        if (empty($translate)) {
            abort(404);
        }
        if ($translate->supplier_id == config('constants.FREE_DEFAULT_SUPPLIER_ID')) {
            $data['translate'] = $translate;
            return view('free.translate_mail.detail',$data);
        }
        abort(403);
    }

    public function edit_translate_handler(EditTranslateRequest $request){
        $content = trim($request->content_required);
        $word_count = mb_strlen(preg_replace( "/\r|\n|\s/", "", $content ));
        $translate_price = $word_count*env('TRANSLATE_PRICE_PER_WORD');
        $translate = Translate::find($request->translate_id);
        /*Check record exist and permission*/
        if (!empty($translate) && $translate->supplier_id == config('constants.FREE_DEFAULT_SUPPLIER_ID')) {
            /*check translate status*/
            if ($translate->status == config('constants.STATUS_TRANSLATE_NOT_YET_REQUEST')) {
                $translate->content_required = $content;
                $translate->word_count = $word_count;
                $translate->translate_price = $translate_price;
                /*change status if user request*/
                $message = trans('messages.update_translate_request_success');
                if (isset($request->btnRequest)) {
                    $translate->status = config('constants.STATUS_TRANSLATE_WAITING_TRANSLATE');
                    $message = trans('messages.send_translate_request_success');
                }
                if ($translate->save()) {

                    if($translate->status == config('constants.STATUS_TRANSLATE_WAITING_TRANSLATE')){
                        $data['company_name_jp']  = Auth::user()->supplier->company_name_jp;
                        $data['word_count']  = $translate->word_count;
                        $data['translate_price']  = $translate->translate_price;
                        $data['content_required']  = $translate->content_required;

                        Mail::raw($this->notificationSupplierSendTranselate($data), function($message) use ($data)
                        {
                            $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                            $message->subject("【翻訳依頼】セカイコネクト");
                            $message->to('support@couxu.jp');
                            //$message->bcc('nhhthuanck@gmail.com');
                        });
                    }

                    return back()->with('message',$message);
                } else {
                    abort(500);
                }
            }
        }
    }

    public function translate_list(Request $request) {
        /*Get user product list*/
        $per_page = env('PER_PAGES');
        $_free_user = Auth::user()->free_user;
        
        $data['free_user'] = $_free_user;
        $current_page = $request->input('page') ? $request->input('page') : 0;
        $data['current_page'] = $current_page;
        $column = 'created_at';
        $sort = 'desc';
        if ($request->has(['column', 'sort'])) {
            $column   = $request->input('column');
            $sort     = $request->input('sort');
        }
        $pagination_url = 'translate-list?';
        $search = trim($request->input('search'));
        $data['search'] = $search;
        if (empty($search)) {
            $query = Translate::where('supplier_id', config('constants.FREE_DEFAULT_SUPPLIER_ID'));
            if ($column != '' && $sort != '') {
                $query->orderBy($column, $sort);
            }
            $translate_list = $query->paginate($per_page);
        } else {
            $search_value = $search;
            $search_string = '%'.$search.'%';
            $query = Translate::where('supplier_id', config('constants.FREE_DEFAULT_SUPPLIER_ID'))
                ->where(function ($query) use ($search_string, $search_value) {
                    $query->orWhere('id', '=', $search_value)
                        ->orWhere('content_required', 'like', $search_string)
                        ->orWhere('word_count', '=', $search_value)
                        ->orWhere('translate_price','=', $search_value);
                });
            if ($column != '' && $sort != '') {
                $query->orderBy($column, $sort);
            }
            $translate_list = $query->paginate($per_page);
        }
        $pagination_url .= ($column !== '' && $sort !== '') ? '&column=' . $column . '&sort=' . $sort : null;
        $pagination_url .= ($request->input('search') !== '' && !empty($request->input('search'))) ? '&search=' . $request->input('search') : null;
        $translate_list->setPath($pagination_url);
        $data['translate_list'] = $translate_list;
        return view('free.translate_mail.list', $data);
    }

    public function sort(Request $request) {

        if($request->isMethod('post')){
            /*Get user product list*/
            $per_page = env('PER_PAGES');

            $current_page = $request->input('page') ? $request->input('page') : 0;
            $data['current_page'] = $current_page;
            $column = $request->input('column') ? $request->input('column') : '';
            $sort = $request->input('sort') ? $request->input('sort') : '';
            $pagination_url = 'translate-list?';
            $search = trim($request->input('search'));
            $data['search'] = $search;
            if (empty($search)) {
                $query = Translate::where('supplier_id', config('constants.FREE_DEFAULT_SUPPLIER_ID'));
                if ($column != '' && $sort != '') {
                    $query->orderBy($column, $sort);
                }
                $translate_list = $query->paginate($per_page);
            } else {
                $search_value = $search;
                $search_string = '%'.$search.'%';
                $query = Translate::where('supplier_id', config('constants.FREE_DEFAULT_SUPPLIER_ID'))
                    ->where(function ($query) use ($search_string, $search_value) {
                        $query->orWhere('id', '=', $search_value)
                            ->orWhere('content_required', 'like', $search_string)
                            ->orWhere('word_count', '=', $search_value)
                            ->orWhere('translate_price','=', $search_value);
                    });
                if ($column != '' && $sort != '') {
                    $query->orderBy($column, $sort);
                }
                $translate_list = $query->paginate($per_page);
            }

            $pagination_url .= ($column !== '' && $sort !== '') ? '&column=' . $column . '&sort=' . $sort : null;
            $pagination_url .= ($request->input('search') !== '' && !empty($request->input('search'))) ? '&search=' . $request->input('search') : null;
            $translate_list->setPath($pagination_url);

            $data['translate_list'] = $translate_list;
            $content = '';
            foreach ($translate_list as $translate)
            {
                $content.= '<tr>'.
                    '<td>'.$translate->id.'</td>'.
                    '<td>'.$translate->created_at.'</td>'.
                    '<td>'.nl2br(htmlspecialchars($translate->content_required)).'</td>'.
                    '<td>'.nl2br(htmlspecialchars($translate->content_result)).'</td>';
                if ($translate->status == config('constants.STATUS_TRANSLATE_OK')) {
                    $content.= '<td>翻訳完了</td>';
                } elseif ($translate->status == config('constants.STATUS_TRANSLATE_WAITING_TRANSLATE')) {
                    $content.= '<td>翻訳依頼中</td>';
                } else {
                    $content.= '<td>翻訳完了</td>';
                }

                if($translate->status == config('constants.STATUS_TRANSLATE_NOT_YET_REQUEST')) {
                    $content .= '<td class="text-center" style="vertical-align:middle">'.
                        '<form action="'.url('/delete-translate-request').'" method="POST">
                            <a class="btn btn-primary fix-btn-01" href="'.url('translate-detail/'.$translate->id).'">詳細</a>
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                            <input type="hidden" name="translate_id" value="'.$translate->id.'" >
                            <input type="submit" class="btn btn-danger button_create_list_user fix-width-btn-03" name="btnDelete" value="削除">
                        </form>'.
                        '</td>'.
                        '</tr>';
                } else {
                    $content.='<td class="text-center" style="vertical-align:middle">'.
                        '<a class="btn btn-primary fix-btn-01" href="/translate-detail/'.$translate->id.'">詳細</a>'.
                        '</td>'.
                        '</tr>';
                }
            }
            $data['content'] = $content;
            $data['pagination'] = $translate_list->links()->toHtml();

            return json_encode( $data );
        }
    }

    public function delete_translate(DeleteTranslateRequest $request) {
        $translate = Translate::find($request->translate_id);
        if ($translate->supplier_id == Auth::user()->supplier->id) {
            /*check translate status*/
            if ($translate->status == config('constants.STATUS_TRANSLATE_NOT_YET_REQUEST')) {
                if (!$translate->delete()) {
                    abort(500);
                }
                return redirect('/free/translate-list');
            }
        }
    }
}
