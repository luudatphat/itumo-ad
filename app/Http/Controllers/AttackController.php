<?php

namespace App\Http\Controllers;

use App\Models\Attack;
use App\Models\Contract_Report;
use App\Models\Dasboard;
use App\Models\Dashboard_Row;
use App\Models\DashboardBuyerMaster;
use App\Models\DashboardSupplierMaster;
use App\Models\Mtb_Attack_Status;
use App\Models\Supplier;
use Carbon\Carbon;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AttackController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('userDeleted');
    }

    public function new_attack(Request $request) {
        DB::beginTransaction();
        try
        {
            $request_id = $request->request_id;
            $request_object = \App\Models\Request::find($request_id);
            if (empty($request_object)) {
                return;
            }
            $supplier_id = Auth::user()->supplier->id;
            $supplier_item = Supplier::find($supplier_id);
            /*dashboard*/
            $current_year = date('Y');
            $current_month = date('m');
            $current_day = date('d');
            $current_year_month_day = ($current_year*100 + $current_month)*100 + $current_day;
            //dashboard_buyer_masters
            $dashboard_buyer_master = new DashboardBuyerMaster();
            $dashboard_buyer_master->request_id = $request_id;
            $dashboard_buyer_master->supplier_id = $supplier_id;
            $dashboard_buyer_master->mtb_goods_category_id = $request_object->mtb_goods_category_id;
            $dashboard_buyer_master->mtb_attack_status_id = config('constants.STATUS_BID');
            $dashboard_buyer_master->year_month = $request_object->year_month;
            $dashboard_buyer_master->year_month_day = $current_year_month_day;
            $dashboard_buyer_master->save();
            //dashboard_information_column
            #check request_id exist or not
            $dashboard_information_column = Dasboard::where('request_id','=',$request_id);
            if ($dashboard_information_column->count() == 0) {
                $new_dashboard_information_column = new Dasboard();
                $new_dashboard_information_column->request_id = $request_id;
                $new_dashboard_information_column->request_date = $request_object->created_at;
                $new_dashboard_information_column->request_monthly = $request_object->year_month;
                $new_dashboard_information_column->status_bid = 1;
                $new_dashboard_information_column->status_suggestsent = 0;
                $new_dashboard_information_column->status_noreply = 0;
                $new_dashboard_information_column->status_noreponse = 0;
                $new_dashboard_information_column->status_proccess = 0;
                $new_dashboard_information_column->status_sample_free = 0;
                $new_dashboard_information_column->status_sample_charge = 0;
                $new_dashboard_information_column->status_order = 0;
                $new_dashboard_information_column->status_repeat = 0;
                $new_dashboard_information_column->save();
            } else {
                $dashboard_information_column->increment('status_bid');
            }
            //dashboard_information_row
            #check supplier_id exist or not
            $dashboard_information_row = Dashboard_Row::where('supplier_id','=',$supplier_id);
            if ($dashboard_information_row->count() == 0) {
                $new_dashboard_information_row = new Dashboard_Row();
                $new_dashboard_information_row->supplier_id = $supplier_id;
                $new_dashboard_information_row->request_monthly = $request_object->year_month;
                $new_dashboard_information_row->status_bid = 1;
                $new_dashboard_information_row->status_suggestsent = 0;
                $new_dashboard_information_row->status_noreply = 0;
                $new_dashboard_information_row->status_noreponse = 0;
                $new_dashboard_information_row->status_proccess = 0;
                $new_dashboard_information_row->status_sample_free = 0;
                $new_dashboard_information_row->status_sample_charge = 0;
                $new_dashboard_information_row->status_order = 0;
                $new_dashboard_information_row->status_repeat = 0;
                $new_dashboard_information_row->status_return_message = 0;
                $new_dashboard_information_row->status_sample_total = 0;
                $new_dashboard_information_row->save();
            } else {
                $dashboard_information_row->increment('status_bid');
            }
            /*add new attack to DB*/
            $attack = new Attack();
            $attack->request_id = $request_id;
            $attack->supplier_id = $supplier_id;
            $attack->mtb_attack_status_id = config('constants.STATUS_BID');
            $attack->year_month = $request_object->year_month;
            $attack->display_status = 0;
            $attack_result = $attack->save();

            $supplier_item->last_bid = date('Y-m-d H:i:s');
            $supplier_item->save();

            DB::commit();
            return json_encode($attack_result);
        }
        catch (\Exception $e)
        {
            DB::rollback();
            return json_encode(false);
        }
    }

    public function attack_list(Request $request) {
        /* an_tnh_sc_94_start */
        $_free_user = Auth::user()->free_user;
        if ( $_free_user == '1' ) {
            return Redirect::to( '/free/attack_list' );
        }
        $data['free_user'] = $_free_user;
        /* an_tnh_sc_94_end */
        
        /*Get user product list*/
        $per_page = env('PER_PAGES');

        $current_page = $request->input('page') ? $request->input('page') : 0;
        $data['current_page'] = $current_page;
        $column = 'created_at';
        $sort = 'desc';
        if ($request->has(['column', 'sort'])) {
            $column   = $request->input('column');
            $sort     = $request->input('sort');
        }
        $pagination_url = 'attack_list?';

        $from_date = $request->input('from_date');
        $data['from_date'] = $from_date;
        $to_date = $request->input('to_date');
        $data['to_date'] = $to_date;
        $search = trim($request->input('search'));
        $data['search'] = $search;
        $status = $request->input('status');
        $data['status'] = $status;

        $query = Attack::where('supplier_id', Auth::user()->supplier->id);
        $query->select('attacks.*', 'requests.date', 'requests.expire_date', 'buyers.country', 'buyers.company_name', 'mtb_genres.content', 'countries.country_name');
        $query->join('requests', 'attacks.request_id','=','requests.id');
        $query->join('buyers','requests.buyer_id','=','buyers.id');
        $query->join('mtb_genres','requests.mtb_genre_id','=','mtb_genres.id');
        $query->join('countries','countries.id','=','buyers.country');
        $query->where('buyers.deleted_at', '=', null);

        if (empty($status) && empty($from_date) && empty($to_date) && empty($search)) {
            if ($column != '' && $sort != '') {
                /*sc-177*/
                $query->where('display_status','=',0);
                /*end sc-177*/
                $query->orderBy($column, $sort);
            }
        } else {
            /*search by text*/
            if (!empty($search) && $search != ''){
                $search_value = $search;
                $search_string = '%'.$search.'%';
                $query->where(function ($func_query) use ($search_string, $search_value) {
                    $func_query->where('requests.date', 'like', $search_string)
                        ->orWhere('attacks.id', '=', $search_value)
                        ->orWhere('requests.expire_date', 'like', $search_string)
                        ->orWhere('buyers.country','like', $search_string)
                        ->orWhere('buyers.company_name','like', $search_string)
                        ->orWhere('buyers.name','like', $search_string)
                        ->orWhere('buyers.surname','like', $search_string)
                        ->orWhere('buyers.email_address','like', $search_string)
                        ->orWhere('countries.country_name','like', $search_string)
                        /* an_tnh_sc_132_start */
                        ->orWhere('mtb_genres.content','like',$search_string)
                        ->orWhere('requests.product_description_jp', 'like', $search_string)
                        ->orWhere('buyers.company_strength', 'like', $search_string)
                        /* an_tnh_sc_132_end */
                        ->orWhere('requests.id', 'like', $search_string);

                });
            }
            /*search by status*/
            /*sc-177*/
            if (!empty($status)) {
               if(is_numeric($status)){
                    $query->where('mtb_attack_status_id',$status);
                    $query->where('display_status','=',0);
                    $pagination_url .= '&status='.$status;
                }else{
                    $query->where('attacks.display_status','=',1);
                    $pagination_url .= '&status='.$status;
                }
            }
            else{
                $query->where('display_status','=',0);
                $pagination_url .= '&status='.$status;
            }
            /*end sc-177*/
            /*search by date*/
            if (!empty($from_date)) {
                $query->where('requests.date','>=', date_format(date_create($from_date),'Y-m-d'));
                $pagination_url .= '&from_date='.$from_date;
            }
            if(!empty($to_date)) {
                $query->where('requests.date','<=', date_format(date_create($to_date),'Y-m-d'));
                $pagination_url .= '&to_date='.$to_date;
            }
            if ($column != '' && $sort != '') {
                $query->orderBy($column, $sort);
            }
        }
        $attack_list = $query->paginate($per_page);
        $pagination_url .= ($column !== '' && $sort !== '') ? '&column=' . $column . '&sort=' . $sort : null;
        $pagination_url .= ($search !== '' && !empty($search)) ? '&search=' . $search : null;
        $attack_list->setPath($pagination_url);

        $data['attack_list'] = $attack_list;
        $data['mtb_attack_status'] = Mtb_Attack_Status::all();
        return view('attack.list', $data);
    }

    public function sort(Request $request) {
        $data = [];
        $per_page = env('PER_PAGES');

        if($request->isMethod('post')){
            $search = $request->input('search');
            $data['search'] = $search;
            $status = $request->input('status');
            $data['status'] = $status;
            $from_date = $request->input('from_date');
            $data['from_date'] = $from_date;
            $to_date = $request->input('to_date');
            $data['to_date'] = $to_date;

            $current_page = $request->input('current_page') ? $request->input('current_page') : 0;
            $data['current_page'] = $current_page;
            $column   = $request->input('column');
            $sort     = $request->input('sort');

            $pagination_url = 'attack_list?column=' . $column . '&sort=' . $sort;
            $query = Attack::where('supplier_id', Auth::user()->supplier->id)->where('display_status','=',0);
            $query->select('attacks.*', 'requests.date', 'requests.expire_date', 'buyers.country', 'buyers.company_name', 'mtb_genres.content','countries.country_name');
            $query->join('requests', 'attacks.request_id','=','requests.id');
            $query->join('buyers','requests.buyer_id','=','buyers.id');
            $query->join('mtb_genres','requests.mtb_genre_id','=','mtb_genres.id');
            $query->join('countries','countries.id','=','buyers.country');

            if (empty($status) && empty($from_date) && empty($to_date) && empty($search)) {
                if ($column != '' && $sort != '') {
                    /*sc-177*/
                    $query->where('display_status','=',0);
                    /*end sc-177*/
                    $query->orderBy($column, $sort);
                }
            } else {
                /*search by text*/
                if (!empty($search) && $search != ''){
                    $search_value = $search;
                    $search_string = '%'.$search.'%';
                    $query->where(function ($func_query) use ($search_string, $search_value) {
                        $func_query->where('requests.date', 'like', $search_string)
                            ->orWhere('attacks.id', '=', $search_value)
                            ->orWhere('requests.expire_date', 'like', $search_string)
                            ->orWhere('buyers.country','like', $search_string)
                            ->orWhere('buyers.company_name','like', $search_string)
                            ->orWhere('buyers.name','like', $search_string)
                            ->orWhere('buyers.surname','like', $search_string)
                            ->orWhere('buyers.email_address','like', $search_string)
                            ->orWhere('countries.country_name','like', $search_string)
                            /* an_tnh_sc_132_start */
                            ->orWhere('mtb_genres.content','like',$search_string)
                            ->orWhere('requests.product_description_jp', 'like', $search_string)
                            ->orWhere('buyers.company_strength', 'like', $search_string)
                            /* an_tnh_sc_132_end */
                            ->orWhere('requests.id', 'like', $search_string);

                    });
                }
                /*search by genre*/
                /*sc-177*/
                if (!empty($status)) {
                   if(is_numeric($status)){
                        $query->where('mtb_attack_status_id',$status);
                        $query->where('display_status','=',0);
                        $pagination_url .= '&status='.$status;
                    }else{
                        $query->where('attacks.display_status','=',1);
                        $pagination_url .= '&status='.$status;
                    }
                }
                else{
                    $query->where('display_status','=',0);
                    $pagination_url .= '&status='.$status;
                }
                /*end sc-177*/
                /*search by date*/
                if (!empty($from_date)) {
                    $query->where('requests.date','>=', date_format(date_create($from_date),'Y-m-d'));
                    $pagination_url .= '&from_date='.$from_date;
                }
                if(!empty($to_date)) {
                    $query->where('requests.date','<=', date_format(date_create($to_date),'Y-m-d'));
                    $pagination_url .= '&to_date='.$to_date;
                }
                if ($column != '' && $sort != '') {
                    $query->orderBy($column, $sort);
                }
            }
            $attack_list = $query->paginate($per_page);
            $pagination_url .= ($request->input('search') !== '' && !empty($request->input('search'))) ? '&search=' . $request->input('search') : null;
            $attack_list->setPath($pagination_url);

            $data['$attack_list'] = $attack_list;
            $content = '';
            foreach ($attack_list as $attack)
            {
                $checked = ($attack->status_agency == 1) ? "checked" : "";
                $content .= '<tr>'.
                    '<td>'.$attack->request->id.'</td>'.
                    '<td><a href="/buyer_detail/'.$attack->id.'"><button class="btn-add">詳細</button></a></td>'.
                    '<td>'.$attack->mtb_attack_status->content.'</td>'.
                    '<td>'.date_format(date_create($attack->request->date),'Y-m-d').'</td>'.
                    '<td>'.date_format(date_create($attack->request->expire_date),'Y-m-d').'</td>'.
                    '<td>'.$attack->request->buyer->countries->country_name.'</td>'.
                    '<td>'.$attack->request->buyer->company_name.'</td>'.
                    '<td>'.$attack->request->product_description_jp.'</td>'.
                    '<td class="text-center">'.'<input name="status_agency" class="a_id" type="checkbox" value="'.$attack->id.'"'.$checked. '>'.'</td>'.
                    '</tr>';
            }
            if ($content == '') {
                $content = '<tr><td colspan="9">データなし。</td></tr>';
            }
            $data['content'] = $content;
            $data['pagination'] = $attack_list->links()->toHtml();

            return json_encode( $data );
        }
    }

    public function change_status_agency(Request $request){
        $attack_item = Attack::find($request->attack_id);
        if(empty($attack_item)){
            return;
        }
        DB::beginTransaction();
        try {
            $year_current               = Carbon::now()->format("Ym");
            $current_supplier_id        = Auth::user()->supplier->id;
            $dashboard_supplier_masters = DashboardSupplierMaster::where('supplier_id', '=', $current_supplier_id)->where('year_month', '=', $year_current)->first();
            if (empty($dashboard_supplier_masters)) {
                $dashboard_supplier_masters              = new DashboardSupplierMaster();
                $dashboard_supplier_masters->supplier_id = $current_supplier_id;
                $dashboard_supplier_masters->year_month  = $year_current;
            }
            $dashboard_supplier_masters->count_agency += $attack_item->status_agency ? (-1) : 1;
            $attack_item->status_agency = !$attack_item->status_agency;
            $attack_item->save();
            $dashboard_supplier_masters->save();
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            abort(500, 'Unauthorized action.');
        }
    }
}
