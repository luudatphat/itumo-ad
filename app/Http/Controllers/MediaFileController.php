<?php

namespace App\Http\Controllers;

use App\Models\MailAttachment;
use App\Models\MailTemplatesAttachments;
use App\Models\Media_File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MediaFileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('userDeleted');
    }

    public function download_report($file_type,$media_file_id)
    {
        $media_file = Media_File::find($media_file_id);
        if (empty($media_file)) {
            abort(404);
        }
        if ($file_type == 'invoice')
        {
            $file_owner_supplier_id = $media_file->contract_report_invoice->supplier_id;
        }
        else
        {
            $file_owner_supplier_id = $media_file->contract_report_packing_list->supplier_id;
        }
        $current_authenticated_supplier_id = Auth::user()->supplier->id;
        if ($file_owner_supplier_id == $current_authenticated_supplier_id)
        {
            $content = base64_decode($media_file->content);
            header("Content-Type: {$media_file->file_type}");
            header("Content-Transfer-Encoding: base64");
            header("Content-Disposition: attachment; filename={$media_file->file_name}");
            echo $content;
        }
        else
        {
            abort(403);
        }
    }

    public function download_file($media_file_id) {
        $media_file = Media_File::find($media_file_id);
        if (empty($media_file)) {
            abort(404);
        }
        $content = base64_decode($media_file->content);
        header("Content-Type: {$media_file->file_type}");
        header("Content-Transfer-Encoding: base64");
        header("Content-Disposition: attachment; filename={$media_file->file_name}");
        echo $content;
        die();
    }

    public function download_mail_attach_file($mail_attachment_id){
        $mail_attachment = MailAttachment::find($mail_attachment_id);
        if (empty($mail_attachment)) {
            abort(404);
        }
        if ($mail_attachment->mail->supplier_id == Auth::user()->supplier->id) {
            $this->download_file($mail_attachment->media_file_id);
        }
        abort(403);
    }

    public function download_mail_template_attach_file($media_file_id){
        $mail_file_attach = MailTemplatesAttachments::where('media_file_id',trim($media_file_id))->first();
        if (empty($mail_file_attach)) {
            abort(404);
        }
        if ($mail_file_attach->templates_mail->supplier_id == Auth::user()->supplier->id) {
            $this->download_file($media_file_id);
        }
        abort(403);

    }

    public function display_image($media_file_id) {
        $media_file = Media_File::find($media_file_id);
        if (empty($media_file)) {
            abort(404);
        }
        $content = base64_decode($media_file->content);
        header("Content-Type: {$media_file->file_type}");
        header("Content-Transfer-Encoding: base64");
        echo $content;
        exit;
    }
}
