<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

//    protected function authenticated(Request $request, $user)
//    {
//        return redirect($this->redirectTo);
//    }

    protected function credentials(Request $request)
    {
        return [
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'type' => 2,
            'status' => 1,
            'deleted_at' => null,
        ];
    }
    /*
     * Creator : Duong
     * Customize lại hàm báo lỗi để thêm thông báo lỗi tài khoản chưa active
     **/
    protected function sendFailedLoginResponse(Request $request)
    {
        //get user status
        $user = User::where('email',$request->email)->first();
        if(isset($user))
        {
            $status = $user->status;
        }
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors([
                $this->username() => ((isset($status) && $status == 2) ? Lang::get('auth.not_yet_active') :Lang::get('auth.failed')),
            ]);
    }
}
