<?php

namespace App\Http\Controllers\Auth;

use App\Models\Supplier;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Validator;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Config;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        /* an_tnh_sc_94_start */
        if ( isset( $data['register_free'] ) && $data['register_free'] == 'register_free' ) {
            return Validator::make($data, [
                'company_name_jp' => 'required',
                'representative_mobile_number' => 'required',
                'email' => 'required|email|unique:users,email',
                /* an_tnh_sc_94_sc_151_start */
                'company_website' => 'url',
                /* an_tnh_sc_94_sc_151_end */
                'contact_name_kanji' => 'required',
                'password' => 'required|min:6|confirmed',
                'password_confirmation' => 'required|same:password',
                'invitation_code' => 'one_byte'
            ]);
        } else {
            return Validator::make($data, [
                'company_name_jp' => 'required',
                'zip_code' => 'required|one_byte',
                'province' => 'required',
                'address' => 'required',
                'representative_mobile_number' => 'required',
                'email' => 'required|email|unique:users,email',
                'company_website' => 'required|url',
                'representative_name_kanji' => 'required',
                'representative_name_kana' => 'required|kana',
                'contact_name_kanji' => 'required',
                'contact_name_kana' => 'required|kana',
                'contact_email_address' => 'required|email',
                'contact_mobile_number' => 'required',
                'password' => 'required|min:6|confirmed',
                'password_confirmation' => 'required|same:password',
            ]);
        }
        /* an_tnh_sc_94_end */
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return DB::transaction(function () use ($data){
            /* an_tnh_sc_94_start */
            if ( isset( $data['register_free'] ) && $data['register_free'] == 'register_free' ) {
                $create_user =  User::create([
                    'type' => 2,
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'status' => 2,
                    'free_user' => 1
                ]);
                Supplier::create([
                    'user_id' => $create_user->id,
                    'company_name_jp' => $data['company_name_jp'],
                    'representative_email_address' => $data['email'],
                    'company_email_address' => $data['email'],
                    'representative_mobile_number' => $data['representative_mobile_number'],
                    'company_phone_number' => $data['representative_mobile_number'],
                    'contact_name_kanji' => $data['contact_name_kanji'],
                    'company_website' => $data['company_website'],
                    'invitation_code' => $data['invitation_code'],
                    /* an_tnh_sc_94_sc_152_start */
                    'mtb_member_type_id' => 2
                    /* an_tnh_sc_94_sc_152_end */
                ]);
            } else {
                $create_user =  User::create([
                    'type' => 2,
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'status' => 2
                ]);
                Supplier::create([
                    'user_id' => $create_user->id,
                    'company_name_jp' => $data['company_name_jp'],
                    'zip_code' => $data['zip_code'],
                    'province' => $data['province'],
                    'address' => $data['address'],
                    'representative_email_address' => $data['email'],
                    'company_email_address' => $data['email'],
                    'representative_mobile_number' => $data['representative_mobile_number'],
                    'company_phone_number' => $data['representative_mobile_number'],
                    'representative_name_kanji' => $data['representative_name_kanji'],
                    'representative_name_kana' => $data['representative_name_kana'],
                    'contact_name_kanji' => $data['contact_name_kanji'],
                    'contact_name_kana' => $data['contact_name_kana'],
                    'contact_email_address' => $data['contact_email_address'],
                    'contact_mobile_number' => $data['contact_mobile_number'],
                    'company_website' => $data['company_website'],
                ]);
            }
            /* an_tnh_sc_94_end */

            //send verification mail to user
            $data['email']  = $create_user->email;
            
            /* an_tnh_sc_94_sc_148_sc_149_start */
            

            Mail::raw($this->formatTemplateMail($data), function($message) use ($data)
            {
                $title_mail_user = '';
                $title_mail_admin = '';
                if ( isset( $data['register_free'] ) && $data['register_free'] == 'register_free' ) {
                    $title_mail_user = '【無料会員 仮登録完了】セカイコネクト';
                    $title_mail_admin = '【セカイコネクト】無料会員登録がありました';
                } else {
                    $title_mail_user = '【仮登録完了】セカイコネクト';
                    $title_mail_admin = '新規法人登録がありました';
                }
                $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                $message->subject( $title_mail_user );
                $message->to($data['email']);
            });
            Mail::raw($this->notificationRegisterSupplier($data), function($message) use ($data)
            {
                $title_mail_user = '';
                $title_mail_admin = '';
                if ( isset( $data['register_free'] ) && $data['register_free'] == 'register_free' ) {
                    $title_mail_user = '【無料会員 仮登録完了】セカイコネクト';
                    $title_mail_admin = '【セカイコネクト】無料会員登録がありました';
                } else {
                    $title_mail_user = '【仮登録完了】セカイコネクト';
                    $title_mail_admin = '新規法人登録がありました';
                }
                $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                $message->subject( $title_mail_admin );
                $message->to('support@couxu.jp');
            });
            /* an_tnh_sc_94_sc_148_sc_149_end */

            return $create_user;
        });
    }
    /*
     * Creator : Duong
     * Override register method to disable auto login after register
     * */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        // $this->guard()->login($user);

        /* an_tnh_sc_130_start */
        session(['register_success' => '仮登録完了が完了しました。
        この度は、セカイコネクトにご登録頂き、ありがとうございます。<br />
        ご入力頂いたIDとパスワードで翌営業日からログインが可能となります。']);
        /* an_tnh_sc_130_end */

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    private function formatTemplateMail ($data)
    {
        /* an_tnh_sc_94_sc_148_sc_149_start */
        if ( isset( $data['register_free'] ) && $data['register_free'] == 'register_free' ) {
            /* an_tnh_sc_94_sc_162_start */
            $text = "この度は、セカイコネクトにご登録頂き、
ありがとうございます。

ご入力頂いたIDとパスワードで
翌営業日から10日間ログインが可能となります。

また、活用方法含めて弊社サポート担当より、
改めてご連絡させて頂きます。

※セカイコネクト ログインURLのブックマークをお願いします※
URL：https://jpsupplier.commerce-sourcing.com/login

※セカイコネクト活用方法セミナー開催中※
URL：https://peraichi.com/landing_pages/view/seminar2018
※※※※※※※※※※※※※※※※※※※※

何か不明点、懸念点あればいつでもご連絡ください。
ーーーーーーーーー問合せ窓口ーーーーーーーーーーーー
TEL：03-5298-5190
Mail：info@couxu.jp
営業時間：10:00〜18:00
ーーーーーーーーーーーーーーーーーーーーーーーーーー
今後とも、よろしくお願いいたします。
本メールは配信専用です。ご返信なさらぬようご注意ください。
―――――――――――――――――――――――■―――――――――――――――――■■□
COUXU株式会社
セカイコネクト サポート窓口
■■■〒101-0042 東京都千代田区神田東松下町31-1 神田三義ビル4F
■■■Tel：03-5298-5190
■■■Fax：03-3525-8972
■■■Mail：support@couxu.jp
■■■HP：http://couxu.jp
■■■Media：http://world-conect.com/
――――――――■――――――――――――――――――――――――――――――――■□■";
            /* an_tnh_sc_94_sc_162_end */
        } else {
            $text = "この度は、セカイコネクトにご登録頂き、
            ありがとうございます。

            ご入力頂いたIDとパスワードで
            翌営業日からログインが可能となります。

            セカイコネクトのログインURLのブックマークをお願いします。
            URL：https://jpsupplier.commerce-sourcing.com/login

            よろしくお願いいたします。
            本メールは配信専用です。ご返信なさらぬようご注意ください。
            ―――――――――――――――――――――――■―――――――――――――――――■■□
            COUXU株式会社
            サポート窓口
            ■■■〒101-0042 東京都千代田区神田東松下町31-1 神田三義ビル4F
            ■■■Tel：03-5298-5190
            ■■■Fax：03-3525-8972
            ■■■Mail：support@couxu.jp
            ■■■HP：http://couxu.jp
            ■■■Media：http://world-conect.com/
            ――――――――■――――――――――――――――――――――――――――――――■□■";
        }
        /* an_tnh_sc_94_sc_148_sc_149_end */
        return $text;
    }

    private function notificationRegisterSupplier($data){
        /* an_tnh_sc_94_sc_148_sc_149_start */
        if ( isset( $data['register_free'] ) && $data['register_free'] == 'register_free' ) {
            $text = "会社名：".$data['company_name_jp']."
            担当者名：".$data['contact_name_kanji']."
            メールアドレス：".$data['email'];
        } else {
            $text = "会社名：".$data['company_name_jp']."
            担当者名：".$data['contact_name_kanji']."
            メールアドレス：".$data['contact_email_address'];
        }
        /* an_tnh_sc_94_sc_148_sc_149_end */
        return $text;
    }
}
