<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;


    /*override sendResetLinkEmail to add extra condition (type)*/
    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $credentials = $request->only('email');
        $credentials = \Arr::add($credentials, 'type', '2');
        $credentials = \Arr::add($credentials, 'status', '1');
        $response = $this->broker()->sendResetLink(
            $credentials
        );

        if ($response === Password::RESET_LINK_SENT) {
            return back()->with(['status'=> trans($response),'flash_level' => 'success', 'flash_messages' => 'ご入力頂いたメールアドレスにパスワード再設定の為のメールをお送りしました。']);
        }

        // If an error was returned by the password broker, we will get this message
        // translated so we can notify a user of the problem. We'll redirect back
        // to where the users came from so they can attempt this process again.
        return back()->withErrors(
            ['email' => trans($response), 'flash_level' => 'danger', 'flash_messages' => 'アップデートに失敗しました。']
        );
    }
}
