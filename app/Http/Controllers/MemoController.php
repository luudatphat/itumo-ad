<?php

namespace App\Http\Controllers;

use App\Models\Attack;
use App\Models\Contact;
use App\Models\Contract_Report;
use App\Models\Dasboard;
use App\Models\Dashboard_Row;
use App\Models\DashboardBuyerMaster;
use App\Models\DashboardSupplierMaster;
use App\Models\Memo;
use App\Models\Mtb_Attack_Status;
use App\Models\Supplier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Contract_Report_Product;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class MemoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('userDeleted');
    }
    public function add_memo(Request $request){
        $this->validate($request, [
            'attack_id' => 'required|integer',
            'attack_status' => 'integer',
            'memo-date-create' => 'required|date_format:Y-m-d'
        ]);

        DB::beginTransaction();
        try
        {
            $attack_id = $request->attack_id;
            $memo_content = trim($request->memo);
            $content = '';
            if ($memo_content != '') {
                $content = $memo_content;
            }
            if (!empty($request->attack_status)) {
                /*check status change or not*/
                $current_attack = Attack::find($attack_id);
                if ($current_attack->supplier_id == Auth::user()->supplier->id) {
                    if (empty($current_attack)) {
                        abort(404);
                    }
                    #get new status info
                    $new_attack_status = Mtb_Attack_Status::find($request->attack_status);
                    #add status to content when user change status
                    if ($current_attack->mtb_attack_status_id != $request->attack_status) {
                        //add line break if $content not null
                        if ($content != '') {
                            $content .= "\n".$new_attack_status->content;
                        } else {
                            $content .= $new_attack_status->content;
                        }
                        /*Update Attack Status*/
                        $current_attack->mtb_attack_status_id = $request->attack_status;
                        $current_attack->save();
                        $arr_status = [
                            config('constants.STATUS_BID') => 'status_bid',
                            config('constants.STATUS_SUGGESTSENT') => 'status_suggestsent',
                            config('constants.STATUS_NOREPLY') => 'status_noreply',
                            config('constants.STATUS_NOREPONSE') => 'status_noreponse',
                            config('constants.STATUS_PROCCESS') => 'status_proccess',
                            config('constants.STATUS_SAMPLE_FREE') => 'status_sample_free',
                            config('constants.STATUS_SAMPLE_CHARGE') => 'status_sample_charge',
                            config('constants.STATUS_ORDER') => 'status_order',
                            config('constants.STATUS_REPEAT') => 'status_repeat',
                        ];
                        /*Dash board*/
                        $supplier_id = Auth::user()->supplier->id;
                        $attack_date = strtotime($current_attack->created_at);
                        $attack_year = date('Y',$attack_date);
                        $attack_month = date('m', $attack_date);
                        $attack_day = date('d', $attack_date);
                        $attack_year_month_day = ($attack_year*100 + $attack_month)*100 + $attack_day;
                        //dashboard_buyer_masters
                        $dashboard_buyer_master = new DashboardBuyerMaster();
                        $dashboard_buyer_master->request_id = $current_attack->request->id;
                        $dashboard_buyer_master->supplier_id = $supplier_id;
                        $dashboard_buyer_master->mtb_goods_category_id = $current_attack->request->mtb_goods_category_id;
                        $dashboard_buyer_master->mtb_attack_status_id = $request->attack_status;
                        $dashboard_buyer_master->year_month = $current_attack->year_month;
                        $dashboard_buyer_master->year_month_day = $attack_year_month_day;
                        $dashboard_buyer_master->save();
                        //dasboard_information_column
                        $dashboard_information_column = Dasboard::where('request_id','=',$current_attack->request->id);
                        $dashboard_information_column->increment($arr_status[$request->attack_status]);
                        //dasboard_information_row
                        $dashboard_information_row = Dashboard_Row::where('supplier_id','=',$supplier_id);
                        $dashboard_information_row->increment($arr_status[$request->attack_status]);
                        /*SC-73 (Change status 1 => 2)*/
                        if($request->attack_status == config('constants.STATUS_SUGGESTSENT')){
                            $supplier_item = Supplier::find($supplier_id);
                            $supplier_item->last_suggest = date('Y-m-d H:i:s');
                            $supplier_item->save();
                        }
                        /*End SC-73*/
                        #calculate total status have response
                        if ($request->attack_status == config('constants.STATUS_NOREPONSE') || $request->attack_status == config('constants.STATUS_PROCCESS')) {
                            $dashboard_information_row->increment('status_return_message');
                        }
                        #calculate total status sent sample
                        if ($request->attack_status == config('constants.STATUS_SAMPLE_FREE') || $request->attack_status == config('constants.STATUS_SAMPLE_CHARGE')) {
                            $dashboard_information_row->increment('status_sample_total');
                        }

                        if($request->attack_status == config('constants.STATUS_REPEAT')){

                            $contract_report = Contract_Report::where('attack_id', $attack_id)->get()->first();
                            $contract_report_product = Contract_Report_Product::where('contract_report_id', $contract_report->id)->get();
                            $total_money = 0;
                            foreach ($contract_report_product as $product){
                                $total_money += $product->price * $product->quantity;
                            }

                            $dashboard_supplier_masters = DashboardSupplierMaster::where('supplier_id','=',$supplier_id)->where('year_month', '=', $current_attack->year_month)->first();
                            if(empty($dashboard_supplier_masters)){
                                $dashboard_supplier_masters = new DashboardSupplierMaster();
                                $dashboard_supplier_masters->supplier_id = $supplier_id;
                                $dashboard_supplier_masters->year_month = $current_attack->year_month;
                            }

                            /* If number contact is 1, + money, else (2,3,4,5) + money_repeat*/
                            if($request->attack_status == config('constants.STATUS_ORDER')) {
                                $dashboard_supplier_masters->money += $total_money;
                            }

                            if($request->attack_status == config('constants.STATUS_REPEAT')){
                                $dashboard_supplier_masters->money_repeat += $total_money;
                            }

                            $dashboard_supplier_masters->save();
                        }
                    }
                }
            }
            if ($content != '') {
                $memo = new Memo();
                $memo->attack_id = $attack_id;
                $memo->content = $content;
                $all_data = $request->all();

                if(Carbon::parse($all_data['memo-date-create']) != Carbon::today()) {
                    $memo->created_at = Carbon::parse($all_data['memo-date-create']);
                }
                $memo->save();
                DB::commit();
                return Redirect::to(URL::previous() . "#content-memo")->with('message_memo',trans('messages.add_memo_success'));
            }
            return Redirect::to(URL::previous() . "#content-memo");
        }
        catch (\Exception $e)
        {
            DB::rollback();
            abort(500, 'Unauthorized action.');
        }
    }
}
