<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditSupplierRequest;
use App\Models\Mtb_Commission_Price;
use App\Models\Mtb_Create_Customer_Management_Page_Status;
use App\Models\Mtb_Customer_Stage;
use App\Models\Mtb_Genre;
use App\Models\Mtb_Member_Type;
use App\Models\Mtb_Posting_Progress;
use App\Models\Supplier;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SupplierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('userDeleted');
    }
    public function edit(){
        /* an_tnh_sc_94_start */
        $_free_user = Auth::user()->free_user;
        $data['free_user'] = $_free_user;
        /* an_tnh_sc_94_end */
        
        //get all supplier
        $suppliers = Supplier::find(Auth::user()->supplier->id);
        $data['suppliers'] = $suppliers;

        //get master table customer stages
        $mtb_customer_stages = Mtb_Customer_Stage::all();
        $data['mtb_customer_stages'] = $mtb_customer_stages;

        //get Supporter form table User
        $supporters = User::where('type', 1)->get();
        $data['supporters'] = $supporters;

        //get master table member type
        $mtb_member_types = Mtb_Member_Type::all();
        $data['mtb_member_types'] = $mtb_member_types;

        //get master master table  commission price
        $mtb_commission_prices = Mtb_Commission_Price::all();
        $data['mtb_commission_prices'] = $mtb_commission_prices;

        //get master table mtb_create_customer_management_page_statuses
        $mtb_create_customer_management_page_statuses = Mtb_Create_Customer_Management_Page_Status::all();
        $data['mtb_create_customer_management_page_statuses'] = $mtb_create_customer_management_page_statuses;

        //get master table mtb_posting_progresses
        $mtb_posting_progresses = Mtb_Posting_Progress::all();
        $data['mtb_posting_progresses'] = $mtb_posting_progresses;

        //get master table genre
        $mtb_genres = Mtb_Genre::all();
        $data['mtb_genres'] = $mtb_genres;

        //get master table supplier_available_categories
        $supplier_available_categories = Supplier::find($suppliers->id)->mtb_genres()->get();
        $data['supplier_available_categories'] = $supplier_available_categories;

        //get mtb_genres_id from table supplier_available_categories
        $mtb_genres_id = DB::table('supplier_available_categories')->select('mtb_genre_id')->where('supplier_id',$suppliers->id)->get();
        $data['mtb_genres_id'] = $mtb_genres_id;

        $status_user = User::find($suppliers->user_id)->status;
        $data['status_user'] = $status_user;

        return view('supplier.detail',$data);
    }
    public function edit_handler(EditSupplierRequest $request){
        DB::beginTransaction();
        try{

            //get supplier id
            $supplier_id = Auth::user()->supplier->id;

            $supplier = Supplier::find($supplier_id);
            $supplier->company_email_address = $request->txtCompanyEmailAddress;
            $supplier->company_website = $request->txtCompanyWebsite;
            $supplier->representative_mobile_number = $request->txtRepresentativeMobileNumber;
            $supplier->contact_name_kanji = $request->txtContactNameKanji;
            $supplier->contact_name_kana = $request->txtContactNameKana;
            $supplier->contact_email_address = $request->txtContactEmailAddress;
            $supplier->contact_mobile_number = $request->txtContactMobileNumber;
            $supplier->contact_skype_id = $request->txtContactSkypeID;
            $supplier->contact_line_id = $request->txtContactLineID;
            $supplier->contact_wechat_id = $request->txtContactWechatID;
            $supplier->contact_whatsapp_id = $request->txtContactWhatsappID;
            $supplier->contact_gmail_address = $request->txtContactGmailAddress;
            $supplier->contact_facebook = $request->txtContactFacebook;
            $supplier->company_strength = $request->txtCompanyStrength;
            $supplier->save();
            //delete record on table supplier_available_categories
            DB::table('supplier_available_categories')->where('supplier_id', '=', $supplier_id)->delete();
            //insert record on table supplier_available_categories
            if (is_array($request->cbl_genres)) {
                foreach ($request->cbl_genres as $value) {
                    DB::table('supplier_available_categories')->insert( ['supplier_id' => $supplier_id, 'mtb_genre_id' => $value]);
                }
            }
            DB::commit();
            return redirect('/profile')->with('message',trans('messages.save_success'));;
        }catch (\Exception $e){
            DB::rollBack();
            abort(500);
        }
    }
}
