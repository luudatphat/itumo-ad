<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendMailToBuyerRequest;
use App\Mail\PriceList;
use App\Models\Attack;
use App\Models\Dasboard;
use App\Models\Dashboard_Row;
use App\Models\DashboardBuyerMaster;
use App\Models\MailAttachment;
use App\Models\MailTemplate;
use App\Models\Media_File;
use App\Models\Memo;
use App\Models\SuplierEmailAddress;
use App\Models\Supplier;
use App\Models\Price_List;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class MailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('userDeleted');
    }

    public function send_email_to_buyer(SendMailToBuyerRequest $request) {
        DB::beginTransaction();
        try {
            $supplier_id = Auth::user()->supplier->id;
            $supplier_item = Supplier::find($supplier_id);
            $subject = $request->mail_title;
            //$cc = $request->mail_cc;
            if(trim($request->mail_cc) !== ''){
                $cc = array_filter(array_map('trim',explode(',',$request->mail_cc)));
            }else{
                $cc = '';
            }
            $content = $request->mail_content;
            #insert attach file to DB
            //$mail_attach_file = NULL;
            $mail_attach_file = array();
            $files = $request->file('mail_attach_file');

            if ($request->hasFile('mail_attach_file')) {
                foreach ($files as $file) {
                    if ($file->isValid()) {
                        $mail_attach_file[] = $this->insert_file_to_db($file);
                    } else {
                        return view('errors.display', ['error_message' => trans('messages.file_upload_error')]);
                    }
                }
//                if ($request->mail_attach_file->isValid()) {
//                    $mail_attach_file = $this->insert_file_to_db($request->mail_attach_file);
//                    $mail_attach_file->mime_type = $request->mail_attach_file->getMimeType();
//                } else {
//                    return view('errors.display', ['error_message' => trans('messages.file_upload_error')]);
//                }
            }
            /*get mail template attach file*/
            //$mail_template_attach_file = NULL;
            $mail_template_attach_file = array();
            if ($request->mail_template != NULL) {
                $mail_template_attach_file = MailTemplate::find($request->mail_template)->mail_attachmennt;
            }

            /*get price list id list*/
            $price_list_id_list = $request->price_list_id;

            /*get buyer email*/
            $attack = Attack::find($request->attack_id);
            $buyer_email = $attack->request->buyer->email_address;

            // Set from email, default is Login email
            $from = Auth::user()->email;
            if($request->sender != NULL){
                $from = SuplierEmailAddress::find($request->sender)->email;
            }

            /*Send mail*/
            $price_list_obj = new PriceList($subject, $cc, $content,  $mail_attach_file, $mail_template_attach_file, $price_list_id_list, $from);
            Mail::to($buyer_email)->send($price_list_obj);
            /*Save email to DB*/
            //Save email
            $mail = new \App\Models\Mail();
            $mail->supplier_id = $supplier_id;
            $mail->attack_id = $request->attack_id;
            $mail->title = $request->mail_title;
            $mail->content = $request->mail_content;
            //mail_cc = null ???
            $request->mail_cc ? $mail->cc = $request->mail_cc : $mail->cc = '';
            $mail->save();
            //Save email attachments
            #mail attach file
            if ($mail_attach_file != NULL) {
                foreach ($mail_attach_file as $item) {
                    $mail_attachment = new MailAttachment();
                    $mail_attachment->mail_id = $mail->id;
                    $mail_attachment->media_file_id = $item->id;
                    $mail_attachment->type = config('constants.TYPE_ATTACHMENT');
                    $mail_attachment->save();
                }
            }
            #mail attach file
            if ($mail_template_attach_file != NULL) {
                foreach ($mail_template_attach_file as $item) {

                    $mail_attachment = new MailAttachment();
                    $mail_attachment->mail_id = $mail->id;
                    $mail_attachment->media_file_id = $item->media_files->id;
                    $mail_attachment->type = config('constants.TYPE_ATTACHMENT');
                    $mail_attachment->save();
                }
            }
            #pricelist
            if (!empty($price_list_obj->price_list)) {
                foreach ($price_list_obj->price_list as $price_list) {
                    $media_file = new Media_File();
                    $media_file->file_name = $price_list->price_list_name.'.pdf';
                    $media_file->content = base64_encode($price_list->pdf_content);
                    $media_file->file_type = 'application/pdf';
                    $media_file->save();
                    //save to mail_attachments
                    $mail_attachment = new MailAttachment();
                    $mail_attachment->mail_id = $mail->id;
                    $mail_attachment->media_file_id = $media_file->id;
                    $mail_attachment->type = config('constants.TYPE_PRICE_LIST');
                    $mail_attachment->save();
                }
            }
            /*Add memo*/
            $memo = new Memo();
            $memo->attack_id = $request->attack_id;
            $memo->content = $subject;
            $memo->save();
            /*chakge attack status and update dashboard if status = 1*/
            if ($attack->mtb_attack_status_id == config('constants.STATUS_BID')) {
                /*Change attack status*/
                $attack->mtb_attack_status_id = config('constants.STATUS_SUGGESTSENT');
                $attack->save();
                /*Dash board*/
                $attack_date = strtotime($attack->created_at);
                $attack_year = date('Y',$attack_date);
                $attack_month = date('m', $attack_date);
                $attack_day = date('d', $attack_date);
                $attack_year_month_day = ($attack_year*100 + $attack_month)*100 + $attack_day;
                //dashboard_buyer_masters
                $dashboard_buyer_master = new DashboardBuyerMaster();
                $dashboard_buyer_master->request_id = $attack->request->id;
                $dashboard_buyer_master->supplier_id = $supplier_id;
                $dashboard_buyer_master->mtb_goods_category_id = $attack->request->mtb_goods_category_id;
                $dashboard_buyer_master->mtb_attack_status_id = config('constants.STATUS_SUGGESTSENT');
                $dashboard_buyer_master->year_month = $attack->year_month;
                $dashboard_buyer_master->year_month_day = $attack_year_month_day;
                $dashboard_buyer_master->save();
                //dasboard_information_column
                $dashboard_information_column = Dasboard::where('request_id','=',$attack->request->id);
                $dashboard_information_column->increment('status_suggestsent');
                //dasboard_information_row
                $dashboard_information_row = Dashboard_Row::where('supplier_id','=',$supplier_id);
                $dashboard_information_row->increment('status_suggestsent');

                $supplier_item->last_suggest = date('Y-m-d H:i:s');
                $supplier_item->save();
            }
            DB::commit();
            return Redirect::to(URL::previous() . "#content-memo")->with('message',trans('messages.send_mail_success'));
        } catch (\Exception $e) {
            DB::rollBack();
            abort(500);
            //echo $e;
        }
    }

    public function index(Request $request){
        /* an_tnh_sc_94_start */
        $_free_user = Auth::user()->free_user;
        if ( $_free_user == '1' ) {
            return Redirect::to( '/free/mail-list' );
        }
        $data['free_user'] = $_free_user;
        /* an_tnh_sc_94_end */

        $per_page = env('PER_PAGES');
        $current_page = $request->input('page') ? $request->input('page') : 0;
        $data['current_page'] = $current_page;
        $column = 'created_at';
        $sort = 'desc';
        if ($request->has(['column', 'sort'])) {
            $column   = $request->input('column');
            $sort     = $request->input('sort');
        }
        $pagination_url = 'mail-list?';
        $search = trim($request->input('search'));
        $data['search'] = $search;
        if (empty($search)) {
            $query = \App\Models\Mail::where('supplier_id', Auth::user()->supplier->id);
            if ($column != '' && $sort != '') {
                $query->orderBy($column, $sort);
            }
            $mail_list = $query->paginate($per_page);
        } else {
            $search_value = $search;
            $search_string = '%'.$search.'%';
            $query = \App\Models\Mail::where('supplier_id', Auth::user()->supplier->id)
                ->where(function ($query) use ($search_string, $search_value) {
                    $query->orWhere('id', '=', $search_value)
                        ->orWhere('title', 'like', $search_string)
                        ->orWhere('attack_id','=', $search_value)
                        ->orWhereHas('attack', function ($query) use ($search_string) {
                            $query->whereHas('request',function ($query) use ($search_string) {
                                $query->whereHas('buyer', function ($query) use ($search_string) {
                                    $query->where('company_name', 'like', $search_string);
                                });
                            });
                        });
                });
            if ($column != '' && $sort != '') {
                $query->orderBy($column, $sort);
            }
            $mail_list = $query->paginate($per_page);
        }
        $pagination_url .= ($column !== '' && $sort !== '') ? '&column=' . $column . '&sort=' . $sort : null;
        $pagination_url .= ($request->input('search') !== '' && !empty($request->input('search'))) ? '&search=' . $request->input('search') : null;
        $mail_list->setPath($pagination_url);
        $data['mail_lists'] = $mail_list;
        return view('mails.list', $data);
    }

    public function sort(Request $request) {
        if($request->isMethod('post')){
            $per_page = env('PER_PAGES');
            $current_page = $request->input('page') ? $request->input('page') : 0;
            $data['current_page'] = $current_page;
            $column = $request->input('column') ? $request->input('column') : '';
            $sort = $request->input('sort') ? $request->input('sort') : '';
            $pagination_url = 'mail-list?';
            $search = trim($request->input('search'));
            $data['search'] = $search;
            if (empty($search)) {
                $query = \App\Models\Mail::where('supplier_id', Auth::user()->supplier->id);
                if ($column != '' && $sort != '') {
                    $query->orderBy($column, $sort);
                }
                $mail_list = $query->paginate($per_page);
            } else {
                $search_value = $search;
                $search_string = '%'.$search.'%';
                $query = \App\Models\Mail::where('supplier_id', Auth::user()->supplier->id)
                    ->where(function ($query) use ($search_string, $search_value) {
                        $query->orWhere('id', '=', $search_value)
                            ->orWhere('title', 'like', $search_string)
                            ->orWhere('attack_id','=', $search_value)
                            ->orWhereHas('attack', function ($query) use ($search_string) {
                                $query->whereHas('request',function ($query) use ($search_string) {
                                    $query->whereHas('buyer', function ($query) use ($search_string) {
                                        $query->where('company_name', 'like', $search_string);
                                    });
                                });
                            });
                    });
                if ($column != '' && $sort != '') {
                    $query->orderBy($column, $sort);
                }
                $mail_list = $query->paginate($per_page);
            }

            $pagination_url .= ($column !== '' && $sort !== '') ? '&column=' . $column . '&sort=' . $sort : null;
            $pagination_url .= ($request->input('search') !== '' && !empty($request->input('search'))) ? '&search=' . $request->input('search') : null;
            $mail_list->setPath($pagination_url);

            $data['mail_lists'] = $mail_list;
            $content = '';
            foreach ($mail_list as $mail)
            {
                $content.= '<tr>'.
                    '<td>'.$mail->id.'</td>'.
                    '<td>'.$mail->created_at.'</td>'.
                    '<td><a href="/buyer_detail/'.$mail->attack_id.'">'.$mail->attack_id .'</a></td>'.
                    '<td>'.$mail->attack->request->buyer->company_name.'</td>'.
                    '<td>'.$mail->title.'</td>';
                    $content .= '<td class="text-center" style="vertical-align:middle">'.
                        '<a class="btn btn-primary fix-btn-01" href="'.url('mail-detail/'.$mail->id).'">詳細</a>'.
                        '</td>'.
                        '</tr>';
            }
            $data['content'] = $content;
            $data['pagination'] = $mail_list->links()->toHtml();

            return json_encode( $data );
        }
    }

    public function edit($mail_id){
        /* an_tnh_sc_94_start */
        $_free_user = Auth::user()->free_user;
        if ( $_free_user == '1' ) {
            return Redirect::to( '/free/mail-detail/' . $mail_id );
        }
        $data['free_user'] = $_free_user;
        /* an_tnh_sc_94_end */
        
        $mail = \App\Models\Mail::find($mail_id);
        if (empty($mail)) {
            abort(404);
        }
        if ($mail->supplier_id == Auth::user()->supplier->id) {
            $data['mail'] = $mail;
            $attachments = \App\Models\MailAttachment::where('mail_id', $mail_id)
                ->where('type', config('constants.TYPE_ATTACHMENT'))
                ->get();
            //$attachment_lists = $mail->mail_attachmennt->where('type','=',config('constants.TYPE_ATTACHMENT'));
            $attachment_lists = array();
            foreach ($attachments as $item) {
                $media_files = \App\Models\Media_File::withTrashed()->where('id', $item->media_file_id)->get(['file_name', 'deleted_at']);
                if (!empty($media_files)) {
                    $attachment_lists[] = array(
                        'id' => $item->id,
                        'file_name' => $media_files[0]->file_name,
                        'deleted_at' => $media_files[0]->deleted_at,
                    );
                }
            }
            $data['attachment_lists'] = $attachment_lists;
            //$data['price_lists'] = $mail->mail_attachmennt->where('type','=',config('constants.TYPE_PRICE_LIST'));
            return view('mails.detail',$data);
        }
        abort(403);
    }
}
