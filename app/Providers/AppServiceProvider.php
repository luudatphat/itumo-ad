<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Validator::extend('multibyte', function ($attribute, $value, $parameters, $validator) {
            return !mb_check_encoding($value, 'ASCII') && mb_check_encoding($value, 'UTF-8');
        });
        Validator::extend('one_byte', function ($attribute, $value, $parameters, $validator) {
            return mb_check_encoding($value, 'ASCII') && mb_check_encoding($value, 'UTF-8');
        });
        Validator::extend('kana', function ($attribute, $value, $parameters, $validator) {
            if (strlen($value) > 0 && !preg_match("/^[\sァ-ヶｦ-ﾟー・.]+$/u", $value)) {
                return false;
            }
            return true;
        });

        Validator::extend("emails", function($attribute, $value, $parameters) {
            $rules = [
                'email' => 'email',
            ];
            $value = array_map('trim',explode(',', $value));
            foreach ($value as $email) {
                $data = [
                    'email' => $email
                ];
                $validator = Validator::make($data, $rules);
                if ($validator->fails()) {
                    return false;
                }
            }
            return true;
        });
    }
}
