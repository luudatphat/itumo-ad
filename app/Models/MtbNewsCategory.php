<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MtbNewsCategory extends Model
{
    protected $table = "mtb_news_categories";
}
