<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Buyer_Handled_Good extends Model
{
    protected $table = 'buyer_handled_goods';
}
