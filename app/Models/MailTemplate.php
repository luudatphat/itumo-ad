<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MailTemplate extends Model
{
    use SoftDeletes;
    protected $table = "mail_templates";
    protected $dates = ['deleted_at'];

    public function media_file() {
        return $this->belongsTo('App\Models\Media_File','attach_file');
    }

    public function mail_attachmennt(){
        return $this->hasMany('App\Models\MailTemplatesAttachments', 'mail_templates_id');
    }
}
