<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Attack_Status extends Model
{
    protected $table = 'mtb_attack_statuses';

    public function attacks() {
        return $this->hasMany('App\Models\Attack');
    }
}
