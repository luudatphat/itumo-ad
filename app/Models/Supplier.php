<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    /*Create table relationships*/
    public function mtb_genres(){
        return $this->belongsToMany('App\Models\Mtb_Genre','supplier_available_categories','supplier_id','mtb_genre_id');
    }

    public function supplier_available_categories()
    {
        return $this->hasMany('App\Models\Supplier_Available_Category');
    }

    public function mtb_contract_period(){
        return $this->belongsTo('App\Models\Mtb_Contract_Period');
    }

    public function mtb_contract_status(){
        return $this->belongsTo('App\Models\Mtb_Contract_Status');
    }

    public function mtb_customer_stage(){
        return $this->belongsTo('App\Models\Mtb_Customer_Stage');
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function mtb_member_type() {
        return $this->belongsTo('App\Models\Mtb_Member_Type');
    }

    public function mtb_commission_price() {
        return $this->belongsTo('App\Models\Mtb_Commsion_Price');
    }

    public  function mtb_create_customer_management_page_status() {
        return $this->belongsTo('App\Models\Mtb_Create_Customer_Management_Page_Status');
    }

    public function mtb_posting_progress() {
        return $this->belongsTo('App\Models\Mtb_Posting_Progress');
    }

    public function attacks() {
        return $this->hasMany('App\Models\Attack');
    }
    protected $guarded = array();
}