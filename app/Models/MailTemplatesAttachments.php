<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MailTemplatesAttachments extends Model
{
    //
    protected $table = 'mail_templates_attachments';

    public function media_files(){
        return $this->belongsTo('App\Models\Media_File','media_file_id');
    }

    public function templates_mail(){
        return $this->belongsTo('App\Models\MailTemplate', 'mail_templates_id');
    }
}
