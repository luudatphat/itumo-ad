<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class MyResetPassword extends Notification
{
    use Queueable;
    public $token;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
//        Mail::raw($this->notificationResetPasswordSupplier($notifiable->email), function($message)
//        {
//            $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
//            $message->subject("Notification Reset Password Supplier");
//            $message->to('sekai@staff.distance-c.asia');
//        });
        return (new MailMessage)
            ->subject('【パスワード再設定通知】セカイコネクト')
            ->view('notifications::template-email-reset')
            ->action('Reset Password', url('password/reset', $this->token));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    private function notificationResetPasswordSupplier ($variable)
    {
        $text = "There is a company reset password. 
Account: ".$variable." !";
        return $text;
    }
}
