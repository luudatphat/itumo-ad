<?php

namespace App\Listeners;

use Carbon\Carbon;
use Illuminate\Auth\Events\Login;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        //
        $event->user->last_login = date('Y-m-d H:i:s');
        $suplier = \App\Models\Supplier::where('user_id', '=', $event->user->id)->get()->first();

        //die(var_dump($suplier));
        $suplier->last_login = date('Y-m-d H:i:s');

        $year_current = Carbon::now()->format("Ym");

        $dashboard = \App\Models\DashboardSupplierMaster::where('supplier_id', '=', $suplier->id)
            ->where('year_month', '=', $year_current)->first();

        if($dashboard){
            $dashboard->count_login = $dashboard->count_login + 1;
            $dashboard->save();
        }else{
            $dashboard = new \App\Models\DashboardSupplierMaster();
            $dashboard->supplier_id = $suplier->id;
            $dashboard->year_month = $year_current;
            $dashboard->count_login = 1;
            $dashboard->save();
        }

        $suplier->save();
        $event->user->save();
    }
}
